#encoding:utf-8

require_relative 'DamageToUI'
require_relative 'Weapon'
require_relative 'WeaponType'


module Deepspace
  

  
# Represents damages done on a spaceship after loosing a combat.
# They indicate which elements are going to be lost if the spaceship loses the combat.
# A damage is composed by a number of shields to lose and:
#   - A number of weapons to lose (nWeapons)
#   - A list of weapons to lose (weapons)
#


  
class Damage
  
  
  # Initializers
	# ==========================================================================

  private_class_method :new
  
	# Class initializer.
	# @param s [Integer] number of shields that will be lost
  # @param w number of weapons that will be lost
  # @param t WeaponTypes that will be lost 
 
  def initialize(s)
    @nShields=s
  end
  

  
  # Getters
	# ==========================================================================

  attr_reader :nShields
 
  

  
  
  # Setters
	# =========================================================================

  
  
  
  # Reduces by 1 the number of shield boosters to be removed
  def discardShieldBooster()
    if (@nShields > 0)
      @nShields -=1
    end
  end
  
  
  # Checks if a given weapon array contains a WeaponType
  # @param w weapon array
  # @param t WeaponType to be looked for
  # @returns [integrer] position of the WeaponType in the array.
  
  def arrayContainsType(w,t)
    found = false
    pos = -1
    i =0
    while (!found && i<w.length)
      
      if (w[i].type==t)
        found=true
        pos =i
      else
        i+=1
      end
    end
    
    return pos
  end
  
  
  def adjust(s)
		return [s.length, @nShields].min
	end
  
  # String representation, UI version
	# ==========================================================================
  
  
  def getUIversion
    return  DamageToUI.new(self)
  end
  
  
  def to_s
    message = "[Damage]: Number of Shields: #{@nShields} \n"
    return message
  end
  


end


end
