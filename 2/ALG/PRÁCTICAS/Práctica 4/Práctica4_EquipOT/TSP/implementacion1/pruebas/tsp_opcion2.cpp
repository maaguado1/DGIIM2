#include <iostream>
#include <chrono>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>
#include <climits>
#include <stdlib.h>

using namespace std;


struct Punto{
  float x;
  float y;
};

float distancia(Punto uno, Punto dos){
  return sqrt( pow((dos.x- uno.x), 2) + pow((dos.y-uno.y), 2));
}

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T){
  int n = T.size();
  for (int i = 0; i < n; i++)
    {
      cout << "\n| ";
      for (int j = 0; j< n; j ++){
    cout << T[i][j] <<" " ;
  }

  cout  << "  |";

  }
  cout << "\n\n";
  }


  void salidavec (vector<int> vector){
    cout << "\nsacando...";
    for (int i =0; i < vector.size(); i ++){
      cout << vector[i] << " ";
    }
    cout << "\n";
  }

  /**
   * @brief Genera una matriz con las distancias entre los puntos
   * @param nodos, los puntos sobre los que calculamos las distancias
   * @return una matriz con las distancias entre los puntos
   *
   * Matriz[i][j] := distance(pi, pj)
   *
   * */
vector<vector<int>> genera_matriz(vector<Punto> nodos){
  vector<vector<int>> T;
  int n =nodos.size();
  for (int i =0; i < n; i ++){
    vector<int> fila;
    for (int j = 0; j < n; j ++){
      if (j < i )
        fila.push_back(T[j][i]);
      else if (j == i)
        fila.push_back(0.0);
      else
      fila.push_back(distancia( nodos[i], nodos[j]));

    }
    T.push_back(fila);
  }
  return T;
}


/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor (vector<int> solucion, vector<vector<int>> T){
  int valor=0;
  for (int i =0; i < solucion.size()-1; i++){
    valor += T[solucion[i]][solucion[i+1]];
  }
  valor += T[solucion[0]][solucion[T.size()-1]];
  return valor;
}

/**
 * @brief Calcula la mejor solucion para un conjunto de puntos, por Shortest Neighbor First
 * @param T, la matriz de distancias de los puntos con los que trabajamos
 * @pre T.size() > 0
 * @return el camino cuya distancia es la minima, si todo sale bien
 *         un vector vacio, si ocurre algun error
 * */

  vector<int> solucion_cercania(vector<vector<int>> distancias){
  	vector<int> solucion;
  	int n = distancias.size();
  	int distancia_minima = 0;
  	int distancia = 0;
  	int posicion= rand()%n;
  	int i;
  	solucion.push_back(posicion);

  	while(solucion.size() < distancias.size()){
  		i=posicion;
  		distancia_minima=INT_MAX;
  		for(int j=0; j< distancias.size(); j++){
  			if(find(solucion.begin(),solucion.end(),j) == solucion.end()){
  				distancia=distancias[i][j];
  				if(distancia<distancia_minima){
  					posicion=j;
  					distancia_minima=distancia;
  				}
  			}
  		}
  		solucion.push_back(posicion);
  	}
  	return solucion;
  }


vector<Punto> leerFichero( char *archivo){
  ifstream entrada(archivo);
  vector<Punto> puntos;
  char* cadena;

  int tamanio;
  entrada >> tamanio;
  Punto insertar;
  int contador =0;
  float x, y, num;
  while (contador < tamanio){
    entrada >> num;
    entrada>>x;
    entrada>>y;
    insertar.x=x;
    insertar.y=y;
    puntos.push_back(insertar);
    contador ++;
  }

  return puntos;
  entrada.close();
}




vector<int> optimizacion (vector<vector<int>> T, vector<int> solucion){
  float valor1= calcular_valor(solucion, T), valor2;
  cout << "\nVALOR INICIAL: " << valor1;
  int n = T.size();
  int i =0, cambio;
  int j =i+1;
  vector<int> sol2 = solucion;
  while (i < n){

    j = i+1;


    while (j < n){
      valor2= valor1;
      solucion = sol2;
      cambio = solucion[i];
      solucion[i]= solucion[j];
      solucion[j]= cambio;
      valor2=calcular_valor(solucion, T);

      if (valor2 >= valor1){
        j ++;
      }
      else{ //Mejora el camino
        sol2 = solucion;
        valor1= calcular_valor(solucion, T);
        i =0;
        j = i+1;
      }
    }
    i ++;
    j ++;

  }
  return sol2;
}


int main (int argc, char *argv[]){

  vector<Punto> points = leerFichero(argv[1]);


  vector<vector<int>> T= genera_matriz(points);

  //Hallamos la solución por cercanía:

  vector<int> solucion = solucion_cercania(T);

  cout << "\nla solucion con cercania es " ;
  salidavec(solucion);
  solucion = optimizacion(T, solucion);

  cout << "\nla solucion con opt es " ;
  salidavec(solucion);

}
