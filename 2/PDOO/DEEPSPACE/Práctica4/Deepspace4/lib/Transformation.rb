
module Deepspace
  
  
# Enum that represents a shot result received by an enemy ship or a space station
module Transformation
    NOTRANSFORM=:notransform
    GETEFFICIENT=:getefficient
    SPACECITY=:spacecity
end # enum

end 