## TEST ED



##### 1. El TDA cola con prioridad no es más que un caso particular del TDA Cola

FALSO

##### 2. El orden en que las hojas se listan en los recorridos pre, in y post en un ArbolBinario es el mismo en los 3 casos

VERDADERO. El orden en que se listan es el mismo (en particular hojas).

##### 3. Un AVL puede reconstruirse de forma unívoca dado su recorrido en inorden.

FALSO. Contraejemplo

~~~
	
{1,2,3,4}

		2				3
	   / \			   / \
	  1	  4			  2	  4
	  	 /			 /
	  	3			1
~~~



##### 4. Es imposible que un AB sea AVL y APO a la vez

VERDADEROO. Solo cuando todas las claves son las mismas o tiene profun 1. (se hace el dibujito de incompatibilidad).

##### 5. En un esquema de Hashing doble nunca puede ocurrir que para 2 claves k1, k2 coincidan $h(k1)=h(k2)$ y $h_0(k1)=h_0(k2)$

FALSEE

CONTRAEJ:

$h(k)=k \mod 5 \\ h_0(k)=3-k \mod 3$



##### 6. Un analista tiene que resolver un problema de tamaño 100 y usa un algoritmo $O(n^2)$. Uno de sus colaboradores consigue obtener una solución que es $O(n)$. El analista debe olvidarse de la primera solución y usar la de su colaborador de mejor eficiencia de tiempo.



FALSOO, el tiempo no va con la eficiencia (la eficiencia en espacio y las constantes pueden influir en ej. de tamaño pequeño mimimimi).



##### 7. Si la eficiencia de un algoritmo viene dada por la función $f(n)=1+2+..+n$, ese algoritmo es $O(max(1,2,...,n))$.

FALSO 

$\sum _{i=1}^{n}i= n \frac {a_1+a_n} {2} = n \frac {1+n}{2} = O(n^2)$



##### 8. Puede darse la siguiente definición

~~~~c++
stack<int>::iterator p;
~~~~

FALSE, stack no tiene iterator.

##### 9. Un APO puede reconstruirse con su postorden.

YAS (condición geométrica), y un ABB con el pre.￼



##### 10. En un hashing doble se puede usar $h_o(x)=[(B-1)-(x \mod B)] \mod B$

NOPE, no se anula jej



##### 11. Es correcto en hashing cerrado la función $h(k)= [k+ random(M)] \mod M$, con M primo.

NOPE, no se puede recuperar la clave.



##### 12. El TDA con prioridad se puede implementar usando un heap

TRUE.



##### 13.  La definición `priority_queue<int>::iterator p` es correcta

FALSE, solo se puede acceder al más prioritario.



##### 14. El elemento máximo en un ABB<int> se encuentra en el nodo más profundo

FALSE, se encuentra en la hoja más a la derecha.



##### 15. Considerar un map<int, int>M donde hacemos M[3]=7 si hacemos: 

~~~~
map<int,int>::iterator p= M.find(3);
p->second=9;

~~~~

##### tras esto el valor de M[3] sigue siendo 7.

FALSO, se actualiza a 9.



##### 16. Si A es una hash cerrada con 50% de elementos vacíos y 40% de elementos borrados, y B otra con 50% de vacíos y sin elementos borrados, A y B son igual de eficientes en la búsqueda.

FALSE, las casillas borradas forman parte de una cadena de búsqueda.



##### 17. 





## COSITAS TEORÍA

- HASHING
  - Por lo que se hace el mod tiene que ser primo
  - Se tiene que verificar que se anule.
  - ahorasigo















