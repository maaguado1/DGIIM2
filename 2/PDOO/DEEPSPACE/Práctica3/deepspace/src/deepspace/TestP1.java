package deepspace;


public class TestP1 { 
    public static void main(String[] args) {
        Loot botin = new Loot(1, 1, 1, 1, 1);
        SuppliesPackage suministros = new SuppliesPackage(3.0f, 50f, 3.5f);
        ShieldBooster potenciador = new ShieldBooster("Ligero", 3.5f, 0);
        Weapon arma = new Weapon("Rayo", WeaponType.LASER, 0);
        Dice dado = new Dice();
        
        System.out.format("\nTipo de arma: %s\n", arma.toString());
        
        System.out.format("\nShieldBoost: %s\n", potenciador.toString());
        
        System.out.format("\nSuministros obtenidos: %s\n", botin.toString());

        System.out.format("\nPontenciador del escudo: %s\n", potenciador.toString());
        
        System.out.format("\nSupplies Package: %z\n", suministros.toString());
        
        int i;
        
        for(i = 0; i < 100; i++){
            dado = new Dice();
            System.out.format("\nNumero de hangares: %d\n", dado.initWithNHangars());
            System.out.format("\nNumero de armas: %d\n", dado.initWithNWeapons());
            System.out.format("\nNumero de escudos: %d\n", dado.initWithNShields());
            System.out.format("\nComienzo: %d\n", dado.whoStarts(2));
            System.out.format("\nPrimer disparo: %s\n", dado.firstShot());
            System.out.format("\n¿Se ha movido?: %b\n", dado.spaceStationMoves(i/100));
        }
        
        System.out.format("\nProbando nuevas clases...");
        
        Hangar hang = new Hangar(15);
        for (i =0; i < 5; i ++){
            hang.addWeapon(arma);
            hang.addShieldBooster(potenciador);
        }
        
        System.out.format("\nHangar lleno %s\n", hang.toString());
        
        
        Damage damag = new Damage(3, 2);
        System.out.format("\nDaños que haría %s\n", damag.toString());
        
        
        EnemyStarShip enemy = new EnemyStarShip("eNEMIGO", 23f, 2.0f, botin, damag);
        System.out.format("\n Disparo de 3.0f, resultado %s\n", enemy.receiveShot(3.0f).toString());
        System.out.format("\nNave enemiga:%s\n", enemy.toString());
        
        
        
        
       
        
        
    }
    
}