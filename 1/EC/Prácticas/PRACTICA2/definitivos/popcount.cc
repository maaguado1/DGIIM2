//------------------------------------------------------------------------------
// popcount.cc
// http://www.dalkescientific.com/writings/diary/archive/2011/11/02/faster_popcount_update.html
//------------------------------------------------------------------------------

#include <x86intrin.h> // __rdtscp

#include <algorithm>   // generate
#include <array>       // array
#include <bitset>      // bitset
#include <chrono>      // now
#include <functional>  // bind
#include <iomanip>     // setw
#include <iostream>    // cout endl
#include <numeric>     // iota

//------------------------------------------------------------------------------

const unsigned GAP = 12;            // gap between columns
const unsigned REP = 100;           // how many times we repeat the experiment

//------------------------------------------------------------------------------

std::array<unsigned, 1 << 16> list; // 64K * 4B = 256KB

//------------------------------------------------------------------------------

unsigned popcount00(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount01(unsigned elem)
{
	unsigned count = 0;

	for (unsigned i = 0; i < 8 * sizeof(decltype(elem)); ++i)
	{
		unsigned mask = 1 << i;
		count += (elem & mask) != 0;
	}

	return count;
}

//------------------------------------------------------------------------------

unsigned popcount02(unsigned elem)
{
	unsigned count = 0;
	do {
		count += elem & 0x1;
		elem >>= 1;
	} while (elem);
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount03(unsigned elem)
{
	unsigned count = 0;
	for (unsigned i = 0; i < 8 * sizeof(decltype(elem)); ++i)
		asm("shr %1     \n"
		    "adc $0, %0 \n"
		    : "+r" (count)
		    : "r" (elem));
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount04(unsigned elem)
{
	unsigned count = 0;
	while (elem)
		asm("shr %1     \n"
		    "adc $0, %0 \n"
		    : "+r" (count), "+r" (elem));
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount05(unsigned elem) // from o'hallaron
{
	unsigned count = 0;
	for (unsigned j = 0; j < 8; ++j)
	{
		count += elem & 0x01010101; // binary --> 0000 0001 0000 0001...
		elem >>= 1;
	}
	count += (count >> 16);
	count += (count >>  8);
	return count & 0xff;
}

//------------------------------------------------------------------------------

unsigned popcount06(unsigned elem) // gustavo
{
	elem = (elem & 0x55555555) + ((elem >>  1) & 0x55555555);
	elem = (elem & 0x33333333) + ((elem >>  2) & 0x33333333);
	elem = (elem & 0x0f0f0f0f) + ((elem >>  4) & 0x0f0f0f0f);
	elem = (elem & 0x00ff00ff) + ((elem >>  8) & 0x00ff00ff);
	elem = (elem & 0x0000ffff) + ((elem >> 16) & 0x0000ffff);
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount07(unsigned x) // from wikipedia
{
	const unsigned m1 = 0x55555555;
	const unsigned m2 = 0x33333333;
	const unsigned m4 = 0x0f0f0f0f;
	const unsigned h01 = 0x01010101;

	x -= (x >> 1) & m1;             // put count of each 2 bits into those 2 bits
	x = (x & m2) + ((x >> 2) & m2); // put count of each 4 bits into those 4 bits
	x = (x + (x >> 4)) & m4;        // put count of each 8 bits into those 8 bits
	return (x * h01) >> 24;         // returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
}

//------------------------------------------------------------------------------

unsigned popcount08(unsigned elem) // by Wegner
{
	unsigned count;
	for(count = 0; elem; ++count)
		elem &= elem - 1;
	return count;
}

//------------------------------------------------------------------------------

unsigned char lt8[256];

unsigned popcount09(unsigned elem)3ws
{
	return lt8[(elem & 0x000000ff)      ] +
	       lt8[(elem & 0x0000ff00) >>  8] +
	       lt8[(elem & 0x00ff0000) >> 16] +
	       lt8[(elem & 0xff000000) >> 24];
}

//------------------------------------------------------------------------------

unsigned popcount10(unsigned elem)
{
	unsigned count = 0;
	asm("movzbl %%dl, %%ecx    \n"
	    "addb lt8(%%ecx), %%al \n"
	    "movzbl %%dh, %%ecx    \n"
	    "addb lt8(%%ecx), %%al \n"
	    "shr $16, %%edx        \n"
	    "movzbl %%dl, %%ecx    \n"
	    "addb lt8(%%ecx), %%al \n"
	    "movzbl %%dh, %%ecx    \n"
	    "addb lt8(%%ecx), %%al \n"
	    : "+a" (count)
	    : "d" (elem));
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount11(unsigned elem)
{
	unsigned count = 0;
	for (unsigned char *b = (unsigned char*)&elem ;
	     b < (unsigned char*)&elem + 4;
	     ++b)
		asm("xlat            \n"
		    "addb %%al, %%cl \n"
		    : "+c" (count)
		    : "a" (*b), "b" (lt8));
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount12(unsigned elem)
{
	unsigned count = lt8[elem & 0xff];
	while (elem >>= 8)
		count += lt8[elem & 0xff];
	return count;
}

//------------------------------------------------------------------------------

unsigned char lt16[65536];

unsigned popcount13(unsigned elem)
{
	return lt16[elem & 0x0000ffff] + lt16[elem >> 16];
}

//------------------------------------------------------------------------------

unsigned popcount14(unsigned elem)
{
	return __builtin_popcount(elem);
}

//------------------------------------------------------------------------------

unsigned popcount15(unsigned elem)
{
	std::bitset<sizeof(elem) * 8> bits(elem);
	return bits.count();
}

//------------------------------------------------------------------------------

unsigned popcount16(unsigned elem)
{
	if (elem == 0)
		return 0;
	else
		return (elem & 1) + popcount16(elem >> 1);
}

//------------------------------------------------------------------------------

unsigned popcount17(unsigned elem)
{
	unsigned count = 0;
	unsigned char *b = (unsigned char*)(&elem), *e = b + 4;
	do {
		count += lt8[*b];
	} while (++b != e);
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount18(unsigned elem)
{
	unsigned count;
	asm("popcnt %1, %0":"=r"(count):"r"(elem));
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount19(unsigned elem)
{
	unsigned count = 0;
	do {
		asm("shr %1     \n"
		    "adc $0, %0 \n"
		    : "+r" (count), "+r" (elem));
	} while (elem);
	return count;
}

//------------------------------------------------------------------------------

unsigned popcount20(unsigned elem)
{
	unsigned count = elem - ((elem >> 1) & 033333333333) 
	                      - ((elem >> 2) & 011111111111);
	return ((count + (count >> 3)) & 030707070707) % 63;
}

//------------------------------------------------------------------------------

unsigned popcount21(unsigned u)
{
	unsigned c;
	asm("popcnt %1, %0":"=r"(c):"r"(u));
	return c;
}

//------------------------------------------------------------------------------

template <class _F> void test (_F& __f, const char* msg)
{
	using namespace std::chrono;

	unsigned id; // needed by __rdtscp()

	unsigned long long cycle_overhead = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		unsigned long long t2 = __rdtscp(&id);
		cycle_overhead = std::min(cycle_overhead, std::max(t1, t2) - std::min(t1, t2));
	}

	unsigned long long cycles = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		__f(list[0]);
		unsigned long long t2 = __rdtscp(&id);
		cycles = std::min(cycles, std::max(t1, t2) - std::min(t1, t2));
	}
	cycles -= cycle_overhead;

	auto time_overhead = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		auto t1 = high_resolution_clock::now();
		auto t2 = high_resolution_clock::now();
		time_overhead = std::min(time_overhead, t2 - t1);
	}

	unsigned result;
	auto time = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		result = 0;
		auto t1 = high_resolution_clock::now();
		std::for_each(list.begin(), list.end(), [&](unsigned x){ result += __f(x); });
		auto t2 = high_resolution_clock::now();
		time = std::min(time, t2 - t1);
	}
	time -= time_overhead;

	std::cout << '"' << std::setw(GAP * 2 - 2) << msg << '"'
	          << std::setw(GAP) << result
	          << std::setw(GAP) << cycles
	          << std::setw(GAP) << std::fixed << std::setprecision(2)
	          << time.count() / static_cast<double>(list.size())
	          << std::setw(GAP) << std::fixed << std::setprecision(2)
	          << time.count() / 1000.0
	          << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	std::iota(list.begin(), list.end(), 0);

	for (unsigned i = 0; i < 256; ++i)
		lt8[i] = __builtin_popcount(i);

	for (unsigned i = 0; i < 65536; ++i)
		lt16[i] = __builtin_popcount(i);

	std::cout << "#" << std::setw(GAP * 2 - 1) << "popcount"
	          << std::setw(GAP) << "value"
	          << std::setw(GAP) << "cycles"
	          << std::setw(GAP) << "time(ns)"
	          << std::setw(GAP) << "total(us)"
	          << std::endl;

	test(popcount00, "popcount00(empty)");
	test(popcount01, "popcount01(for)");
	test(popcount02, "popcount02(while)");
	test(popcount03, "popcount03(inline)");
	test(popcount04, "popcount04(inline v2)");
	test(popcount05, "popcount05(0'hallaron)");
	test(popcount06, "popcount06(gus)");
	test(popcount07, "popcount07(wikipedia)");
	test(popcount08, "popcount08(Wegner)");
	test(popcount09, "popcount09(lt8)");
	test(popcount10, "popcount10(lt8 v2)");
	test(popcount11, "popcount11(lt8 v3)");
	test(popcount12, "popcount12(lt8 v4)");
	test(popcount13, "popcount13(lt16)");
	test(popcount14, "popcount14(builtin)");
	test(popcount15, "popcount15(bitset)");
	test(popcount16, "popcount16(recursive)");
	test(popcount17, "popcount17(lt8+ptr)");
	test(popcount18, "popcount18(popcnt)");
	test(popcount19, "popcount19(do while)");
	test(popcount20, "popcount20(O(1))");
	test(popcount21, "popcount21(popcnt)");
}

//------------------------------------------------------------------------------

