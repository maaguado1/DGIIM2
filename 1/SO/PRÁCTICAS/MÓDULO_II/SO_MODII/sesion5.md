## MÓDULO II: Sesión 5

### Gestión y control de señales

## 1. Señales

Mecanismo de sincronización, el manejador es la función que se invoca cuando llega una señal. Las señales pueden ser depositadas o pendientes, y se crea una máscara de bloqueo, que almacena las señales bloqueadas.

| SEÑAL | DESCRIPCIÓN |
|----|----|
|SIGHUP | Term. Desconecta el terminal (demonios) |
|SIGINT | Term. Interrupción de teclado |
|SIGQUIT| Core. Terminación de teclado |
|SIGILL | Core. Ejecución de instrucción ilegal |
| SIGABRT | Core. Aborto por `abort` |
|SIGFPE | Core. Excepción coma flotante |
|SIGKILL | Term. Señal para terminar |
|SIGSEGV  | Core. Referencia inválida memoria |
|SIGPIPE | Term. Tubería rota |
|SIGALRM | Term. `alarm ` |
|SIGTERM | Term. Señal terminación |
|SIGUSR1/2 | Term. Señales definidas por usuario |
|SIGCHILD| Ign. Proceso hijo term o parado |
|SIGCONT | Cont. Continuar |
|SIGSTOP| Stop. Parar proceso |
|SIGTSTP | Stop. Parar escritura en la tty |
|SIGTTIN | Stop.  Entrada de la tty |
|SIGTTOU | Stop. Salida a la tty |


## 2. Llamadas al sistema

- **kill** enviar señal
- **sigaction** establecer la acción que realiza un proceso  (no pueden: SIGKILL SIGSTOP)
- **sigprocmask** cambiar las señales bloqueadas
- **sigpending** examina las señales pendientes
- **sigsuspend** reemplaza la máscara y suspende


### 2.1 La llamada kill

#### SINTAXIS

~~~
#include <sys/types.h>
#include <signal.h>

int kill(pid_t pid, int sig);
~~~

#### ARGS

- **pid** >0, se envía sig al proceso con pid (éxito 0, <0 error)
- **pid** = 0, sig se envía a cada proceso del grupo actual.
- **pid** = -1, se envía sig a todos los procesos (menos el primero) en la tabla de procesos (de altos a bajos)
- **pid** < -1, se envía sig a cada proceso en el grupo de procesos -pid.
- **sig** =0, no se envía nada (comprobación de errores).


### 2.2 La llamada sigaction

#### SINTAXIS
~~~
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
~~~

#### ARGS

- **signum** cualquier señal menos SIGKILL O SIGSTOP.
- **act** != NULL, la nueva acción para signum es act.
- **oldact** != NULL, la antigua se guarda ahí

Devuelve 0 si éxito, -1 si error

#### ESTRUCTURAS

~~~
struct sigaction {
  void (*sa_handler)(int);
  void (*sa_sigaction)(int, siginfo_t *, void *);
  sigset_t sa_mask;
  int sa_flags;
  void (*sa_restorer)(void); //OBSOLETO
}
~~~
donde,

- **sa_handler ** especifica la acción asociada con signum  (SIG_DFL para default, SIG_IGN para ignorar, puntero a función manejadora).
- **sa_mask** establece la máscara de señales que se bloquean, la señal que manda el manejador se bloquea si no se pone SA_NODEFER/SA_NOMASK.

~~~
int sigemptyset(sigset_t *set); //Inicializa a vacío el set de Señales
                                //éxito 0, -1 error

int sigfillset(sigset_t *set); //Inicializa un conjunto con las señales
                                //mismo ret

int sigismember(const sigset_t *set, int senyal); //busca una señal
                                                  // 1 si está, 0 si no


int sigaddset(sigset_t *set, int signo); //Añade una señal
                                          // 0 éxito -1 error

int sigdelset(sigset_t *set, int signo); //Elimina una señal
                                          //mismo ret
~~~

- **sa_flags** opciones que modifican el comportamiento

| FLAG | Descripción |
| -----| ------ |
|SA_NOCLDSTOP | no se recibe señal cuando el hijo se pare |
| SA_ONESHOT| Restaura al predeterminado (SA_RESETHAND) |
| SA_RESTART | Comportamiento BSD, las llamadas se reinician al interrumpirse |
| SA_NOMASK | Impide la recepción de la señal |
| SA_SIGINFO | Se configura sa_sigaction |

- **sa_sigaction**, tiene un parámetro siginfo_t:
~~~
siginfo_t {
  int si_signo; /* Número de señal */
  int si_errno; /* Un valor errno */
  int si_code; /* Código de señal */
  pid_t si_pid; /* ID del proceso emisor */
  uid_t si_uid; /* ID del usuario real del proceso emisor */
  int si_status; /* Valor de salida o señal */
  clock_t si_utime; /* Tiempo de usuario consumido */
  clock_t si_stime; /* Tiempo de sistema consumido */
  sigval_t si_value; /* Valor de señal */
  int /* señal POSIX.1b */
  si_int;
  void * si_ptr; /* señal POSIX.1b */
  void * si_addr; /* Dirección de memoria que ha producido el fallo */
  int si_band; /* Evento de conjunto */
  int si_fd; /* Descriptor de fichero */
}
~~~

### 2.3 La llamada sigprocmask

Examina máscara de señales

#### SINTAXIS
~~~
int sigprocmask(int how, const sigset_t *set, sigset_t *oldset);
~~~
#### ARGS

- **how** tipo de cambio (SIG_BLOCK: unión de ambos; SIG_UNBLOCK: los que hay en set se desbloquean; SIG_SETMASK: las bloqueadas son las de set)
- **set** puntero al nuevo conjunto, si es null, sigprocmask se usa de consulta
- **oldset** conjunto anterior


### 2.3. La llamada sigpending

#### SINTAXIS

~~~
int sigpending(sigset_t *set);
~~~

### 2.4. La llamada sigsuspend

#### SINTAXIS

~~~
int sigsuspend(const sigset_t *mask); //puntero al nuevo conjunto de enmascaradas
~~~

### COSITAS:

- No se puede bloquear SIGKILL y SIGSTOP
- Comportamiento indefinido después de ignorar SIGFPE, SIGILL, SIGSEGV
- Sigaction puede llamarse con un argumento nulo (2º) par conocer el manejador actual. (2º y 3º nulos para comprobar si la señal es válida)
- Cosas varias especificas (leer)
