#encoding:utf-8

require_relative 'Weapon'
require_relative 'ShieldBooster'
require_relative 'WeaponType'
require_relative 'HangarToUI'

module Deepspace
  
 # Class to represent a certain hangar
#
# @author María Aguado Martínez
  
class Hangar
 
  
  # Constructors
	# ==========================================================================
  
  
  
  # Class initializer
	# @param capacity [Integer] maximum number of shields and weapons the hangar can have
  def initialize( capacity)
    
    @shieldBoosters= []
    @weapons=[]
    @maxElements=capacity
    
  end
  
  
  # Copy constructor
	# @param otro instance which is going to be copied
 
  def self.newCopy(otro)
    copy =Hangar.new(otro.maxElements)
    
    for h in otro.shieldBoosters
      copy.addShieldBooster(h)
    end
    
    for w in otro.weapons
      copy.addWeapon(w)
    end
    
    return copy
  end
  
  
  
  # Getters
	# ==========================================================================
  def shieldBoosters
    return @shieldBoosters
  end
  
  def weapons
    return @weapons
  end
  
  
  def maxElements
    return @maxElements
  end
  
  
  
  # Checks if there's space left for more elements at the hangar
	# @return [Boolean] true, if there's space left for at least one more element;
	#                   false, if the hangar is full
  
  def spaceAvailable
    i= @weapons.length + @shieldBoosters.length
    return i < maxElements
    
  end
  
  
  
  
  # Setters
	# ==========================================================================

  
  # Adds a new weapon to the hangar
	# @param w [Weapon] the weapon to be added
	# @return [Boolean] true, if the operation runs successfully;
	#                   false, if something fails (no room for another weapon)
  def addWeapon(w)
    if (spaceAvailable)
      @weapons << w
      return true
    else
      return false
    end
  end
  
  
  # Adds a new shield booster to the hangar
	# @param w [ShieldBooster] the shield booster to be added
	# @return [Boolean] true, if the operation runs successfully;
	#                   false, if something fails (no room for another booster)
  def addShieldBooster(w)
    if (spaceAvailable)
      @shieldBoosters << w
      return true
    else
      return false
    end
  end
  
  
  
  # Removes a weapon from the hangar
	# @param w [Integer] the position in which the weapon that wants to be removed is located
	# @return [Weapon] nil, if position is invalid or removal operation is unsuccessful;
	#                  the weapon deleted, if removal is successful
  def removeWeapon(w)
    if w>=0 and w < @weapons.length
      otro = @weapons.at(w)
      @weapons.delete_at(w)
    end

    return otro
    
  end
  
  
  # Removes a shield booster from the hangar
	# @param s [Integer] the position in which the booster that wants to be removed is located
	# @return [Weapon] nil, if position is invalid or removal operation is unsuccessful;
	#                  the shield booster deleted, if removal is successful
  def removeShieldBooster(w)
    if w>=0 and w < @shieldBoosters.length
      otro = @shieldBoosters.at(w)
      @shieldBoosters.delete_at(w)
    end
    return otro
    
  end
  
  
  
  # String representation, UI version
	# ==========================================================================

  def getUIversion
    return HangarToUI.new(self)
  end
  
 def to_s
        message=   "[Hangar]: Maxelements: #{@maxElements} \n\t\tShields: ";
        for s in @shieldBoosters
          message += s.to_s + " "
        end
        message += "\n\t\tWeapons = "
        for w in @weapons
          message += w.to_s + " "
        end
        return message;
        
  end

 


end
end
