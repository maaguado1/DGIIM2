## CHULETA EXAMEN TEORÍA 2

### Especificadores de acceso

Permiten restringir el acceso a atributos y métodos, y ocultan detalles de la implementación (usamos el nivel más restrictivo posible).

#### Java

Cada atributo o método debe incluir su especificador.

##### 1. private

- Sólo se accede desde el código de la propia clase (desde instancia a clase o de clase a clase).
- Se puede acceder a elementos privados de otra instancia de la misma clase.
- NO pueden acceder subclases

##### 2. package

- Los elementos son públicos dentro del paquete y privados fuera

##### 3. protected

- Son públicos dentro del mismo paquete
- DENTRO DE UNA INSTANCIA
  - Son accesibles desde subclases que **estén en otro paquete**, es decir dentro de una misma instancia, se podrá acceder a elementos protegidos definidos en cualquiera de sus superclases.
- UNA INSTANCIA DISTINTA:
  - Para acceder a protected de una instancia distinta desde ámbito de clase o de instancia
    - La instancia tiene que ser de la misma clase o de una subclase de la misma. (debe ser un-yo)
    - El elemento tiene que estar declarado en la clase propietaria del código que llama o desde una superclase de ella. (debe ser visible por mí).

##### 3. public

Todos pueden acceder





#### Ruby

ATRIBUTOS: siempre privados

MÉTODOS: públicos por defecto pero se pueden modificar (el initialize es privado)

###### 1. private

- No se pueden usar mediante receptor de mensaje explícito.
- Solo se puede usar un método privado de la propia instancia.
- Si B  hereda de A
  - En ámbito de instancia se puede llamar a métodos de instancia.
  - En ámbito de clase se puede llamar a métodos de clase.
- No se puede acceder a métodos privados de clase/instancia desde ámbito de instancia/clase.

##### 2. protected

- Sí pueden ser invocados con receptor de mensaje explícito
  - La clase del código que invoca debe ser la misma o una subclase de la clase donde se declaró dicho método.
- NO EXISTEN MÉTODOS DE CLASE PROTECTED

TENER CUIDADO CON LA FALSA SEGURIDAD: se puede hacer un consultor para un atributo privado, pero al devolver una referencia dicho atributo se podrá modificar.

```Ruby
class Padre
  
  @instanciaDeClase =1
  @duda =2
  @@deClase = 11
  @@duda = 22
  
  
  def initialize
    @deInstancia = 333
    @duda = 444
  end
  
  
  def self.salida
    
    puts @instanciaDeClase+1
    puts @duda+1 unless @duda.nil?
    puts @@deClase+1
    puts @@duda +1
    
  end
  
  def salida
    
    puts @deInstancia+1
    puts @duda+1 
    puts @@deClase+1
    puts @@duda +1
    
  end
end


class Hija<Padre
  
  @instanciaDeClase = -1
  
  @@deClase = -11
  def modifica
    @duda += 111
  end
end

Padre.salida #2 3 -10 23
Hija.salida #0 -10 23
p=Padre.new
p.salida #334 445 -10 23
h = Hija.new
h.salida #334 445 -10 23

h.modifica
h.salida #334 556 -10 23
```



### Clases absractas

Sirven para modelar entidades de las que se conoce la información, la funcionalidad pero no se sabe cómo realizan la funcionalidad. 

#### Java

Se declaran como abstractas y no dan implementación para alguno de sus métodos, que se llamarán abstractos.

NO SE INSTANCIAN CLASES ABSTRACTAS (sí declarar una variable cuyo tipo sea una clase abstracta). Aún así, las clases abstractas pueden tener constructores. En ese caso, no se les llamará de forma directa pero se deberá llamar desde las clases derivadas para inicializar los atributos que sean necesarios.

Si una subclase de una clase abstracta no implementa alguno de los métodos de la primera, fuerza a sus descendientes a hacerlo.

#### Ruby

**Ruby** no soporta las clases abstractas, pues mucho de lo que aporta la clase abstracta viene de los tipos de datos, y claramente en ruby no hay, no tiene sentido implementarlas ni dar soporte.

Podemos crear clases no instanciables, que hagan la función de una clase abstracta, en ese caso:

- Hacemos privado el método new (para que no sea instanciable).
- Hacemos public el método new en las clases derivadas (heredan todos los atributos).

### Interfaces

Define un determinado protocolo de comportamiento que además se puede reutilizar. Las clases realizan las interfaces, es decir que obligamos a que la clase tenga el comportamiento que define la interfaz. 



#### Java

Se pueden declarar variables del tipo que define la interfaz, en ese caso, dichas variables podrán referenciar instancias de clases que realicen dicha interfaz.

~~~java
interface UnaCosa{
    
    void DiceCosa();
}

class OtraCosa implements UnaCosa{
    void DiceCosa(){
        System.out.println("Hola");
    }
}

//Fuera de Clase
UnaCosa una = new OtraCosa();

~~~



Una clase puede realizar varias interfaces al igual que una interfaz puede heredar de una o más interfaces.

Elementos que puede tener:

- Constantes de clase (`public static final`).
- Definición de métodos (sin implementar)
- Métodos default (con implementación)
- Métodos de clase static, métodos de la propia interfaz, no de las clases que la realizan.

Las interfaces NO TIENEN CONSTRUCTOR, no vale pa na.

Cositas varias:

- No se pueden instanciar, pero sí declarar variables de ese tipo.

- Se pueden redefinir los métodos default

- Una clase puede heredar de otra y además realizar varias interfaces.

- Una clase abstracta puede indicar que realiza una interfaz sin implementar alguno de sus métodos.

  - Se fuerza a sus descendientes a hacerlo.

- No tienen métodos ni atributos privados, tampoco tiene sentido.

  

### Clases parametrizables

Son clases definidas en función de un tipo de dato, en las que se generalizan un conjunto de operaciones válidas para diferentes tipos de datos. El parámetro suele ser un tipo de dato, por lo que en **Ruby** no tiene sentido.

Permite pasar tipos como parámetros a clases e interfaces, y además podemos filtrar para que el parámetro sea de una subclase de otro o realice una interfaz:

- Se utiliza comprobación de tipos en tiempo de compilación: mediante el filtrado anterior, para que se restrinjan los tipos dentro de las subclases del tipo que queremos.
- La implementación de un método puede requerir que T disponga de un determinado método (que haga una interfaz).
- Las interfaces también pueden ser paramétricas

#### Java

No se crea una instancia de cada clase paramétrica cada vez que se utiliza con un tipo, sino que en tiempo de ejecución el parámetro pasa a ser del tipo:

- Object
- Del tipo que se le haya puesto como restricción.

Se pueden añadir cosas que **sean-un** tipo del que hemos puesto (subclases).

COSITAS:

- Las interfaces también se pueden hacer paramétricas (si nos queremos referir dentro de otro "parámetro " <> a que una clase realice la interfaz, ponemos **T extends Copiable<T>**).
- 

### Polimorfismo y ligadura dinámica

El polimorfismo es la capacidad de un identificador de referenciar objetos de distintos tipos (en ruby por ej se da de forma natural porque cualquier variable puede referenciar cualquier tipo de objeto), con ciertas limitaciones:

- Principio de Liskov: si B es un subtipo de A, se pueden utilizar instancias de B donde se esperan instancias de A.

En una variable tenemos:

- Tipo estático, clase en la que se declara la variable
- Tipo dinámico: clase a la que pertenece el objeto referenciado en un momento determinado por una variable.

Y en torno a estos conceptos aparecen:

- Ligadura estática: cuando el enlace del código a ejecutar por una llamada se hace en tiempo de compilación.
- Ligadura dinámica: cuando dicho enlace se hace en base al tipo dinámico. En java y en ruby es por defecto esto.

#### Reglas

- El tipo estático limita (en tiempo de compilación):
  - Lo que puede referenciar una variable
  - Los métodos que pueden ser invocados

#### Casts

Se le indica al compilador que considere que el tipo de la variable es otro, con limitaciones:

- Downcasting: el tipo de la variable es de una subclase del tpo con que se declaró
- Upcasting: se le indica que considere temporalmente que el tipo es de una superclase del tipo con que se declaró.
  - Es innecesario porque en tiempo de ejecución se mira el tipo dinámico.

### Herencia en el Ámbito de clase

#### Java

Hay algunas diferencias con la herencia en clase y en instancia:

1. No se permite la redefinición de métodos de clase.
2. Se produce lo que se llama **variable shadowing** , es decir cuando en una subclase se declara una variable de clase con el mismo nombre, se "tapa" a la variable de clase de la clase padre.
3. Sin embargo, si se tiene que "subir" a la clase padre para buscar un método, se utilizarán las variables de clase de la clase en la que estemos.

#### Ruby

Las clases son first class citizens, no hay nada de lo anterior:













