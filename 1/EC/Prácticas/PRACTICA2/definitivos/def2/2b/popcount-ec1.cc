//------------------------------------------------------------------------------
// popcount.cc
// http://www.dalkescientific.com/writings/diary/archive/2011/11/02/faster_popcount_update.html
//------------------------------------------------------------------------------

#include <x86intrin.h> // __rdtscp

#include <algorithm>   // generate
#include <array>       // array
#include <bitset>      // bitset
#include <chrono>      // now
#include <functional>  // bind
#include <iomanip>     // setw
#include <iostream>    // cout endl
#include <numeric>     // iota

//------------------------------------------------------------------------------

const unsigned GAP = 12;   // gap between columns
const unsigned REP = 100;  // how many times we repeat the experiment

std::array<unsigned, 1 << 16> list; // 64K x 4B = 256KB

//------------------------------------------------------------------------------

unsigned popcount00(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount01(unsigned elemento)
{
    unsigned suma = 0;
    for (unsigned i = 0; i < sizeof(elemento)*8; ++i)
    {
        unsigned mascara = 1 << i;
        suma += (elemento & mascara) != 0;
    }
    return suma;
}

//------------------------------------------------------------------------------

unsigned popcount02(unsigned elemento)
{
    unsigned suma = 0, mascara = 1;
    for (unsigned i = 0; i < sizeof(elemento)*8; ++i)
    {
        suma += (elemento & mascara) != 0;
        mascara <<= 1;
    }
    return suma;
}

//------------------------------------------------------------------------------

unsigned popcount03(unsigned elemento)
{
    unsigned suma = 0;
    while (elemento != 0) 
    {
        suma += elemento & 1;
        elemento >>= 1;
    }
    return suma;
}

//------------------------------------------------------------------------------

unsigned popcount04(unsigned elemento)
{
    asm("    push %ebp          \n"
        "    xor %eax, %eax    \n"
        "    mov %esp, %ebp    \n"
        "    mov 8(%ebp), %edx \n"
        ".L0:                    \n"
        "    test %edx, %edx   \n"
        "    jz .L1              \n"
        "    shr %edx           \n"
        "    adc $0, %eax       \n"
        "    jmp .L0             \n"
        ".L1:                    \n"
        "    pop %ebp           \n"
        "    ret                 \n");
}

//------------------------------------------------------------------------------

unsigned popcount05(unsigned elemento)
{
    unsigned suma = 0;
    while (elemento != 0)
    {
        elemento >>= 1;
        asm("adc $0, %0":"+r"(suma));
    }
    return suma;
}

//------------------------------------------------------------------------------

unsigned popcount06(unsigned elemento)
{
    unsigned suma = 0;
    asm("    clc          \n"
        "0:               \n"
        "    adc $0, %0   \n"
        "    shr %1       \n"
        "    jnz 0b       \n"
        "    adc $0, %0   \n"
        :"+r"(suma):"r"(elemento));
    return suma;
}

//------------------------------------------------------------------------------

unsigned popcount07(unsigned elem) // from wikipedia
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount08(unsigned elem) // by Wegner
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount09(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount10(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount11(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount12(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount13(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount14(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount15(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

unsigned popcount16(unsigned elem)
{
	return elem;
}

//------------------------------------------------------------------------------

template <class _F> void test (_F& __f, const char* msg)
{
	using namespace std::chrono;

	unsigned id; // needed by __rdtscp()

	unsigned long long cycle_overhead = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		unsigned long long t2 = __rdtscp(&id);
		cycle_overhead = std::min(cycle_overhead, std::max(t1, t2) - std::min(t1, t2));
	}

	unsigned long long cycles = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		__f(list[0]);
		unsigned long long t2 = __rdtscp(&id);
		cycles = std::min(cycles, std::max(t1, t2) - std::min(t1, t2));
	}
	cycles -= cycle_overhead;

	auto time_overhead = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		auto t1 = high_resolution_clock::now();
		auto t2 = high_resolution_clock::now();
		time_overhead = std::min(time_overhead, t2 - t1);
	}

	unsigned result;
	auto time = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		result = 0;
		auto t1 = high_resolution_clock::now();
		std::for_each(list.begin(), list.end(), [&](unsigned x){ result += __f(x); });
		auto t2 = high_resolution_clock::now();
		time = std::min(time, t2 - t1);
	}
	time -= time_overhead;

	std::cout << '"' << std::setw(GAP * 2 - 2) << msg << '"' 
	          << std::setw(GAP) << result
	          << std::setw(GAP) << cycles
	          << std::setw(GAP) << std::fixed << std::setprecision(2)
	          << duration_cast<nanoseconds>(time).count() / static_cast<double>(list.size())
	          << std::setw(GAP) << duration_cast<nanoseconds>(time).count() / 1000.0
	          << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	std::iota(list.begin(), list.end(), 0);

	std::cout << "#" << std::setw(GAP * 2 - 1) << "popcount"
	          << std::setw(GAP) << "value"
	          << std::setw(GAP) << "cycles"
	          << std::setw(GAP) << "time(ns)"
	          << std::setw(GAP) << "total(us)"
	          << std::endl;

	test(popcount00, "vacia");
	test(popcount01, "for");
	test(popcount02, "for v2");
	test(popcount03, "while");
	test(popcount04, "asm while");
	test(popcount05, "asm while v2");
	test(popcount06, "asm do while");
	test(popcount07, "");
	test(popcount08, "");
	test(popcount09, "");
	test(popcount10, "");
	test(popcount11, "");
	test(popcount12, "");
	test(popcount13, "");
	test(popcount14, "");
	test(popcount15, "");
	test(popcount16, "");
}

//------------------------------------------------------------------------------
