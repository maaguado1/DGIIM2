

CERCANIA

$$\mu^2_{Y/X} =  1 - (5.23217*10⁻⁹)/(8,56481840445231*10⁻⁶ ) = 0,999389109056033$$

~~~
% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[]
\begin{tabular}{lllll}
\cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Número de ciudades} & \cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Tiempo(seg)} &  &  &  \\
{\color[HTML]{000000} 10}                                         & {\color[HTML]{000000} 4,00E-06}                            &  &  &  \\
{\color[HTML]{000000} 15}                                         & {\color[HTML]{000000} 5,00E-06}                            &  &  &  \\
{\color[HTML]{000000} 20}                                         & {\color[HTML]{000000} 8,00E-06}                            &  &  &  \\
{\color[HTML]{000000} 25}                                         & {\color[HTML]{000000} 9,00E-06}                            &  &  &  \\
{\color[HTML]{000000} 30}                                         & {\color[HTML]{000000} 1,40E-05}                            &  &  &  \\
{\color[HTML]{000000} 35}                                         & {\color[HTML]{000000} 1,80E-05}                            &  &  &  \\
{\color[HTML]{000000} 40}                                         & {\color[HTML]{000000} 2,40E-05}                            &  &  &  \\
{\color[HTML]{000000} 45}                                         & {\color[HTML]{000000} 4,00E-05}                            &  &  &  \\
{\color[HTML]{000000} 50}                                         & {\color[HTML]{000000} 4,60E-05}                            &  &  &  \\
{\color[HTML]{000000} 55}                                         & {\color[HTML]{000000} 5,70E-05}                            &  &  &  \\
{\color[HTML]{000000} 60}                                         & {\color[HTML]{000000} 6,20E-05}                            &  &  &  \\
{\color[HTML]{000000} 65}                                         & {\color[HTML]{000000} 9,60E-05}                            &  &  &  \\
{\color[HTML]{000000} 70}                                         & {\color[HTML]{000000} 8,90E-05}                            &  &  &  \\
{\color[HTML]{000000} 75}                                         & {\color[HTML]{000000} 8,50E-05}                            &  &  &  \\
{\color[HTML]{000000} 80}                                         & {\color[HTML]{000000} 0,000102}                            &  &  &  \\
{\color[HTML]{000000} 85}                                         & {\color[HTML]{000000} 0,000125}                            &  &  &  \\
{\color[HTML]{000000} 90}                                         & {\color[HTML]{000000} 0,000134}                            &  &  &  \\
{\color[HTML]{000000} 95}                                         & {\color[HTML]{000000} 0,000141}                            &  &  &  \\
{\color[HTML]{000000} 100}                                        & {\color[HTML]{000000} 0,000164}                            &  &  &  \\
{\color[HTML]{000000} 105}                                        & {\color[HTML]{000000} 0,00018}                             &  &  &  \\
{\color[HTML]{000000} 110}                                        & {\color[HTML]{000000} 0,000254}                            &  &  &  \\
{\color[HTML]{000000} 115}                                        & {\color[HTML]{000000} 0,000288}                            &  &  &  \\
{\color[HTML]{000000} 120}                                        & {\color[HTML]{000000} 0,000265}                            &  &  &  \\
{\color[HTML]{000000} 125}                                        & {\color[HTML]{000000} 0,000277}                            &  &  &  \\
{\color[HTML]{000000} 130}                                        & {\color[HTML]{000000} 0,000302}                            &  &  &  \\
{\color[HTML]{000000} 135}                                        & {\color[HTML]{000000} 0,000369}                            &  &  &  \\
{\color[HTML]{000000} 140}                                        & {\color[HTML]{000000} 0,000365}                            &  &  &  \\
{\color[HTML]{000000} 145}                                        & {\color[HTML]{000000} 0,000391}                            &  &  &  \\
{\color[HTML]{000000} 150}                                        & {\color[HTML]{000000} 0,000438}                            &  &  &  \\
{\color[HTML]{000000} 155}                                        & {\color[HTML]{000000} 0,000721}                            &  &  &  \\
{\color[HTML]{000000} 160}                                        & {\color[HTML]{000000} 0,000626}                            &  &  &  \\
{\color[HTML]{000000} 165}                                        & {\color[HTML]{000000} 0,000588}                            &  &  &  \\
{\color[HTML]{000000} 170}                                        & {\color[HTML]{000000} 0,000606}                            &  &  &  \\
{\color[HTML]{000000} 175}                                        & {\color[HTML]{000000} 0,000706}                            &  &  &  \\
{\color[HTML]{000000} 180}                                        & {\color[HTML]{000000} 0,001039}                            &  &  &  \\
{\color[HTML]{000000} 185}                                        & {\color[HTML]{000000} 0,000977}                            &  &  &  \\
{\color[HTML]{000000} 190}                                        & {\color[HTML]{000000} 0,001081}                            &  &  &  \\
{\color[HTML]{000000} 195}                                        & {\color[HTML]{000000} 0,001143}                            &  &  &  \\
{\color[HTML]{000000} 200}                                        & {\color[HTML]{000000} 0,00094}                             &  &  & 
\end{tabular}
\end{table}
Escape special TeX symbols (%, &, _, #, $)
~~~
INSERCION
$$\mu^2_{Y/X} =  1 - (0,0000000565609)/(1,20*10⁻³) = 0,999952962691594$$

~~~
% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[]
\begin{tabular}{lllll}
\cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Número de ciudades} & \cellcolor[HTML]{00AAAD}{\color[HTML]{000000} tiempo(seg)} &  &  &  \\
{\color[HTML]{000000} 10}                                         & {\color[HTML]{000000} 2,20E-05}                            &  &  &  \\
{\color[HTML]{000000} 15}                                         & {\color[HTML]{000000} 4,10E-05}                            &  &  &  \\
{\color[HTML]{000000} 20}                                         & {\color[HTML]{000000} 9,20E-05}                            &  &  &  \\
{\color[HTML]{000000} 25}                                         & {\color[HTML]{000000} 0,000151}                            &  &  &  \\
{\color[HTML]{000000} 30}                                         & {\color[HTML]{000000} 0,000246}                            &  &  &  \\
{\color[HTML]{000000} 35}                                         & {\color[HTML]{000000} 0,000242}                            &  &  &  \\
{\color[HTML]{000000} 40}                                         & {\color[HTML]{000000} 0,000395}                            &  &  &  \\
{\color[HTML]{000000} 45}                                         & {\color[HTML]{000000} 0,000357}                            &  &  &  \\
{\color[HTML]{000000} 50}                                         & {\color[HTML]{000000} 0,000504}                            &  &  &  \\
{\color[HTML]{000000} 55}                                         & {\color[HTML]{000000} 0,000605}                            &  &  &  \\
{\color[HTML]{000000} 60}                                         & {\color[HTML]{000000} 0,000572}                            &  &  &  \\
{\color[HTML]{000000} 65}                                         & {\color[HTML]{000000} 0,000662}                            &  &  &  \\
{\color[HTML]{000000} 70}                                         & {\color[HTML]{000000} 0,0008}                              &  &  &  \\
{\color[HTML]{000000} 75}                                         & {\color[HTML]{000000} 0,00085}                             &  &  &  \\
{\color[HTML]{000000} 80}                                         & {\color[HTML]{000000} 0,001069}                            &  &  &  \\
{\color[HTML]{000000} 85}                                         & {\color[HTML]{000000} 0,001078}                            &  &  &  \\
{\color[HTML]{000000} 90}                                         & {\color[HTML]{000000} 0,001259}                            &  &  &  \\
{\color[HTML]{000000} 95}                                         & {\color[HTML]{000000} 0,001525}                            &  &  &  \\
{\color[HTML]{000000} 100}                                        & {\color[HTML]{000000} 0,001699}                            &  &  &  \\
{\color[HTML]{000000} 105}                                        & {\color[HTML]{000000} 0,001998}                            &  &  &  \\
{\color[HTML]{000000} 110}                                        & {\color[HTML]{000000} 0,002431}                            &  &  &  \\
{\color[HTML]{000000} 115}                                        & {\color[HTML]{000000} 0,002615}                            &  &  &  \\
{\color[HTML]{000000} 120}                                        & {\color[HTML]{000000} 0,002926}                            &  &  &  \\
{\color[HTML]{000000} 125}                                        & {\color[HTML]{000000} 0,003323}                            &  &  &  \\
{\color[HTML]{000000} 130}                                        & {\color[HTML]{000000} 0,003814}                            &  &  &  \\
{\color[HTML]{000000} 135}                                        & {\color[HTML]{000000} 0,004196}                            &  &  &  \\
{\color[HTML]{000000} 140}                                        & {\color[HTML]{000000} 0,005204}                            &  &  &  \\
{\color[HTML]{000000} 145}                                        & {\color[HTML]{000000} 0,005984}                            &  &  &  \\
{\color[HTML]{000000} 150}                                        & {\color[HTML]{000000} 0,006329}                            &  &  &  \\
{\color[HTML]{000000} 155}                                        & {\color[HTML]{000000} 0,006402}                            &  &  &  \\
{\color[HTML]{000000} 160}                                        & {\color[HTML]{000000} 0,007135}                            &  &  &  \\
{\color[HTML]{000000} 165}                                        & {\color[HTML]{000000} 0,007848}                            &  &  &  \\
{\color[HTML]{000000} 170}                                        & {\color[HTML]{000000} 0,008188}                            &  &  &  \\
{\color[HTML]{000000} 175}                                        & {\color[HTML]{000000} 0,009087}                            &  &  &  \\
{\color[HTML]{000000} 180}                                        & {\color[HTML]{000000} 0,009758}                            &  &  &  \\
{\color[HTML]{000000} 185}                                        & {\color[HTML]{000000} 0,010513}                            &  &  &  \\
{\color[HTML]{000000} 190}                                        & {\color[HTML]{000000} 0,011513}                            &  &  &  \\
{\color[HTML]{000000} 195}                                        & {\color[HTML]{000000} 0,013424}                            &  &  &  \\
{\color[HTML]{000000} 200}                                        & {\color[HTML]{000000} 0,013615}                            &  &  & 
\end{tabular}
\end{table}
Escape special TeX symbols (%, &, _, #, $)
Compress whitespace    Smart output formatting
~~~

MST

$$\mu^2_{Y/X} =  1 - (0,0000649995)/(0,054156991975838) = 0,998799794862518$$

~~~
% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[]
\begin{tabular}{lllll}
\cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Número de ciudades} & \cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Tiempo(seg)} &  &  &  \\
{\color[HTML]{000000} 10}                                         & {\color[HTML]{000000} 0,000106}                            &  &  &  \\
{\color[HTML]{000000} 15}                                         & {\color[HTML]{000000} 0,000239}                            &  &  &  \\
{\color[HTML]{000000} 20}                                         & {\color[HTML]{000000} 0,000608}                            &  &  &  \\
{\color[HTML]{000000} 25}                                         & {\color[HTML]{000000} 0,000979}                            &  &  &  \\
{\color[HTML]{000000} 30}                                         & {\color[HTML]{000000} 0,001484}                            &  &  &  \\
{\color[HTML]{000000} 35}                                         & {\color[HTML]{000000} 0,002079}                            &  &  &  \\
{\color[HTML]{000000} 40}                                         & {\color[HTML]{000000} 0,003078}                            &  &  &  \\
{\color[HTML]{000000} 45}                                         & {\color[HTML]{000000} 0,003457}                            &  &  &  \\
{\color[HTML]{000000} 50}                                         & {\color[HTML]{000000} 0,004331}                            &  &  &  \\
{\color[HTML]{000000} 55}                                         & {\color[HTML]{000000} 0,005277}                            &  &  &  \\
{\color[HTML]{000000} 60}                                         & {\color[HTML]{000000} 0,00634}                             &  &  &  \\
{\color[HTML]{000000} 65}                                         & {\color[HTML]{000000} 0,007596}                            &  &  &  \\
{\color[HTML]{000000} 70}                                         & {\color[HTML]{000000} 0,008953}                            &  &  &  \\
{\color[HTML]{000000} 75}                                         & {\color[HTML]{000000} 0,010426}                            &  &  &  \\
{\color[HTML]{000000} 80}                                         & {\color[HTML]{000000} 0,012018}                            &  &  &  \\
{\color[HTML]{000000} 85}                                         & {\color[HTML]{000000} 0,013962}                            &  &  &  \\
{\color[HTML]{000000} 90}                                         & {\color[HTML]{000000} 0,016771}                            &  &  &  \\
{\color[HTML]{000000} 95}                                         & {\color[HTML]{000000} 0,017814}                            &  &  &  \\
{\color[HTML]{000000} 100}                                        & {\color[HTML]{000000} 0,019649}                            &  &  &  \\
{\color[HTML]{000000} 105}                                        & {\color[HTML]{000000} 0,022039}                            &  &  &  \\
{\color[HTML]{000000} 110}                                        & {\color[HTML]{000000} 0,02402}                             &  &  &  \\
{\color[HTML]{000000} 115}                                        & {\color[HTML]{000000} 0,025957}                            &  &  &  \\
{\color[HTML]{000000} 120}                                        & {\color[HTML]{000000} 0,027958}                            &  &  &  \\
{\color[HTML]{000000} 125}                                        & {\color[HTML]{000000} 0,030806}                            &  &  &  \\
{\color[HTML]{000000} 130}                                        & {\color[HTML]{000000} 0,033569}                            &  &  &  \\
{\color[HTML]{000000} 135}                                        & {\color[HTML]{000000} 0,036613}                            &  &  &  \\
{\color[HTML]{000000} 140}                                        & {\color[HTML]{000000} 0,039958}                            &  &  &  \\
{\color[HTML]{000000} 145}                                        & {\color[HTML]{000000} 0,043285}                            &  &  &  \\
{\color[HTML]{000000} 150}                                        & {\color[HTML]{000000} 0,045643}                            &  &  &  \\
{\color[HTML]{000000} 155}                                        & {\color[HTML]{000000} 0,049535}                            &  &  &  \\
{\color[HTML]{000000} 160}                                        & {\color[HTML]{000000} 0,0528}                              &  &  &  \\
{\color[HTML]{000000} 165}                                        & {\color[HTML]{000000} 0,058086}                            &  &  &  \\
{\color[HTML]{000000} 170}                                        & {\color[HTML]{000000} 0,05989}                             &  &  &  \\
{\color[HTML]{000000} 175}                                        & {\color[HTML]{000000} 0,065513}                            &  &  &  \\
{\color[HTML]{000000} 180}                                        & {\color[HTML]{000000} 0,067416}                            &  &  &  \\
{\color[HTML]{000000} 185}                                        & {\color[HTML]{000000} 0,070933}                            &  &  &  \\
{\color[HTML]{000000} 190}                                        & {\color[HTML]{000000} 0,076362}                            &  &  &  \\
{\color[HTML]{000000} 195}                                        & {\color[HTML]{000000} 0,0823}                              &  &  &  \\
{\color[HTML]{000000} 200}                                        & {\color[HTML]{000000} 0,085784}                            &  &  & 
\end{tabular}
\end{table}
~~~



TAREAS

$$\mu^2_{Y/X} =  1 - (2283290)/(995860443438,319) =0,999997707218903 $$

~~~
% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[]
\begin{tabular}{lllll}
\cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Tamaño} & \cellcolor[HTML]{00AAAD}{\color[HTML]{000000} Tiempo(seg*10⁻⁶)} &  &  &  \\
{\color[HTML]{000000} 100}                            & {\color[HTML]{000000} 5,52E+02}                                 &  &  &  \\
{\color[HTML]{000000} 300}                            & {\color[HTML]{000000} 3,34E+03}                                 &  &  &  \\
{\color[HTML]{000000} 500}                            & {\color[HTML]{000000} 8,50E+03}                                 &  &  &  \\
{\color[HTML]{000000} 700}                            & {\color[HTML]{000000} 1,65E+04}                                 &  &  &  \\
{\color[HTML]{000000} 900}                            & {\color[HTML]{000000} 2,50E+04}                                 &  &  &  \\
{\color[HTML]{000000} 1100}                           & {\color[HTML]{000000} 3,71E+04}                                 &  &  &  \\
{\color[HTML]{000000} 1300}                           & {\color[HTML]{000000} 5,21E+04}                                 &  &  &  \\
{\color[HTML]{000000} 1500}                           & {\color[HTML]{000000} 7,03E+04}                                 &  &  &  \\
{\color[HTML]{000000} 1700}                           & {\color[HTML]{000000} 8,96E+04}                                 &  &  &  \\
{\color[HTML]{000000} 1900}                           & {\color[HTML]{000000} 1,12E+05}                                 &  &  &  \\
{\color[HTML]{000000} 2100}                           & {\color[HTML]{000000} 1,38E+05}                                 &  &  &  \\
{\color[HTML]{000000} 2300}                           & {\color[HTML]{000000} 1,65E+05}                                 &  &  &  \\
{\color[HTML]{000000} 2500}                           & {\color[HTML]{000000} 1,90E+05}                                 &  &  &  \\
{\color[HTML]{000000} 2700}                           & {\color[HTML]{000000} 2,22E+05}                                 &  &  &  \\
{\color[HTML]{000000} 2900}                           & {\color[HTML]{000000} 2,57E+05}                                 &  &  &  \\
{\color[HTML]{000000} 3100}                           & {\color[HTML]{000000} 2,93E+05}                                 &  &  &  \\
{\color[HTML]{000000} 3300}                           & {\color[HTML]{000000} 3,36E+05}                                 &  &  &  \\
{\color[HTML]{000000} 3500}                           & {\color[HTML]{000000} 3,78E+05}                                 &  &  &  \\
{\color[HTML]{000000} 3700}                           & {\color[HTML]{000000} 4,23E+05}                                 &  &  &  \\
{\color[HTML]{000000} 3900}                           & {\color[HTML]{000000} 4,69E+05}                                 &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  &  \\
{\color[HTML]{000000} }                               & {\color[HTML]{000000} }                                         &  &  & 
\end{tabular}
\end{table}
~~~
