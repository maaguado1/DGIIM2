/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import View.GUI.MainWindow2;
import deepspace.CombatElement;

import java.util.ArrayList;
import java.util.Collections;
import deepspace.GameUniverse;
/**
 *
 * @author profe
 */
public class Controller2 {
    private static final Controller2 instance = new Controller2();
    
    
    private GameUniverse game;
    private MainWindow2 view;
    
    private Controller2 () {}
    
    public static Controller2 getInstance () {
      return instance;
    }
    
    public void setModelView (GameUniverse aGame, MainWindow2 aView) {
      game = aGame;
      view = aView;
    }
    
    public void start() {
        game.init();
        view.updateView();
        view.showView();
    }
    
    
    public ArrayList<CombatElement> getArray1(){return game.getArray1();}
    public ArrayList<CombatElement> getArray2(){return game.getArray2();}
    
    public void transfer (ArrayList<Integer> elements){
        game.transferelements(elements);
        
    }
    
    public void addElement(){
        game.addElement();

    }
    
    public void finish (int i) {
        if (view.confirmExitMessage()) {
          System.exit(i);
        }
    }
      
    
}
