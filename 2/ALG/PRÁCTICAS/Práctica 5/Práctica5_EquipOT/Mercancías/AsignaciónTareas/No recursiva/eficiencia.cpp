#include <algorithm>
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto
{
  float x;
  float y;
};

struct Arista
{
  int inicio;
  int final;
};

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: F U N C I O N E S    C O M U N E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Generacion de datos para el problema
//==============================================================================

double random_double(double min, double max)
{

  return (rand() / (double)RAND_MAX) * (max - min) + min;
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Genera los puntos necesarios para el problema
 * @param num_points, la cantidad de puntos con la que queremos trabajar
 * @return un vector de puntos, cuyas coordenadas estan en el intevalo [0, num_points]
 * */
vector<vector<int>> generarProblema(int n)
{
  srand(time(NULL));
  vector<vector<int>> Matriz;
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;

    for (int j = 0; j < n; j++)
    {
      double x = random_double(1, 100);
      fila.push_back(x);
    }
    Matriz.push_back(fila);
  }
  return Matriz;
}
/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[i][solucion[i]];
  }
  return valor;
}

void ConfiguracionSiguiente(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T, bool &final)
{
  int nuevovalor;
  
  if (solucion[nivel] < solucion.size())
  {
    final=false;
    solucion[nivel] = solucion[nivel] + 1;
    if (solucion[nivel] == 1)
    {
      nuevovalor = costeactual + T[nivel][solucion[nivel]-1];
    }
    else
    {
      nuevovalor = costeactual + T[nivel][solucion[nivel]-1] - T[nivel][solucion[nivel] - 2];
    }

    costeactual = nuevovalor;
  }
}

bool Factible(vector<int> solucion, int nivel)
{
  bool factibilidad = true;
  if (solucion[nivel] <= solucion.size()){
  for (int i = 0; i < nivel && factibilidad; i++)
  {
    if (solucion[nivel] == solucion[i])
      factibilidad = false;
  }
  }
  else
  {
      factibilidad=false;
  }
  
  return factibilidad;
}

bool Final(vector<int> solucion, int nivel)
{
  return (nivel == solucion.size() - 1 && Factible(solucion, nivel));
}
bool HayHermanos(vector<int> solucion, int nivel)
{
  return solucion[nivel] < solucion.size();
}

void ConfiguracionAnterior(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T)
{
  costeactual = costeactual - T[nivel][solucion[nivel]-1];
  solucion[nivel] = 0;
}

vector<int> Backtracking(vector<vector<int>> T)
{

  int N = T.size();
  int nivel = 0;
  bool esfinal=false;
  //Solución inicial
  vector<int> solucion, final;
  for (int i = 0; i < N; i++)
    solucion.push_back(0);

  int costeactual = 0;
  int coste = INT_MAX;
  do{
    ConfiguracionSiguiente(nivel, solucion, costeactual, T, esfinal);

  if (Final(solucion, nivel) && !esfinal){
    esfinal=true;
    if (costeactual < coste)
  {
    coste = costeactual;
    final = solucion;

  }}
  else if (Factible(solucion, nivel) && !esfinal)
  {
    nivel++;
  }
  else
  {
    esfinal = false;
    while (!HayHermanos(solucion, nivel) && (nivel >= 0))
    {
      ConfiguracionAnterior(nivel, solucion, costeactual, T);
      nivel--;
    }
  }
}while(nivel >=0);
  final.push_back(coste);
  return final;
}

//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    printf("Error, modo de ejecución; ./cercania <caracter> <tamaño>");
    exit(-1);
  }

  clock_t t_antes;   // Valor del reloj antes de la ejecución
  clock_t t_despues; // Valor del reloj después de la ejecución

  string algoritmo;
  int tamanio;
  vector<vector<int>> matriz;
  vector<int> solucion3;
  double t3;

  tamanio = atoi(argv[1]);
  //Generamos los problemas
  matriz = generarProblema(tamanio);

  t_antes = clock();
  solucion3 = Backtracking(matriz);

  t_despues = clock();
  int coste = solucion3[solucion3.size()-1];
  solucion3.pop_back();
  salidavec(solucion3);

  t3 = ((double)(t_despues - t_antes)) / CLOCKS_PER_SEC;

  //Mostramos dicho tiempo
  cout << tamanio << " " << t3 <<  endl;
}
