#include "receta.h"
#include "recetas.h"
#include "instrucciones.h"
#include "color.h"
#include <fstream>


using namespace std;

void salidareceta(const receta& r){
  cout << FRED("CODE: ") << r.getCode() << FRED(" NOMBRE: ") << r.getNombre() << FRED(" PLATO: ") << r.getPlato() << endl;
}


void salidaingredientes(const receta& r){
  for (receta::const_iterator it=r.begin(); it!= r.end(); ++it)
      cout << (*it).first << " " << (*it).second << endl;
}


void salidanutricion(const receta& r){
  cout << BOLD("Calorías: ") << r.getCalorias() << BOLD("\nHidratos: ") << r.getHc() << BOLD("\nGrasas: ") << r.getGrasas() << BOLD("\nProteínas: ") << r.getProteinas() << BOLD("\nFibra: ") << r.getFibra() << endl;
}


int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        cout << FBLU("ERROR: formato entrada\nIntroduce:\n ./cocinero_integral <fichero_acciones> <fichero_receta> <fichero_ingredientes> <ruta_ficheros_instrucciones>" )<< endl;
        return 0;
    }
    ifstream f(argv[1]);
    if (!f)
    {
        cout << "No existe el fichero " << argv[1] << endl;
    }

    /***********************************************************************/

    //SECTION 1: Obtengo las acciones, recetas, ingredientes.
    acciones r;
    f >> r;
    cout << "Fichero de acciones leído.\n" << endl;
    cout << "Pulse una tecla para continuar" << endl;
    cin.get();
    f.close();

    //Lectura de recetas
    f.open(argv[2]);
    if (!f)
    {
        cout << "No existe el fichero " << argv[2] << endl;
    }
    recetas rec;
    f >> rec;
    cout << "Fichero de recetas leído.\n" << endl;
    cout << "Pulse una tecla para continuar" << endl;
    cin.get();
    f.close();

    //Lectura de los ingredientes
    
    f.open(argv[3]);
    if (!f)
    {
        cout << "No existe el fichero " << argv[3] << endl;
    }
    ingredientes ings;
    f >> ings;
    cout << "Fichero de ingredientes leído. Almaceno nutricionales." << endl;
    rec.setValores(ings);
    cout << "Pulse una tecla para continuar" << endl;
    cin.get();
    f.close();


    //Lectura de todas las instrucciones (un archivo por cada receta)
    string ruta = string(argv[4]);
    string nombre_dir ="";
    string nombre_rec="";
    int i=0;
    for (recetas::iterator it=rec.begin(); it!= rec.end(); ++it){
        
        //Creo la ruta
        nombre_dir = ruta + "/R"+ to_string(i+1) + "m.txt";
        nombre_rec = "R"+ to_string(i+1);
        cout << "Abriendo... "<< nombre_dir << endl;
        f.open(nombre_dir);
        if (!f)
        {
            cout << "No existe el fichero " << nombre_dir << endl;
        }
        instrucciones inst(r);
        f >> inst;
        inst.setIngs(ings);
        f.close();

        rec[nombre_rec].setInstrucciones(inst);
        ++i;
    }

  /***********************************************************************/

  

  //SECTION 3: SALIDA POR PANTALLA


  for (recetas::iterator it= rec.begin(); it!=rec.end(); ++it){
    salidareceta(*it);

  }

  cout << "Pulse una tecla para continuar" << endl;
  cin.get();
  cout << "Dime el código de una receta";
  string c;
  cin >> c;
  if (rec.count(c)>=1)
  {

    receta s=rec[c];
    salidareceta(s);
    cout << FRED("\nINGREDIENTES: ") << endl;
    salidaingredientes(s);

    cout << FRED("\nINFORMACIÓN NUTRICIONAL: " )<< endl;
    salidanutricion(s);

    cout << FRED("\nPASOS A SEGUIR: ") << endl;
    cout << s.getInstrucciones();

  }
  else{
    cout << "ERROR. La receta introducida no existe. " << endl;
  }
  string p;
  cout << UNDL("Dime el código de la primera receta para fusionar: ");
  cin >> c;
  cout << UNDL("Dime el código de la segunda receta para fusionar: ");
  cin >> p;
  receta fusion;
  if (rec.count(c) == 0 && rec.count(p) != 0){
    cout << "La receta " << c << " no existe. Mostrando " << p << endl;
    fusion = rec[p];
  }
    
  else if (rec.count(c) != 0 && rec.count(p) == 0){
    cout << "La receta " << p << " no existe. Mostrando " << c << endl;
    fusion = rec[c];
  }
  else if (rec.count(c)==0 && rec.count(p)==0){
    cout << "Ninguna receta existe. Error";
    exit(-1);
  }
  else
    fusion = (rec[c].fusionar(rec[p]));

  salidareceta(fusion);
  cout << FRED("\nINGREDIENTES: ") << endl;
  salidaingredientes(fusion);

  cout << FRED("\nINFORMACIÓN NUTRICIONAL: " )<< endl;
  salidanutricion(fusion);

  cout << FRED("\nPASOS A SEGUIR: ") << endl;
  cout << fusion.getInstrucciones();

  return 1;
  }
