package deepspace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maguado
 */
class Loot {
    
    private int nSupplies;
    private int nWeapons;
    private int nShields;
    private int nHangars;
    private int nMedals;
    
    Loot(int su, int we, int sh, int ha, int me){
        this.nSupplies = su;
        this.nWeapons = we;
        this.nShields = sh;
        this.nHangars = ha;
        this.nMedals = me;
    }
    
    public int getNSupplies (){
        return nSupplies;
    }
    

    public int getNWeapons() {
            return nWeapons;
      }


    public int getNShields() {
            return nShields;
       }

    public int getNHangars() {
            return nHangars;
       }

    public int getNMedals() {
            return nMedals;
      }
    
    public String toString(){
        return "LOOT: \nSupplies= " + this.getNSupplies() + " Weapons= " + this.getNWeapons() + " Shields= " + this.getNShields() + " Hangars= " + this.getNHangars() + " Medals= " + this.getNMedals();
    }
    
    LootToUI getUIversion(){
        return new LootToUI(this);
    }
}
    
