
package deepspace;

import java.util.ArrayList;
import java.util.*;

/**
 * Represents damages done on a spaceship after loosing a combat.
 * They indicate which elements are going to be lost after losing the combat.
 * 
 * A damage is composed by a number of shields to lose and:
 *   - A number of weapons to lose (nWeapons)
 *   - A list of weapons to lose (weapons)
 * 
 * @author maguado
 */
public class Damage {
    
    
    
    /**
     * Number of weapons that will be lost.
     */
    private int nWeapons;
    
   
    /**
     * WeaponTypes that will be lost.
     */
    private ArrayList<WeaponType> weapons;
    
    
    /**
     * Number of shields that will be lost.
     */
    private int nShields;
    
    
    
    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------
    
    
    
    /**
    * Class initializer.
    * @param w number of weapons that will be lost.
    * @param s number of shields that will be lost.
    */
    Damage(int w, int s){
        nWeapons=w;
       nShields=s;
       weapons = null;
    }
    
    
    
    /**
    * Class initializer.
    * @param a WeaponTypes that will be lost.
    * @param s number of shields that will be lost.
    */
    Damage( ArrayList<WeaponType> a, int s){
        nShields = s;
        weapons = new ArrayList<WeaponType>(a);
        nWeapons = -1;
    }
    
    
    /**
    * Class initializer.
    * @param d Damage from which we make the copy.
    */
    Damage (Damage d){
        
        nWeapons = d.getNWeapons();
        nShields = d.getNShields();
        weapons = new ArrayList<WeaponType>(d.getWeapons());
        
        
    }
    
    
    // -------------------------------------------------------------------------
    // Getters
    // -------------------------------------------------------------------------
    
    
   /**
     * Getter for nShields
     * @return nShields
     */
    public int getNShields(){
        return nShields;
        
    }
    
    
    /**
     * Getter for nWeapons
     * @return nWeapons
     */
    public int getNWeapons(){
        return nWeapons;
    }
    
    
    /**
     * Getter for weapons
     * @return weapons
     */
    public ArrayList<WeaponType> getWeapons(){
        return weapons;
    }
    
    
    
    
    
    
    
    /**
     * Removes a given type of weapon.
     * If a list of weapons is not available, the number of weapons decreases by 1
     * @param w weapon whose type wants to be removed
     */
    
    
    public void discardWeapon(Weapon w){
        if (weapons != null)
            weapons.remove(w.getType());
        else if (nWeapons>0)
             nWeapons --;
    }
    
    
    
    /**
     * Reduces the number of shields.
     */
    public void discardShieldBooster(){
        if (nShields >0)
            nShields --;
        
    }
    
    
    
    /**
     * Checks whether the damage is affecting or not.
     * @return true, if damage has effect;
     *         false, if damage has no effect
     */
    public boolean hasNoEffect(){
        if (nWeapons==0 && weapons == null && nShields==0)
            return true;
        
        else if (nWeapons == -1 && weapons.isEmpty() && nShields == 0)
            return true;
        else
            return false;
    }
    
    
    /**
     * Checks if a given array of weapons contains a WeaponType 
     * @param w weapons where to look for.
     * @param t weaponType that has to be looked for.
     * @return int the position of the type in weapon vector.
     */
    
    private int arrayContainsType(ArrayList<Weapon> w,WeaponType t){
       boolean found = false;
        int position=-1;
        int pos =0;
        while(!found && pos<w.size()){
            if (w.get(pos).getType() == t){
                found = true;
                position = pos;
                

            }
            pos++;
        }
        return position;
    }
    
    
    
    
    /**
     * Creates a copy of current objet where weapons and shields which are
     * not included in arrays given as parameters are discarded. That's to say,
     * we shrink the Damage to the parameters
     * @param w weapons to fit
     * @param s shields to fit
     * @return a copy of the object adjusted as explained above
     */
    
    
    public Damage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s){
        if (nWeapons != -1){
            int min1;
            int min2;
            min1= (s.size() < nShields)? s.size(): nShields;
            min2=(w.size() < nWeapons) ? w.size():nWeapons;
            Damage ajustado = new Damage(min2, min1);
            return ajustado;
        }
        else{
            int min1=(s.size() < nShields)? s.size(): nShields;
            ArrayList<Weapon> w1= new ArrayList<Weapon>(w);
            ArrayList<WeaponType> w2= new ArrayList<WeaponType>(weapons);
            
            
            
            ArrayList<WeaponType> w3=new ArrayList<WeaponType>();
            for (WeaponType t: w2){
                if (arrayContainsType(w1, t) != -1){
                    w3.add(t);
                    w1.remove(arrayContainsType(w1,t));
                }
            }
            
            Damage ajustado= new Damage(w3, min1);
            return ajustado;
        }
        
        
    }
    
    
    DamageToUI getUIversion() {
        return new DamageToUI(this);
    }
    
    
    public String toString(){
        String mens2 = (weapons == null)?" ": weapons.toString();
        String message = "[Damage]: Number of Shields: " + getNShields()
                + ", Number of Weapons: " + getNWeapons()
                + ", Weapon Type: " + mens2;
    
        return message;
    }
    
}
