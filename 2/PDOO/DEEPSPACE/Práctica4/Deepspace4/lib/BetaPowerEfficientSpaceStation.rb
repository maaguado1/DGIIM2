#encoding:utf-8

require_relative 'PowerEfficientSpaceStation'
require_relative 'BetaPowerEfficientSpaceStationToUI'
require_relative 'Dice'
module Deepspace
class BetaPowerEfficientSpaceStation < PowerEfficientSpaceStation
  
  @@EXTRAEFFICIENCY = 1.2
  
  def initialize(station)
    super(station)
    
  end
  
  def fire()
    dice = Dice.new()
    
    if (dice.extraEfficiency)
      return super*@@EXTRAEFFICIENCY
    else
      return super
    end
  end
  
  
  
  def getUIversion
    return BetaPowerEfficientSpaceStationToUI.new(self)
  end
  
    
end


end