int suma(int* array, int len)
{
	int res = 0;
	for (int i = 0; i < len; ++i)
		res += array[i];
	return res;
}
