/*
 * Nucleo original
 */
#include "nucleo.h"

extern int R[];
extern int M;
extern int N;

inline void core()
{
    int i, j, x1, x2;

    for (j = 0; j < 40000; j++)
    {
        x1 = 0;
        x2 = 0;
        for (i = 0; i < 5000; i += 8)
        {
            x1 += 2 * s[i].a + j;
            x2 += 3 * s[i].b - j;
            x1 += 2 * s[i + 1].a + j;
            x2 += 3 * s[i + 1].b - j;
            x1 += 2 * s[i + 2].a + j;
            x2 += 3 * s[i + 2].b - j;
            x1 += 2 * s[i + 3].a + j;
            x2 += 3 * s[i + 3].b - j;
            x1 += 2 * s[i + 4].a + j;
            x2 += 3 * s[i + 4].b - j;
            x1 += 2 * s[i + 5].a + j;
            x2 += 3 * s[i + 5].b - j;
            x1 += 2 * s[i + 6].a + j;
            x2 += 3 * s[i + 6].b - j;
            x1 += 2 * s[i + 7].a + j;
            x2 += 3 * s[i + 7].b - j;
        }
        if (x1 < x2)
            R[j] = x1;
        else
            R[j] = x2;
    }
}
