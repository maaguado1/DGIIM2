

unsigned popcount00(unsigned elemento)
{
    return elemento;
}




unsigned popcount01 (unsigned elemento)
{
        unsigned suma=0;
        for (unsigned i=0; i < sizeof(elemento)*8; i ++){
            unsigned mascara =1 <<i;
            suma += (elemento & mascara)!=0;

        }
        return suma;
}


unsigned popcount02 (unsigned elemento)
{
        unsigned suma=0;
        unsigned mascara = 1;
        for (unsigned i=0; i < sizeof(elemento)*8; i ++){
            mascara <<= 1;
            suma += (elemento & mascara)!=0;

        }
        return suma;
}



unsigned popcount03 (unsigned elemento)
{
        unsigned suma=0;
        unsigned mascara = 1;
        while(elemento != 0 ){
            
            suma += (elemento & 1);
            elemento >>=1;

        }
        return suma;
}


unsigned popcount04 (unsigned elemento)
{
        asm("       push %ebp              \n"
            "       xorl %eax, %eax       \n"
            "       mov %esp, %ebp        \n"
            "       mov 8(%ebp), %edx      \n"
            ".L0:                           \n"
            "       test %edx, %edx       \n"
            "       jz .L1                  \n"
            "       mov %edx, %ecx        \n"
            "       shr %edx               \n"
            "       adc $0, %eax           \n"
            "       jmp .L0                 \n"
            ".L1:                         \n"
            "   pop %ebp                   \n"
            "ret");
}





unsigned popcount05 (unsigned elemento)
{
        unsigned suma=0;
       while(elemento!=0)
       {
           elemento >>=1;
           asm("adc $0, %0": "+r"(suma));

       }



        return suma;
}






