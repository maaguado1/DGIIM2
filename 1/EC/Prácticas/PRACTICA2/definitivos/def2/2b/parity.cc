//------------------------------------------------------------------------------
// parity.cc
//------------------------------------------------------------------------------

#include <x86intrin.h> // __rdtscp

#include <algorithm>   // generate
#include <array>       // array
#include <bitset>      // bitset
#include <chrono>      // now
#include <functional>  // bind
#include <iomanip>     // setw
#include <iostream>    // cout endl
#include <numeric>     // iota

//------------------------------------------------------------------------------

const unsigned GAP = 12;   // gap between columns
const unsigned REP = 100;  // how many times we repeat every algorithm

std::array<unsigned, 1 << 16> list; // 64K x 4B = 256KB

//------------------------------------------------------------------------------

unsigned parity00(unsigned elemento) // return true if parity of elem is even
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity01(unsigned elemento) // return true if parity of elem is even
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity02(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity03(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity04(unsigned elemento) // like 3 with an extra "test"
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity05(unsigned elemento) // like 3 without the extra "test"
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity06(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity07(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity08(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity09(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity10(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity11(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity12(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity13(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity14(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity15(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

template <class _F> void test (_F& __f, const char* msg)
{
	using namespace std::chrono;

	unsigned id; // needed by __rdtscp()

	unsigned long long cycle_overhead = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		unsigned long long t2 = __rdtscp(&id);
		cycle_overhead = std::min(cycle_overhead, std::max(t1, t2) - std::min(t1, t2));
	}

	unsigned long long cycles = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		__f(list[0]);
		unsigned long long t2 = __rdtscp(&id);
		cycles = std::min(cycles, std::max(t1, t2) - std::min(t1, t2));
	}
	cycles -= cycle_overhead;

	auto time_overhead = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		auto t1 = high_resolution_clock::now();
		auto t2 = high_resolution_clock::now();
		time_overhead = std::min(time_overhead, t2 - t1);
	}

	unsigned result;
	auto time = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		result = 0;
		auto t1 = high_resolution_clock::now();
		result = std::count_if(list.begin(), list.end(), __f);
		auto t2 = high_resolution_clock::now();
		time = std::min(time, t2 - t1);
	}
	time -= time_overhead;

	std::cout << '"' << std::setw(GAP * 2 - 2) << msg << '"' 
	          << std::setw(GAP) << result
	          << std::setw(GAP) << cycles
	          << std::setw(GAP) << std::fixed << std::setprecision(2)
	          << duration_cast<nanoseconds>(time).count() / static_cast<double>(list.size())
	          << std::setw(GAP) << duration_cast<nanoseconds>(time).count() / 1000.0
	          << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	std::iota(list.begin(), list.end(), 0);

	std::cout << "#" << std::setw(GAP * 2 - 1) << "parity"
	          << std::setw(GAP) << "value"
	          << std::setw(GAP) << "cycles"
	          << std::setw(GAP) << "time(ns)"
	          << std::setw(GAP) << "total(us)"
	          << std::endl;

	test(parity00, "parity00(empty)");
	test(parity01, "parity01(for)");
	test(parity02, "parity02(while)");
	test(parity03, "parity03(do while)");
	test(parity04, "parity04(asm)");
	test(parity05, "parity05(asm v2)");
	test(parity06, "parity06(tree)");
	test(parity07, "parity07(tree v2)");
	test(parity08, "parity08(asm v3)");
	test(parity09, "parity09(lt8)");
	test(parity10, "parity10(lt16)");
	test(parity11, "parity11(HD)");
	test(parity12, "parity12(builtin)");
	test(parity13, "parity13(bitset)");
	test(parity14, "parity14(recursive)");
	test(parity15, "parity15(lt16 v2)");
}

//------------------------------------------------------------------------------
