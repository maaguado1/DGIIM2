# suma.s: Sumar los elementos de una lista
#         llamando a función, pasando argumentos mediante registros
#         retorna: código retorno 0, comprobar suma en %eax mediante gdb/ddd

# SECCIÓN DE DATOS (.data, variables globales inicializadas)
.section .data

	.macro linea 
		.int 2,3,4,1
	.endm
lista: .irpc i, 12345678
		linea
	.endr


	
longlista:
	.int (.-lista)/4 # .= contador posiciones. Aritmética de etiquetas.
resultado:
	.quad -1
formato:
	.ascii "suma = %d / 0x%x\n"




# SECCIÓN DE CÓDIGO (.text, instrucciones máquina)
.section .text



main: .global main

	mov $lista, %ebx    # dirección del array lista
	mov longlista, %ecx # número de elementos a sumar
	call suma           # llamar suma(&lista, longlista);
	mov %eax,resultado  # salvar resultado


	push resultado      # apila resultado
#	push resultado      # apila resultado
	push $formato       # apila formato
	xor %eax, %eax      # number of arguments in registers
	call printf         # llamada a función printf(&formato, resultado)
	add $12, %esp       # dejar pila intacta
			    # void _exit(int status);
	mov $1, %eax 		# exit: servicio 1 kernel Linux
	mov $0, %ebx 		# status: código a retornar (0=OK)
	int $0x80    		# llamar _exit(0);







# SUBRUTINA: int suma(int* lista, int longlista);
#            entrada: 1) %ebx = dirección inicio array
#                     2) %ecx = número de elementos a sumar
#            salida:     %eax = resultado de la suma






suma:
	push %edx    # preservar %edx (se usa aquí como índice)
	mov $0, %eax # poner a 0 acumulador
	mov $0, %edx
	mov $0, %esi # poner a 0 índice

bucle:
	add (%ebx,%esi,4), %eax # acumular i-ésimo elemento
	adc $0, %edx		# suma con acarreo
	inc %esi                # incrementar índice
	cmp %esi,%ecx           # comparar con longitud
	jne bucle               # si no iguales, seguir acumulando
	pop %esi                # recuperar %edx antiguo
	ret

