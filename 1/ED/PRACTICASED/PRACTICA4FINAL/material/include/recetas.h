/**
 * @file recetas.h 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c recetas
 */



#include <string>
#include <assert.h>
#include <map>
#include "receta.h"



using namespace std;

#ifndef RECETAS_H
#define RECETAS_H

/**
 * @brief T.D.A. Recetas
 * Un elemento del TDA Recetas es un conjunto de elementos del tipo Receta, identificadas por su código.
 * Una instancia @c Recetas se organiza en un map de recetas identificadas cada una de manera única por su código.
 * Debe cumplir que el número de elementos de map datos sea > 0.
*/
class recetas{
    private:
        map<string,receta> datos;


    public:

        /**
         * @brief Constructor por defecto
         * Inicializa todo al vacío
        */
        recetas();


        /**
         * @brief Constructor de copia
         * @param const recetas& orig: conjunto de recetas a copiar en el objeto apuntado por this
        */
        recetas(const recetas& orig);

        /**
         * @brief Actualiza el valor nutricional de la
         * @param nueva: receta a insertar en el conjunto
        */
        void setValores(const ingredientes& valores);


        /**
         * @brief Inserta una nueva receta en el conjunto de rectas apuntado por this
         * @param nueva: receta a insertar en el conjunto
        */
        void insertar(const receta& nueva);
        

        /**
         * @brief Borra una receta del conjunto
         * @param codigo: código que identifica a la receta que se quiere eliminar
        */
        void borrar(const string& codigo);



        /**
         * @brief Calcula el total de las calorías del objeto apuntado por this
         * Suma las calorías de cada receta del conjunto
         * @return El total de calorías de todas las recetas
        */
        float getCalorias(); //Añadido para el nutricion_recetas



        
        /**
         * @brief Operator [] (tipo constante): permite acceder a un elemento del conjunto de recetas
         * @param c: código que identifica a la receta a la que se quiere acceder
         * @return La receta (tipo constante) representada por el código pasado como parámetro
        */
        const receta& operator[](const string& c)const;


        /**
         * @brief Operator []: permite acceder a un elemento del conjunto de recetas
         * @param c: código que identifica a la receta a la que se quiere acceder
         * @return La receta representada por el código pasado como parámetro
        */
        receta& operator[](const string& c);



        /**
         * @brief Operador de salida para Recetas
         * @param os: flujo de salida
         * @param i: objeto del tipo Recetas que se quiere imprimir
        */
        friend std::ostream & operator<<(std::ostream & os, const recetas & i);
        
        
        
        /**
         * @brief Operador de entrada para Recetas
         * @param is: flujo de entrada
         * @param i: objeto del tipo Recetas que se quiere introducir
        */
        
        
        
        friend std::istream & operator>>(std::istream & is, recetas & i);
        /**
         * @brief Número de recetas que hay en el objeto apuntado por this
         * @return Número de recetas que hay en el conjunto apunntado por this
        */
        
        
        
        inline int size() const {return datos.size();}


        /**
         * @brief Busca elementos con la misma clave 
         * @param clave clave que hay que buscar
         * @return número de apariciones de elementos con esa clave
         *
        */
       inline int count(const string& clave)const{return datos.count(clave);};
       
        /**
         * @brief Implementación y especificación del iterador para el TDA Recetas
         * Recorre un objeto del tipo recetas
        */
        class iterator{
            private:

                map<string,receta>::iterator it;

            public:
                iterator():it(0){};
                iterator(const recetas::iterator &i):it(i.it){};
                ~iterator(){};
                inline recetas::iterator& operator=(const recetas::iterator& orig){it=orig.it; return *this;};
                inline receta& operator*()const {return (*it).second;};
                inline recetas::iterator & operator++(){it++; return *this;};
                inline recetas::iterator & operator--(){it--; return *this;};
                inline bool operator==(const recetas::iterator &i)const {return it==i.it;};
                inline bool operator!=(const recetas::iterator &i)const {return it!=i.it;};
                friend class const_iterator;
                friend class recetas;


        };

        /**
         * @brief Implementación y especificación del iterador constante para el TDA recetas
         * Recorre un objeto del tipo recetas, teniendo en cuenta que sus elementos se referencian de manera constante
        */
        class const_iterator{

            private:
                map<string, receta>::const_iterator it;

            public:

                const_iterator():it(0){};
                const_iterator(const  recetas::const_iterator &i):it(i.it){};
                ~const_iterator(){};
                inline const_iterator & operator++(){it++; return *this;};
                inline const_iterator & operator--(){it--; return *this;};
                inline recetas::const_iterator& operator=(const recetas::const_iterator& orig){this->it=orig.it; return *this;};
                inline const receta& operator*()const {return (*it).second;};
                inline bool operator==(const recetas::const_iterator &i){return it==i.it;};
                inline bool operator!=(const recetas::const_iterator &i){return it!=i.it;};
                friend class recetas;


        };

        /**
         * @brief Iterador begin() pàra el TDA Recetas
         * @return Un iterador que apunta a la primera receta del conjunto
        */
        inline iterator begin(){recetas::iterator i; i.it=datos.begin(); return i;};
        /**
         * @brief Iteradir end() para el TDA Rcetas
         * @return Un iterador que apunta a la última receta del conjunto
        */
        inline iterator end(){recetas::iterator i; i.it=datos.end(); return i;}
        /**
         * @brief Iterador constante begin() para el TDA Recetas
         * @return Un iterador constante que apunta al primer elemento del conjunto
        */
        inline const_iterator begin()const {recetas::const_iterator i; i.it=datos.begin(); return i;}
        /**
         * @brief Iterador constante end() para el TDA Recetas
         * @return Un iterador constante que apunta al último elemento del conjunto
        */
        inline const_iterator end()const {recetas::const_iterator i; i.it=datos.end(); return i;}


        
       
};


#endif