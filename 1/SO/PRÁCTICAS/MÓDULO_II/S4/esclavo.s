#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>


int esPrimo(int num){
	int numero=num;
	bool divide=false, esprimo= true;
	while (numero%1 ==0 && esprimo){
		for (int i = 0; i < numero/2 &&  esprimo; i ++)
			if (numero%i ==0){
				esprimo=false;
				numero = numero/i;


			}
	}
	return esprimo;

}


int main (int argc, char *argv[]){
	int inicio, fin, i;
	inicio = atoi(argv[1]);
	fin = atoi(argv[2]);

	
	for (i= inicio; i < fin ; i ++)
		if (esPrimo(i))
			write (STDOUT_FILENO, &i, sizeof(int));
	return 0;

}












