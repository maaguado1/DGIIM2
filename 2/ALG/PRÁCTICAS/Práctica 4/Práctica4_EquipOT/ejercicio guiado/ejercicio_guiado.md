#Ejercicio guiado: Subsecuencia de caracteres más larga.

Este ejercicio se proporciona ya resuelto, pero vamos a estudiar la metodología de diseño que se ha seguido en su creación. Además, compararemos las dos formas que se han utilizado para implementar dicho algoritmo: Fuerza Bruta(usando recursividad) y Programación Dinámica.

Este problema consiste en lo siguiente:
Dadas dos secuencias de caracteres X=(x<sub>1</sub>, x<sub>2</sub>,..., x<sub>m</sub>) e Y=(y<sub>1</sub>, y<sub>2</sub>,...,y<sub>n</sub>), de longitudes m y n respectivamente, encontrar la máxima subsecuencia de caracteres común que aparecen en ambas cadenas de izquierda a derecha(no necesariamente de forma contigua. Por ejemplo, para las cadenas *S= ABAZDC* y *T= BACBAD*, la máxima subsecuencia común tiene longitud 4 y es *ABAD*.

**Implementación mediante Fuerza bruta(recursividad)**

*Idea:

Comenzamos comparando las cadenas en orden inverso, caracter por caracter. Entonces, tenemos dos casos posibles:

1)Si los caracteres son iguales:
Se añade un "1" al resultado y se eliminan los últimos caracteres de ambos strings y se realiza una llamada recursiva a dicho método, con las cadenas modificadas.
2)Si los caracteres son diferentes:
Se devuelve el máximo entre la llamada recursiva a dicho método con parámetros la *cadena 1* (eliminando su último caracter ) y la *cadena 2*, y la llamada recursiva a dicho método con parámetros la *cadena 1* la *cadena 2* (eliminando su último caracter).

*Código:
~~~
public class LongestCommonSubsequence {

    public static int LCS(String A, String B) {
        if (A.length() == 0 || B.length() == 0) {
          return 0;
        }
        int lenA = A.length();
        int lenB = B.length();
        
        if (A.charAt(lenA - 1) == B.charAt(lenB - 1)) {
          return 1 + LCS(A.substring(0, lenA - 1), B.substring(0, lenB - 1));
        } 
        else {
          return Math.max(
              LCS(A.substring(0, lenA - 1), B.substring(0, lenB)),
              LCS(A.substring(0, lenA), B.substring(0, lenB - 1)));
        }
      }
      
      public static void main(String[] args) {
        String A = "ACBDEA";
        String B = "ABCDA";
        System.out.println("LCS :" + LCS(A, B));
      }
      
}
~~~
Para dichos valores de las cadenas, la salida obtenida es 4, es decir, la subsecuencia de caracteres común a ambos tiene longitud 4.

Sin embargo, esta implementación tiene un inconveniente y es que es muy poco eficiente, ya que en una cadena de longitud n, puede haber 2 subsecuencias, por lo que si usamos Divide y Vencerás, la eficiencia va a terminar siendo O(2<sup>n</sup>), pues los subproblemas se van a repetir.

**Implementación mediante Programación dinámica**

*Idea:

Utilizar la técnica *Bottom-Up* que es un enfoque de programación dinámica consistente en resolver el problema para entradas lo más pequeñas posibles y almacenar dichos resultados para el futuro. Así, cuando se este calculando el problema para datos de entrada grandes, usaremos las soluciones almacenadas(la solución para problemas con entradas pequeñas).
En este caso, se almacenan las soluciones de los problemas secundarios en una matriz de soluciones y dicha matriz la usaremos cuando sea necesario(*Memoization*). De esta manera, esta matriz nos ahorra el cálculo de operaciones repetidas.

*Código:
~~~
public class LongestCommonSubsequence {

	public static int find(char[] A, char[] B) {
		int[][] LCS = new int[A.length + 1][B.length + 1];
		String[][] solution = new String[A.length + 1][B.length + 1];

		for (int i = 0; i <= B.length; i++) {
			LCS[0][i] = 0;
			solution[0][i] = "0";
		}

		for (int i = 0; i <= A.length; i++) {
			LCS[i][0] = 0;
			solution[i][0] = "0";
		}

		for (int i = 1; i <= A.length; i++) {
			for (int j = 1; j <= B.length; j++) {
				if (A[i - 1] == B[j - 1]) {
					LCS[i][j] = LCS[i - 1][j - 1] + 1;
					solution[i][j] = "diagonal";
				} else {
					LCS[i][j] = Math.max(LCS[i - 1][j], LCS[i][j - 1]);
					if (LCS[i][j] == LCS[i - 1][j]) {
						solution[i][j] = "top";
					} else {
						solution[i][j] = "left";
					}
				}
			}
		}
		
		String x = solution[A.length][B.length];
		String answer = "";
		int a = A.length;
		int b = B.length;
		while (x != "0") {
			if (solution[a][b] == "diagonal") {
				answer = A[a - 1] + answer;
				a--;
				b--;
			} else if (solution[a][b] == "left") {
				b--;
			} else if (solution[a][b] == "top") {
				a--;
			}
			x = solution[a][b];
		}
		System.out.println(answer);
		
		for (int i = 0; i <= A.length; i++) {
			for (int j = 0; j <= B.length; j++) {
				System.out.print(" " + LCS[i][j]);
			}
			System.out.println();
		}
		return LCS[A.length][B.length];
	}

	public static void main(String[] args) {
		String A = "CASA";
		String B = "COSA";
		System.out.println("LCS :" + find(A.toCharArray(), B.toCharArray()));
	}
}
~~~

*Metodología de diseño seguida:

**1) Naturaleza N-etápica.**

Es obvio que el problema se puede dividir en *n* etapas diferentes, en las que se tomará una decisión independientemente de la decisión tomada en la etapa anterior: en la etapa *n* debemos elegir qué cadena de caracteres dejamos fija y qué puntero movemos hacia la derecha.
Por otro lado, una solución optimal de decisiones será aquella que maximice la función objetivo, es decir, aquella que dé como resultado la subsecuencia de caracteres más larga.

**2)Cumplimiento del POB(Principio del Óptimo de Bellman).**

Es necesario que se cumpla el principio de optimalidad de Bellman para que haya optimalidad en programación dinámica.

Tenemos que escribir el valor del problema en la etapa *n*, en función de los valores obtenidos en la etapa 1 y las etapas anteriores a la etapa actual(etapa *n*). Así, obtenemos subproblemas más simples, pues no tendremos que volver a calcular todas las soluciones, sino que usaremos las anteriormente calculadas, ya que tenemos asegurado que utilizamos sólo las óptimas.
En este problema en concreto, la función que hay que maximizar es el número de caracteres comunes entre las dos cadenas introducidas.

Veamos que dicho principio se cumple relacionando los problemas de mayor tamaño, con los problemas de menor tamaño:

Consideramos:

$$
Z= {z_{1},...,z_{k}} 
$$

cualquier subsecuencia común mas larga de X e Y, siendo

$$
X= {x_{1},...,x_{n}}\\
Y= {y_{1},...,y_{m}}
$$

Además, definimos:
$$
X_{i}
$$

a las *i* primera letras de de *X*

Veamos dos casos:
1)
$$
x_{n} = y_{m}
$$

Si consideramos otra subsecuencia *Z'* que no termine en *x<sub>n</sub>*=*y<sub>m</sub>, esta puede ser más larga añadiendo ese elemento al final. En caso de que *Z* no contuviese a estos elementos , llegaríamos a una contradicción, pues *Z* no sería la subsecuencia común más larga.Por lo tanto, dicho esto, podemos concluir que:

-La LCS *Z* debe terminar en x<sub>n</sub>=y<sub>m</sub>
-*Z<sub>k-1</sub>* es una subsecuencia común de X<sub>n-1</sub> e Y<sub>m-1</sub>.
-Luego *Z<sub>k-1</sub>* es la subsecuencia común más larga de X<sub>n-1</sub> e Y<sub>m-1</sub>, pues, en otro caso, *Z* no sería la subsecuencia común más larga de *X* e *Y*. Aquí vemos que la solución a problemas grandes tiene en cuenta la optimalidad de las soluciones a problemas menores (la solución óptima *Z* depende de que *Z<sub>k-1</sub>* también sea óptima).


2)
$$
x_{n} \neq  y_{m} \  \ y \ \ x_{n} \neq z_{k}
$$

Como en este caso *Z* no termina en x<sub>n</sub> es claro que:
-*Z* es la subsecuencia común más larga de X<sub>m-1</sub> Y *Y*( *Z* sería la subsecuencia común más larga de los anteriores términos de *X* y todo los términos de *Y*).
-No hay una subsecuencia común más larga de X<sub>m-1</sub> y *Y*, pues en caso contrario *Z* no sería LCS de *X* e *Y*. Luego, nos encontramos de nuevo que la subsecuencia común *Z* calculada entre *X* y *Y* depende de la *Z* calculada entre *X<sub>n-1</sub>* y *Y*, o lo que es lo mismo, la optimalidad de problemas grandes depende de la optimalidad de problemas más pequeños.

3)
$$
x_{n} \neq  y_{m} \  \ y \ \ y_{m} \neq z_{k}
$$
En este caso ocurre lo mismo que en 2), salvo que ahora *Z* pasa a ser la subcadena más larga entre *X*(todos los elementos de *X*) y *Y<sub>m-1</sub> (los elementos anteriores de *Y*).

También lo podemos enfocar de otra forma:

Para el caso base:
$$
f_{1}(i,j) = máx\left\lbrace\ \delta_{ij} \right\rbrace
$$
dónde  $$\delta_{ij}$$ es la función que dadas dos posiciones de cada una de las cadenas devuelve *1* si ambos caracteres son iguales, o *0* si no lo son.

Para la etapa n tendremos:
$$
f_{n}(i,j) = \delta_{ij}+ max\left\lbrace\ f(i-1,j),f(i,j-1) \right\rbrace
$$
, ya que en cada etapa lo que se hace es mirar si los dos últimos elementos de cada cadena coinciden. Si son iguales, se devolvería un *1*, sino cabría comprobar si la subcadena común que se obtiene al eliminar el último elemento de la *primera cadena* y comparándola con todos los elementos de la *segunda cadena* es de mayor longitud que en el caso inverso. Luego, es obvio que la solución proporcionada en la etapa *n* es óptima, pues como se puede ver estamos calculando el máximo de dos funciones que son optimales.
**3)Planteamiento de recurrencia.**

Supongamos que ** (*cadena 1*) tiene longitud *i* y *Y* (*cadena 2*) tiene longitud *j*. La recurrencia que se ha utilizado para llevar a cabo el problema es la siguiente:

$$
LCS[i][j]= \left\{ \begin{array}{lcc}
             0 &   si  & i=0 & y/ó & j=0  \\
             \\ LCS[i-1][j-1]+1 &  si & i,j>0 & y & x_{i}=y_{j} \\
             \\ Max(LCS[i-1][j],LCS[i][j-1]) &  si  & i,j>0 & y & x_{i} \neq y_{j}
             \end{array}
   \right.
$$
Siendo *LCS*= longitud de la subsecuencia común más larga de *X* e *Y* 

Dicha recurrencia se basa en lo siguiente:

-> Si alguna de las cadenas o ambas son vacías, es lógico que la longitud de la subsecuencia común a ambas es 0, pues no tendrán ningún elemento en común.

->Si el último caracter de *X* coincide con el último caracter de *Y*, se aumenta en 1 la longitud de la subsecuencia común y se eliminan esas posiciones de ambas cadenas, de ahí que se reduzca *i* y *j* en 1.

->Por último, si el último caracter de *X* no coincide con el último caracter de *X*, se escoge la subsecuencia común con mayor longitud (la que maximice la longitud de la subsecuencia común) decidiendo entre los elementos anteriores de *X* y todos los elementos de *Y*, o todos los elementos de *X* y los anteriores elementos de *Y*.

Cabe destacar que al mismo tiempo que se va creando la matriz LCS, se va construyendo otra matriz(la llamaremos *solucion*) que nos indicará el recorrido que tenemos que seguir para ir obteniendo la subcadena común. Esta matriz se construye de la siguiente manera





- Si $x_i=y_j$ , entonces 
$$solucion[i][j] = "diagonal"$$

- Si $x_{i} \neq y_{j}$ , entonces

    - Si 
	  $$Max(LCS[i-1][j],LCS[i][j-1]) == LCS[i-1][j]$$, entonces
	
	  $$solucion[i][j] = "top"$$
	
    - Si
	  $$Max(LCS[i-1][j],LCS[i][j-1]) == LCS[i][j-1]$$, entonces
	
	  $$solucion[i][j] = "left"$$



-> si: x<sub>i</sub> = y<sub>j</sub> , entonces 
$$
solucion[i][j] = "diagonal"\\
$$

-> si: x<sub>i</sub> != y<sub>j</sub> , entonces

-> si 
	$$
	Max(LCS[i-1][j],LCS[i][j-1]) == LCS[i-1][j] \\
	solucion[i][j] = "top"
	$$
	
-> si
	$$
	Max(LCS[i-1][j],LCS[i][j-1]) == LCS[i][j-1] \\
	solucion[i][j] = "left"
	$$

Cada vez que nos encontremos con "*diagonal*" nos indicará que el caracter que se encuentra en dicha fila o columna se añade a la solución final pues coincide en ambas cadenas.Seguidamente, nos desplazaremos a la posición que se encuentra una fila más arriba y una columna más a la izquierda, lo que simula la eliminación de dicho caracter en ambas cadenas.
Cuando nos encontremos con "*left*", nos desplazaremos en la fila que nos encontremos, hacia la izquierda.
Por último, cuando aparezca "*top*", nos desplazaremos en la misma columna en la que nos encontramos, una fila más arriba.

**Ejemplo concreto**
Supongamos que la cadena X="CASA" y la cadena Y="COSA".

Es evidente que la solución  es :
LCS:3
Subcadena común más larga: "CSA"

Dicho algoritmo, en este caso, genera la siguiente matriz LSC:
$$
\begin{equation}
	\begin{matrix}
	   & & C & O & S & A\\
   	& 0 & 0 & 0 & 0 & 0\\
	C & 0 & 1 & 1 & 1 & 1\\
	A & 0 & 1 & 1 & 1 & 2\\
	S & 0 & 1 & 1 & 2 & 2\\
	A & 0 & 1 & 1 & 2 & 3\\
	\end{matrix}
\end{equation}
$$

Además, a partir de dicha matriz, también se va creando al mismo tiempo otra matriz(la llamaremos *solucion*)que nos ayudará a conocer qué caracteres formarán la subcadena común más larga, y es la siguiente:

$$
\begin{equation}
	\begin{matrix}
	 & & C & O & S & A\\
   	 & 0 & 0 & 0 & 0 & 0\\
	 C & 0 & diagonal & left & left & left\\
	 A & 0 & top & top & top & diagonal\\
	 S & 0 & top & top & diagonal & top\\
	 A & 0 & top & top & top & diagonal\\
	\end{matrix}
\end{equation}
$$


El recorrido que se sigue en la matriz anterior es el siguiente:

1)Comenzamos en la posición que se encuentra más abajo y más a la derecha(solucion[n-1][m-1]) y avanzaremos hasta que nos encontremos el caracter "*0*". En este caso, la posición de la que partimos(*solucion[4][4]*) contiene la palabra "diagonal", luego nos fijamos que hace referencia a la letra "A", luego la incluimos en la solución:
$$
answer= \ "A"  +  \  answer 
$$
 Seguidamente, nos desplazamos a la posición que se encuentra una fila más arriba y una columna más a la derecha.(*solucion[3][3]*).

 2)Una vez nos encontramos en la nueva posición, nos percatamos que contiene la palabra "diagonal", luego repetimos el proceso anterior:
$$
 answer= \ "S" + \ "A"
$$
 Nueva posición: *solucion[2][2]*.

 3)Ya en la nueva posición, vemos que contiene la palabra "top".Luego, por las indicaciones dadas anteriormente, avanzamos una fila más arriba, en concreto a la posición *solucion[1][2]*.

 4)En la nueva posición, nos encontramos con la palabra "left", luego avanzamos en la misma fila, una columna más a la izquierda(*solucion[1][1]*).

 5)Finalmente, en dicha posición encontramos "diagonal", por lo tanto añadimos la letra asociada a dicha fila/columna:

$$
 answer= \ "C" + \ "SA"
$$

 Luego, llegamos a la conclusión de que la subsecuencia común más larga ambas cadenas introducidas es "CSA".

**Comparación Fuerza Bruta y Programación Dinámica**

Comparamos ambos algoritmos para las mismas cadenas con longitudes desde 0 hasta 15 y veamos como se comporta cada uno de los algoritmos:

![](/home/joseja/Escritorio/ALGORITMICA_PROGRAMACION_DINAMICA/grafica_comparacion.png) 

Como podemos observar en la gráfica, el algoritmo de fuerza bruta a partir de un cierto tamaño su tiempo crece muy rápido, pues como hemos dicho antes su eficiencia es O(2<sup>n</sup>), mientras que el tiempo del algoritmo con programación dinámica aumenta mucho más lento, pues su eficiencia es O(n*m), con n y m la dimensión de las respectivas cadenas. Esto demuestra que la utilización de programación dinámica en este problema hace que se obtengan datos más eficientes, pues como sabemos, la programación dinámica siempre nos dará la solución más óptima, obviamente cuando dicho problema cumpla los requisitos básicos para su utilización. 


