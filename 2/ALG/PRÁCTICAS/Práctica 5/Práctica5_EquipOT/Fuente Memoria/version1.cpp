#include <algorithm>
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto
{
  float x;
  float y;
};

struct Arista
{
  int inicio;
  int final;
};

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: F U N C I O N E S    C O M U N E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Generacion de datos para el problema
//==============================================================================

double random_double(double min, double max)
{

  return (rand() / (double)RAND_MAX) * (max - min) + min;
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Genera los puntos necesarios para el problema
 * @param num_points, la cantidad de puntos con la que queremos trabajar
 * @return un vector de puntos, cuyas coordenadas estan en el intevalo [0, num_points]
 * */
vector<vector<int>> generarProblema(int n)
{
  srand(time(NULL));
  vector<vector<int>> Matriz;
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;

    for (int j = 0; j < n; j++)
    {
      double x = random_double(1, 100);
      fila.push_back(x);
    }
    Matriz.push_back(fila);
  }

  SalidaMatriz(Matriz);
  return Matriz;
}
/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[i][solucion[i]];
  }
  return valor;
}



/**
 * @brief Función que calcula la siguiente configuración dentro del árbol de posibles soluciones
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * @param costeactual coste de la solución actual, que hay que modificar
 * @param T matriz de costes
 * @param final valor que indica si la solución actual es una posible solución. De serlo, al generar la siguiente
 * configuración dejará de serlo.
 * 
 * Esta función tiene en cuenta si se ha llegado al extremo de un nivel, de ser así, no modifica el vector.
 * 
 * 
 * */

void ConfiguracionSiguiente(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T, bool &final)
{
  int nuevovalor;
  
  if (solucion[nivel] < solucion.size())
  {
    final=false;
    solucion[nivel] = solucion[nivel] + 1;
    if (solucion[nivel] == 1)
    {
      nuevovalor = costeactual + T[nivel][solucion[nivel]-1];
    }
    else
    {
      nuevovalor = costeactual + T[nivel][solucion[nivel]-1] - T[nivel][solucion[nivel] - 2];
    }

    costeactual = nuevovalor;
  }
}




/**
 * @brief Función que verifica si la solución actual es factible
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a verificar
 * @return bool true si es factible, false si no lo es 
 * 
 * */
bool Factible(vector<int> solucion, int nivel)
{
  bool factibilidad = true;
  if (solucion[nivel] <= solucion.size()){
  for (int i = 0; i < nivel && factibilidad; i++)
  {
    if (solucion[nivel] == solucion[i])
      factibilidad = false;
  }
  }
  else
  {
      factibilidad=false;
  }
  
  return factibilidad;
}




/**
 * @brief Función que comprueba si estamos ante una posible solución del problema.
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a valorar
 * 
 * Esta función tiene en cuenta si la solución es factible y si el tamaño de la solución a valorar coincide con el tamaño esperado.
 * */


bool Final(vector<int> solucion, int nivel)
{
  return (nivel == solucion.size() - 1 && Factible(solucion, nivel));
}



/**
 * @brief Función que comprueba si el nodo actual tiene hermanos dentro del árbol de soluciones.
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * 
 * 
 * */
bool HayHermanos(vector<int> solucion, int nivel)
{
  return solucion[nivel] < solucion.size();
}


/**
 * @brief Función que calcula la configuración anterior dentro del árbol de posibles soluciones
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * @param costeactual coste de la solución actual, que hay que modificar
 * @param T matriz de costes 
 * 
 * */
void ConfiguracionAnterior(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T)
{
  costeactual = costeactual - T[nivel][solucion[nivel]-1];
  solucion[nivel] = 0;
}



/**
 * @brief Función que calcula la solución del problema mediante backtracking
 * @param T matriz de costes
 * 
 * Esta función parte de una solución nula (0,0,0,...,0) y va recorriendo el árbol formado por las posibles combinaciones. Cuando
 * llega a una solución factible, y está en uno de los nodos finales, lo comprueba con la solución que aportaba el menor valor hasta la fecha.
 * Además, cuando llega al final de un nivel, es decir que no hay más hermanos, sube al nivel anterior para todos los hijos del siguiente padre.
 * @return vector<int> vector con la asignación minimal.
 * */
vector<int> Backtracking(vector<vector<int>> T)
{

  int N = T.size();
  int nivel = 0;
  bool esfinal=false;
  //Solución inicial
  vector<int> solucion, final;
  for (int i = 0; i < N; i++)
    solucion.push_back(0);

  int costeactual = 0;
  int coste = INT_MAX;
  do{
    ConfiguracionSiguiente(nivel, solucion, costeactual, T, esfinal);

  if (Final(solucion, nivel) && !esfinal){
    esfinal=true;
    if (costeactual < coste)
  {
    coste = costeactual;
    final = solucion;

  }}
  else if (Factible(solucion, nivel) && !esfinal)
  {
    nivel++;
  }
  else
  {
    esfinal = false;
    while (!HayHermanos(solucion, nivel) && (nivel >= 0))
    {
      ConfiguracionAnterior(nivel, solucion, costeactual, T);
      nivel--;
    }
  }
}while(nivel >=0);
  final.push_back(coste);
  return final;
}

//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    printf("Error, modo de ejecución; ./cercania <caracter> <tamaño>");
    exit(-1);
  }

  clock_t t_antes;   // Valor del reloj antes de la ejecución
  clock_t t_despues; // Valor del reloj después de la ejecución

  string algoritmo;
  int tamanio;
  vector<vector<int>> matriz;
  vector<int> solucion3;
  double t3;

  tamanio = atoi(argv[1]);
  //Generamos los problemas
  matriz = generarProblema(tamanio);

  t_antes = clock();
  solucion3 = Backtracking(matriz);

  t_despues = clock();
  int coste = solucion3[solucion3.size()-1];
  solucion3.pop_back();
  salidavec(solucion3);

  t3 = ((double)(t_despues - t_antes)) / CLOCKS_PER_SEC;

  //Mostramos dicho tiempo
  cout << tamanio << " " << t3 << "EL COSTE ES: " << coste << endl;
}
