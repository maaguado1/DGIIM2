
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;



//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: F U N C I O N E S    C O M U N E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Generacion de datos para el problema
//==============================================================================

double random_double(double min, double max)
{

  return (rand() / (double)RAND_MAX) * (max - min) + min;
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Genera los puntos necesarios para el problema
 * @param num_points, la cantidad de puntos con la que queremos trabajar
 * @return un vector de puntos, cuyas coordenadas estan en el intevalo [0, num_points]
 * */
vector<vector<int>> generarProblema(int n)
{
  srand(time(NULL));
  vector<vector<int>> Matriz;
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;

    for (int j = 0; j < n; j++)
    {
      double x = random_double(1, 100);
      fila.push_back(x);
    }
    Matriz.push_back(fila);
  }

  SalidaMatriz(Matriz);
  return Matriz;
}
/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[i][solucion[i]];
  }
  return valor;
}





 /**
  * @brief Calcula la asignacion optima mediante backtracking.
  * @param v, vector que indica si cada nodo está visitado o no
  * @param T, la matriz de distancias de los puntos con los que trabajamos
  * @param posicion, el número de centro de distribucion
  * @param n , número de nodos
  * @param nivel, el nivel del árbol de soluciones en el que estamos
  * @param costemin, el coste mínimo obtenido hasta la fecha
  * @param solucionparcial, la solucion que estamos construyendo, es decir las asignaciones a 
  * los puntos de venta.
  * @param solucion, la mejor solucion que hemos obtenido hasta la fecha
  * @pre T.size() > 0
  * */
void recursivo(vector<bool> &v , int posicion, vector<vector<int>> T, int n, int nivel, int costeactual, int &costemin, vector<int> solucionactual, vector<int> &solucion){
  if (nivel-1 == n)
  {
    int nuevo = costeactual + T[posicion-1][solucionactual[posicion]-1];
    if (nuevo < costemin)
    {
      costemin = nuevo;
      solucion = solucionactual;
    }
    return;
  }

  for (int i = 0; i <= n; i++)
  {
    if (!v[i])
    {

      v[i] = true;
      solucionactual.push_back(i);
      if (posicion ==0)
        recursivo(v, posicion+1, T, n, nivel + 1,  costeactual, costemin, solucionactual, solucion);
      else
          recursivo(v, posicion+1, T, n, nivel + 1,  costeactual + T[posicion-1][solucionactual[posicion]-1], costemin, solucionactual, solucion);
      // Mark ith node as unvisited
      solucionactual.pop_back();
      v[i] = false;
    }
  }
}

//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    cout << "Error, modo de ejecución; ./cercania <caracter> <tamaño>" << endl;
    exit(-1);
  }

  clock_t t_antes;   // Valor del reloj antes de la ejecución
  clock_t t_despues; // Valor del reloj después de la ejecución

  string algoritmo;
  int tamanio, minimo=INT_MAX;;
  vector<vector<int>> matriz;
  vector<int> solucion, solucion2;
  vector<bool> visitas;
  
  double t3;

  tamanio = atoi(argv[1]);
  for (int i =0; i <= tamanio; i ++)
    visitas.push_back(false);
  //Generamos los problemas
  matriz = generarProblema(tamanio);



  visitas[0] = true;
  solucion2.push_back(0);
  vector<int> solparcial = solucion2;
  t_antes = clock();
  recursivo(visitas, 0, matriz, tamanio, 1, 0, minimo, solparcial, solucion2);
  t_despues = clock();
  
  cout << "\nLa asignación con el segundo algoritmo es... (valor " << minimo << ")"<< endl;
  salidavec(solucion2);
  double t = ((double)(t_despues - t_antes)) / CLOCKS_PER_SEC;
  cout << "\nHa tardado ... " <<  t << endl; 


}
