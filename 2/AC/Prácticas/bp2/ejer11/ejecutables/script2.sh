#!/bin/bash

printf "\n\nEJECUCION 6000"

for ((N=1; N<=12; N++))
do
	set $OMP_NUM_THREADS= $N
	printf "\n $N"
	./partea 6000

done

printf "\n\nEJECUCION 25000"

for ((N=1; N<=12; N++))
do
	set $OMP_NUM_THREADS= $N
	printf "\n $N"
	./partea 25000

done
