public class LongestCommonSubsequence {

    public static int LCS(String A, String B) {
        if (A.length() == 0 || B.length() == 0) {
          return 0;
        }
        int lenA = A.length();
        int lenB = B.length();
        if (A.charAt(lenA - 1) == B.charAt(lenB - 1)) {
          return 1 + LCS(A.substring(0, lenA - 1), B.substring(0, lenB - 1));
        } else {
          return Math.max(
              LCS(A.substring(0, lenA - 1), B.substring(0, lenB)),
              LCS(A.substring(0, lenA), B.substring(0, lenB - 1)));
        }
      }
      public static void main(String[] args) {
        String A = "ACBDEA";
        String B = "ABCDA";
        System.out.println("LCS :" + LCS(A, B));
      }
}
