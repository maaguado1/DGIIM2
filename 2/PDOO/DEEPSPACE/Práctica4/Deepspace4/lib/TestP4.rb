#encoding:utf-8

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

require_relative "SpecificDamage"
require_relative "Damage"
require_relative "Hangar"
require_relative "Weapon"
require_relative "GameUniverse"
require_relative "EnemyStarShip"
require_relative "SpaceStation"
require_relative "SpaceCity"

require_relative "NumericDamage"


module Deepspace
  class TestP4
    def self.main     
      puts "Testing NumericDamage"
        puts "================================================================================"
        
        test_numeric = Deepspace::NumericDamage.new(3, 4)
        puts "The created numeric damage is #{test_numeric}"
        puts ""

        puts "Creating a copy of the damage:"
        test_numeric_copy = Deepspace::NumericDamage.newCopy(test_numeric)
        puts "The copy is: #{test_numeric_copy}"
        puts ""

        puts "The UI version of the damage is: #{test_numeric.getUIversion}"
        puts ""

        puts "Our damage hasNoEffect: #{test_numeric.hasNoEffect}"
        puts "Empty damagae hasNoEffect: #{Deepspace::NumericDamage.new(0, 0).hasNoEffect}"
        puts ""

        puts "Testing adjust"
        weapons = []
        weapons << 2
        weapons << 2
        shields = []
        shields << 2
        puts "The adjusted damage is #{test_numeric.adjust(shields, weapons)}"
        puts ""

        puts "Discarding shields"
        5.times do |i|
            test_numeric.discardShieldBooster
            puts "\tState: #{test_numeric}"
        end

        puts "Discarding weapons"
        5.times do |i|
            test_numeric.discardWeapon("nothing")
            puts "\tState: #{test_numeric}"
        end  
        
        
      suppliesPackages=[]
      suppliesPackages.push(SuppliesPackage.new(1,30,2))
      suppliesPackages.push(SuppliesPackage.new(3,0,1))
      suppliesPackages.push(SuppliesPackage.new(1,0,3))
     
      puts "\nCreamos un Universo"
      gameUniverse=GameUniverse.new
      nombres=["Juan", "Antonio"]
      gameUniverse.init(nombres)
      puts gameUniverse.to_s
      puts "\nCreamos Ciudad"
      gameUniverse.createSpaceCity
      puts gameUniverse.to_s
     
      
      
      transformEfficientLoot0=Loot.new(1,1,1,0,1,true,false)
      if transformEfficientLoot0.efficient
        puts "hola"
      end
      base=SpaceStation.new("BASE",suppliesPackages[2])
      
      if base.setLoot(transformEfficientLoot0)==Transformation::GETEFFICIENT
        puts "TodoBien"
        base=BetaPowerEfficientSpaceStation.new(base)
      end
      puts base.to_s

      puts "\nCombatimos enemigo"
      enemy=EnemyStarShip.new("Malo",1,2,transformEfficientLoot0, NumericDamage.new(1,1))
      station=SpaceStation.new("Juan", SuppliesPackage.new(500,50,500))
      
      puts gameUniverse.combatGo(station, enemy)
      puts gameUniverse.to_s
      
      
    end
    
    
    
  end
  
TestP4.main  
 
end
