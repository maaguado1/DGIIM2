#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ftw.h>




int main (int argc, char* argv[]){
	int pid=fork();
	int numero=atoi(argv[1]);
	if (pid!=0){
		
		if ((numero%2)!=0)
			printf("\n%d no es par", numero);
		else
			printf("\n%d es par", numero);

	}
	else{
		
		if ((numero%4)!=0)
			printf("\n%d no es divisible por 4", numero);
		else
			printf("\n%d es divisiblepor 4", numero);



	}
	
	

}
