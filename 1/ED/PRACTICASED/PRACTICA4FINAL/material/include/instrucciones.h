/**
 * @file instrucciones.h 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c instrucciones
 */

 #include <iostream>
 #include <string>
 #include "arbolbinario.h"
 #include "acciones.h"
 #include "ingredientes.h"

 #include <map>

 using namespace std;


#ifndef INSTRUCCIONES_H
#define INSTRUCCIONES_H

/**
 * @brief T.D.A. Instrucciones
 * Una instancia @c instrucciones se organiza en un Arbol Binario, que contiene los pasos que se deben
 * dar para cocinar una receta. Los nodos hoja son la base de la receta y el resto serán acciones del conjunto permitido.
*/
 class instrucciones{
     private:
        ArbolBinario<string> datos;   //almacena los pasos que deben darse para llevar a cabo una receta
        acciones acc;                 //conjunto de acciones permitidas en cualquiera de las recetas
        ingredientes ings;            //ingredientes necesarios para llevar a cabo dichos pasos

    public:
    /**
         * @brief Constructor por defecto
         * Inicializa todo al vacío
        */
        instrucciones();
        /**
         * @brief Constructor de copia
         * @param const instrucciones& orig: conjunto de instrucciones a copiar en el objeto apuntado por this
        */
        instrucciones(const instrucciones& orig);

        /**
         * @brief Constructor de copia de acciones
         * @param const instrucciones& orig: conjunto de instrucciones a copiar en el objeto apuntado por this
        */
        instrucciones(const acciones& acci);

        /**
         * @brief Operador de asignacion
         * @param const instrucciones& orig: conjunto de instrucciones a copiar en el objeto apuntado por this
        */  
        instrucciones& operator=(const instrucciones& orig);

        /**
         * @brief Operador de suma
         * @param const instrucciones& orig: conjunto de instrucciones para fusionar 
        */  
        instrucciones operator+(const instrucciones& otro)const;


        /**
         * @brief Consulta de los ingredientes
         * @return ingredientes válidos para esta receta
        */
        inline ingredientes getIngs() const {return ings;};

        /**
         * @brief Completa la clase con los ingredientes válidos
         * @param ing ingredientes válidos para esta receta
        */
        void setIngs(const ingredientes& ing);
       
        /**
         * @brief Operador de consulta
         * @returnconst accioness& conjunto de acciones apuntado por *this.
        */
        inline acciones getAcciones()const {return acc;};
        
        /**
         * @brief Operador de salida para instrucciones
         * @param os: flujo de salida
         * @param nombres: objeto del tipo instrucciones que se quiere imprimir
        */
        friend std::ostream & operator<<(std::ostream & os, const instrucciones & nombres);
        /**
         * @brief Operador de entrada para instrucciones
         * @param is: flujo de entrada
         * @param nombres: objeto del tipo instrucciones que se quiere introducir
        */
        friend std::istream & operator>>(std::istream & is, instrucciones & nombres);
        /**
         * @brief Número de pasos que hay en el objeto apuntado por this
         * @return Número de pasos que hay en el conjunto apunntado por this
        */
        inline int size () const{return datos.size();};

        

 };

 #endif