### Algoritmo TSP: idea

La idea consiste en que la solución sea un conjunto de aristas tales que conecten a todos los nodos y su longitud total sea mínima.

Entonces, las especificaciones de nuestra solución son:

- Lista de candidatos: las aristas del grafo original
- Lista de candidatos utilizados: las aristas que conectan cualquiera de los nodos seleccionados, que no sean vértice.
- Función solución: el número de aristas es |V|+1
- Función de selección: se selecciona la arista a, que parta de cualquiera de los dos nodos-vértices, tal que su longitud es mínima.
- Criterio de fiabilidad: no se pueden formar ciclos, siempre se cumple pues no cogemos aristas que conecten con nodos que están conectados ya.
- F. objetivo: encontrar un ciclo hamiltoniano minimal del graf

Tenemos:

T= {vector de nodos conectados} = {n1, n2}, nodos conectados por la arista  de mínima distancia.

C={aristas}\\a[n1,n2]

Repetir hasta que C = vacio{

​		Seleccionar arista a[n1, ni] o a[n2, ni], de peso mínimo

​		Si a conecta con n1:

​					C= C\\{aristas que conecten con n1}Ua[ni,n2]

​					n1=ni	

​		Si a conecta con n2:

​					C= C\\{aristas que conecten con n2}	U a[ni, n2]

​					n1=ni

​		T= T U {ni}		

}