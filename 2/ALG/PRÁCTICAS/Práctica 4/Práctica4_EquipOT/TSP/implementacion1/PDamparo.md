## Descripción del algoritmo
Intentemos visualizar nuestro problema a través de un grafo: dado un grafo con arcos de longitud no negativa, se trata de encontrar un circuito de longitud mínima que comience y termine en el mismo vértice y pase exactamente una vez por cada uno de los vértices restantes.

Sea G = (N, A) un grafo orientado. 
Consideramos N = {1, 2, 3, ..., n} nodos. Sea L<sub>ij</sub> la longitud del arco (i, j) siendo infinito si no existe el arco. Si suponemos que el circuito empieza en el vértice 1 (en nuestro programa esta decisión será tomada al azar), se compone por tanto del arco (1, j) con j distinto de 1, seguido de un camino de j a 1 que pasa exactamente una vez por cada vértice de N\{1, j}. Si el circuito es óptimo, el camino de j a 1 debe serlo también: se puede aplicar por tanto el principio de optimalidad.

A groso modo el primero de los algoritmos que vamos a implementar resolvería este problema siguiendo los siguientes pasos:

1. Construir una matriz de distancias
2. Función g que calcula el mínimo de dos números: g(i, S) = min(L<sub>ij</sub> + g(j, S \ {j})), donde S = {conjunto de vértices en estudio}.
3. Hacer S=*Vacío* y g(i, S) = L<sub>i1</sub> para todo vértice i = 2, 3, ..., n.
4. Para #S=1 hasta n-1
        Para i=2 hasta n-1
         Hallar g(i, S)
5. Devolver el g que contiene el mínimo

**Veamos un ejemplo:**

Consideremos la siguiente matriz de distancias:
<center>![ejemplo](/home/amparo/Escritorio/PDalg/ejemplo.png  "ejemplo")</center>

Se inicializa:

	g(2, Vacío) = 5
	g(3, Vacío) = 6
	g(4, Vacío) = 8
	
que coinciden con los valores de la primera columna.
	
Calcula g, conteniendo S un solo elemento:

g(2, {3})= L<sub>23</sub> + g(3, Vacio) = 15
	g(2, {4})= L<sub>24</sub> + g(4, Vacio) = 18
	Idem para g(3,{2}) y g(3,{4})
	Idem para g(4,{2}) y g(4,{3})
	
Calcula g, conteniendo S dos elementos:

g(2, {3,4})= min( L<sub>23</sub> + g(3, {4}), L<sub>24</sub> + g(4, {3})) = min(29, 25) = 25
Idem para g(3, con el resto) y
para g(4, con el resto)

Por último, calcula:

g( 1, {2,3,4} ) = min (35,40,43) = 35

**¿Cómo hemos implementado el algoritmo?**

Una vez que teníamos claro como debía funcionar el algoritmo, implementamos varias funciones que facilitaran la legibilidad y funcionalidad del programa principal.
DIchas funciones, son las siguientes:

**int g(int i, vector<int> S, vector<vector<int>> distancias)**
Esta función es la que se corresponde con la función g que hemos descrito anteriormente. Hemos añadido un tercer parámetro, *distancias*, que se corresponde con la matriz de datos con la que trabajamos a lo largo del problema. Hemos preferido pasarla como parámetro para mantener la independencia de esta función, es decir, para que no la tengamos que implementar dentro del programa principal, ya que la matriz de distancias se calcula en cada ejecución dependiendo de los datos de entrada.

**vector<int> S_quitar(int j, vector<int> S)**
Devuelve un vector que es la copia de S pero sin el elemento que ocupa en S la posición j. Vimos útil implementar esta función para automatizar aún más el proceso de recursividad dentro de la función g, ya que el resultado de g(i,S) depende del resultado de g(i,S\{j}).

**vector<int> solucion_PD(vector<vector<int>> distancias)**
Implementa el allgoritmo de programación dinámica en sí, haciendo uso de las funciones anteriores. Se corresponde con el paso 4 y 5 de la descripción del algoritmo. Devuelve directamente un vector que se corresponde con el recorrido que buscasmos, sin repetir ciudades, y con un coste mínimo.

Así, en el programa principal nos limitamos a generar la matriz de datos y a llamar a la función *solucion_PD* aplicada a dicha matriz.

## Comparación con Greedy

Los algoritmos voraces resultan una idea intuitiva y fácil de implementar, recordemos cómo funcionaban. Tenemos una colección de objetos candidatos a solución, unas determinadas condiciones que debe cumplir la solución y un conjunto de objetos solución, que es un subconjunto de la primera colección de objetos. Con este tipo de algoritmos no examinamos toda la colección de objetos candidatos, por eso son bastante más rápidos. El inconveniente que tienen es que en general tampoco nos dan la solución óptima, aunque suelen dar aproximaciones bastante buenas.

Por otra parte, la programación dinámica es una manera iterativa de implementar algoritmos recursivos. Además tiene una ventaja sobre la recursividad: no repetimos operaciones ya hechas, sino que todos los cálculos que hacemos, los vamos guardando y así nos ahorramos un montón de operaciones. Con programación dinámica se suele afinar bastante la solución obtenida con algoritmos voraces, hasta el punto de llegar a tener la solución óptima. 

El enfoque que define la programación dinámica tiene ciertos parecidos con el que propone la técnica greedy. La diferencia principal es que en el método greedy  siempre se genera solo una sucesión de decisiones, mientras que en programación dinámica pueden generarse muchas sucesiones de decisiones. 

En conclusión, la principal ventaja de los algoritmos voraces es la facilidad que implica trabajar con ellos y su velocidad, mientras que con programación dinámica nos aseguramos encontrar la solución óptima.