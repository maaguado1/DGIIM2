/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.GUI;

import deepspace.HangarToUI;
import deepspace.ShieldToUI;
import deepspace.WeaponToUI;
import java.awt.Component;
import java.util.ArrayList;

/**
 *
 * @author maguado
 */
public class HangarView extends javax.swing.JPanel {
 
    /**
     * Creates new form HangarToUI
     */
    public HangarView() {
        initComponents();
        Weapon.setOpaque(true);
        Shield.setOpaque(true);

    }

    
    void SetHangar(HangarToUI h){
       if (h!= null){

        S1.setText(Integer.toString(h.getMaxElements()));
        Weapon.removeAll();
        Shield.removeAll();
        ArrayList<WeaponToUI> armas = h.getWeapons();
        ArrayList<ShieldToUI> escudos = h.getShieldBoosters();
        WeaponView arma;
        ShieldView escudo;
        for (WeaponToUI w: armas){
            arma = new WeaponView();
            arma.SetWeapon(w);
            Weapon.add(arma);
        }
        
        
        for (ShieldToUI s: escudos){
            escudo = new ShieldView();
            escudo.SetShield(s);
            Shield.add(escudo);
        }
        repaint();
        revalidate();
       }
        
    }
    
    
    ArrayList<Integer> getSelectedWeapons(){
        ArrayList<Integer> selectedWeapons = new ArrayList<>();
        int i =0;
        for (Component c: Weapon.getComponents()){
            
            if ( ((WeaponView) c).isSelected()){
                selectedWeapons.add(i);
            }
            i++;
        }
        return selectedWeapons;
    }
    
    ArrayList<Integer> getSelectedShields(){
        ArrayList<Integer> selectedShields = new ArrayList<>();
        int i =0;
        for (Component c: Shield.getComponents()){
            
            if ( ((ShieldView) c).isSelected()){
                selectedShields.add(i);
            }
            i++;
        }
        return selectedShields;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Hangar = new javax.swing.JLabel();
        S = new javax.swing.JLabel();
        S1 = new javax.swing.JLabel();
        ScrollWeapon = new javax.swing.JScrollPane();
        Weapon = new javax.swing.JPanel();
        ScrollShield = new javax.swing.JScrollPane();
        Shield = new javax.swing.JPanel();

        jLabel1.setText("jLabel1");

        setBackground(new java.awt.Color(153, 220, 214));

        Hangar.setFont(new java.awt.Font("AnjaliOldLipi", 0, 15)); // NOI18N
        Hangar.setText("HANGAR");

        S.setFont(new java.awt.Font("AnjaliOldLipi", 0, 15)); // NOI18N
        S.setText("Size:");

        S1.setFont(new java.awt.Font("AnjaliOldLipi", 1, 15)); // NOI18N
        S1.setText("S");

        Weapon.setBackground(new java.awt.Color(15, 188, 186));
        Weapon.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Weapon.setForeground(new java.awt.Color(15, 188, 186));
        ScrollWeapon.setViewportView(Weapon);

        Shield.setBackground(new java.awt.Color(15, 188, 186));
        Shield.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        ScrollShield.setViewportView(Shield);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Hangar)
                        .addGap(222, 222, 222)
                        .addComponent(S)
                        .addGap(128, 128, 128)
                        .addComponent(S1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ScrollWeapon, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ScrollShield, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Hangar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ScrollWeapon, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(S1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(S))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ScrollShield)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Hangar;
    private javax.swing.JLabel S;
    private javax.swing.JLabel S1;
    private javax.swing.JScrollPane ScrollShield;
    private javax.swing.JScrollPane ScrollWeapon;
    private javax.swing.JPanel Shield;
    private javax.swing.JPanel Weapon;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
