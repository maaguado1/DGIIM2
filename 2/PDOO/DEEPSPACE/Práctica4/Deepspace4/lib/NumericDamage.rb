#encoding:utf-8

require_relative 'Damage'
require_relative 'NumericDamageToUI'

module Deepspace
  

class NumericDamage < Damage
  
  
  public_class_method :new
  def initialize(w,s)
    super(s)
    @nWeapons = w
    
  end
  
  
  # Class copy initializer.
	# @param d [Damage] makes a copy of the given damage.
  def self.newCopy(d)
      return new(d.nWeapons, d.nShields)

  end
  
  
    # Getters
	# ==========================================================================

  
  attr_reader :nWeapons
  
  
   
  # Checks whether the damage is affecting or not
	# @return [Boolean] true, if damage has no effect; false, otherwise
  def hasNoEffect
    if (@nWeapons == 0) and @nShields==0
      return true
    else
      return false
    end
  end
  
  
  # Setters
	# =========================================================================

  
  
  def discardWeapon(w)
    
    if (@nWeapons > 0)
      @nWeapons -=1
    end
  end
  
  
  # Creates a copy of current objet where shields which are not
	# included in arrays given as parameters are discarded. That's to say, we
	# shrink the Damage to the parameters
	# @param s [Array<ShieldBooster>] shields to fit
  # @param w [Array<Weapon>] weapons to fit
	# @return [Integer] a copy of the object adjusted as explained above
  
  def adjust(w,s)
    
      min1=super(s)
      min2=(w.length < @nWeapons)? w.length: @nWeapons
      otro= self.class.new(min2, min1)
   
    return otro
    
    
  end
  
  
   def getUIversion
    return  NumericDamageToUI.new(self)
  end
  
  def to_s
    return super + "Number of weapons: #{@nWeapons}"
                
  end
  
  
  
  
end

end
