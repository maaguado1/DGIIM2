/**
  * @file ingrediente.cpp
  * @brief Implementación del TDA Ingrediente
  *
  */


#include <assert.h>
#include <string>
#include <sstream>
#include <iostream>
#include <ingrediente.h>

using namespace std;


  int StringToNumber ( const string &Text )
  {
     istringstream ss(Text);
     int result;
     return ss >> result ? result : 0;
  }


Ingrediente::Ingrediente(){
    _nombre=" ";
    _tipo=" ";
    _calorias=0;
    _hidratos=0;
    _proteina=0;
    _grasa=0;
    _fibra=0;
    
}




Ingrediente::Ingrediente(const Ingrediente& original){
    _nombre = original._nombre;
    _tipo = original._tipo;
    _calorias=original._calorias;
    _hidratos=original._hidratos;
    _proteina=original._proteina;
    _grasa=original._grasa;
    _fibra=original._fibra;
    
}
    
Ingrediente& Ingrediente::operator=   (const Ingrediente& original){
   if (this != &original){
    _nombre =original.getNombre();
    _tipo = original.getTipo();
    _calorias=original.getCalorias();
    _hidratos=original.getHidratos();
    _proteina=original.getProteina();
    _grasa=original.getGrasa();
    _fibra=original.getFibra();
   }
   return *this;
    
}


bool Ingrediente::operator <(const Ingrediente& original)const {
    bool resultado=false;
    if ( _tipo.compare(original.getTipo())<0)
        resultado=true;
    else if ( _tipo.compare(original.getTipo())==0)
        ( _nombre<original.getNombre())?(resultado=true ): (resultado=false);
    return resultado;   
}


bool Ingrediente::operator >(const Ingrediente& original) const{
    bool resultado=false;
    if (_tipo.compare(original.getTipo())>0)
        resultado=true;
    
    else if (_tipo.compare( original.getTipo())==0)
        (_nombre>original.getNombre())?(resultado=true ): (resultado=false);
    return resultado;   
}

bool Ingrediente::operator==(const Ingrediente& original){
    bool igual =false;
    if (  _tipo.compare( original.getTipo())==0 &&   _nombre.compare(original.getNombre())==0 &&   _calorias==original.getCalorias() &&   _hidratos ==original.getHidratos() &&   _fibra==original.getFibra() &&   _grasa== original.getGrasa()&&   _proteina== original.getProteina())
        igual=true;
    
    return igual;
}
        
    
    


std::istream & operator>>(std::istream & is,  Ingrediente & i){
    string palabra;
    getline(is, palabra, ';');
    i._nombre=palabra;
    int num;
    

    getline(is, palabra,  ';');
    num=StringToNumber(palabra);
    i._calorias=num;
    
    getline(is, palabra,  ';');
    num=StringToNumber(palabra);
    i._hidratos=num;
    
    getline(is, palabra,  ';');
    num=StringToNumber(palabra);
    i._proteina=num;
    
    getline(is, palabra,  ';');
    num=StringToNumber(palabra);
    i._grasa=num;
    
    
    getline(is, palabra,  ';');
    num=StringToNumber(palabra);
    i._fibra=num;
    
    getline(is, palabra,  '\n');
    i._tipo=palabra;
    
    
    return is;
 }
  
std::ostream & operator<<(std::ostream & os, const Ingrediente & i){
    os << i.getNombre() << "; " << i.getCalorias() << "; " << i.getHidratos() <<"; " << i.getProteina() << "; " << i.getGrasa() <<"; " << i.getFibra() << "; " << i.getTipo();
    if (!os){
        cerr << "ERROR al copiar el ingrediente";
        exit(1);
    }
    return os;         
     
 }
    
    
    
     
    
    


    
    
