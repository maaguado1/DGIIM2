#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>



/*

typedef struct _dirdesc {
int dd_fd;
long dd_loc;
long dd_size;
long dd_bbase;
long dd_entno;
long dd_bsize;
char *dd_buf;
} DIR;


//La estructura struct dirent conforme a POSIX 2.1 es la siguiente:

struct dirent {
long d_ino; 
char d_name[256]; 
};

*/


int main (int argc, char *argv[]){
	
	

	
	if (argc != 3){
		printf ("\nError %d en el número de argumentos", errno);
		perror("\nError en argumentos");
		exit(-1);

	}

	int numero = strtol(argv[2], NULL, 8);
	char *nombre=argv[1];
	char cadena[100];
	char cadena2[100];
	DIR *directorio;
	struct dirent *fichero;
	struct stat atributos;
	struct stat atributos2;

	if ((directorio=opendir(nombre))==NULL){
		printf ("\nError %d en la apertura del directorio", errno);
		perror("\nError en directorio");
		exit(-1);
	}

	while ( (fichero=readdir(directorio)) !=NULL){
		
		
		sprintf( cadena, "%s/%s", nombre, fichero->d_name);
		

		if (stat(cadena, &atributos) <0){
			printf ("\nError %d en la lectura de attributos", errno);
			perror("\nError en fichero");
			exit(-1);
		}
		
		
		if (S_ISREG(atributos.st_mode)){

			sprintf(cadena2, "%s", fichero->d_name);
			printf("%s: %o ", cadena2, atributos.st_mode);
			
			chmod (cadena, numero);
			
			if ( chmod(cadena, numero)<0){
				printf ("\nError %d en la actualización de permisos", strerror(errno));

			}
			
			else {
				stat(cadena, &atributos);
				printf("%o \n", atributos.st_mode);




			}
		}
		
		



	}
	closedir(directorio);
	return 0;


}
