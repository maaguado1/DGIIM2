## EXAMEN SISTEMAS OPERATIVOS Temas 1 y 2
### Alejandro Salas

#### Pregunta 1 (1.25p): Con respecto al apoyo hardware al SO:

##### a) Explica los modos de direccionamiento de memoria de la arquitectura IA32, modelos de memoria flat, segmented y real-address mode.

- Modelo IA-32: En este modelo los programas no acceden directamente a memoria física sino indirectamente usando los modelos de memoria flat, segmented y real-adress.

- Flat memory model: Es un espacio lineal con direcciones consecutivas en el rango [0, 2^32-1], que permite direccionar con granularidad de un byte. Esto es, cuando nos referimos a una dirección de memoria, el procesador puede acceder y direccionar toda la memoria disponible sin resolver paginación ni segmentación.

- Segmented memory model: En este modelo de memoria, los programas ven el espacio de memoria como un grupo de espacios independientes llamados segmentos. No se requiere que todos los programas sean de la misma longitud, aunque hay una longitud máxima de segmento.
Una dirección lógica está compuesta por: el número de segmento y un desplazamiento. Un esquema de segmentación sencillo hará uso de una tabla de segmentos por cada proceso y una lista de bloques libres. Cada entrada de la tabla tiene:
  -dirección inicial de la memoria principal del segmento.
  -longitud del segmentos
  Cuando un proceso entra en el estado Ejecutando, la dirección de su tabla de segmentos se carga en un registro especial utilizado por el hardware de gestión de memoria.

- Real-adress model: Proporcionado por compatibilidad hacia atrás con 8086. Se implementa la segmentacioón con limitaciones en el tamaño de los segmentos, 64KB y en el espacio de memoria final accesible.


##### b) Describe los pasos, hardware y software, que se llevan a cabo para la resolución de una llamada al sistema.

Cuando se realiza una llamada al sistema (syscall):
- El hardware apila la palabra de estado de programa (PSW) y el PC actuales, activa el bit de modo kernel y carga el PC con el contenido del vector correspondiente al gestor general de llamadas, esto es, con la rutina de la llamada al sistema.

- El software, o sistema operativo, se activa, pues esta llamada implica un salto a una rutina que es parte del código del sistema operativo. Por tanto ejecutará software núcleo (se ha cambiado el modo).
Los pasos que realiza son:
  - Se asocia un número con cada llamada al sistema proporcionada por el SO.
  - El interfaz de llamadas invoca la llamada requerida por el programador y devuelve el estado de finalización o valores de retorno.
  - Los parámetros se pueden pasar por registros de la CPU, en memoria o en la pila.

Esto es, en resumen, si durante la ejecución de un proceso se encuentra una llamada al sistema, se busca en la biblioteca el código asociado a la llamada que se quiere ejecutar. El procesador cambia a modo kernel e invoca al manejador de llamadas del sistema, que salva los registros en la pila. El manejador busca la llamada, carga en el PC el primer valor de la rutina de servicio de llamada. La llamada entonces se ejecuta hasta que o se finaliza o se bloquea. Si la llamada es bloqueante, el proceso se cambia al estado BLOQUEADO, si no, una vez acabada la ejecución de la rutina de llamada se restaurarán los registros y se vuelve a hacer el context_switch para recuperar todos los datos de pila.



#### Pregunta 2 (1.25p): Con respecto a virtualización:

##### a) ¿Cuál es la diferencia entre los dos enfoques de virtualización explicados en clase: hipervisor tipo1 e hipervisor tipo2?

El hipervisor tipo 1 se ejecuta directamente en el host HW para proporcionar VM a los SO invitados, mientras que el hipervisor tipo 2 se ejecuta sobre un SO convencional y el SO invitado se ejecuta sobre la abstracción proporcionada por el hipervisor. Los de tipo 1 tienen mejor rendimiento que los de tipo 2 ya que disponen de todos los recursos para las VM y los múltiples niveles de abstracción entre el SO invitado y el HW real no permiten alto rendimiento de la máquina virtual.
Sin embargo, los de tipo 2 permiten realizar virtualización sin tener que dedicar toda la máquina a dicho fin.


##### b) Describe en qué consiste la técnica de virtualización denominada virtualización asistida por hardware. Básese para la explicación en la arquitectura x86 y los rings del procesador.

La virtualización asistida por hardware, son extensiones introducidas en la arquitectura del procesador x86 para facilitar las tareas de virtualización al software ejecutado en el sistema.

Si hay cuatro anillos o niveles de privilegio en la arquitectura x86, desde el 0 o de mayor privilegio, que se destina a las operaciones kernel de SO, al 3, con privilegios menores que es el utilizado por los procesos de usuario, en esta nueva arquitectura se introduce un anilllo interior o ring -1 que será el que un hipervisor usará para aislar todas las capas superiores de software de las operaciones de virtualización.



#### Pregunta 3 (2.5p): Responda a las siguientes cuestiones sobre el concepto de hebra:

##### a) ¿Qué ventajas proporciona el modelo de hebras frente al tradicional?

1. Lleva mucho menos tiempo crear un nuevo hilo en un proceso existente que crear un proceso totalmente nuevo.
2. Lleva menos tiempo finalizar un hilo que un proceso.
3.  Lleva menos tiempo cambiar entre dos hilos dentro del mismo proceso.
4.  Los hilos mejoran la eficiencia de la comunicación entre diferentes programas que están ejecutando. En la mayor parte de los sistemas operativos, la comunicación entre procesos independientes  requiere  la  intervención  del  núcleo  para  proporcionar  protección  y  los  mecanismos necesarios de comunicación. Sin embargo, ya que los hilos dentro de un mismo proceso comparten memoria y archivos, se pueden comunicar entre ellos sin necesidad de invocar al núcleo.
5. Mientras la hebra de una tarea está bloqueada, otra puede estar ejecutándose, por lo que el proceso avanza.
6. Usan los sistemas multiprocesador de manera eficiente, a más procesadores, más rendimiento.



##### b) ¿Cuál es el inconveniente en la implementación de hebras de usuario a la hora de se que realice una llamada al sistema bloqueante por parte del programa?

Cuando en ULT se bloquea un hilo, se bloquea todo el proceso, pues se tiene que esperar al núcleo. Es necesario usar el jacketing.


##### c) Justifique el grado de paralelismo real alcanzado por una aplicación con varias hebras, teniendo en cuenta que los programas utilizan una biblioteca de hebras a nivel usuario y el núcleo no planifica hebras sino procesos.

El grado de paralelismo es NULO. Cuando se utilizan hebras a nivel usuario, el kernel sólo asigna procesos a procesadores, por lo que no se puede asignar más de un procesador a más de una hebra de la misma tarea. En conclusión, las hebras se ejecutan secuencialmente


#### Pregunta 4: ¿Cómo implementa Linux el concepto de hebra? Explíquelo utilizando el PCB de Linux (struct task_struct) y la llamada al sistema clone().


Un proceso en Linux se representa por una estructura de datos **task_struct**, que contiene informacíon sobre:

- **El estado** del proceso.
- **Información de planificación.** Información necesitada por Linux para planificar procesos. Un proceso puede ser normal o de tiempo real y tener una prioridad.
- **Identificadores**, de proceso, padre, usuario y grupo.
- **Comunicación entre procesos**: mecanismo IPC.
- **Enlaces** a padres, hermanos e hijos.
- **Tiempos y temporizadores**. Incluye el tiempo de creación y la cantidad de tiempo de procesador. Se pueden asociar a un proceso temporizadores mediante una llamada al sistema. De esta manera se manda una señal al proceso cuando finaliza el temporizador.
- **Sistema de archivos**, punteros al directorio actual y raíz del proceso, y a cualquier archivo abierto por este proceso.
- **Espacio de direcciones** virtual asignado al proceso.
- **Contexto específico del procesador**, información de los registros y la pila.
- **Ejecutando**, dos estados.
- **Interrumpible/Ininterrumpible** Son estados bloqueados. En el Ininterrumpible un proceso está esperando directamente sobre un estado de hardware, no maneja ninguna señal.
- **Detenido/Zombie**



Linux proporciona una solución única en la que no diferencia hilos y procesos. Los hilos a nivel de usuario se asocian con procesos a nivel de núcleo. Múltiples hilos de nivel de usuario que constituyen un único proceso a nivel de usuario se asocian con procesos Linux a nivel de núcleo y comparten el ID de grupo. Esto permite a estos procesos compartir recursos sin cambiar el contexto.
No se define ningún tipo de estructura de datos independiente para un hilo.
La llamada al sistema `fork()` en Linux se implementa con la llamada `clone()`.

**LLAMADA CLONE**
En Linux se crea un proceso copiando los atributos del proceso actual, es decir, un nuevo proceso se puede clonar de forma que comparte recursos (archivos, manejadores de señales y memoria virtual).
Aunque los procesos clonados comparten el mismo espacio de memoria, no pueden compartir la pila, por tanto es necesario crea espacios de pila separados.



#### Pregunta 5: En un SO con una política de planificación apropiativa, enumere las distintas partes del SO que deben comprobar la posibilidad de desplazar al proceso que actualmente se está ejecutando y proponga un pseudocódigo que describa cómo se realizaría dicha comprobación en cada parte.

En una política de planificación apropiativa, las partes del sistema operativo que se encargan de comprobar la posible expulsión son:

- El planificador del sistema operativo (**program load** se encarga de pedir el PID, actualizar PCB, etc.), cuando entra un trabajo nuevo al sistema, o cuando se produce una interrupción periódica, comprueba si dicho trabajo tiene mayor prioridad de ejecución (según la política en la que estemos) y de ser así, intercambia.

Implementamos la función planif_CPU, que nos indica si el trabajo se tiene que ir, según la política en la que estemos.

~~~
if (planif_CPU)
then
  DISPATCH //Encolar en LISTO, el PID

~~~

#### Pregunta 6: Con respecto a la planificación de procesos responda a las siguientes cuestiones:

##### a) ¿Qué algoritmo de planificación provoca una mayor penalización a los procesos limitados por E/S frente a los procesos limitados por CPU? ¿Por qué?

Los procesos cortos se verán más penalizados en el FCFS, debido a que cada vez que dicho proceso, limitado por E/S, se bloquee como resultado a la llamada al manejador de E/S, se situará en bloqueados, y cuando dicho evento se complete, pasará a Listos. Es decir, que tendrá que volver mínimo una vez a la cola de listos y esperar.



##### b) Describa los factores a considerar a la hora de diseñar un algoritmo de planificación basado en colas múltiples con realimentación. En particular, justifique cómo asociaría los conceptos de quantum y prioridades a su diseño.

Las colas múltiples de realimentación se dan cuando un proceso se mueve a través de distintas colas. Se necesita considerar:
- El número de colas tratadas.
- La política de planificación dentro de cada cola.
- Método para determinar cuándo un proceso se traslada de cola.
- Método para determinar en qué cola se inserta un proceso.
- Algoritmo de planificación entre colas, esto es, de qué cola se seleccionan procesos.

Para poder añadir el concepto de quantum, se podría establecer un límite de tiempo o __timeout__ de ejecución, de esta manera, se evita el monopolio de CPU por parte de un proceso largo.

Las prioridades se pueden añadir tanto en la política de planificación interna a cada cola como la política de planificación entre colas, añadiendo una prioridad a cada cola.

En resumen, cuando un proceso cumple su rodaja de tiempo, pasa a una cola con menor prioridad que la anterior. De esta forma los procesos cortos se completarán rápidamente mientras que los largos irán degradándose. Dentro de cada cola se utiliza una política FCFS, y en la última el round_robin.

#### Pregunta 7: Con respecto al núcleo de Linux visto en clase:

##### a) Describe los pasos que ejecuta el núcleo de Linux en la función do_exit().

Las funciones implementando llamadas al sistema bajo Linux son prefijadas con sys_, pero ellas son usualmente concernientes sólo al chequeo de argumentos o a formas específicas de la arquitectura de pasar alguna información y el trabajo actual es realizado por las funciones do_. Por lo tanto, es con sys_exit() el cual llama a do_exit() para hacer el trabajo. Aunque otras partes del núcleo a veces invocan a sys_exit() mientras que deberían realmente de llamar a do_exit().

La función do_exit() es encontrada en kernel/exit.c. Los puntos que destacar sobre do_exit() son:

- Usa un cierre global del núcleo (cierra pero no abre).
- Llama schedule() al final, el cual nunca regresa.
- Establece el estado de tareas a TASK_ZOMBIE.
- Notifica cualquier hijo con current->pdeath_signal, si no 0.
- Notifica al padre con una current->exit_signal, el cual es usualmente igual a SIGCHLD.
- Libera los recursos asignador por fork, cierra los archivos abiertos, etc,



##### b) Desciba como se comporta el planificador schedule(), para los procesos planificados mediante la clase de planificación CFS.

El planificador de Linux utiliza la clase CFS para decidir el orden de ejecución de los procesos, esta clase almacena información sobre los tiempos consumidos por los procesos y los utiliza para calcular periódicamente su vruntime, o tiempo virtual, que se calcula teniendo en cuenta la prioridad estática, el tiempo de uso de CPU y el peso del proceso, que a su vez depende de la prioridad.
Cuando schedule es llamado, es decir se decide qué proceso ejecutar, se elige el que menos vruntime tenga. El valor de vruntime se calcula cada cierto tiempo por el planiicador, cuando llega un nuevo proceso o cuando el proceso actual se bloquea.
