#encoding:utf-8


module Deepspace
  
# Enum that represents the two types of characters of the game
module GameCharacter
    SPACESTATION=:spacestation
    ENEMYSTARSHIP=:enemystarship;
end # enum GameCharacter

end # module Deepspace
