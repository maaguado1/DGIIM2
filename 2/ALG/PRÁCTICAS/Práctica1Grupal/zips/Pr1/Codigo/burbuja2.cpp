/**
   @file Ordenación por burbuja
*/


#include <iostream>
using namespace std;
#include <ctime>
#include <cstdlib>
#include <climits>
#include <cassert>


inline static
void burbuja(int T[], int num_elem);

static void burbuja_lims(int T[], int inicial, int final);

inline void burbuja(int T[], int num_elem)
{
  burbuja_lims(T, 0, num_elem);
};


static void burbuja_lims(int T[], int inicial, int final)
{
  int i, j;
  int aux;
  for (i = inicial; i < final - 1; i++)
    for (j = final - 1; j > i; j--)
      if (T[j] < T[j-1])
        {
          aux = T[j];
          T[j] = T[j-1];
          T[j-1] = aux;
        }
}


int main(int argc, char* argv[])
{
  int n = atoi(argv[1]);
  clock_t tantes, tdespues;

  int * T = new int[n];
  assert(T);

  cout << "Tamaño " << n;
  srandom(time(0));

  for(int contador=0; contador<10; contador++){
    for (int i = 0; i < n; i++)
    {
      T[i] = random();
    };

    tantes = clock();
    burbuja(T, n);
    tdespues = clock();

    cout << " " << (double)(tdespues - tantes) / CLOCKS_PER_SEC;
  }

  cout << endl;
  delete [] T;

  return 0;
};



