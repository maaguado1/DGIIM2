#include "acciones.h"
#include "instrucciones.h"
#include "ingredientes.h"

#include <fstream>


using namespace std;
int main(int argc, char *argv[])
{
  if (argc != 4)
  {
    cout << "Dime el fichero con las acciones e instrucciones" << endl;
    return 0;
  }
  ifstream f(argv[1]);
  if (!f)
  {
    cout << "No existe el fichero " << argv[1] << endl;
  }
  
  
  /***********************************************************************/

  //SECTION 3: Test sobre TDA recetas. Operadores de lectura y escritura

  acciones rall;
  
  f >> rall;
  cout << "Todas las acciones: " << endl
       << rall << endl;
  cout << "Numero de acciones " << rall.size() << endl;
  cout << "Pulse una tecla para continuar" << endl;
  cin.get();
  ingredientes ings;
  f.close();
  f.open(argv[2]);
  f >> ings;


  instrucciones iall(rall);
  iall.setIngs(ings);
  f.close();
  f.open(argv[3]);
  cout << "voy a meter en el arbol todo: va a salir mal"<< endl;
  f>> iall;
  cout << "enseño todo jeje" << endl;
  cout << iall;
  /***********************************************************************/

  //SECTION 4: Consultar una receta por codigo
  cout << "Dime el nombre de una accion:";
  string c;
  cin >> c;
  if (rall[c] != ' ')
  {
    cout << "La acción es " << rall[c] << endl;
  }
  else
  {
    cout << "La acción con nombre " << c << " no existe" << endl;
  }
  /***********************************************************************/

  //SECTION 5: Borramos la receta con un código. Comprobamos el
  //funcionamiento del iterador de recetas
  rall.borrar(c);
  cout << "Tras el borrado " << endl;
  cout << rall;
}
