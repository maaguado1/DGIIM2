#include <algorithm>
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto
{
  float x;
  float y;
};

struct Arista
{
  int inicio;
  int final;
};
/**
 * @brief Función para leer un fichero que contendrá un conjunto de nodos.
 * @param Un string, que hace referencia al nombre del fichero de entrada.
 * @param Un vector de Punto, donde iremos introduciendo los nodos del fichero(por ello, se pasa por referencia).
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_entrada(const string origen, vector<Punto> &nodos)
{

  ifstream f(origen);
  bool return_value;

  if (!f)
  {
    cerr << "Fallo al abrir el fichero\n";
    return_value = false;
  }
  else
  {
    float n;
    string dimension;

    do
    {
      f >> dimension;
    } while (dimension != "DIMENSION:");

    f >> n;

    string coordenadas;

    do
    {
      f >> coordenadas;
    } while (coordenadas != "NODE_COORD_SECTION");

    nodos.clear();

    int i, j;
    Punto P;
    float temporal;
    vector<float> vector_coordenadas;
    for (j = 0; j < n; j++)
    {
      f >> i;
      f >> temporal;
      P.x = temporal;

      f >> temporal;
      P.y = temporal;
      nodos.push_back(P);
    }
  }

  return_value = true;

  f.close();
  return return_value;
}

/**
 * @brief Función para sacar un fichero que contendrá la solución del problema
 * @param Un string, que hace referencia al nombre del fichero de salida.
 * @param Un vector de enteros, que es el resultado obtenido por el algoritmo escogido.
 * @param Un vector de Punto, que contiene los nodos.
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_salida(const string fichero, const vector<int> &resultados, vector<Punto> nodos)
{
  ofstream f(fichero);

  if (!f)
  {
    cerr << "Fallo al abrir el archivo de salida";
    return false;
  }
  else
  {
    f << "DIMENSION: " << resultados.size() << endl;
    f << "TOUR_SECTION" << endl;

    for (int i = 0; i < resultados.size(); i++)
      f << resultados[i] + 1 << " " << nodos[i].x << " " << nodos[i].y << endl;

    f << resultados[0] + 1 << " " << nodos[0].x << " " << nodos[0].y << endl;

    f.close();

    return true;
  }
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */
int distancia(Punto uno, Punto dos)
{
  return rint(sqrt(pow((dos.x - uno.x), 2) + pow((dos.y - uno.y), 2)));
}

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

/**
 * @brief Genera una matriz con las distancias entre los puntos
 * @param nodos, los puntos sobre los que calculamos las distancias
 * @return una matriz con las distancias entre los puntos
 *
 * Matriz[i][j] := distance(pi, pj)
 *
 * */
vector<vector<int>> genera_matriz(vector<Punto> nodos)
{
  vector<vector<int>> T;
  int n = nodos.size();
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;
    for (int j = 0; j < n; j++)
    {
      if (j < i)
        fila.push_back(T[j][i]);
      else if (j == i)
        fila.push_back(0.0);
      else
        fila.push_back(distancia(nodos[i], nodos[j]));
    }
    T.push_back(fila);
  }
  return T;
}

/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[solucion[i]][solucion[i + 1]];
  }
  return valor;
}

vector<int> append(vector<int> &uno, vector<int> extra)
{
  for (int i = 0; i < extra.size(); i++)
  {
    uno.push_back(extra[i]);
  }
  return uno;
}

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: P R O G R A M A C I Ó N   D I N Á M I C A   : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int VISITADO_TODAS;
vector<int> programaciondinamica(vector<vector<int>> T, int pos, int mascara)
{

  vector<int> respuesta;
  respuesta.push_back(pos);
  if (mascara == VISITADO_TODAS)
  {
    respuesta.push_back(0);
    return respuesta;
  }

  int rep = INT_MAX;
  vector<int> uno, otro;

  for (int nodo = 0; nodo < T.size(); nodo++)
  {
    //Si la ciudad no está visitada
    if ((mascara & (1 << nodo)) == 0)
    {

      //Comprobamos todas las combinaciones con dicha ciudad
      uno = programaciondinamica(T, nodo, mascara | (1 << nodo));
      int nuevaresp = T[pos][nodo] + calcular_valor(uno, T);
      if (nuevaresp < rep)
      {
        rep = nuevaresp;
        otro = uno;
      }
    }
  }
  append(respuesta, otro);
  return respuesta;
}


//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: B A C K T R A C K I N G    : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

 /**
  * @brief Calcula el algoritmo óptimo mediante una resolución por backtracking.
  * @param v, vector que indica si cada nodo está visitado o no
  * @param T, la matriz de distancias de los puntos con los que trabajamos
  * @param posicion, el número del nodo en el que estamos
  * @param n , número de nodos
  * @param nivel, el nivel del árbol de soluciones en el que estamos
  * @param costemin, el coste mínimo obtenido hasta la fecha
  * @param solucionparcial, la solucion que estamos construyendo
  * @param solucion, la mejor solucion que hemos obtenido hasta la fecha
  * @pre T.size() > 0
  * */
void backtracking(vector<bool> &v, int posicion, vector<vector<int>> T,
                  int n, int nivel, int costeactual, int &costemin, vector<int> solucionparcial, vector<int> &solucion)
{

  if (nivel == n)
  {
    if (costeactual + T[posicion][0] < costemin)
    {
      costemin = costeactual + T[posicion][0];
      solucion = solucionparcial;
    }
    return;
  }

  for (int i = 0; i < n; i++)
  {
    if (!v[i])
    {

      v[i] = true;
      solucionparcial.push_back(i);
      backtracking(v, i, T, n, nivel + 1,
                   costeactual + T[posicion][i], costemin, solucionparcial, solucion);

      // Mark ith node as unvisited
      solucionparcial.pop_back();
      v[i] = false;
    }
  }
}

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: B R A N C H & B O U N D   : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

struct nodo{
	vector<int> visitadas;
	int CI;
	int valor_actual;
	
	bool operator< (nodo otro) const{
		return CI < otro.CI;
	}
};

vector<int> obtener_arcos(vector<vector<int>> T){
	vector<int> mejor_arco;
	for (int i=0; i < T.size(); i++){
		int min = INT_MAX;
		for(int j=0; j<T.size(); j++)
			if(T[i][j] < min && i!=j){
				min=T[i][j];
			}
		mejor_arco.push_back(min);
	}
	return mejor_arco;
}

int CI(vector<int> visitadas, vector<int> arcos, int valor_actual, int n){
	int suma = valor_actual;
	for (int i=1; i < n; i++)
		if (find(visitadas.begin(),visitadas.end(),i)==visitadas.end())
			suma += arcos[i];
			
	suma = arcos[0];
	return suma;	
}

int podar(multiset<nodo> &s, int min, int podas){
	multiset<nodo>::iterator it;
	while (!s.empty() && (*s.rbegin()).CI >= min){
		it = s.end();
		it--;
		s.erase(it);
		podas++;
	}
	return podas;
}

vector<int> solucion_ByB(vector<vector<int>> T, int& podas, int& expansiones, int& max_size){
	vector<int> solucion, visitadas;
	multiset<nodo> nodos_vivos;
	vector<int> arcos = obtener_arcos(T);
	visitadas.push_back(0);
	nodo inicial={visitadas,0,0};
	nodos_vivos.insert(inicial);
	solucion = solucion_cercania(T);
	int C = calcular_valor2(solucion,T);
	
	
	while(!nodos_vivos.empty()){
		nodo x=*nodos_vivos.begin();
		nodos_vivos.erase(nodos_vivos.begin());
		
		if (x.visitadas.size() == T.size()){
			x.valor_actual+=T[x.visitadas.front()][x.visitadas.back()];
			if (x.valor_actual < C){
				solucion = x.visitadas;
				C=x.valor_actual;
				podas = podar(nodos_vivos,C,podas);
			}
			
		}else{
			nodo y;
			expansiones++;
			
			for(int i=1; i < T.size(); i++){
				if (find(x.visitadas.begin(), x.visitadas.end(), i) == x.visitadas.end()){
					y=x;
					y.valor_actual+=T[i][x.visitadas.back()];
					y.visitadas.push_back(i);
					y.CI=CI(y.visitadas,arcos,y.valor_actual,T.size());
					if (y.CI < C)
						nodos_vivos.insert(y);
				}	
			}
			if (max_size < nodos_vivos.size())
				max_size = nodos_vivos.size();
		}
	}
	return solucion;	
}	


//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    printf("Error, modo de ejecución; ./cercania <nombre_fichero>");
    exit(-1);
  }

  clock_t t_antes,t_antes2;   // Valor del reloj antes de la ejecución
  clock_t t_despues,t_despues2; // Valor del reloj después de la ejecución
  vector<Punto> ciudades;
  string f_entrada, algoritmo;
  string f_salida;
  vector<vector<int>> matriz;
  vector<int> solucion;
  vector<int> soluciondinamica;
  vector<int> solucion_bb;
  vector<bool> v;
  int minimo = INT_MAX;
  int podas=0, expansiones=0, max_size=1;
  double t4;

  f_entrada = argv[1];


  //Abrimos el fichero de entrada
  if (!fichero_entrada(f_entrada, ciudades))
  {
    printf("Error en el fichero de entrada\n");
    return 1;
  }


  //Calculamos la matriz de distancias y aplicamos el algoritmo indicado.
  matriz = genera_matriz(ciudades);

  //Mostramos las coordenadas de las ciudades introducidas
  cout << "Las coordenadas de las ciudades introducidas son " << endl;
  for (int i = 0; i < ciudades.size(); i++)
  {
    cout << "(" << ciudades[i].x << "," << ciudades[i].y << ")" << endl;
    v.push_back(false);
  }

  cout << "\nLa matriz de distancias es: ";
  SalidaMatriz(matriz);


  //Calculamos la solucion por backtracking
  v[0] = true;
  solucion.push_back(0);
  vector<int> def = solucion;
  t_antes = clock();
  backtracking(v, 0, matriz, ciudades.size(), 1, 0, minimo, def, solucion);
  t_despues = clock();

  //Mostramos la solucion backtracking
  solucion.push_back(solucion[0]);
  cout << "\nEl orden de ciudades recorrido por backtracking es el siguiente: " << endl;
  for (int i = 0; i < solucion.size(); i++)
  {
    cout << solucion[i] + 1 << endl;
  }
  cout << "\nY su valor es: " << minimo;

  //Mostramos el tiempo
  cout << ((double)(t_despues - t_antes)) / CLOCKS_PER_SEC << endl;

  f_salida = f_entrada.append("_salida");
  
  //Calculamos la solucion por programacion dinamica
  VISITADO_TODAS = (1 << matriz.size()) - 1;
  soluciondinamica = programaciondinamica(matriz, 0, 1);
  cout << "\nEl orden de ciudades recorrido por programación dinámica es el siguiente: " << endl;
  for (int i = 0; i < soluciondinamica.size(); i++)
  {
    cout << soluciondinamica[i] + 1 << endl;
  }
  cout << "\nY su valor es: " << calcular_valor(soluciondinamica, matriz) << endl;
  
  //Calculamos la solucion por Branch & Bound
  t_antes2=clock();
  solucion_bb=solucion_ByB(matriz,podas,expansiones, max_size);
  t_despues2=clock();
  for (int i = 0; i < solucion_bb.size(); i++)
  {
    cout << solucion_bb[i] + 1 << endl;
  }
  
  t4=calcularvalor2(solucion,matriz);

  cout << "Solución obtenida por B&B" << endl << " Tiempo: " << ((double)(t_despues2-t_antes2))/CLOCKS_PER_SEC << " Distancia: " << t4 << endl;
  cout << "nº expansiones: " << expansiones << "	nº podas: " << podas;

  

  //Introducimos en el fichero de salida la solución(el recorrido).
  if (fichero_salida(f_salida, solucion, ciudades))
    cout << "\nGuardado. Salida en: " << fichero_salida << endl;
  else
  {
    cerr << "\nError al guardar el fichero\n";
    return 1;
  }
}
