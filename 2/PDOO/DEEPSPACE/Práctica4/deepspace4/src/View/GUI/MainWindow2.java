/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.GUI;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import controller.Controller2;
/**
 *
 * @author maguado
 */
public class MainWindow2 extends javax.swing.JFrame {

    
    
    private static MainWindow2 instance = null;
    private String appName;
    static Controller2 controller = Controller2.getInstance();
    private ElementPanelView panel1, panel2;
    public static MainWindow2 getInstance(){
        
        if (instance == null){
            instance = new MainWindow2();
        }
        return instance;
    }
    /**
     * Creates new form MainWindow2
     */
    public MainWindow2() {
        initComponents();
        appName = "Deepspace_MaríaAguado";
        setTitle(appName);
        
        panel1 = new ElementPanelView();
        panel2 = new ElementPanelView();
        repaint();
        revalidate();
        setLocationRelativeTo(null);
        getContentPane().setBackground(new Color(203,236,236) );
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                Controller2.getInstance().finish(0);
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        Panel1 = new javax.swing.JPanel();
        Transferir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Panel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton1.setText("Añadir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        Panel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        Transferir.setText("Intercambio");
        Transferir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TransferirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(26, 26, 26)
                .addComponent(Transferir)
                .addGap(57, 57, 57))
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Panel2, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 544, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(291, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addComponent(Panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(Panel2, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(Transferir))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TransferirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TransferirActionPerformed
        ArrayList<Integer> elements = panel1.getSelectedItems();
        
        controller.transfer(elements);
        updateView();
    }//GEN-LAST:event_TransferirActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       controller.addElement();
        updateView(); // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    
    
    
     public void updateView(){
        panel1.SetArray(controller.getArray1());
       
        panel2.SetArray(controller.getArray2());
        repaint();
        revalidate();
    }
     
     
     
     public void showView(){
        this.setVisible(true);
    }
    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel1;
    private javax.swing.JPanel Panel2;
    private javax.swing.JButton Transferir;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables

    public boolean confirmExitMessage() {
        return (JOptionPane.showConfirmDialog(this, "Are you sure you want to end the game?", getAppName(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION);
    }

    private String getAppName() {
        return appName;}
}
