//María Aguado Martínez
#ifndef COMPORTAMIENTOJUGADOR_H
#define COMPORTAMIENTOJUGADOR_H

#include "comportamientos/comportamiento.hpp"

#include <list>
#include <vector>
#include <algorithm>
#include <set>



template <typename T>
struct Compara
{
	bool operator()(const T &a, const T &n) const
	{
		if ((a.fila > n.fila) or (a.fila == n.fila and a.columna > n.columna) )
			return true;
		else
			return false;
	}
};


struct estado
{
  estado(): objeto(0){}
  int fila;
  int columna;
  int orientacion;
  int objeto; //0=nada, 1=bikini, 2=zapatillas
  vector<bool> objetivosvisitados;
  

  inline bool operator<(const estado &otro) const {
		return ((fila < otro.fila) or (fila == otro.fila and columna < otro.columna) or
	      (fila == otro.fila and columna == otro.columna and orientacion < otro.orientacion));
	}

};





class ComportamientoJugador : public Comportamiento
{
public:
  ComportamientoJugador(unsigned int size) : Comportamiento(size)
  {
    hayPlan = false;
    destino.fila = -1;
    destino.columna = -1;
    numdestinosConseguidos=0;
    cantidadRecarga=0;
    destinos=false;

    misionCompletada=false;
    justCharged=true;
    casillasDescubiertas=0;
    umbralDinamico=0;
    instantesSimulacion=0;
  }


  ComportamientoJugador(std::vector<std::vector<unsigned char>> mapaR) : Comportamiento(mapaR)
  {
    hayPlan = false;
    destino.fila = -1;
    destino.columna = -1;
    numdestinosConseguidos=0;
    cantidadRecarga=0;
    destinos=false;

    misionCompletada=false;
    justCharged=true;
    casillasDescubiertas=0;
    umbralDinamico=0;
    instantesSimulacion=0;
  }
  ComportamientoJugador(const ComportamientoJugador &comport) : Comportamiento(comport) {}
  ~ComportamientoJugador() {}

  Action think(Sensores sensores);
  int interact(Action accion, int valor);
  void VisualizaPlan(const estado &st, const list<Action> &plan);
  ComportamientoJugador *clone() { return new ComportamientoJugador(*this); }

private:
  // Declarar Variables de Estado
  estado actual;
  list<estado> objetivos;
  list<Action> plan;
  set<estado> recargas; 
  bool hayPlan;
  bool esperando_aldeano; //

  estado destino;
  estado siguiente; 
  bool misionCompletada; 
  bool destinos; 
  bool recarga; 
  bool justCharged; 
  int umbralDinamico; //
  int cantidadRecarga; //
  int instantesSimulacion; //
  int modificacionDesconocida; //
  int numdestinosConseguidos; //
  int casillasDescubiertas; //

  // Métodos privados de la clase
  bool pathFinding(int level, const estado &origen, const list<estado> &destino, list<Action> &plan);
  bool pathFinding_Profundidad(const estado &origen, const estado &destino, list<Action> &plan);
  bool pathFinding_Anchura(const estado &origen, const estado &destino, list<Action> &plan);
  bool pathFinding_CostoUniforme(const estado &origen, const estado &destino, list<Action> &plan);
  bool pathFinding_A(const estado &origen, const estado &destino, list<Action> &plan, int &costo);
  bool pathFinding_Amodificado(const estado &origen, const estado &destino, list<Action> &plan, int &costo);
  bool pathFinding_CostoUniformeVarios(const estado &origen, const list<estado> &destino, list<Action> &plan);

  bool pathFinding_AVarios(const estado &origen, const list<estado> &destino, list<Action> &plan);
  bool Check(estado &origen, const list<estado> &destinos);

  int calcularCostoActual(int i, const char &celda);
  int hvarios(const estado &st1, const list<estado> &destinos);
  int h(const estado &st1, const estado &st2);
  void PintaPlan(list<Action> plan);
  bool HayObstaculoDelante(estado &st);

  int calcularCostoModificado(int i, const char &celda, int objeto);
  void ProcesarCasilla(char tipocelda, int bateria);
  void FinalMision();
  void ReiniciarObjetivos(int num_destinos, vector<unsigned int> sensdestinos, int nivel);
  void ActualizarMapa(vector<unsigned char> terreno, int fila, int columna, Orientacion sentido);
  estado ObjetivoCercano();
  estado RecargaCercana();
};

#endif

