## MÓDULO II: Sesión 2

### Llamadas al sistema para el Control de Procesos
## 1. Creación de Procesos


### 1.1 Identificadores de proceso

Cada proceso tiene un PID, init (inicializa el sistema /sbin/init) es el 1.
Llamadas al sistema:

~~~
#include <unistd.h>
#include <sys/types.h>
pid_t getpid(void); // devuelve el PID del proceso que la invoca.
pid_t getppid(void); // devuelve el PID del proceso padre del proceso que
                    // la invoca.
uid_t getuid(void); // devuelve el identificador de usuario real del
                    // proceso que la invoca.
uid_t geteuid(void); // devuelve el identificador de usuario efectivo del
                    // proceso que la invoca.
gid_t getgid(void); // devuelve el identificador de grupo real del proceso
                    // que la invoca.
gid_t getegid(void); // devuelve el identificador de grupo efectivo del
                    // proceso que la invoca.
~~~

El usuario real viene de comprobar el logon sobre el arhivo /etc/passwd al acceder al sistema.
El usuario efectivo es igual que el UID, salvo si se ejecuta con suid, entonces es el propietario.

### 1.2. Llamada al sistema fork()

Se crea un nuevo proceso, devuelve 0 si es el hijo, devuelve el PID del hijo si es el padre.
NOTAS:
- La llamada write no realiza buffering, el printf si, si se quiere desactivar: `int setvbuf(FILE *stream, char *buf, int mode , size_t size);` (trabaja sobre streams, no fd).
- Los flujos para trabajar sobre STDIN_FILENO y demás son:
  ~~~
  #include <stdio.h>
extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;
  ~~~


### 1.3.Llamadas wait, waitpid, exit

SINTAXIS
~~~
#include <sys/types.h>
#include <sys/wait.h>

pid_t wait(int *wstatus);

pid_t waitpid(pid_t pid, int *wstatus, int options); //pid puede ser <-1, -1, 0, >0

#include <stdlib.h>

void exit(int status);
~~~


## Llamadas exec

SINTAXIS
~~~
#include <unistd.h>
extern char **environ;

int execl(const char *path, const char *arg, ...);
int execlp (const char *file, const char *arg, ...);
int execle(const char *path, const char *arg , ..., char * const envp[]);
int execv(const char *path, char *const argv[]);
int execvp(const char *file, char *const argv[]);
~~~

- **const char *arg ** argumentos del programa a ejecutar (terminada por NULL).

DESCRIPCIÓN

- **execv y execvp** vector de argumentos terminada por 0. El primer argumento es el nombre.
- **execle** entorno del proceso que ejecuta (parámetro detrás de NULL).
- Las demás funciones obtienen el entorno de `environ`
- **execlp y execvp** son como el shell si no hay path (:/bin:/usr/bin)
- Si un archivo no tiene permiso (execve devuelve EACCES), buscan en el resto.
- Si no se reconoce la cabecera (ENOEXEC), ejecutan el shell.
- Falla si: no se puede crear esp. de direcciones, o si no se pasan bien los args.


## La llamada CLONE

SINTAXIS
~~~
#define _GNU_SOURCE
#include <sched.h>
int clone(int (*func) (void *), void *child_stack, int flags, void *func_arg,
...
/* pid_t *ptid, struct user_desc *tls, pid_t *ctid */ );
Retorna: si éxito, el PID del hijo; -1, si error
~~~

- **func** función que ejecuta el hijo
- **funf_arg** parámetro para func
- **flags**: el byte menor = señal de terminación (SIGCHILD), otros bytes = ver tabla

| FLAG | Significado | Flag | Significado |
|--|----|--|----|
|CLONE_CHILD_CLEARTID| Limpia el TID con exec o exit | CLONE_NEWUTS | nuevo namespace UTS |
|CLONE_CHILD_SETTID| Escribe el TID del hijo en CTID | CLONE_PARENT | padre del hijo =padre del llamador |
|CLONE_FILES| Comparten la tabla de archivos abiertos | CLONE_PARENT_SETTID| Tid del hijo en ptid |
| CLONE_FS | Comparten atributos de SA | CLONE_PTRACE | Si el padre está traceado, el hijo también |
|CLONE _IO | Comparten contextos de E/S | CLONE_SETTLS | Almacenamiento local (tls) para el hijo
|CLONE_NEWIPC | Nuevo namespace V IPC |CLONE_SIGHAND | Comparten señales |
| CLONE_NEWNET | Nuevo namespace de red | CLONE_SYSVSEM | Comparten valores para semáforos |
| CLONE_NEWNS | Nuevo namespace de montaje| CLONE_THREAD | Hijo en el mismo grupo de hilos que el padre|
|CLONE_NEWPID| nuevo namespace de PID |CLONE_UNTRACED | no hace CLONE_PTRACE |
| CLONE_NEWUSER | nuevo namespace UID | CLONE_VFORK | El padre se suspende hasta que el hijo hace exec() |
| CLONE_VM | Comparten espacio de m.v | ||



NOTA:
- para crear un hilo `CLONE_VM|CLONE_FILES|CLONE_FS|CLONE_THREAD|CLONE_SIGHAND`
- para reservar la pila
  ~~~
  void **stack;
  stack=(void**)malloc(150000);
  ~~~
