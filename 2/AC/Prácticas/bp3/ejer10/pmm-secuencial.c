#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#ifdef _OPENMP
  #include <omp.h>
#else
  #define omp_get_thread_num() 0
  #define omp_get_wtime() (1.0*clock()/CLOCKS_PER_SEC)
#endif

#define _CHUNK


void SalidaMatriz(double** T, int n){
  for (int i = 0; i < n; i++)
    {
    	printf("\n| ");
      for (int j = 0; j< n; j ++){
          printf(" %f " ,T[i][j]);
  }
    printf(" | ");
  }
  printf("\n\n ");
}


int main (int argc, char *argv[]){


	if (argc <2){

		printf ("Falta argumento\n");
		exit (-1);
	}




	unsigned int N = atoi(argv[1]);
	double **M1, **M2, **M3, t1, t2, ncgt;

	M1= (double**) malloc(N*sizeof(double*));
	M2= (double**)malloc(N*sizeof(double*));
	M3 = (double**) malloc (N*sizeof(double*));


	if (M1 == NULL || M2== NULL || M3== NULL){
		printf ("\nError en la reserva de memoria");
		exit(-1);

	}

	for (int i=0; i < N; i++){
		M1[i]=(double*) malloc(N*sizeof(double));
    M2[i]=(double*) malloc(N*sizeof(double));
    M3[i]=(double*) malloc(N*sizeof(double));
		if (M1[i] == NULL || M3[i] == NULL || M2[i] == NULL ){
			printf ("Error en la reserva de memoria 2");
			exit(-1);

		}
	}



		//Inicialización de la matriz

	for (int i=0; i < N; i ++){


		for (int j=0; j < N; j++){
				M1[i][j] = j-i +1;
			M2[i][j] = j-i +2;
      M3[i][j] = 0;
			}

	   }


	t1 = omp_get_wtime();


	//Cálculo
	double sumai;

  for (int i=0; i < N; i++){

		for (int j=0; j < N; j++){
      sumai =0;
		    for (int k =0; k < N; k ++){
          sumai+= M1[i][k] * M2[k][j];
        }
        M3[i][j]= sumai;

		}


	}


	t2 = omp_get_wtime();
	ncgt = t2-t1;

	//Salida

	if (N <= 11){
		printf("Tiempo:%11.9f\n",ncgt);
    SalidaMatriz(M1, N);
    SalidaMatriz(M2, N);

    printf ("\nResultado: ");
    SalidaMatriz(M3, N);


	}
	else {

		printf("%11.9f", ncgt);




	}

 // libera el espacio reservado para v2

	for (int i=0; i < N; i++){
		free(M1[i]);
    free(M2[i]);
    free(M3[i]);

}
	 // libera el espacio reservado para v3
	 free(M1);
   free(M2);
   free(M3);
  return 0;


}
