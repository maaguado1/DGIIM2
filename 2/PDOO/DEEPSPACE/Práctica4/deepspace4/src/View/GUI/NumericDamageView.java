/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.GUI;

import deepspace.DamageToUI;
import deepspace.NumericDamageToUI;


/**
 *
 * @author maguado
 */
public class NumericDamageView extends javax.swing.JPanel {

    /**
     * Creates new form DamageView
     */
    public NumericDamageView() {
        initComponents();
    }

    
    void SetDamage(NumericDamageToUI d){
        if (d != null){
            S.setText(Integer.toString(d.getNShields()));
            Nweap.setText(d.getWeaponInfo());
            repaint();
        }
        else{
            S.setVisible(false);
            Nweap.setVisible(false);
            repaint();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Title = new javax.swing.JLabel();
        Escudos = new javax.swing.JLabel();
        Weapons = new javax.swing.JLabel();
        S = new javax.swing.JLabel();
        Nweap = new javax.swing.JLabel();

        setBackground(new java.awt.Color(153, 220, 214));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        Title.setFont(new java.awt.Font("AnjaliOldLipi", 0, 15)); // NOI18N
        Title.setText("DAMAGE");

        Escudos.setFont(new java.awt.Font("AnjaliOldLipi", 0, 13)); // NOI18N
        Escudos.setText("SHIELDS:");

        Weapons.setFont(new java.awt.Font("AnjaliOldLipi", 0, 13)); // NOI18N
        Weapons.setText("WEAPONS:");

        S.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        S.setText("Shields");

        Nweap.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        Nweap.setText("NWeap");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Title)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Escudos)
                            .addComponent(Weapons))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(S)
                            .addComponent(Nweap))))
                .addContainerGap(124, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Title)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Escudos)
                    .addComponent(S))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Weapons)
                    .addComponent(Nweap))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Escudos;
    private javax.swing.JLabel Nweap;
    private javax.swing.JLabel S;
    private javax.swing.JLabel Title;
    private javax.swing.JLabel Weapons;
    // End of variables declaration//GEN-END:variables
}
