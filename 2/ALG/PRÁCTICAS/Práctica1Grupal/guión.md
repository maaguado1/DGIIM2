Amparo Almohalla Santiago, Jose Javier Vilella García, María Aguado Martínez  y Javier Gómez Garalu.

# Análisis de Eficiencia de Algoritmos
## Índice:

1. Cálculos previos
	- Ejecución de los programas
	
	- Resultados y tablas
	
	- Gráficas
2. Eficiencia empírica
	- Comparación de los datos obtenidos
	- Algoritmos cuadráticos
	- Algoritmos logarítmicos
	- Algoritmos de ordenación
	- Algoritmo cúbico
	- Algoritmo exponencial
3. Eficiencia híbrida
  - Algoritmos cuadráticos
  	- Burbuja
  	- Selección
  	- Inserción
  
  - Algoritmos logarítmicos
  	- Heapsort
  	- Quicksort
  	- Mergesort
  
  - Algoritmo cúbico
  	- Floyd
  - Algoritmo exponencial
  	- Hanoi
4. Coeficiente de correlación y umbral

***

> ## Cálculos previos



**EJECUCIÓN DE LOS PROGRAMAS**

Antes de comenzar con la ejecución de los distintos programas, hemos creado un script que automatiza la ejecución de cada programa. El código es el siguiente:

	#!/bin/bash
	contador=1000
	iteracion = 0
	echo "" >> salidaBurbuja.dat
	while [ $contador -le 25000 ]
	do
		echo " Tamaño $contador " >> salidaBurbuja.dat
		iteracion = 0
		while [ $iteracion -le 10 ]
		do 
			./burbuja $contador >> salidaBurbuja.dat
			((iteracion+=1))
		done
		((contador+=1000))
	done
	cat salidaBurbuja.dat

De esta forma, estamos ejecutando el programa 10 veces para cada tamaño, para tener 10 tiempos distintos según los datos aleatorios para los que se haya ejecutado este programa. A estos 10 tiempos, a priori distintos, para cada tamaño, le calcularemos luego la media, para obtener así resultados mucho más próximos a la realidad.

Para cada programa modificamos la macro, escribiendo salidaNombrePrograma.dat y ./NombrePrograma. Así, al final de todas las ejecuciones, tendremos un fichero distinto para cada programa con sus respectivos resultados. Además también hemos modificado los códigos de los programas, para que calculen el tiempo de ejecución a través de la biblioteca c_time. Así, el código resultante para el programa de burbuja, llamado burbuja2, es el siguiente:

	/**
	   @file Ordenación por burbuja
	   @name burbuja2.cpp
	*/


	#include <iostream>
	using namespace std;
	#include <ctime>
	#include <cstdlib>
	#include <climits>
	#include <cassert>


	inline static
	void burbuja(int T[], int num_elem);
	
	static void burbuja_lims(int T[], int inicial, int final);
	
	inline void burbuja(int T[], int num_elem)
	{
	  burbuja_lims(T, 0, num_elem);
	};


	static void burbuja_lims(int T[], int inicial, int final)
	{
	  int i, j;
	  int aux;
	  for (i = inicial; i < final - 1; i++)
	    for (j = final - 1; j > i; j--)
	      if (T[j] < T[j-1])
		{
		  aux = T[j];
		  T[j] = T[j-1];
		  T[j-1] = aux;
		}
	}


	int main(int argc, char* argv[])
	{
	  int n = atoi(argv[1]);
	  clock_t tantes, tdespues;
	
	  int * T = new int[n];
	  assert(T);
	
	  srandom(time(0));


Es decir, en cada programa hay que asegurarse de que se siga el siguiente esquema:

	int main(int argc, char* argv[]){
		int n=atoi(argv[1]);    // n ya nos lo pasa la macro como argumento
		clock_t tantes, tdespues;
		
		//Generación de los datos aleatorios
		
		tantes=clock();
		Algoritmo(n);
		tdespues=clock();
		
		cout << " " << (double)(tdespues - tantes) / CLOCKS_PER_SEC;
		
		// Finalización del programa
	}

En algunos programas no ha sido necesario incorporar estas modificaciones, porque ya lo tenían en cuenta. Tras la ejecución de la macro, obtenemos el siguiente fichero con los datos que utilizaremos posteriormente:

	// salidaBurbuja2.dat
	Tamaño 1000 0.002454 0.002664 0.002293 0.002127 0.002028 0.001944 0.001963 0.001944 0.002054 0.002025
	Tamaño 2000 0.007768 0.007725 0.007749 0.007774 0.007736 0.007647 0.007702 0.007704 0.007837 0.007847
	Tamaño 3000 0.018314 0.018143 0.018088 0.017983 0.017979 0.018292 0.018167 0.018273 0.018174 0.018219
	Tamaño 4000 0.034299 0.034223 0.03426 0.034058 0.034482 0.034103 0.034089 0.0342 0.034171 0.034501
	Tamaño 5000 0.056076 0.056048 0.056102 0.056545 0.05584 0.055909 0.056758 0.056384 0.056339 0.056255
	Tamaño 6000 0.084751 0.085653 0.085265 0.084177 0.084772 0.085315 0.085058 0.085058 0.084224 0.084953
	Tamaño 7000 0.119245 0.120079 0.120338 0.118306 0.120027 0.120453 0.119965 0.120458 0.121283 0.120203
	Tamaño 8000 0.160599 0.163395 0.16141 0.161353 0.162264 0.161823 0.162273 0.161277 0.161369 0.164132
	Tamaño 9000 0.208522 0.209704 0.209654 0.209494 0.210175 0.20804 0.208272 0.207995 0.20917 0.207743
	Tamaño 10000 0.266891 0.264332 0.262773 0.263422 0.266119 0.264638 0.264571 0.263497 0.262592 0.266214
	Tamaño 11000 0.322439 0.324907 0.323866 0.323387 0.321588 0.322939 0.323811 0.324556 0.325781 0.324371
	Tamaño 12000 0.392891 0.393149 0.389519 0.388655 0.389204 0.391569 0.387528 0.392154 0.39133 0.392516
	Tamaño 13000 0.462069 0.460438 0.462497 0.461747 0.46458 0.467152 0.464203 0.465225 0.464071 0.462821
	Tamaño 14000 0.543916 0.544353 0.54201 0.545479 0.544798 0.545586 0.545933 0.54669 0.540677 0.54667
	Tamaño 15000 0.630592 0.627605 0.628334 0.626425 0.626297 0.626858 0.624865 0.628109 0.630383 0.638732
	Tamaño 16000 0.721512 0.720854 0.726693 0.722017 0.722899 0.721507 0.726643 0.726736 0.726655 0.720469
	Tamaño 17000 0.827373 0.822485 0.823788 0.82445 0.824822 0.821991 0.824305 0.819861 0.821148 0.820383
	Tamaño 18000 0.931485 0.932082 0.93248 0.93359 0.923487 0.925978 0.923197 0.934974 0.92516 0.932198
	Tamaño 19000 1.03285 1.0276 1.03231 1.03815 1.03339 1.03839 1.03873 1.03405 1.03369 1.03892
	Tamaño 20000 1.16114 1.15355 1.16044 1.15727 1.14966 1.1537 1.1529 1.15011 1.15202 1.15532
	Tamaño 21000 1.27586 1.27562 1.27612 1.28441 1.28606 1.27931 1.27459 1.2866 1.2847 1.28011
	Tamaño 22000 1.4125 1.39976 1.40347 1.40125 1.40579 1.40934 1.40581 1.4122 1.40566 1.39917
	Tamaño 23000 1.54635 1.53822 1.54954 1.54917 1.5472 1.55346 1.5546 1.53599 1.5542 1.54197
	Tamaño 24000 1.68725 1.69722 1.69071 1.6943 1.68456 1.68819 1.68138 1.68248 1.68867 1.67765
	Tamaño 25000 1.84231 1.84058 1.83886 1.82739 1.84017 1.83556 1.8352 1.83953 1.84378 1.83312

Y así para cada programa de nuestra práctica. Es un procedimiento repetitivo en el cual, como ya hemos dicho antes, tan sólo hay que modificar los nombres del fichero de salida y del programa a ejecutar en la macro. 

**RESULTADOS Y TABLAS**

En los ficheros de salida, como el que se puede observar en el apartado anterior, obtenemos para cada tamaño 10 tiempos de ejecución diferentes, dependientes de los datos aleatorios que se han generado para cada iteración. A estos resultados, les calcularemos la media geométicra, y estas medias serán los datos con los que trabajremos, pudiendo así tener una percepción mucho más realista.  De este modo, para cada programa hemos obtenido una tabla como la siguiente:

![medias](//home/maguado/Escritorio/proyecto/fotos/fotos_amparo/medias.png)



A partir de este momento, nos referiremos a las medias de todos estos resultados (última columna de las tablas) como a los `tiempos de ejecución para cada tamaño`.

**GRÁFICAS**

En el guión de la práctica también se nos pide que representemos en gráficas todos los resultados obtenidos tras la ejecución de los diferentes programos. Para ello utilizaremos `gnuplot`, programa al que recurriremos más tarde de nuevo para calcular la eficiencia híbrida. Para ello, en la línea de comandos y ya dentro de gnuplot, ejecutamos:

	gnuplot> plot ’salida.dat’ title ’Eficiencia XXX’ with points
	gnuplot> set xlabel "Tamaño"
	gnuplot> set ylabel "Tiempo (seg)"
	gnuplot> set output "fichero.png" (o "fichero.pdf")

donde "salida.dat" son los respectivos ficheros que contienen los datos resultantes de la ejecución de los programas. Todos estos ficheros tienen que tener la siguiente forma:

	100 0
	5100 0.53
	10100 2.12
	15100 4.72
	20100 8.39
	25100 13.11
	30100 18.73
	35100 25.4
	....

La primera columna es el tamaño n para el que ejecutamos el programa, representado en el eje x de nuestras gráficas, y la segunda columna son los distintos tiempos de ejecución. Con este procedimiento hemos obtenido todas las gŕaficas de nuestra memoria.


## 1. Eficiencia empírica

Ha llegado el momento de comprobar que el estudio realizado previamente acerca de la eficiencia teórica de nuestros algoritmos (dicho estudio no es objeto de esta práctica, si no que partimos ya con la eficiencia teórica dada para cada programa) se corresponde con los resultados que obtenemos en la realidad. Para ello realizamos todos los pasos descritos previamente en el apartado anterior para cada programa. Mediremos la eficiencia empírica sobre los recursos empleados (en términos de tiempo) para cada tamaño dado de las entradas. En el caso de los algoritmos de ordenación, el tamaño viene dado por el número de componentes del vector a ordenar. En otro tipo de problemas, como es el caso del algoritmo para resolver el problema de las torres de Hanoi, el tamaño se corresponde con el valor del entero que representa el número de discos. Para el caso del algoritmo de Floyd, que calcula los caminos mínimos entre todos los pares de nodos en un grafo dirigido, el tamaño es el número de nodos del grafo.



En primer lugar, analizaremos los resultados de los miembros del grupo obtenidos para cada algoritmo.

- Algoritmos cuadráticos

  - Algoritmo de la burbuja:

    <img src="TODOS/burbaja_todos.png" alt="burbaja_todos" style="zoom:33%;" />

    <img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/burbuja.png" alt="burbuja" style="zoom:67%;" />

  - Algoritmo por inserción:

    <img src="/home/maguado/Escritorio/ACPr2/TODOS/insercion_todos.png" alt="insercion_todos" style="zoom:33%;" />

    <img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/insercion.png" alt="insercion" style="zoom:67%;" />

  - Algoritmo por selección:

    <img src="//home/maguado/Escritorio/ACPr2/TODOS/seleccion_todos.png" alt="seleccion_todos" style="zoom:33%;" />
    
    <img src="//home/maguado/Escritorio/proyecto/fotos/comparacion/seleccion.png" alt="seleccion" style="zoom: 67%;" />

- Algoritmos logarítmicos

  - Mergesort:

    <img src="//home/maguado/Escritorio/ACPr2/TODOS/mergesort_todos.png" alt="mergesort_todos" style="zoom:33%;" />

    

  - Heapsort:

    <img src="/home/maguado/Escritorio/ACPr2/TODOS/heapsort_todos.png" alt="heapsort_todos" style="zoom:33%;" />

  <img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/heapsort.png" alt="heapsort" style="zoom:67%;" />

  - Quicksort:

    <img src="/home/maguado/Escritorio/ACPr2/TODOS/quicksort_todos.png" alt="quicksort_todos" style="zoom:33%;" />
    
    <img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/quicksort.png" alt="quicksort" style="zoom:67%;" />

  

- Algoritmo cúbico

  - Floyd

    <img src="/home/maguado/Escritorio/ACPr2/TODOS/floyd_todos.png" alt="floyd_todos" style="zoom:33%;" />
    
    <img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/floyd.png" alt="floyd" style="zoom:67%;" />

- Algoritmo exponencial

  - Hanoi
  
    <img src="/home/maguado/Escritorio/ACPr2/TODOS/hanoi_todos.png" alt="hanoi_todos" style="zoom:33%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/comparacion/hanoidef.png" alt="hanoidef" style="zoom: 67%;" />

Por tanto, procedemos a analizar, en cada caso, el más eficiente.

#### Algoritmos cuadráticos

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/burbuja.png" alt="burbuja" style="zoom:50%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/insercion.png" alt="insercion" style="zoom:50%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/seleccion.png" alt="seleccion" style="zoom:50%;" />

#### Algoritmos O(nlog(n))

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/heapsort.png" alt="heapsort" style="zoom:50%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/mergesort.png" alt="mergesort" style="zoom:50%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/quicksort.png" alt="quicksort" style="zoom:50%;" />

#### Algoritmos de ordenación

![ordenacion](/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/ordenacion.png)

#### Algoritmo cúbico (Floyd)

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/floyd-cubico.png" alt="floyd-cubico" style="zoom:67%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/floyd.png" alt="floyd" style="zoom: 50%;" />

#### Algoritmo exponencial (Hanoi)

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/hanoi-exponencial.png" alt="hanoi-exponencial" style="zoom:67%;" />

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/hanoi.png" alt="hanoi" style="zoom:50%;" />

---

## 2. Eficiencia híbrida

------

  La eficiencia híbrida consiste en ajustar nuestro conjunto de puntos, resultantes de la eficiencia empírica, a una función. Esta función no es una cualquiera, si no que es la resultante de nuestro estudio teórico, es decir, la f(x) a la que nos referimos cuando decimos que un algoritmo es O(f(x)). Así, ajustaremos los algoritmos de selección, burbuja e inserción a una función cuadrática $$ f(x) = a~2~*x^2+a~1~*x+a~0~$$
  Floyd lo ajustaremos a la siguiente función:
  $$ g(x) = a~3~*x^3+a~2~*x^2+a~1~*x+a~0~$$
  Mergesort, quicksort y hesapsort los ajustaremos a una función como la siguiente:
  $$h(x)=a*x*log(x)+b$$
  Hanoi lo ajustaremos a:
  $$t(x) = a*2^x + b$$

  Al ajustar a los puntos obtenidos, obtenemos las constantes ocultas (valores de los parámetros). Así, podremos saber cuánto tiempo aproximadamente utilizará el algoritmo para cualquier entrada de tamaño x. Para no realizar este ajuste tediosamente, utilizaremos la heramienta `gnuplot`. Para ello, en la línea de comandos, y ya dentro de gnuplot, ejecutaremos las siguientes órdenes:

	gnuplot> f(x) = a0*x*x+a1*x+a2
	gnuplot> fit f(x) ’salida.dat’ via a0,a1,a2
	gnuplot> plot ’salida.dat’, f(x) title ’Curva ajustada’

De la salida de este ajuste para cada algoritmo (el cual podremos ver explícitamente en los apartados posteriores), principalmente nos interesa el apartado *Final Set of Parameters*, que son las respectivas constantes ocultas resultantes.





**Algoritmos cuadŕaticos**

##### 1.Burbuja



	After 12 iterations the fit converged.
	final sum of squares of residuals : 5.22143e-05
	rel. change during last iteration : -2.84424e-08
	
	degrees of freedom    (FIT_NDF)                        : 22
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.00154058
	variance of residuals (reduced chisquare) = WSSR/ndf   : 2.37338e-06
	
	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 3.16531e-09      +/- 6.641e-12    (0.2098%)
	a1              = -5.88589e-06     +/- 1.779e-07    (3.022%)
	a2              = 0.00627741       +/- 0.001004     (15.99%)
	
	correlation matrix of the fit parameters:
		        a0     a1     a2
	a0              1.000 
	a1             -0.971  1.000 
	a2              0.774 -0.884  1.000 

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/burbuja-ajuste.png" alt="burbuja-ajuste" style="zoom:67%;" />

$$f(x)=(3.16531e-09)*x^2+(-5.88589e-06)*x+0.00627741 $$

##### 2.Selección

~~~
	After 6 iterations the fit converged.
	final sum of squares of residuals : 3.55354e-05
	rel. change during last iteration : -7.88811e-12

	degrees of freedom    (FIT_NDF)                        : 22
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.00127092
	variance of residuals (reduced chisquare) = WSSR/ndf   : 1.61524e-06

	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 1.29423e-09      +/- 5.478e-12    (0.4233%)
	a1              = 1.13456e-07      +/- 1.467e-07    (129.3%)
	a2              = -0.000196507     +/- 0.0008279    (421.3%)

	correlation matrix of the fit parameters:
		        a0     a1     a2
	a0              1.000 
	a1             -0.971  1.000 
	a2              0.774 -0.884  1.000 
~~~

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/seleccion-ajuste.png" alt="seleccion-ajuste" style="zoom:67%;" />

$$f(x)=(1.29423e-09)*x^2+(1.13456e-07)*x-0.000196507$$

##### 3.Inserción

~~~
	After 5 iterations the fit converged.
	final sum of squares of residuals : 0.00100818
	rel. change during last iteration : -8.05918e-08

	degrees of freedom    (FIT_NDF)                        : 22
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.00676952
	variance of residuals (reduced chisquare) = WSSR/ndf   : 4.58264e-05

	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 1.08661e-09      +/- 2.918e-11    (2.685%)
	a1              = -1.25013e-07     +/- 7.816e-07    (625.2%)
	a2              = -0.00063727      +/- 0.00441      (692%)

	correlation matrix of the fit parameters:
		        a0     a1     a2
	a0              1.000 
	a1             -0.971  1.000 
	a2              0.774 -0.884  1.000
~~~

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/insercion.png" alt="insercion" style="zoom:67%;" />

$$f(x)=(1.08661e-09)*x^2+(-1.25013e-07)*x-0.00063727$$





**Algoritmos O(n * log(n))**

##### 1. Mergesort



~~~
After 5 iterations the fit converged.
final sum of squares of residuals : 8.24798e-07
rel. change during last iteration : -2.19515e-11

degrees of freedom    (FIT_NDF)                        : 23
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.000189369
variance of residuals (reduced chisquare) = WSSR/ndf   : 3.58608e-08

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a0              = 2.02318e-08      +/- 5.072e-10    (2.507%)
a1              = -0.000100854     +/- 7.404e-05    (73.41%)

correlation matrix of the fit parameters:
                a0     a1     
a0              1.000 
a1             -0.859  1.000 
~~~
<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/ajuste-mergesort.png" alt="ajuste-mergesort" style="zoom:67%;" />

$$f(x)=(2.02318e-08)*x*log(x)-0.000100854$$



 ##### 2.Quicksort

	After 5 iterations the fit converged.
	final sum of squares of residuals : 5.35999e-08
	rel. change during last iteration : -6.67918e-14
	
	degrees of freedom    (FIT_NDF)                        : 23
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 4.82745e-05
	variance of residuals (reduced chisquare) = WSSR/ndf   : 2.33043e-09
	
	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 1.23833e-08      +/- 1.293e-10    (1.044%)
	a1              = 3.56109e-05      +/- 1.887e-05    (53%)
	
	correlation matrix of the fit parameters:
		        a0     a1     
	a0              1.000 
	a1             -0.859  1.000
<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/ajuste-quicksort.png" alt="ajuste-quicksort" style="zoom:67%;" />

$$f(x)=(1.23833e-08)*x*log(x)+3.56109e-05 $$



##### 3. Heapsort

	After 5 iterations the fit converged.
	final sum of squares of residuals : 2.19123e-07
	rel. change during last iteration : -1.64504e-12
	
	degrees of freedom    (FIT_NDF)                        : 23
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 9.76068e-05
	variance of residuals (reduced chisquare) = WSSR/ndf   : 9.52709e-09
	
	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 1.69543e-08      +/- 2.614e-10    (1.542%)
	a1              = 0.00012929       +/- 3.816e-05    (29.52%)
	
	correlation matrix of the fit parameters:
		        a0     a1     
	a0              1.000 
	a1             -0.859  1.000 



<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/ajuste-heapsort.png" alt="ajuste-heapsort" style="zoom:67%;" />

$$f(x)=(1.69543e-08)*x*log(x)+0.00012929 $$






**Algoritmo cúbico - Floyd**
~~~
After 11 iterations the fit converged.
final sum of squares of residuals : 9.08107e-06
rel. change during last iteration : -2.76093e-14

degrees of freedom    (FIT_NDF)                        : 21
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.000657595
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.32432e-07

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a3              = 5.60277e-09      +/- 4.504e-10    (8.04%)
a2              = -5.4282e-08      +/- 1.779e-07    (327.8%)
a1              = 1.19929e-05      +/- 2.012e-05    (167.7%)
a0              = -0.00028508      +/- 0.000616     (216.1%)

correlation matrix of the fit parameters:
                a3     a2     a1     a0     
a3              1.000 
a2             -0.987  1.000 
a1              0.926 -0.973  1.000 
a0             -0.719  0.795 -0.898  1.000 
~~~

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/floyd-ajuste.png" alt="floyd-ajuste" style="zoom:67%;" />

$$f(x)=(5.60277e-09)*x^3+(-5.4282e-08)*x^2+(1.19929e-05)*x-0.00028508 $$






**Algoritmo exponencial - Hanoi**
~~~
After 4 iterations the fit converged.
final sum of squares of residuals : 4.30942e-05
rel. change during last iteration : -5.66429e-10

degrees of freedom    (FIT_NDF)                        : 24
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.00134
variance of residuals (reduced chisquare) = WSSR/ndf   : 1.79559e-06

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a0              = 6.19782e-09      +/- 1.839e-11    (0.2966%)
a1              = 1.19929e-05      +/- 0.0002794    (2330%)

correlation matrix of the fit parameters:
                a0     a1     
a0              1.000 
a1             -0.340  1.000
~~~
<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/ajuste-hanoi.png" alt="ajuste-hanoi" style="zoom:67%;" />

$$f(x)=(6.19782e-09 )*2^x+1.19929e-05$$

---
## Otras pruebas/home/maguado.

En este último apartado de la práctica vamos a realizar distintas pruebas para analizar la variación de la eficiencia empírica en función de parámetros externos. Para ello vamos a compilar y ejecutar uno de nuestros algoritmos con optimización, y lo compararemos con sus resultados sin optimización. También estudiaremos la eficiencia híbrida de dos programas, ajustándolos a funciones que, a priori, no corresponden con su eficiencia, tras el correspondiente estudio teórico. Veamos estos resultados:

**Ajuste Mergesort a lineal**
Vamos a realizar de nuevo el estudio de la eficiencia híbrida de nuestro algoritmo Mergesort, pero esta vez ajustando los resultados a una función lineal:$$f(x)=a*x+b$$
Ejecutando las siguientes órdenes en gnuplot:
~~~
gnuplot> f(x)=a1*x+a0
gnuplot> fit f(x) "salidaMergesort.dat" via a1,a0
~~~
obtenemos los siguientes resultados:
~~~
After 1 iterations the fit converged.
final sum of squares of residuals : 1.55085e-06
rel. change during last iteration : -2.46611e-10

degrees of freedom    (FIT_NDF)                        : 23
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.000259669
variance of residuals (reduced chisquare) = WSSR/ndf   : 6.74281e-08

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a0              = 1.69539e-08      +/- 0.0001071    (6.315e+05%)
a1              = 1.92504e-07      +/- 7.202e-09    (3.741%)

correlation matrix of the fit parameters:
                a0     a1     
a0              1.000 
a1             -0.874  1.000 
~~~
Dentro también del gnuplot, ejecutamos:
~~~
	gnuplot> plot "salidaMergesort.dat",f(x) title "Ajuste lineal Mergesort"
	gnuplot> set xlabel "TamaÃ±o"
	gnuplot> set ylabel "Tiempo (seg)"
	gnuplot> replot
	gnuplot> set output "lineal-mergesort.png"
~~~
y obtenemos así la siguiente gráfica:

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/lineal-mergesort.png" alt="lineal-mergesort" style="zoom:67%;" />

$$f(x)=(1.92504e-07)*x+1.69539e-08$$



**Ajuste Heapsort a cuadrática**
Vamos a realizar el mismo proceso que en el apartado anterior, pero esta vez ajustando los resultados de Heapsort a una función cuadrática: $$f(x)=a*x^2+b*x+c$$
Ejecutando las siguientes órdenes en gnuplot:
~~~
gnuplot> f(x)=a2*x*x+a1*x+a0
gnuplot> fit f(x) "salidaMergesort.dat" via a2,a1,a0
~~~
obtenemos los siguientes resultados:
~~~
After 7 iterations the fit converged.
final sum of squares of residuals : 1.62027e-07
rel. change during last iteration : -1.28796e-06

degrees of freedom    (FIT_NDF)                        : 22
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 8.58189e-05
variance of residuals (reduced chisquare) = WSSR/ndf   : 7.36488e-09

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a2              = -1.21938e-14     +/- 3.699e-13    (3034%)
a1              = 1.74256e-07      +/- 9.908e-09    (5.686%)
a0              = 1.63527e-08      +/- 5.59e-05     (3.419e+05%)

correlation matrix of the fit parameters:
                a2     a1     a0     
a2              1.000 
a1             -0.971  1.000 
a0              0.774 -0.884  1.000 
~~~
Dentro también del gnuplot, ejecutamos:
~~~
gnuplot> plot "salidHeapsort.dat",f(x) title "Ajuste cuadrática Heapsort"
gnuplot> set xlabel "Tamaño"
gnuplot> set ylabel "Tiempo (seg)"
gnuplot> replot
gnuplot> set output "cuadratica-heapsort.png"
~~~
y obtenemos así la siguiente gráfica:

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/cuadratica-heapsort.png" alt="cuadratica-heapsort" style="zoom:67%;" />

$$f(x)=(-1.21938e-14)*x^2+(1.74256e-07)*x+1.63527e-08$$



**Selección con optimización**
Otra prueba que vamos a hacer es compilar el programa de ordenación mediante selección con optimización. Para ello compilamos el programa del siguiente modo:
~~~
g++ -O2 -o seleccionOpti seleccion.cpp
chmod a+x seleccionOpti
~~~
Y ejecutamos la macro con ./seleccionOpti. Obtenemos el siguiente fichero con los datos de salida:
~~~
//salidaSeleccionOpti.dat
1 -1e-08
2 0
3 -1e-08
4 2e-08
5 3e-08
6 3e-08
7 5e-08
8 5e-08
9 5e-08
10 6e-08
11 1e-07
12 1e-07
13 9e-08
14 1.4e-07
15 1.6e-07
16 1.9e-07
17 2.2e-07
18 2e-07
19 2.7e-07
20 2.9e-07
21 3.2e-07
22 2.9e-07
23 3.9e-07
24 4.1e-07
25 4.5e-07
26 3.9e-07
~~~


La gráfica correspondiente a estos datos es la siguiente:

<img src="/home/maguado/Escritorio/proyecto/fotos/fotos_amparo/seleccionOpti.png" alt="seleccionOpti" style="zoom:67%;" />

Podemos observar fácilmente que los tiempos de ejecución con respecto a la ejecución del programa de ordenación por selección sin optimización son mucho más próximos a 0. Sin embargo, la gráfica es mucho más irregular, lo que hace mucho más difícil poder predecir el comportamiento del programa para un determinado tamaño n.



------

## Coeficiente de correlación

------


En este apartado estudiaremos como de bueno son los ajustes que hemos visto anteriormente para cada algoritmo, así como un ejemplo de cálculo del valor umbral entre un algoritmo O(n²) y otro O(nlogn), como serán el algoritmo de inserción y el algoritmo mergesort, respectivamente.

Usaremos dos fórmulas, explicadas a continuación:

En primer lugar,  $\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}$ donde $\sigma^2_{ey}=\sum^{k}_{i=1}\sum^{p}_{j=1}f_{ij}(\hat{y}_{j}-\bar{y})^2$, es la varianza de regresión (nos indica en qué medida queda explicada la variable dependiente mediante el modelo estimado).

El coeficiente de correlación ($\eta^2_{Y/X}$), que es lo que conviene analizar en este caso, viene dado por:

$\eta^2_{Y/X} = \frac{\sigma^2_{ey}}{\sigma^2_{y}}= 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}$, siendo ésta última igualdad la segunda fórmula.



Comenzaremos viendo la bondad de los ajustes para los algoritmos del tipo O(n²):

##### Algoritmo de burbuja:

Hemos utilizado una tabla como la siguiente para calcular los datos que necesitamos para obtener la razón  de correlación, así como las siguientes fórmulas, en cada uno de los algoritmos: 

<img src="/home/maguado/Descargas/BONDAD/burbuja_tabla.png" alt="burbuja_tabla" style="zoom: 33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$8,12969830700309+2,37338*10⁻⁶  = 8,12970068
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (2,37338*10⁻⁶)/8,12970068 = 0,9999997081

Vemos que como el valor de dicho coeficiente es prácticamente 1 el ajuste es perfecto, pues la información que nos proporciona dicho coeficiente es que cuanto más proximo a 1 esté, mejor es el ajuste(mejor se apróxima dicha función a la nube de puntos) y, por el contrario, cuanto más alejado de 1 esté peor es dicho ajuste.

##### Algoritmo de selección:

<img src="/home/maguado/Descargas/BONDAD/seleccion_tabla.png" alt="seleccion_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$3,730016254 + 1,61524*10⁻⁶  = 3,730017869
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (1,61524*10⁻⁶)/3,730017869 = 0,9999959217

En este caso ocurre lo mismo que en el caso anterior, como el valor de dicho coeficiente es prácticamente 1, el ajuste es muy bueno.

##### Algoritmo de inserción:

<img src="/home/maguado/Descargas/BONDAD/insercion_tabla.png" alt="insercion_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,396012474 + 4,58264 * 10⁻⁵ = 0,3960583004
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (4,58264 * 10⁻⁵)/0,3960583004 = 0,9998842938

Ocurre lo mismo que en los dos casos anteriores, el coeficiente de correlación es prácticamente 1 y el ajuste  es magnífico.

Pasamos a estudiar lo que ocurre con los algoritmos del tipo O(nlogn):

##### Algoritmo mergesort:

<img src="/home/maguado/Descargas/BONDAD/mergesort_tabla.png" alt="mergesort_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,000211221 + 3,58608*10⁻⁸ = 0,0002112686
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (3,58608*10⁻⁸)/0,0002112686 = 0,9998302502

Como se puede apreciar, el valor del coeficiente de correlación es muy próximo a 1, luego, como hemos dicho antes, el ajuste es bastante bueno.

En este caso en concreto, hemos aproximado la nube de puntos de dicho algoritmo con un ajuste lineal y hemos obtenido lo siguiente:

##### Algoritmo mergesort con ajuste lineal

<img src="/home/maguado/Descargas/BONDAD/mergesort_lineal_tabla.png" alt="mergesort_lineal_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,000209745708773 + 6,74281*10⁻⁸ = 0,00020981313
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (6,74281*10⁻⁸)/0,00020981313 = 0,9996786278

Como se puede apreciar, el ajuste lineal es muy bueno, pero aun así, por poca diferencia el ajuste nlogn sigue aproximandose mejor a la nube de puntos de dicho algoritmo.

##### Algoritmo quicksort:

<img src="/home/maguado/Descargas/BONDAD/quicksort_tabla.png" alt="quicksort_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$8,66111*10⁻⁵ + 2,33043*10⁻⁹ = 0,00008661343
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (2,33043*10⁻⁹)/0,00008661343 = 0,9999730939

En este caso, el ajuste es también casi perfecto, pues el coeficiente de correlación es prácticamente 1.

##### Algoritmo heapsort

<img src="/home/maguado/Descargas/BONDAD/heapsort_tabla.png" alt="heapsort_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,000171241 + 9,52709*10⁻⁹ = 0,00017125052
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$1 - (9,52709*10⁻⁹)/0,00017125052 = 0,9999443675

Ocurre lo mismo que en el caso anterior, el ajuste es muy bueno.

En este caso, también hemos aproximado dicho algoritmo con un ajuste cuadrático y hemos obtenido los siguientes resultados:

##### Algoritmo heapsort con ajuste cuadrático:

<img src="/home/maguado/Descargas/BONDAD/heapsort_cuadratico_tabla.png" alt="heapsort_cuadratico_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,000171622545503 + 7,36488*10⁻⁹ = 0,00017162991
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (7,36488*10⁻⁹)/0,00017162991 = 0,9999570886

Cómo se puede observar, el ajuste cuadrático se aproxima muy bien a la nube de puntos de dicho algoritmo, pero como ocurre con el algoritmo de mergesort, por una diferencia pequeña, el ajuste nlogn se aproxima mejor a los datos que el ajuste cuadrático, pues el coeficiente de correlación del ajuste nlogn está más próximo a 1 que el del ajuste cuadrático.

##### Algoritmo  de floyd

<img src="/home/maguado/Descargas/BONDAD/floyd_tabla.png" alt="floyd_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,031710693 + 4,32432*10⁻⁷ = 0,03171112543
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$ 1 - (4,32432*10⁻⁷)/0,03171112543 = 0,9999863634

Cómo se puede apreciar, el ajuste cúbico para este algoritmo es casi perfecto, pues la razón de correlación es prácticamente 1.

Finalmente:

##### Algoritmo de hanoi

<img src="/home/maguado/Descargas/BONDAD/hanoi_tabla.png" alt="hanoi_tabla" style="zoom:33%;" />

$\sigma^2_{y}= \sigma^2_{ey}+ \sigma ^2_{ry}=$0,231694443 + 1,79559*10⁻⁶ = 0,2316962386
$\eta^2_{Y/X} = = 1- \frac{\sigma ^2_{ry}}{\sigma ^2_{y}}=$1 - (1,79559*10⁻⁶)/0,2316962386 = 0,999992250

En cuanto a este algoritmo, se puede ver también que el ajuste exponencial 2^n es magnífico, pues el coeficiente de correlación esta muy próximo a 1.

------

## Umbral

------

En relación con el algoritmo mergesort respecto a los algoritmos de ordenación de orden cuadrático , debemos destacar la existencia de un valor umbral, a partir del cual "merece la pena" el uso del mismo, es decir, a partir de un tamaño umbral es más rápido que los otros.

De hecho, esto se refleja en el código utilizado en la práctica, donde existe una constante, cuyo valor debe ser menor que el tamaño del vector para que se le aplique el algoritmo recursivo, en caso contrario el algoritmo usado será el de inserción.



    const int UMBRAL_MS = 100;
    
    static void mergesort_lims(int T[], int inicial, int final)
    {
      if (final - inicial < UMBRAL_MS)
        {
          insercion_lims(T, inicial, final);
        } else {
          int k = (final - inicial)/2;
    
          int * U = new int [k - inicial + 1];
          assert(U);
          int l, l2;
          for (l = 0, l2 = inicial; l < k; l++, l2++)
    	U[l] = T[l2];
          U[l] = INT_MAX;
    
          int * V = new int [final - k + 1];
          assert(V);
          for (l = 0, l2 = k; l < final - k; l++, l2++)
    	V[l] = T[l2];
          V[l] = INT_MAX;
    
          mergesort_lims(U, 0, k);
          mergesort_lims(V, 0, final - k);
          fusion(T, inicial, final, U, V);
          delete [] U;
          delete [] V;
        };
    }

Podemos ver esto empíricamente, con los ajustes realizados en el apartado 2, ejecutamos en gnuplot:

	gnuplot> f(x) = a0*x*x+a1*x+a2
	gnuplot> fit f(x) ’salidaInsercion.dat’ via a0,a1,a2
	gnuplot> g(x) = a*x*log(x)+b
	gnuplot> fit g(x) 'salidaMergesort.dat' via a,b
	gnuplot> plot [500:3000] g(x) title 'Mergesort', f(x) title 'Insercion'

Obteniendo como resultado de la ejecución:
<img src="/home/maguado/Descargas/WhatsApp Image 2020-03-16 at 18.02.27.jpeg" alt="WhatsApp Image 2020-03-16 at 18.02.27" style="zoom:50%;" />

Podemos "hacer zoom" alrededor del punto de corte de las funciones,

	gnuplot> plot [800:920] g(x) title 'Mergesort', f(x) title 'Insercion'

<img src="/home/maguado/Descargas/WhatsApp Image 2020-03-16 at 18.04.14.jpeg" alt="WhatsApp Image 2020-03-16 at 18.04.14" style="zoom:50%;" />


Podemos observar que el tamaño umbral está alrededor de 835, aunque esta información carece de mucho valor, ya que éste variará dependiendo de varios parámetros, por ejemplo de las prestaciones de cada ordenador.


Como conclusiones, podemos extraer que los algoritmos de burbuja, selección e inserción se pueden aproximan muy bien mediante un ajuste cuadrático; los algoritmos mergesort, quicksort y heapsort se ajustan de manera casi perfecta usando el ajuste nlogn, a pesar de que los ajustes lineal y cuadrático también se ajustan muy bien a las nubes de puntos de dichos algoritmos; el algoritmo de floyd se puede aproximar de manera muy buena mediante un ajuste cúbico; y, por último, el algoritmo de hanoi se puede aproximar mediante un ajuste exponencial 2^n obteniendo un ajuste bastante bueno.


