
/**
 *
 * @author maguado
 */
package deepspace;

import java.util.Random;
class Dice {
    
    private final float NHANGARSPPROB, NSHIELDSPROB, NWEAPONSPROB, FIRSTSHOTPROB, EXTRAEFFICIENCYPROB, ELEMENTPROB;
    
    private Random generator;
    
    Dice (){
        this.generator = new Random();
        this.NHANGARSPPROB = 0.25f;
        this.NSHIELDSPROB = 0.25f;
        this.NWEAPONSPROB = 0.33f;
        this.FIRSTSHOTPROB = 0.5f;
        this.EXTRAEFFICIENCYPROB=0.8f;
        this.ELEMENTPROB = 0.5f;
    }
    
    int initWithNHangars(){
        int valor=1;
        float prob = generator.nextFloat();
        if (prob < NHANGARSPPROB )
            valor = 0;
        return valor;
    }
    
    int initWithNWeapons(){
        int valor = 3;
        float prob = generator.nextFloat();
        if (prob < NWEAPONSPROB){
            valor = 1;
        }
        else if (prob < (NWEAPONSPROB*2)){
            valor =2;
        }
        
        return valor;
        
    }
    
    int initWithNShields(){
        int valor=1;
        float prob = generator.nextFloat();
        if (prob < NSHIELDSPROB )
            valor = 0;
        return valor;
    }
    
    int whoStarts(int nPlayers){
        int start = generator.nextInt(nPlayers);
        return start;
    }
    
    GameCharacter firstShot(){
        GameCharacter attack = GameCharacter.ENEMYSTARSHIP;
        float prob = generator.nextFloat();
        if (prob < FIRSTSHOTPROB ){
            attack = GameCharacter.SPACESTATION;
        }
        return attack;
            
    }
    
    boolean spaceStationMoves (float speed){
        boolean valor=false;
        float prob = generator.nextFloat();
        if (prob < speed )
            valor = true;
        return valor;
    }
    
    
    boolean extraEfficiency(){
        float prob = generator.nextFloat();
        if (prob<= EXTRAEFFICIENCYPROB)
            return true;
        else
            return false;
    }
    
    boolean element(){
        float prob = generator.nextFloat();
        if (prob<= ELEMENTPROB)
            return true;
        else
            return false;
    }
    
}
