/*
 * Nucleo original
 */
#include "nucleo.h"

extern int R[];
extern int M;
extern int N;

inline void core()
{
    int i, j, x1, x2;

    for (j = 0; j < 40000; j++)
    {
        x1 = 0;
        x2 = 0;

        for (i = 0; i < 5000; i++)
        {
            x1 += 2 * s[i].a + j;
            x2 += 3 * s[i].b - j;
        }
        if (x1 < x2)
            R[j] = x1;
        else
            R[j] = x2;
    }
}
