// Bibliotecas
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>

#define ARCHIVO_FIFO "ComunicacionFIFO"
#define regla1(mode) ((((mode) & ~S_IFMT) & S_IRUSR) == S_IRUSR)

// Manejador de la señal (SIGPIPE) para cuando se escribe en FIFO no abierto
// aún para lectura ya que por defecto esta señal terminaría el proceso.
static void manejador (int signal){
  printf("Escribo en FIFO aún no abierto para lectura\n");
  return;
}

int main (int argc, char **argv){
  // Tratamiento de señales
  struct sigaction sa;
  sa.sa_handler=manejador; // Establece el handler a manejador
  sigemptyset(&sa.sa_mask);
  // Reiniciar las funciones que hayan sido interrumpidas por un manejador
  sa.sa_flags=SA_RESTART;
  if (sigaction(SIGPIPE, &sa, NULL)==-1){
    perror("Error en el manejador\n");
  }

  pid_t pid;
  int errno;

  // Unbuffered
  if (setvbuf(stdout,NULL,_IONBF,0)!=0){
    printf("Error en setvbuf\n");
  }

  // Creamos el cauce con nombre (FIFO) si no existe
  umask(0);
  mkfifo(ARCHIVO_FIFO,S_IRWXU);

  // Creamos el hijo
  if ((pid=fork())==-1){
    fprintf(stderr, "No se ha podido crear el hijo: %s\n",strerror(errno));
    exit(EXIT_FAILURE);
  }
  else if (pid!=0){ // Código del padre
    // Abrimos el FIFO
    int fd;
    if ((fd=open(ARCHIVO_FIFO,O_RDWR))==-1){
      perror("No se ha podido abrir el archivo FIFO\n");
      exit(EXIT_FAILURE);
    }

    int estado;
    // Leemos el directorio_actual
    DIR *directorio_actual;
    // Estructura dirent: entrada de directorio
    struct dirent *ed;
    struct stat atributos;
    const char *name=".";
    char *cadena;

    // Abrimos el directorio actual
    directorio_actual=opendir(name);
    if (directorio_actual==NULL){
      perror("No se ha podido abrir el directorio actual\n");
      exit(EXIT_FAILURE);
    }

    while((ed=readdir(directorio_actual))>0){
      sprintf(cadena,"%s/%s",name,ed->d_name);
      if (lstat(cadena,&atributos)==-1){
        printf("Error al intentar acceder a los atributos del archivo\n");
        perror("Error en lstat\n");
        exit(EXIT_FAILURE);
      }
      // Comprobamos si el archivo es regular
      if (S_ISREG(atributos.st_mode)){
        printf("Número de inodo: %ld UID del propietario: %d\n",atributos.st_ino,atributos.st_uid);
        // Escribimos el nombre del archivo regular en el FIFO
        if ((write(fd,ed->d_name,strlen(ed->d_name)+1))!=strlen(ed->d_name)+1){
            perror("Error en la escritura del nombre\n");
            exit(EXIT_FAILURE);
        }
        if (!(regla1(atributos.st_mode))){
          if (chmod(cadena,(atributos.st_mode & ~S_IFMT)|S_IRUSR)<0){
            perror("Error en chmod para archivo");
            exit(EXIT_FAILURE);
          }
        }
      }
    }

    if (close(fd)==-1){
      perror("Padre: Error al cerrar el archivo\n");
      exit(EXIT_FAILURE);
    }

    wait(&estado);

    exit(EXIT_SUCCESS);

  }
  else { // pid=0; Código del hijo
    struct stat sb;
    char *memoria;
    int fd2;

    if ((fd2=open(ARCHIVO_FIFO,O_RDWR))==-1){
      perror("No se ha podido abrir el archivo FIFO\n");
      exit(EXIT_FAILURE);
    }

    char archivo [500];
    char ch;
    int fdarchivo;

    while(read(fd2, &ch, sizeof(char))>0){
			archivo[0]=ch;
			int i=1;

			//Leemos nombre de archivp
			while(ch!='\0' && read(fd2, &ch, sizeof(char))>0){
				archivo[i]=ch;
				i++;
			}

      if ((fdarchivo=open(archivo,O_RDONLY))==-1){
        perror("Error al abrir el archivo\n");
        exit(EXIT_FAILURE);
      }

      // Bloqueamos el archivo antes de acceder al mismo
      struct flock cerrojo;

      // Cabecera de configuración del cerrojo
      cerrojo.l_type=F_RDLCK;
      cerrojo.l_whence=SEEK_SET;
      cerrojo.l_start=0; // Establecemos el cerrojo
      cerrojo.l_len=0; // como todo el fichero

      // Establecemos el cerrojo
      while (fcntl(fdarchivo, F_SETLK, &cerrojo)==-1){
        printf("Error al establecer el cerrojo\n");
      }

      if (fstat(fdarchivo,&sb)==-1){
        printf("Error al intentar acceder a los atributos del archivo FIFO\n");
        perror("Error en fstat\n");
        exit(EXIT_FAILURE);
      }

      // Proyectamos el archivo en memoria
      memoria=(char*)mmap(NULL,sb.st_size,PROT_READ,MAP_SHARED,fdarchivo,0);
      if (memoria==MAP_FAILED){
        perror("Falló el mapeo\n");
        exit(EXIT_FAILURE);
      }

      // Cerramos el descriptor de archivo
      if (close(fdarchivo)==-1){
        perror("Hijo: Error al cerrar el archivo\n");
        exit(EXIT_FAILURE);
      }

      // Mostramos el archivo completo
      printf("%s\n",memoria);
      // Eliminamos la proyección del archivo
      if (munmap(memoria,sb.st_size)==-1){
        perror("Error al cerrar la proyección\n");
        exit(EXIT_FAILURE);
      }

      // Una vez finalizado el trabajo, desbloqueamos el archivo entero
      cerrojo.l_type=F_UNLCK;
      cerrojo.l_whence=SEEK_SET;
      cerrojo.l_start=0;
      cerrojo.l_len=0;

      /*if (fcntl(fdarchivo, F_SETLKW, &cerrojo)==-1){
        perror("Desbloqueo del archvio\n");
      }*/

    }

    // Cerramos el descriptor de archivo
    if (close(fd2)==-1){
      perror("Hijo: Error al cerrar el archivo\n");
      exit(EXIT_FAILURE);
    }

    // Borramos el archivo FIFO
    if ((unlink(ARCHIVO_FIFO))==-1){
      perror("Hijo: Error al eliminar el FIFO\n");
      exit(EXIT_FAILURE);
    }
  }

}
