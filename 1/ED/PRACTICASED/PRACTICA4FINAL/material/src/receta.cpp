/**
 * @file receta.cpp
 * @brief implementación del TDA receta
 * @authors María Aguado y Amparo Almohalla
 */



#include <cassert>
#include <iostream>
#include <string>
#include "receta.h"
#include <vector>
#include <sstream>


using namespace std;


receta::receta(){
    code="";
    plato=0;
    nombre="";
    calorias=0;
    hc=0;
    grasas=0;
    proteinas=0;
    fibra=0;
}


receta::receta(const receta& orig){
    code=orig.getCode();
    plato=orig.getPlato();
    nombre=orig.getNombre();
    ings=orig.getIngredientes();
    calorias=orig.getCalorias();
    hc=orig.getHc();
    grasas=orig.getGrasas();
    proteinas=orig.getProteinas();
    fibra=orig.getFibra();
    ings = orig.getIngredientes();
    inst = orig.getInstrucciones();

}

void receta::setValores(const ingredientes& valores){
    ingrediente nuevo;
    float cal, hid, gra, pro, fib;


    for (receta::iterator ot=begin(); ot!=end(); ++ot){

        nuevo = valores.get((*ot).first);
        cal = ((*ot).second * nuevo.getCalorias())/100;
        calorias += cal;
        hid= ((*ot).second * nuevo.getHc())/100;
        hc += hid;
        gra=((*ot).second * nuevo.getGrasas())/100;
        grasas += gra;
        pro= ((*ot).second * nuevo.getProteinas())/100;
        proteinas += pro;
        fib=((*ot).second * nuevo.getFibra())/100;
        fibra += fib;
        
    }
}

float receta::razon(){

    float raz = getProteinas()/getHc();
    return raz;

}

receta& receta::operator=(const receta& rec){
    code=rec.getCode();
    plato=rec.getPlato();
    nombre=rec.getNombre();
    ings=rec.getIngredientes();
    calorias=rec.getCalorias();
    hc=rec.getHc();
    grasas=rec.getGrasas();
    proteinas=rec.getProteinas();
    fibra=rec.getFibra();
    ings = rec.getIngredientes();
    inst = rec.getInstrucciones();
    return *this;
}


bool receta::operator==(const receta& orig){
    bool igual=true;
    if ( plato !=orig.getPlato() || code != orig.getCode() || nombre!=orig.getNombre() ||ings!=orig.getIngredientes()||  calorias!=orig.getCalorias() || hc!=orig.getHc() || grasas!=orig.getGrasas() ||  proteinas!=orig.getProteinas()||  fibra!=orig.getFibra())
        igual=false;
    return igual;
}
   


std::ostream & operator<<(std::ostream & os, const receta & i){
   
   os << "\n" << i.getCode() << ";"<< i.getPlato() << ";" << i.getNombre();

   for (receta::const_iterator it = i.begin(); it != i.end(); ++it)
  {
    os << (*it).first << "\t" << (*it).second << "\t";

  }
   return os;

}




std::istream & operator>>(std::istream & is, receta & r){
    string aux, aux1="", aux2="";
    list<pair<string,unsigned int> > auxiliar;
    pair<string, unsigned int> pareja;

    getline(is,aux,';');
    r.setCode(aux);
    getline(is,aux,';');
    r.setPlato(atoi(aux.c_str()));
    getline(is,aux,';');
    r.setNombre(aux);


    getline(is,aux);
    for(unsigned int i=0; i<=aux.size(); i++){
        if(aux[i]>='0' && aux[i] <= '9')
            aux2 += aux[i];
        else if (aux[i]==';' || i==aux.size()){
            if(i == aux.size())
                aux2+=aux[i];   
            aux1=aux1.substr(0, aux1.size()-1);
            pareja=make_pair(aux1, atoi(aux2.c_str()));
            auxiliar.push_back(pareja);
            aux1="";
            aux2="";
        }
        else
        {
            aux1+=aux[i];
        }
        
    }
    r.setIngredientes(auxiliar);
    return is;
}

receta receta::fusionar(const receta& rec){
    receta fusion;
 
    string cod_fusion="F_"+getCode()+"_"+rec.getCode();
    fusion.setCode(cod_fusion);

    string nombre_fusion="Fusion "+getNombre()+" y "+rec.getNombre();
    fusion.setNombre(nombre_fusion);

    if(getPlato()==rec.getPlato())
        fusion.setPlato(getPlato());
    else if(getPlato()>rec.getPlato())
        fusion.setPlato(getPlato());
    else
        fusion.setPlato(rec.getPlato());
    
    
    fusion.setIngredientes(getIngredientes());
    for(receta::const_iterator it = rec.begin(); it!=rec.end(); ++it){
        receta::iterator pos= fusion.find((*it).first);
        if( pos != fusion.end()){ //El elemento está
            (*pos).second += (*it).second;
        }
            
        else
        {
            fusion.insertar(*it);//hay que encontrar la instruccion en fusion y sumarle la cantidad q se necesita para rec    
        }
        
    }

    fusion.setCalorias(getCalorias()+rec.getCalorias());
    fusion.setFibra(getFibra()+rec.getFibra());
    fusion.setGrasas(getGrasas()+rec.getGrasas());
    fusion.setHc(getHc()+rec.getHc());
    fusion.setProteinas(getProteinas()+rec.getProteinas());

    //Se añade como hijo izquierdo uno y como hijo derecho otro
    instrucciones inst1 = getInstrucciones();
    instrucciones inst2 = rec.getInstrucciones();
    instrucciones fus;
    fus =  inst1 + inst2;
    fusion.setInstrucciones(fus);
    return fusion;
}


receta::iterator receta::find(const string nuevo){
    bool encontrado=false;
    receta::iterator it;
    for (it = begin(); it!= end() && !encontrado; ++it){
        if ((*it).first == nuevo)
            encontrado = true;
    }
    return it;
}

receta::const_iterator receta::find(const string nuevo)const{
    bool encontrado=false;
    receta::const_iterator it;
    for (it =begin(); it!= end() && !encontrado; ++it){
        if ((*it).first == nuevo)
            encontrado = true;
    }
    return it;
}
    