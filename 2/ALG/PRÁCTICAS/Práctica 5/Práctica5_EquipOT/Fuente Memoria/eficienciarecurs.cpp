
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;



//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: F U N C I O N E S    C O M U N E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Generacion de datos para el problema
//==============================================================================

double random_double(double min, double max)
{

  return (rand() / (double)RAND_MAX) * (max - min) + min;
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Genera los puntos necesarios para el problema
 * @param num_points, la cantidad de puntos con la que queremos trabajar
 * @return un vector de puntos, cuyas coordenadas estan en el intevalo [0, num_points]
 * */
vector<vector<int>> generarProblema(int n)
{
  srand(time(NULL));
  vector<vector<int>> Matriz;
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;

    for (int j = 0; j < n; j++)
    {
      double x = random_double(1, 100);
      fila.push_back(x);
    }
    Matriz.push_back(fila);
  }

  return Matriz;
}
/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[i][solucion[i]];
  }
  return valor;
}



/**
 * @brief Función que calcula la siguiente configuración dentro del árbol de posibles soluciones
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * @param costeactual coste de la solución actual, que hay que modificar
 * @param T matriz de costes
 * @param final valor que indica si la solución actual es una posible solución. De serlo, al generar la siguiente
 * configuración dejará de serlo.
 * 
 * Esta función tiene en cuenta si se ha llegado al extremo de un nivel, de ser así, no modifica el vector.
 * 
 * 
 * */
void ConfiguracionSiguiente(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T, bool &final, vector<bool> &usadas)
{
  int nuevovalor;
  int inicio = solucion[nivel];
  if (inicio != 0)
    usadas[inicio]= false;
  if (solucion[nivel] < solucion.size()-1)
  {
    final=false;
    solucion[nivel] = solucion[nivel] + 1;
    while(usadas[solucion[nivel]] && solucion[nivel] < solucion.size()-1){
      
      solucion[nivel] = solucion[nivel] + 1;
      
    }

    if (inicio == 0)
      {
        nuevovalor = costeactual + T[nivel][solucion[nivel]];
      }
      else
      {
        nuevovalor = costeactual + T[nivel][solucion[nivel]] - T[nivel][inicio];
      }

      costeactual = nuevovalor;
    
    }
}




/**
 * @brief Función que verifica si la solución actual es factible
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a verificar
 * @param usadas vector con las asignaciones ya realizadas, se modifica si la solución actual es factible.
 * @return bool true si es factible, false si no lo es 
 * 
 * */
bool Factible(vector<int> solucion, int nivel, vector<bool> &usadas)
{
  int pos = solucion[nivel];
  bool fact = true;
  if ( usadas[pos]   ) {
    fact = false;
  }
  else{
    usadas[solucion[nivel]]=true;
  }
  return  fact;
}



/**
 * @brief Función que comprueba si estamos ante una posible solución del problema.
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a valorar
 * @param usadas vector que indica si cada asignación está realizada
 * 
 * Esta función tiene en cuenta si la solución es factible y si el tamaño de la solución a valorar coincide con el tamaño esperado.
 * */

bool Final(vector<int> solucion, int nivel, vector<bool> &usadas)
{
  bool fin = false;
  if (nivel == solucion.size() - 1 && !usadas[solucion[nivel]]){
    fin = true;
    usadas[solucion[nivel]]=true;
  }
}


/**
 * @brief Función que comprueba si el nodo actual tiene hermanos dentro del árbol de soluciones.
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * 
 * 
 * */
bool HayHermanos(vector<int> solucion, int nivel)
{
  return solucion[nivel] < solucion.size()-1;
}



/**
 * @brief Función que calcula la configuración anterior dentro del árbol de posibles soluciones
 * @param nivel nivel actual del árbol
 * @param solucion solución actual a modificar
 * @param costeactual coste de la solución actual, que hay que modificar
 * @param usada vector con las asignaciones ya realizadas
 * @param T matriz de costes 
 * 
 * */
void ConfiguracionAnterior(int nivel, vector<int> &solucion, int &costeactual, vector<vector<int>> T, vector<bool> &usada)
{
  costeactual = costeactual - T[nivel][solucion[nivel]];
  usada[solucion[nivel]]=false;
  solucion[nivel] = 0;
  
}



/**
 * @brief Función que calcula la solución del problema mediante backtracking
 * @param T matriz de costes
 * 
 * Esta función parte de una solución nula (0,0,0,...,0) y va recorriendo el árbol formado por las posibles combinaciones. Cuando
 * llega a una solución factible, y está en uno de los nodos finales, lo comprueba con la solución que aportaba el menor valor hasta la fecha.
 * Además, cuando llega al final de un nivel, es decir que no hay más hermanos, sube al nivel anterior para todos los hijos del siguiente padre.
 * En este caso, comprueba la factibilidad mediante el vector de asignaciones (usado).
 * @return vector<int> vector con la asignación minimal.
 * */
vector<int> Backtracking(vector<vector<int>> T)
{

vector<bool> usado;
  int N = T.size();
  int nivel = 0;
  bool esfinal=false;
  //Solución inicial
  vector<int> solucion, final;
  for (int i = 0; i < N; i++){
    solucion.push_back(0);
    usado.push_back(false);
  }

  int costeactual = T[0][0];
  int coste = INT_MAX;
  
  do{
     
  if (Final(solucion, nivel, usado) && !esfinal){

    esfinal=true;
    if (costeactual < coste)
  {
    coste = costeactual;
    final = solucion;

  }}
  else if (Factible(solucion, nivel, usado) && !esfinal)
  {
    nivel++;
  }
  else
  {
    esfinal = false;
    while (!HayHermanos(solucion, nivel) && (nivel >= 0))
    {
      ConfiguracionAnterior(nivel, solucion, costeactual, T, usado);
      nivel--;
    }
  }

ConfiguracionSiguiente(nivel, solucion, costeactual, T, esfinal, usado);
}while(nivel >=0);
  final.push_back(coste);
  return final;
}



 /**
  * @brief Calcula la asignacion optima mediante backtracking.
  * @param v, vector que indica si cada nodo está visitado o no
  * @param T, la matriz de distancias de los puntos con los que trabajamos
  * @param posicion, el número de centro de distribucion
  * @param n , número de nodos
  * @param nivel, el nivel del árbol de soluciones en el que estamos
  * @param costemin, el coste mínimo obtenido hasta la fecha
  * @param solucionparcial, la solucion que estamos construyendo, es decir las asignaciones a 
  * los puntos de venta.
  * @param solucion, la mejor solucion que hemos obtenido hasta la fecha
  * @pre T.size() > 0
  * */
void recursivo(vector<bool> &v , int posicion, vector<vector<int>> T, int n, int nivel, int costeactual, int &costemin, vector<int> solucionactual, vector<int> &solucion){
  if (nivel-1 == n)
  {
    int nuevo = costeactual + T[posicion-1][solucionactual[posicion]-1];
    if (nuevo < costemin)
    {
      costemin = nuevo;
      solucion = solucionactual;
    }
    return;
  }

  for (int i = 0; i <= n; i++)
  {
    if (!v[i])
    {

      v[i] = true;
      solucionactual.push_back(i);
      if (posicion ==0)
        recursivo(v, posicion+1, T, n, nivel + 1,  costeactual, costemin, solucionactual, solucion);
      else
          recursivo(v, posicion+1, T, n, nivel + 1,  costeactual + T[posicion-1][solucionactual[posicion]-1], costemin, solucionactual, solucion);
      // Mark ith node as unvisited
      solucionactual.pop_back();
      v[i] = false;
    }
  }
}

//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    cout << "Error, modo de ejecución; ./cercania <caracter> <tamaño>" << endl;
    exit(-1);
  }

  clock_t t_antes;   // Valor del reloj antes de la ejecución
  clock_t t_despues; // Valor del reloj después de la ejecución

  string algoritmo;
  int tamanio, minimo=INT_MAX;;
  vector<vector<int>> matriz;
  vector<int> solucion, solucion2;
  vector<bool> visitas;
  
  double t3;

  tamanio = atoi(argv[1]);
  for (int i =0; i <= tamanio; i ++)
    visitas.push_back(false);
  //Generamos los problemas
  matriz = generarProblema(tamanio);



  visitas[0] = true;
  solucion2.push_back(0);
  vector<int> solparcial = solucion2;
  t_antes = clock();
  recursivo(visitas, 0, matriz, tamanio, 1, 0, minimo, solparcial, solucion2);
  t_despues = clock();
  
  
  double t = ((double)(t_despues - t_antes)) / CLOCKS_PER_SEC;
  cout << tamanio << " " <<  t << endl; 


}
