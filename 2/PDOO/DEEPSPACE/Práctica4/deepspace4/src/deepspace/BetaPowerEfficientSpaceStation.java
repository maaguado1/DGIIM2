/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deepspace;

/**
 *
 * @author maguado
 */
public class BetaPowerEfficientSpaceStation extends PowerEfficientSpaceStation {
    
    private static final float EXTRAEFFICIENCY=1.2f;
    private Dice dice;
    
    
    BetaPowerEfficientSpaceStation(SpaceStation s){
        super(s);
        dice= new Dice();
    }
    
    
   @Override
   public float fire(){

       if (dice.extraEfficiency())
          return super.fire()*EXTRAEFFICIENCY;
       else
           return super.fire();
               
   }
   
   
   
   @Override
    public String toString() {
        String message = "(Beta)" + super.toString();
        return message;
    }
    
    /**
     * To UI.
     */
    @Override
    BetaPowerEfficientSpaceStationToUI getUIversion() {
        return new BetaPowerEfficientSpaceStationToUI(this);
    }
    
    
}
