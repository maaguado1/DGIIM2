

# 2º DGIIM

##### Repositorio que continene apuntes y prácticas del segundo curso del Doble Grado en Ingeniería Informática y Matemáticas.

## 1. Primer cuatrimestre

### Asignaturas de Matemáticas

#### ÁLGEBRA I

| NOMBRE                                                       | DESCRIPCIÓN                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Apuntes Bueso](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/ApuntesBueso) | Libros de  Bueso, con teoría y problemas resueltos           |
| [Apuntes Clase](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/ApuntesClase) | Apuntes propios de clase, separados por temas                |
| [Ejercicios varios](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/Ejerciciosvarios) | Ejercicios resueltos                                         |
| [Examenes](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/Examenes) | Examenes de otros años                                       |
| [Salvavidas Algebra](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/SalvavidasAlgebra) | Conjunto de apuntes por temas, relaciones resueltas, resumenes, entre otros. |
|                                                              |                                                              |

#### ANÁLISIS I

| NOMBRE                                                       | DESCRIPCIÓN                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Apuntes Clase](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/AN%C3%81LISIS/ApuntesClase) | Apuntes propios de clase                                     |
| [Apuntes Varios](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/AN%C3%81LISIS/ApuntesVarios) | Recopilación de apuntes extra                                |
| [Ejercicios resueltos](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/AN%C3%81LISIS/Ejerciciosresueltos) | Ejercicios resueltos                                         |
| [Examenes](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/AN%C3%81LISIS/Examenes) | Exámenes de otros años                                       |
| [Salvavidas Análisis](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/%C3%81LGEBRA/SalvavidasAlgebra) | Conjunto de apuntes por temas, relaciones resueltas, resumenes, entre otros. |
|                                                              |                                                              |

#### TOPOLOGÍA I

| NOMBRE                                                       | DESCRIPCIÓN                                  |
| ------------------------------------------------------------ | -------------------------------------------- |
| [Apuntes Clase](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/TOPOLOG%C3%8DAI/ApuntesClase) | Apuntes propios de clase separados por temas |
| [Exámenes resueltos](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/TOPOLOG%C3%8DAI/Ex%C3%A1menes%20Resueltos) | Exámenes resueltos                           |
| [Examenes](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/TOPOLOG%C3%8DAI/Examenes) | Exámenes de otros años                       |
|                                                              |                                              |

### Asignaturas de Informática



#### ESTRUCTURA DE COMPUTADORES

| NOMBRE                                                       | DESCRIPCIÓN                        |
| ------------------------------------------------------------ | ---------------------------------- |
| [Prácticas](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/EC/Pr%C3%A1cticas) | Prácticas con lenguaje ensamblador |
| [Examenes](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/EC/Examenes) | Exámenes de otros años             |
|                                                              |                                    |

#### ESTRUCTURAS DE DATOS

| NOMBRE                                                       | DESCRIPCIÓN                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Prácticas](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/ED/PRACTICASED) | Prácticas                                                    |
| [Teoría](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/ED) | Recopilación con exámenes de teoría, apuntes, ejercicios,... |
|                                                              |                                                              |

#### SISTEMAS OPERATIVOS

| NOMBRE                                                       | DESCRIPCIÓN                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Prácticas](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/ED/PRACTICASED) | Prácticas                                                    |
| [Teoría](https://gitlab.com/maaguado1/DGIIM2/tree/master/1/ED) | Resolución propia de los exámenes parciales, junto con apuntes por temas y soluciones de las relaciones de ejercicios |
|                                                              |                                                              |