
/* 
 * File:   ingrediente.h
 * Author: maguado
 */

#ifndef INGREDIENTE_H
#define INGREDIENTE_H

using namespace std;

/**
 *  @brief T.D.A. Ingrediente
 *
 *
 * Una instancia @e v del tipo de datos abstracto @c Ingrediente es un conjunto de datos
 * sobre un ingrediente: 
 * -string con el nombre
 * -double con las calorías por 100 gr
 * -double con el porcentaje de hidratos de carbono 
 * -double con el porcentaje de proteínas 
 * -double con el porcentaje de grasas 
 * -double con el porcentaje de fibra 
 * -Tipo de ingrediente (string)
 *  
 * La eficiencia en espacio es @e O(n).
 *
 *
 */


class Ingrediente{
     /**
       * 
       * Un objeto válido @e v del TDA Ingredientes debe cumplir 
       * - @c calorias, hidratos, proteinas, grasas y fibra>=0
       * - @
       * 
       * 
       *
    */
private:
    string _nombre;
    double _calorias, _hidratos, _proteina, _grasa, _fibra ;
    string _tipo;
   
public:
    
// ---------------  Constructores ----------------
    /**
     * @brief Constructor por defecto
     * @param nombre, calorias, hidratos, proteina, grasa, fibra, indican los valores iniciales
     * @note
     *   Inicializa todo vacío, en el caso de que no haya
     */
   
    Ingrediente();
    Ingrediente (const Ingrediente& original);
  // --------------- Otras funciones ---------------
    
    //FUNCIONES DE CONSULTA
    /**
     * @brief Consulta del nombre 
     * @return devuelve 
     * 
     */  
    
    string getNombre ()const {return _nombre;}
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta de las calorías
     * @return devuelve el número de calorías
     * 
     */  
    inline int getCalorias ()const{return _calorias;}
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta de los hidratos
     * @return devuelve el porcentaje de hidratos
     * 
     */  
    inline int getHidratos ()const{return _hidratos;}
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta de la proteína
     * @return devuelve el porcentaje de proteína
     * 
     */  
    
    inline int getProteina ()const{return _proteina;}
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta de la grasa
     * @return devuelve el porcentaje de grasa
     * 
     */  
    
    inline int getGrasa ()const{return _grasa;}
    
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta de la fibra
     * @return devuelve el porcentaje de fibra
     * 
     */  
    
     inline int getFibra () const{return _fibra;}
    
    
    // --------------- Otras funciones ---------------
    /**
     * @brief Consulta del tipo
     * @return devuelve el tipo
     * 
     */  
    
    inline string getTipo () const{return _tipo;}
    
    /**
     * @brief Operador de asignacion
     * @param original Objeto de la clase Ingrediente
     * 
     */
    Ingrediente& operator=(const Ingrediente& original);
    
    
    /**
     * @brief Operador de comparación
     * @param original Objeto de la clase Ingrediente
     * @return valor booleano que indica si el ingrediente es menor 
     * que el original
     * @note la comparación se realiza mediante el tipo de alimento, 
     * ante la igualdad, por el nombre
     */
    bool operator<(const Ingrediente& original)const ;
    
    
    bool operator>(const Ingrediente& original)const ;
    
    /**
     * @brief Operador relacional
     * @param original Objeto de la clase Ingrediente
     * @return valor booleano que indica si el ingrediente es igual 
     * que el original
     * @note la comparación se realiza atributo a atributo
     */
    bool operator==(const Ingrediente& original);
   
    
    /**
 * @brief Serialización de un ingrediente para su inserción en un flujo de salida.
 * @param os Flujo de salida
 * @param i Secuencia a serializar
 * @return El mismo flujo de salida para inserciones consecutivas
 */
friend std::ostream & operator<<(std::ostream & os, const Ingrediente & i);

/**
 * @brief Reconstruye un Diccionario a partir de un serialización
 * @param is Flujo de entrada desde el que se extrae la serialización
 * @param i Diccionario reconstruido
 * @return El mismo flujo de entrada para extracciones consecutivas
 */
friend std::istream & operator>>(std::istream & is, Ingrediente & i);

    
    
    
    
    
    
};
    






#endif /* INGREDIENTE_H */

