#include <iostream>
#include <cstdlib>
#include <vector>
#include <queue>

#include "player.h"
#include "environment.h"

using namespace std;


/*Matriz de valores para cada casilla*/
static int valoresTablero[6][7] = {{3, 4, 5, 7, 5, 4, 3}, 
                                          {4, 6, 8, 10, 8, 6, 4},
                                          {5, 8, 11, 13, 11, 8, 5}, 
                                          {5, 8, 11, 13, 11, 8, 5},
                                          {4, 6, 8, 10, 8, 6, 4},
                                          {3, 4, 5, 7, 5, 4, 3}};
const double masinf = 9999999999.0, menosinf = -9999999999.0;

// Constructor
Player::Player(int jug)
{
   jugador_ = jug;
   inicio=true;
}

// Actualiza el estado del juego para el jugador
void Player::Perceive(const Environment &env)
{
   actual_ = env;
}

double Puntuacion(int jugador, const Environment &estado)
{
   double suma = 0;

   for (int i = 0; i < 7; i++)
      for (int j = 0; j < 7; j++)
      {
         if (estado.See_Casilla(i, j) == jugador)
         {
            if (j < 3)
               suma += j;
            else
               suma += (6 - j);
         }
      }

   return suma;
}

// Funcion de valoracion para testear Poda Alfabeta
double ValoracionTest(const Environment &estado, int jugador)
{
   int ganador = estado.RevisarTablero();

   if (ganador == jugador)
      return masinf; // Gana el jugador que pide la valoracion
   else if (ganador != 0)
      return menosinf; // Pierde el jugador que pide la valoracion
   else if (estado.Get_Casillas_Libres() == 0)
      return 0; // Hay un empate global y se ha rellenado completamente el tablero
   else
      return Puntuacion(jugador, estado);
}

// ------------------- Los tres metodos anteriores no se pueden modificar


/*CONSECUTIVAS HORIZONTALES

Calcula, dada una casilla y un jugador, el número de casillas consecutivas de dicho jugador de manera horizontal, teniendo en cuenta 
el valor de cada casilla de la combinación.



*/
int ConsecutivasHorizontales(const Environment &estado, int fila, int columna, int jugador, int &valor)
{

   int consecutivas = 0;
   bool mismocolor = true;
   for (int j = columna; j < 7 && mismocolor; j++)
   {
      if (estado.See_Casilla(fila, j) != jugador && estado.See_Casilla(fila, j) != (jugador+3))
         mismocolor = false;
      else{
         valor+= valoresTablero[fila][j];
         consecutivas++;
      }

         
   }
   return consecutivas;
}


/*CONSECUTIVAS VERTICALES

Calcula, dada una casilla y un jugador, el número de casillas consecutivas de dicho jugador de manera vartical, teniendo en cuenta 
el valor de cada casilla de la combinación.



*/
int ConsecutivasVerticales(const Environment &estado, int fila, int columna, int jugador, int &valor)
{

   int consecutivas = 0;
   bool mismocolor = true;
   for (int j = fila; j < 7 && mismocolor; j++)
   {
      if (estado.See_Casilla(j, columna) != jugador && estado.See_Casilla(j, columna) != (jugador+3))
         mismocolor = false;
      else
      {
         valor+= valoresTablero[j][columna];
         consecutivas+= valoresTablero[fila][j];
      }
   }
   return consecutivas;
}


/*CONSECUTIVAS DIAGONALES

Calcula, dada una casilla y un jugador, el número de casillas consecutivas de dicho jugador de manera diagonal, tanto hacia la derecha como hacia la izquierda, teniendo en cuenta 
el valor de cada casilla de la combinación.
*/
int ConsecutivasDiagonal1(const Environment &estado, int fila, int columna, int jugador, int &valor)
{

   int consecutivas = 0;
   bool mismocolor = true;
   int col = columna;
   for (int fil = fila; fil < 7 && col < 7 && mismocolor; fil++)
   {
      int ola = estado.See_Casilla(fil, col);
      if (ola != jugador && ola != (jugador+3))
         mismocolor = false;
      else
      {
         consecutivas++;
         col++;
         valor+= valoresTablero[fil][col];
      }
   }
   return consecutivas;
}

int ConsecutivasDiagonal2(const Environment &estado, int fila, int columna, int jugador, int &valor)
{

   int consecutivas = 0;
   bool mismocolor = true;
   int col = columna;
   for (int fil = fila; fil >= 0 && col < 7 && mismocolor; fil--)
   {
      if (estado.See_Casilla(fil, col) != jugador && estado.See_Casilla(fil, col) != (jugador+3))
         mismocolor = false;
      else
      {
         consecutivas++;
         valor+= valoresTablero[fil][col];
         col++;
      }
   }
   return consecutivas;
}



/*
AMENAZA HORIZONTAL

Calcula, dada una casilla, si hay una amenaza partiendo de ella, es decir, cuatro casillas consecutivas de manera horizontal en las que tres son del jugador pasado como parámetro, teniendo el cuenta el
valor de las casillas involucradas.

Si dentro de una amenaza, la casilla de debajo del hueco está lleno, se suma 3 al valor, y si es la de dos por abajo, se suma 2.

*/
int AmenazaHorizontal(const Environment &estado, int fila, int columna, int jugador)
{

   int consecutivas = 0;
   bool mismocolor = true;
   bool otro=true;
   int agujeros=0;
   int valor=0;
   for (int j = columna; j < 7 && otro; j++)
   {
      if (estado.See_Casilla(fila, j) ==0)
         //Si se encuentra una que no es del jugador, desactiva lo de mismocolor
         if(mismocolor && agujeros==0){
            agujeros++;
            mismocolor=false;
            if (fila>0 && estado.See_Casilla(fila-1, j)!=0)
               valor+=3;
            if (fila>1 && estado.See_Casilla(fila-2, j)!=0)
               valor+=1;
         }
            
         else
            otro=false; //Si ya estaba desactivado tenemos dos huecos y si agujero vale 1 tenemos dos huecos en la combinación, por tanto no es amenaza.

      else{

         //Si veníamos de una que no era del color, pero ahora ya es
         if (!mismocolor)
            mismocolor=true;
         valor += valoresTablero[fila][j];
         consecutivas++;
      }

         
   }
   if (consecutivas>=3)
      return valor;
    else return 0;
}


/*
AMENAZA DIAGONAL

Calcula, dada una casilla, si hay una amenaza partiendo de ella, es decir, cuatro casillas consecutivas de manera diagonal en las que tres son del jugador pasado como parámetro, teniendo el cuenta el
valor de las casillas involucradas.
*/
int AmenazaDiagonal1(const Environment &estado, int fila, int columna, int jugador)
{

   int consecutivas = 0;
   bool mismocolor = true;
   bool otro=true;
   int agujeros=0;
   int valor=0;
   int col=columna;
    for (int fil = fila; fil < 7 && col < 7 && otro; fil++)
   {
      if (estado.See_Casilla(fil, col) ==0)
         //Si se encuentra una que no es del jugador, desactiva lo de mismocolor
         if(mismocolor && agujeros==0){
            agujeros++;
            mismocolor=false;
            if (fil>0 && estado.See_Casilla(fil-1, col)!=0)
               valor+=3;
            if (fil>1 && estado.See_Casilla(fil-2, col)!=0)
               valor+=1;
         }
            
         else
            otro=false; //Si ya estaba desactivado tenemos dos huecos y si agujero vale 1 tenemos dos huecos en la combinación, por tanto no es amenaza.

      else{

         //Si veníamos de una que no era del color, pero ahora ya es
         if (!mismocolor)
            mismocolor=true;
         valor += valoresTablero[fil][col];
         consecutivas++;
      }
      col++;

         
   }
   if (consecutivas>=3)
      return valor;
   else return 0;
}




int AmenazaDiagonal2(const Environment &estado, int fila, int columna, int jugador)
{

   int consecutivas = 0;
   bool mismocolor = true;
   bool otro=true;
   int agujeros=0;
   int valor=0;
   int col=columna;
    for (int fil = fila; fil >=0 && col < 7 && otro; fil--)
   {
      if (estado.See_Casilla(fil, col) ==0)
         //Si se encuentra una que no es del jugador, desactiva lo de mismocolor
         if(mismocolor && agujeros==0){
            agujeros++;
            mismocolor=false;
            if (fil>0 && estado.See_Casilla(fil-1, col)!=0)
               valor+=3;
            if (fil>1 && estado.See_Casilla(fil-2, col)!=0)
               valor+=2;
         }
            
         else
            otro=false; //Si ya estaba desactivado tenemos dos huecos y si agujero vale 1 tenemos dos huecos en la combinación, por tanto no es amenaza.

      else{

         //Si veníamos de una que no era del color, pero ahora ya es
         if (!mismocolor)
            mismocolor=true;
         valor += valoresTablero[fil][col];
         consecutivas++;
      }
      col++;

         
   }
   if (consecutivas>=3)
      return valor;
   else return 0;
}



/*Función que calcula, dada una casilla todas las combinaciones de casillas consecutivas partiendo de ella que superen un número determinado, en los 4 modos que hemos visto antes.

*/
int ConsecutivasNumero(const Environment &estado, int fila, int columna, int jugador, int numero)
{
   int valor = 0;
   int consecutivastotales=0;
   consecutivastotales += (ConsecutivasHorizontales(estado, fila, columna, jugador, valor)>=numero)? valor:0;
   valor=0;
   consecutivastotales += (ConsecutivasVerticales(estado, fila, columna, jugador, valor) >= numero) ? valor : 0;
   valor=0;
   consecutivastotales += (ConsecutivasDiagonal1(estado, fila, columna, jugador, valor) >= numero) ? valor : 0;
   valor=0;
   consecutivastotales += (ConsecutivasDiagonal2(estado, fila, columna, jugador, valor) >= numero) ? valor : 0;
   //Se multiplica por el valor de la casilla inicial:
   return consecutivastotales;
}


/*Función que calcula para un tablero dado, el número de combinaciones de más de -número- casillas consecutivas de un jugador determinado*/
int ConsecutivasTablero(const Environment &estado, int jugador, int numero)
{
   int total = 0;
   for (int fila = 0; fila < 7; fila++)
   {
      for (int columna = 0; columna < 7; columna++)
      {

         total += (estado.See_Casilla(fila, columna) == jugador) ? ConsecutivasNumero(estado, fila, columna, jugador, numero) : 0;
      }
   }
   return total;
}

/*Función que calcula para un tablero dado, el número de amenazas de un jugador determinado*/

int AmenazaTablero(const Environment &estado, int jugador)
{
   int total = 0;
   for (int fila = 0; fila < 7; fila++)
   {
      for (int columna = 0; columna < 7; columna++)
      {

         total += (estado.See_Casilla(fila, columna) == jugador) ? AmenazaHorizontal(estado, fila,columna, jugador ) : 0;
         total += (estado.See_Casilla(fila, columna) == jugador) ? AmenazaDiagonal1(estado, fila,columna, jugador ) : 0;
         total += (estado.See_Casilla(fila, columna) == jugador) ? AmenazaDiagonal2(estado, fila,columna, jugador ) : 0;
      }
   }
   return total;
}
// //Funcion heuristica (ESTA ES LA QUE TENEIS QUE MODIFICAR)



/*Función que valora un estado, teniendo en cuenta lo siguiente:
- Primero se comprueba si es un tablero ganador o perdedor para el jugador que llama.
- Después se calcula las combinaciones de dos y tres casillas consecutivas en los 4 modos vistos, para cada jugador.
- Se calculan las amenazas para cada jugador.
- Se da un valor positivo a las combinaciones y amenazas propias y negativo a las del oponente.



*/
double Valoracion(const Environment &estado, int jugador)
{
   int ganador = estado.RevisarTablero();

   if (ganador == jugador)
      return  999999.0; //Gana el jugador que pide la valoracion
   else if (ganador != 0)
      return -999999.0; // Pierde el jugador que pide la valoracion
   else if (estado.Get_Casillas_Libres() == 0)
      return 0; // Hay un empate global y se ha rellenado completamente el tablero
   else
   {

   
      int my_threes = ConsecutivasTablero(estado, jugador, 3);
      int my_twos = ConsecutivasTablero(estado, jugador, 2);
      int my_amenazas=AmenazaTablero(estado, jugador);

      int elotro = (jugador == 1) ? 2 : 1;
      int his_threes = ConsecutivasTablero(estado, elotro, 3);
      int his_twos = ConsecutivasTablero(estado, elotro, 2);
      int his_amenazas=AmenazaTablero(estado, elotro);

      int valor=(5*my_threes+ 4*my_twos+ 4*my_amenazas) - ( 5*his_threes+ 4*his_twos+4*his_amenazas);
      //cout<<"\nLa valoración es " << valor ;
      return valor;
   }
}

// Esta funcion no se puede usar en la version entregable
// Aparece aqui solo para ILUSTRAR el comportamiento del juego
// ESTO NO IMPLEMENTA NI MINIMAX, NI PODA ALFABETA
void JuegoAleatorio(bool aplicables[], int opciones[], int &j)
{
   j = 0;
   for (int i = 0; i < 8; i++)
   {
      if (aplicables[i])
      {
         opciones[j] = i;
         j++;
      }
   }
}

bool Player::Tablerovacio(){
   return actual_.See_Casilla(0, 0) == 0 &&
       actual_.See_Casilla(0, 1) == 0 &&
       actual_.See_Casilla(0, 2) == 0 &&
       actual_.See_Casilla(0, 3) == 0 &&
       actual_.See_Casilla(0, 4) == 0 &&
       actual_.See_Casilla(0, 5) == 0 &&
       actual_.See_Casilla(0, 6) == 0;
}

// Invoca el siguiente movimiento del jugador
Environment::ActionType Player::Think()
{
   const int PROFUNDIDAD_MINIMAX = 6;  // Umbral maximo de profundidad para el metodo MiniMax
   const int PROFUNDIDAD_ALFABETA = 8; // Umbral maximo de profundidad para la poda Alfa_Beta

   Environment::ActionType accion; // acci�n que se va a devolver
   bool aplicables[8];             // Vector bool usado para obtener las acciones que son aplicables en el estado actual. La interpretacion es
                                   // aplicables[0]==true si PUT1 es aplicable
                                   // aplicables[1]==true si PUT2 es aplicable
                                   // aplicables[2]==true si PUT3 es aplicable
                                   // aplicables[3]==true si PUT4 es aplicable
                                   // aplicables[4]==true si PUT5 es aplicable
                                   // aplicables[5]==true si PUT6 es aplicable
                                   // aplicables[6]==true si PUT7 es aplicable
                                   // aplicables[7]==true si BOOM es aplicable

   double valor;       // Almacena el valor con el que se etiqueta el estado tras el proceso de busqueda.
   double alpha, beta; // Cotas de la poda AlfaBeta
   alpha = menosinf;
   beta = masinf;
   int n_act; //Acciones posibles en el estado actual

   n_act = actual_.possible_actions(aplicables); // Obtengo las acciones aplicables al estado actual en "aplicables"
   int opciones[10];

   // Muestra por la consola las acciones aplicable para el jugador activo
   //actual_.PintaTablero();
   cout << " Acciones aplicables ";
   (jugador_ == 1) ? cout << "Verde: " : cout << "Azul: ";
   for (int t = 0; t < 8; t++)
      if (aplicables[t])
         cout << " " << actual_.ActionStr(static_cast<Environment::ActionType>(t));
   cout << endl;

   //--------------------- COMENTAR Desde aqui
   //  cout << "\n\t";
   //  int n_opciones=0;
   //  JuegoAleatorio(aplicables, opciones, n_opciones);

   //  if (n_act==0){
   //    (jugador_==1) ? cout << "Verde: " : cout << "Azul: ";
   //    cout << " No puede realizar ninguna accion!!!\n";
   //    //accion = Environment::actIDLE;
   //  }
   //  else if (n_act==1){
   //         (jugador_==1) ? cout << "Verde: " : cout << "Azul: ";
   //          cout << " Solo se puede realizar la accion "
   //               << actual_.ActionStr( static_cast< Environment::ActionType > (opciones[0])  ) << endl;
   //          accion = static_cast< Environment::ActionType > (opciones[0]);

   //       }
   //       else { // Hay que elegir entre varias posibles acciones
   //          int aleatorio = rand()%n_opciones;
   //          cout << " -> " << actual_.ActionStr( static_cast< Environment::ActionType > (opciones[aleatorio])  ) << endl;
   //          accion = static_cast< Environment::ActionType > (opciones[aleatorio]);
   //       }

   //  //--------------------- COMENTAR Hasta aqui

   //--------------------- AQUI EMPIEZA LA PARTE A REALIZAR POR EL ALUMNO ------------------------------------------------

   // Opcion: Poda AlfaBeta
   // NOTA: La parametrizacion es solo orientativa
   if (inicio && Tablerovacio())
   {
      cout << "ES LA PRIMERA TIRADA" << endl;
      inicio=false;
      return Environment::PUT4;
   }

   valor = Poda_AlfaBeta(actual_, jugador_, 0, PROFUNDIDAD_ALFABETA, accion, alpha, beta);
   cout << "Valor alphabeta: " << valor << "  Accion: " << actual_.ActionStr(accion) << endl;

   return accion;
}


//ALGORITMO DE PODA ALFA BETA
double Player::Poda_AlfaBeta(const Environment &estado, int jugador, int profundidadactual, int profundidadmaxima, Environment::ActionType &accion, double alpha, double beta)
{

   bool acciones[8];
   int n_act = estado.possible_actions(acciones);
   Environment::ActionType action;
   if (profundidadactual == profundidadmaxima || estado.JuegoTerminado())
   {
      int val = Valoracion(estado, jugador);
      //cout << "Nodo final, valoración: " << val << endl;
      return val;
   }
   else if (estado.JugadorActivo() == jugador)
   {
      //Expandimos cada hijo:

      int act = -1;
      Environment hijo = estado.GenerateNextMove(act);
      for (int i =0; i < n_act; i ++){
         
         //cout << "\n\n NIVEL " << profundidadactual << endl;
         Environment::ActionType accion2 = static_cast<Environment::ActionType>(act);
         //cout << "Desde MAX: Expandiendo acción " << hijo.ActionStr(accion2) << endl;
         if (double m = Poda_AlfaBeta(hijo, jugador, profundidadactual + 1, profundidadmaxima, action, alpha, beta); m > alpha)
         {
            alpha = m;
           if (profundidadactual == 0)
               accion = accion2;
         }
         if (alpha >= beta)
            break;
         hijo = estado.GenerateNextMove(act);

      }
      //cout << "Valor de alfa: " << alpha << endl;
      return alpha;
   }
   else
   {

      //Expandimos cada hijo:

      int act = -1;
      //Expandimos cada hijo:
      
      Environment hijo = estado.GenerateNextMove(act);
     for (int i =0;i < n_act; i++){
         //cout << "\n\n NIVEL " << profundidadactual << endl;
         Environment::ActionType accion2 = static_cast<Environment::ActionType>(act);
         //cout << "Desde MIN: Expandiendo acción " << hijo.ActionStr(accion2) << endl;
         if (double m = Poda_AlfaBeta(hijo, jugador, profundidadactual + 1, profundidadmaxima, action, alpha, beta); m < beta)
         {
            beta = m;
            if (profundidadactual == 0)
               accion = accion2;
         }
         if (alpha >= beta)
            break;
          hijo = estado.GenerateNextMove(act);
      }
     // cout << "Valor de beta: " << beta << endl;
      return beta;
   }
}

