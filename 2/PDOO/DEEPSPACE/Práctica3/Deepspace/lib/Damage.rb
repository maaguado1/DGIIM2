#encoding:utf-8

require_relative 'DamageToUI'
require_relative 'Weapon'
require_relative 'WeaponType'


module Deepspace
  

  
# Represents damages done on a spaceship after loosing a combat.
# They indicate which elements are going to be lost if the spaceship loses the combat.
# A damage is composed by a number of shields to lose and:
#   - A number of weapons to lose (nWeapons)
#   - A list of weapons to lose (weapons)
#


  
class Damage
  
  
  # Initializers
	# ==========================================================================

  
  
	# Class initializer.
	# @param s [Integer] number of shields that will be lost
  # @param w number of weapons that will be lost
  # @param t WeaponTypes that will be lost 
 
  def initialize(w,s,t)
    @nShields=s
    @nWeapons=w
    @weapons=t
  end
  
  
  # Class numeric initializer.
	# @param s [Integer] number of shields that will be lost
  # @param w number of weapons that will be lost
  def self.newNumericWeapons(w,s)
    otro = new(w,s,[])
  end
  
  
  # Class specific initializer.
	# @param s [Integer] number of shields that will be lost
  # @param t WeaponTypes that will be lost 
  
  def self.newSpecificWeapons(w,s)
    otro = new(-1,s,w)
  end
  
  
  # Class copy initializer.
	# @param d [Damage] makes a copy of the given damage.
  def self.newCopy(d)
    
    if d.nWeapons == -1
      otro = newSpecificWeapons(d.weapons, d.nShields)
    else
      otro= newNumericWeapons(d.nWeapons, d.nShields)
    end
    return otro;
  end
  
  
  
  # Getters
	# ==========================================================================

  attr_reader :nShields
  attr_reader :nWeapons
  attr_reader :weapons
  
  
  
  # Checks whether the damage is affecting or not
	# @return [Boolean] true, if damage has no effect; false, otherwise
  def hasNoEffect
    if (@nWeapons == 0 or @weapons.length==0) and @nShields==0
      return true
    else
      return false
    end
  end
  
  
  # Setters
	# =========================================================================

  
  
  def discardWeapon(w)
    if (@weapons.length == 0)
      @weapons.delete(w)
    elsif (@nWeapons > 0)
      @nWeapons -=1
    end
  end
  
  
  # Reduces by 1 the number of shield boosters to be removed
  def discardShieldBooster()
    if (@nShields > 0)
      @nShields -=1
    end
  end
  
  
  # Checks if a given weapon array contains a WeaponType
  # @param w weapon array
  # @param t WeaponType to be looked for
  # @returns [integrer] position of the WeaponType in the array.
  
  def arrayContainsType(w,t)
    found = false
    pos = -1
    i =0
    while (!found && i<w.length)
      
      if (w[i].type==t)
        found=true
        pos =i
      else
        i+=1
      end
    end
    
    return pos
  end
  
  
  # Creates a copy of current objet where shields which are not
	# included in arrays given as parameters are discarded. That's to say, we
	# shrink the Damage to the parameters
	# @param s [Array<ShieldBooster>] shields to fit
  # @param w [Array<Weapon>] weapons to fit
	# @return [Integer] a copy of the object adjusted as explained above
  
  def adjust(w,s)
    if @nWeapons == -1
      min1=(s.length < @nShields)? s.length: @nShields
      w1= w
      w2 = @weapons
      w3 = []
      for t in w2
        
        if (arrayContainsType(w1, t) != -1)
          
          w3 << t
          w1.delete_at(arrayContainsType(w1,t))
        end
      end
      
      otro= Damage.newSpecificWeapons(w3,min1)
    else
      min1=(s.length < @nShields)? s.length: @nShields
      min2=(w.length < @nWeapons)? w.length: @nWeapons
      otro= Damage.newNumericWeapons(min2, min1)
    end
    return otro
    
    
  end
  
  
  
  # String representation, UI version
	# ==========================================================================
  
  
  def getUIversion
    return  DamageToUI.new(self)
  end
  
  
  def to_s
    message = "[Damage]: Number of Shields: #{@nShields} , Number of Weapons: #{@nWeapons} "
    message += "Weapon Types: {"
    for w in @weapons
      message += w.to_s + ", "
    end
    return message+ "}"
                
  end
  
  
  
  
  
  
  
end


end
