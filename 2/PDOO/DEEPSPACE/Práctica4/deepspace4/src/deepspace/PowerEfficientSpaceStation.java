
package deepspace;

/**
 *
 * @author maguado
 */
public class PowerEfficientSpaceStation extends SpaceStation {
    
    static final float EFFICIENCYFACTOR= 1.10f;
    
    
    PowerEfficientSpaceStation(SpaceStation estacion){
        super(estacion);
    }
    
    @Override
    public Transformation setLoot(Loot l){
        Transformation trans = super.setLoot(l);
        if (trans == Transformation.SPACECITY)
            return Transformation.NOTRANSFORM;
        else
            return trans;
    }
    
    @Override 
    public float fire(){
        return super.fire()*EFFICIENCYFACTOR;
    }
    
    
    @Override
    public float protection(){
        return super.protection()*EFFICIENCYFACTOR;
    }
    
    @Override
    PowerEfficientSpaceStationToUI getUIversion(){
        return new PowerEfficientSpaceStationToUI(this);
    }
}
