### MEMORIA DE LA PRÁCTICA 1

**María Aguado**

## ANÁLISIS DE EFICIENCIA DE ALGORITMOS

### Tabla de contenidos:

[TOC]

### 1. EFICIENCIA EMPÍRICA

#### Automatización de ejecuciones

Para poder analizar correctamente la ejecución de los distintos programas he creado un script que automatiza la ejecución, y que produce un fichero apto para poder representar los datos obtenidos con `gnuplot`:

```bash
#!/bin/csh 

echo "" >> mergesort2.dat
@ i = 1000

while ( $i < 100000 )
echo "Estoy en la ejecucion $i"
./merg2 $i >>  mergesort2.dat
@ i += 1000
end


```



#### 1.A.- Algoritmos O(n^2)

Para los tres algoritmos de este tipo he usado tamaños desde 1000 a 29000, con un aumento entre ejecuciones de 1000.



Las gráficas que corresponden a las ejecuciones de los distintos algoritmos son:

<img src="assets/burbuja.png" alt="burbuja"  align = "left" style="zoom:50%;" /> <img src="assets/seleccion.png" alt="burbuja"  align = "right" style="zoom:50%;" />



<img src="assets/insercion.png" alt="burbuja"  align = "center" style="zoom:50%;" />

Los resultados obtenidos se pueden ver en la siguiente tabla y representados por la siguiente gráfica:  



<img alt="776f0c08.png" src="assets/SELECCIÓN, INSERCIÓN y BURBUJA.png"
align = "center" style="zoom:50%;"   >

<img src="assets/graf1.png" alt="graf1" style="zoom:50%;" />


Por tanto, podemos concluir que el algoritmo de inserción es más eficiente.


#### 1.B.- Algoritmos O(nlog(n))

Para las ejecuciones de estos algoritmos he usado parámetros desde 1000 hasta 100000, con un aumento de 1000.
Las gráficas que corresponden a la eficiencia empírica de cada algoritmo se encuentran a continuación.

<img alt="36e22b56.png" src="assets/quick.png" align = "left"  width="150" height="" style="zoom:50%;"  ><img alt="0d8b1d5c.png" src="assets/merge.png" align = "right" width="150" height="" style="zoom:50%;"  >















<img alt="ea692fdb.png" src="assets/heap.png" align = "center" width="150" height="" style="zoom:50%;"  >








Los resultados los podemos comparar en la siguiente tabla:

<img src="assets/graf2.png" alt="graf2" style="zoom:50%;" />


Y los tiempos de ejecución se ven en la siguiente gráfica:

<img src="assets/MERGESORT, QUICKSORT y HEAPSORT(1).png" alt="MERGESORT, QUICKSORT y HEAPSORT(1)" style="zoom: 67%;" />

Por tanto, podemos concluir que el algoritmo quicksort es más eficiente en cuanto a ordenación de vectores, pues es el que menos tiempo tarda.

#### 1.C.- Algoritmo de Floyd

Para poder analizar este algoritmo he usado valores de entrada desde 100 hasta 1250, con un aumento entre ejecuciones de 50.

Los resultados obtenidos se pueden ver en la siguiente gráfica:

<img alt="7a74b2d0.png" src="assets/floy.png"  width="150" height="" style="zoom:50%;"  >


#### 1.D.- Algoritmo de Hanoi

A la hora de ejecutar este algoritmo los valores introducidos han sido desde 1 hasta 30, con un incremento de 1.

<img alt="2aa4ebd9.png" src="assets/han.png" align = "center"  width="150" height="" style="zoom:50%;"  >



### 2. Eficiencia híbrida

Para analizar la eficiencia híbrida usamos funciones del mismo orden que la eficiencia teórica para cada caso, y ajustamos dichas funciones mediante gnuplot, para poder comprobar el grado de ajuste de la eficiencia empírica a la eficiencia teórica.

Las funciones que usamos serán:

- Para ajustar  los algoritmos de selección, burbuja e inserción, una función cuadrática :

  $$ f(x) = a~2~*x^2+a~1~*x+a~0~$$

- Floyd lo ajustaremos con una función cúbica:
    $$ f(x) = a~3~*x^3+a~2~*x^2+a~1~*x+a~0~$$

- Mergesort, quicksort y hesapsort los ajustaremos con una función logarítmica:
    $$f(x)=a*x*log(x)+b$$

- Hanoi lo ajustaremos a:
    $$f(x) = a*2^x + b$$

#### 2.A.- Ajustes de acuerdo con el teórico

Las gráficas siguientes corresponden a ajustes realizados según lo obtenido analizando la eficiencia teórica de cada algoritmo.


- Algoritmos O(n²):

Las funciones obtenidas son:

    - Inserción: f(x)= 0.00179471*x²+ 0.00355754x + 0.00275322  
    - Selección: f(x)= -2.86031x² + 1099.79x+  260.292   
    - Burbuja: f(x)= 0.00557252x² -0.00235059x+ 0.0183328

<img alt="a81d6184.png" src="assets/burbuja_ajustado.png" align = "left" width="150" height="" style="zoom:50%;"  > 										<img alt="336947df.png" src="assets/insercion_ajustado.png" align = "right" width="150 " height="" style="zoom:50%;"  >



<img alt="dacb7223.png" src="assets/seleccion_ajustado.png" align = "center" width="150" height="" style="zoom:50%;"  >



- Algoritmos O(n*log(n))



<img alt="9f38cc83.png" src="assets/heaplog.png" align = "left"  width="150" height="" style="zoom:50%;"  >					<img alt="76b98132.png" src="assets/mergelog.png" align = "right"  width="150" height="" style="zoom:50%;"  >

<img alt="f627afce.png" src="assets/quicklog.png" align = "center"  width="150" height="" style="zoom:50%;"  >




- Algoritmos O(n³) - Floyd

  - La función obtenida es: f(x) = 0.0011686x³ + 0.00675433x² + 0.0166939x + 0.00869188  

<img alt="604ca846.png" src="assets/flo.png" align = "center" width="150" height="" style="zoom:50%;"  >


- Algoritmo O(2^n) - Hanoi

  - La función es: f(x)= 4.17755e-08*2^x

<img alt="899d7fc5.png" src="assets/haajus.png" align = "center" width="150" height="" style="zoom:50%;"  >









#### 2.B.- Otros ajustes

Resulta evidente deducir que en el primero y en los dos últimos casos los ajustes son apropiados, pues corresponden en gran medida con los datos obtenidos. Sin embargo, en el caso de los algoritmos con eficiencia logarítmica tenemos más problema a la hora de ver la correspondencia clara, por tanto he decidido probar con un ajuste cuadrático en vez de logarítmico:

<img alt="3128c624.png" src="assets/mergecua.png" align = "left" width="150" height="" style="zoom:50%;"  > 













Podemos ver, que aunque también es un  buen ajuste, no es mejor que el ajuste logarítmico, que es el que se ajusta con la eficiencia teórica.

