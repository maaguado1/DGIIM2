#encoding: utf-8

module Deepspace
  
# Class that represents a supplies package available for a space station.
class SuppliesPackage
  
  
  # Initializers
  # 
  # 
  # Class initializer
	# @param _ammoPower [Float] parametrizes ammunition power
	# @param _fuelUnits [Float] counts fuel units
	# @param _shieldPower [Float] parametrizes shield power
  def initialize(ammo, fuel, shield)
    @ammoPower=ammo;
    @fuelUnits = fuel;
    @shieldPower= shield;
  end
  
  
    # Copy constructor
    def self.newCopy(supplie)
      new(supplie.ammoPower, supplie.fuelUnits, supplie.shieldPower)
      
    end
    
    
    
    # Getters
    def ammoPower
      @ammoPower;
    end
    
    def fuelUnits
      @fuelUnits;
    end
    
    def shieldPower
      @shieldPower;
    end
    
    
    def to_s
      mensaje = "[SuppliesPackage] -> ammoPower #{@ammoPower}, fuel Units #{@fuelUnits}, shieldPower #{@shieldPower}"
      return mensaje
    end
end

end # module Deepspace