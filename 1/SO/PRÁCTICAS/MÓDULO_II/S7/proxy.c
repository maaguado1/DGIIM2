#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>

#define BUF_SIZE 1024
#define LONG_NOMBRE 50

int main(int argc, char** argv){
    FILE* tmp = tmpfile();
    char buf[BUF_SIZE];
    int leidos;

    // Lectura de stdin (fifo personalizado) del mensaje a imprimir
    // Aquí es donde es importante que el fifo personalizado esté abierto en RDONLY
    // para que cuando el cliente termine de escribir read devuelva 0
    while ((leidos = read(STDIN_FILENO, buf, sizeof(buf))) != 0){
        if (leidos == -1){
            perror("Proxy read de stdin");
            exit(EXIT_FAILURE);
        }
        // El mensaje a imprimir se va guardando en un archivo temporal mientras se recibe
        if (fwrite(buf, 1, leidos, tmp) != leidos){
            perror("Proxy fwrite en tmp");
            exit(EXIT_FAILURE);
        } else {
            //printf("PROXY: leídos %d bytes\n", leidos);
        }
    }
    //printf("PROXY: terminado de leer de stdin\n");

    // Hemos terminado de leer los datos, esperamos a que el archivo bloqueo
    // no tenga cerrojo y lo ponemos nosotros (F_SETLKW)
    int fdbloqueo = open("bloqueo", O_RDWR);
    struct flock cerrojo;
    cerrojo.l_type = F_WRLCK;
    cerrojo.l_whence = SEEK_SET;
    cerrojo.l_start = 0;
    cerrojo.l_len = 0;
    if (fcntl(fdbloqueo, F_SETLKW, &cerrojo) == -1){
        perror("Proxy error lock cerrojo");
        exit(EXIT_FAILURE);
    }

    // Leemos los datos escritos antes en el archivo temporal y los imprimimos
    fseek(tmp, 0, SEEK_SET);
    while ((leidos = fread(buf, 1, sizeof(buf), tmp)) != 0){
        if (write(STDOUT_FILENO, buf, leidos) != leidos){
            perror("Proxy error escribiendo en pantalla");
            exit(EXIT_FAILURE);
        }
        //printf("PROXY: escritos %d bytes\n", leidos);
    }
    
    //printf("PROXY: terminado de escribir en stdout\n");

    // Quitamos el bloqueo
    cerrojo.l_type = F_UNLCK;
    cerrojo.l_whence = SEEK_SET;
    cerrojo.l_start = 0;
    cerrojo.l_len = 0;
    if (fcntl(fdbloqueo, F_SETLKW, &cerrojo) == -1){
        perror("Proxy error unlock cerrojo");
        exit(EXIT_FAILURE);
    }

    // Cerramos archivos
    close(fdbloqueo);
    fclose(tmp);
    close(STDIN_FILENO); // fifo personalizado

    // Eliminamos fifo personalizado, ya que la comunicación ha terminado
    char nombrefifo[LONG_NOMBRE];
    sprintf(nombrefifo, "fifo.%d", getpid());
    if (unlink(nombrefifo) == -1){
        perror("Proxy unlink fifo personalizado");
        exit(EXIT_FAILURE);
    }
    return 0;
}