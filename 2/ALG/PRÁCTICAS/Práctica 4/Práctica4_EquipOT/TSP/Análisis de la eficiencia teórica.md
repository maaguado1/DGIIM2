### Especificaciones teóricas

#### Naturaleza n-etápica

Para comprobar que la naturaleza de este problema hace que sea apto para resolverlo por programación dinámica, consideraremos su resolución como un problema de toma de decisión multietápico. Resulta evidente que cumple este requisito pues:

- El problema no se puede resolver en un sólo intento, ya que la solución que podamos conseguir en una sola etapa no va a ser siempre la óptima.
- No nos podemos saltar ninguna etapa, es decir, para poder resolverlo hay que ir disminuyendo en 1 la complejidad del problema. De no ser así, estaríamos teniendo en cuenta un nodo menos en el recorrido, y la inclusión de ese nodo de forma posterior cambiaría drásticamente el valor del ciclo obtenido, de nuevo sin poder asegurar la optimalidad en todos los casos.



#### Principio de optimalidad de Bellman

Suponemos que estamos en un caso como el descrito en el enunciado del problema.

Sin pérdida de generalidad, como el recorrido buscado es cíclico, fijamos el origen en un nodo 0. Supongamos que en una determinada etapa de un ciclo óptimo que ha empezado en 0 se ha llegado al nodo i y que quedan todavía k nodos $j_1,j_2, ..., j_k$ por visitar antes de volver al 0. 

Entonces es evidente que como el ciclo está considerado óptimo, el recorrido desde i pasando por todos los $j_1,j_2, ..., j_k$ hasta volver a 0 debe ser de longitud mínima. 

Lo demostramos por contrarrecíproco, pues si no fuera así, el ciclo entero no se consideraría óptimo al ser posible reducir su valor eligiendo otra ruta entre los $j_1,j_2, ..., j_k$ hasta el 0.

Por tanto, el recorrido solución $N_1, N_2, ... N_n$ es óptima si y sólo si lo es la solución entre dos nodos cualesquiera $N_i,...N_j$, es decir, que la solución es óptima en cada una de las subetapas.



#### Ecuación de recurrencia

Realizamos las especificaciones matemáticas de esta resolución:

- Sea $S\subseteq N-\{{1}\}$ un conjunto de nodos y consideramos un nodo $i \in N-S$.
- Definimos $g(i,S)$ como la función que asigna a cada nodo $i$ la longitud del camino más corto desde $i$ al nodo 1 que pasa por todos los nodos de $S$ exactamente una vez.

Con la descripción de estos conjuntos podemos hacer varias observaciones:

- $S$ serán los nodos que todavía no se han visitado.
- La longitud del circuito óptimo será $g(1, N-\{{1}\})$, es decir, por definición la longitud del camino desde el nodo 1 hasta el nodo 1 que pasa por todos los nodos una sola vez (ciclo Hamiltoniano).

Ahora vemos que si aplicamos el Principio de Optimalidad de Bellman, se verificará que:

$g(1, N-\{{1}\})= Min_{2 \leq j \leq n}[L_{1j}+g(j, N-\{{1,j}\}) ] $ , donde $L_{1j}$ es la distancia del nodo 1 al nodo j.

De una forma más clara, vemos todas las combinaciones entre los nodos que no incluyan a 1 y escogemos la que menor valor tiene.

Podemos incluir el conjunto $S$ que hemos definido antes y vemos que 

$g(i,S)= Min _{j\in S} [L_{ij} + g(j, S-\{{j}\})]$

Es decir que el recorrido óptimo que parte de i, recorre todos los nodos de $S$ (no visitados) y vuelve al 1 es el mismo que el recorrido que resulta de escoger el nodo $j$ que minimice la suma $L_{ij} + g(j, S-\{{j}\})$, (del nodo i al j, y del j a todos los demás) y volver a aplicarle el algoritmo al nodo j.

Rápidamente nos damos cuenta de que hemos llegado a una recurrencia, se trata de resolver el problema con una dimensión menor hasta llegar a dimensión 1, en la cual el recorrido devuelto es simplemente {j, 1}.

Aplicamos la misma fórmula para calcular g en todos los conjuntos $S$ que contienen dos nodos, luego con los de 3 y así sucesivamente hasta llegar a N-1, etapa en  la cual minimizamos todos los valores obtenidos para conseguir el recorrido óptimo.

### Segunda implementación

#### Descripción teórica del algoritmo

Para esta implementación, lo único que hemos cambiado ha sido la representación del conjunto $S$ mencionado en las especificaciones teóricas. Para poder mejorar un poco el algoritmo hemos implementado una **máscara**. 

Una **máscara** se utiliza como número base con el cual se realizan operaciones and u or para verificar una condición. En nuestro caso, la máscara se inicializa como 1, es decir

00000...(n veces)1

##### Actualización de la máscara

A medida que se van visitando las ciudades hacemos la operación básica **or** para añadir un 1 en la posición del nodo que ha sido visitado: `mascara = mascara | 1<<nodo`

Por ejemplo, si tenemos 8 nodos y hemos visitado el nodo 3, tendremos

00000001

que pasará a ser

​		00000001

or

​		00000100 

= 	00000101

##### Comprobación de la máscara

Para comprobar que la ciudad en la que estamos no ha sido visitadam hacemos una operación **and**, `mascara&(1<<nodo)`. 

Por ejemplo, en el caso anterior, si volvemos a encontrarnos en el nodo 3 el resultado del and será:

00000101&00000100=00000100

que claramente es distinto de 0. 

Es decir, esa operación comprueba que no hay un 1 en la posición del nodo que se quiere visitar.



##### Comprobación del caso base

El caso base, en este caso, que $S = \empty$ , se da cuando todos los nodos están visitados, y eso se consigue comprobar mediante un valor predefinido, en nuestro caso una variable global llamada `VISITADOTODAS`, que se inicializa con (`1<<N -1`), básicamente pone a 1 todas las posiciones de la máscara.

A la hora de comprobar si estamos en el caso base, vemos si la máscara es igual a dicho valor, en cuyo caso devolvemos el recorrido mencionado anteriormente {j, 1}.

#### Análisis de la eficiencia teórica

Sabemos por la teoría de la programación dinámica, que esta técnica de resolución de algoritmos es mucho menos eficiente que cualquiera de las otras que ya hemos analizado. Vamos a comprobarlo en el aspecto teórico:

Como ya hemos explicado, usamos una máscara para determinar si un nodo ha sido visitado o no, básicamente es la forma de ir eliminando los nodos ya visitados del conjunto $S$ de la anterior implementación. Se almacena esta máscara en un entero, que de primeras tendrá un valor de

0...(n veces)0

hasta llegar a 

11...(n veces)1, que indica que todos los nodos están visitados.

Por tanto vemos que el bucle interior for, que recorre todas las ciudades (n) tiene una eficiencia interior de $O(2^n)$, que es el número de operaciones que nos lleva recorrer todas las ciudades.

Realmente lo que estamos haciendo es todas las combinaciones posibles de las ciudades que todavía no están visitadas. Entonces tendríamos 

$O(2(n-1) + \sum_{k=1} ^{n-2}(n-1)k*C_{n-2}^k)= \\O(2(n-1) + (n-1)\sum_{k=1} ^{n-2}k*C_{n-2}^k) =  \\O(2(n-1) + (n-1)(n-2)*2^{(n-2)-1}= O(n^22^n) $

#### Análisis de la eficiencia empírica

Hemos ejecutado el programa con valores desde 1 hasta 12 obteniendo:

![ef](eficiencia/implementacion 1/ef.png)

#### Análisis de la eficiencia híbrida

~~~
*******************************************************************************
Mon May 11 16:53:43 2020


FIT:    data read from "eficiencia1.dat"
        format = z
        #datapoints = 12
        residuals are weighted equally (unit weight)

function used for fitting: f(x)
	f(x)=(a*x**2+ b*x+c)*2**(x)
fitted parameters initialized with current variable values

iter      chisq       delta/lim  lambda   a             b             c            
   0 3.6016087198e+09   0.00e+00  9.25e+03    1.753818e-03   1.000000e+00   1.000000e+00
   7 9.9133582927e+03  -1.04e-06  9.25e-04    4.900616e-02  -9.115350e-01   4.173815e+00

After 7 iterations the fit converged.
final sum of squares of residuals : 9913.36
rel. change during last iteration : -1.03607e-11

degrees of freedom    (FIT_NDF)                        : 9
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 33.1886
variance of residuals (reduced chisquare) = WSSR/ndf   : 1101.48

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a               = 0.0490062        +/- 0.007899     (16.12%)
b               = -0.911535        +/- 0.1715       (18.81%)
c               = 4.17382          +/- 0.9263       (22.19%)

correlation matrix of the fit parameters:
                a      b      c      
a               1.000 
b              -0.998  1.000 
c               0.991 -0.997  1.000 
~~~





![ajuste](eficiencia/implementacion 1/ajuste.png)

