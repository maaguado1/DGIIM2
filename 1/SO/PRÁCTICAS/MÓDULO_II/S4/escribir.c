#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#define NOMBRE_FIFO "FIFOpet"
#define FIFO_GENERAL "FIFO."

void error_argumentos(){
    printf("Uso: lanzador <num_cli> <dir_pathname>\n");
    exit(EXIT_FAILURE);
}

void error(char* mensaje){
	perror(mensaje);
	exit(EXIT_FAILURE);
}

struct mensaje_t{
	int num; // entre el 1 y 20
	char metadato; // 'i' o 's'
	int _pid;
};

int n_hijos_finalizados = 0;
static void sigchld_handler(int n){	
	int pid = wait(NULL);
	n_hijos_finalizados++;
	printf("Lanzador: finalización hijo %d\n", pid);
}

int main(int argc, char** argv){
	if (argc != 3)
		error_argumentos();			
	
	// MANEJO DE SEÑALES
	struct sigaction sa;
	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);	

	if (sigaction(SIGCHLD, &sa, NULL) == -1)
		error("Sigaction sigchld");
	
	//Creacion y apertura de FIFOpet
	char nombre_fifo[40];
	sprintf(nombre_fifo, "%s", NOMBRE_FIFO);
	if (mkfifo(nombre_fifo, S_IRUSR | S_IWUSR) == -1)
		error("\nError en el mkfifo.\n");
	
	//Descriptor de archivo
	int fd_pet;			
			
	// MAIN LOOP
	// Evitar el uso de atoi y atol, usamos strtol(long) y strtoi(int)
	// strtoi (char* algo, NULL, base)
	int numero = strtol((argv[1]), NULL, 10); 
	pid_t PID;
	for (int i = 0; i<numero; i++){
		if ((PID = fork()) == -1)
			error("\nError en el fork.\n");					
	
		//Variable declarada dentro del for, es como si no estuvieran declaradas fuera
		char nombre_fifo_personalizado[50];
		// CLIENTE
		if ( PID == 0 ){
			PID = getpid(); 
			// srand con la fecha para que la semilla sea unica, y no se vaya repitiendo
			srand (PID);
			
			//Creacion del FIFO personalizado
			sprintf(nombre_fifo_personalizado, "%s%d", FIFO_GENERAL, PID);
			if (mkfifo(nombre_fifo_personalizado, S_IRUSR | S_IWUSR) == -1)
				error("\nError en el mkfifo del fifo personalizado.\n");
															
			struct mensaje_t msg;
			
			// al poner %19 generamos un numero entre 0 y 19, al sumarle 1, entre 1 y 20
			msg.num = rand() % 20 +1; 
			
			//generamos el numero para elegir que metadato mostrar			
			msg.metadato = (rand()%2 ? 'i' : 's');
				
			msg._pid = PID;		
			
			//Abrimos FIFOpet, solo de escritura puesto que vamos a escribir en el namas
			if ((fd_pet = open(nombre_fifo, O_WRONLY)) == -1)
				error("Error en apertura de FIFOpet.\n");
			
			//Escribimos el mensaje en FIFOpet
			if(write(fd_pet, &msg, sizeof(msg)) != sizeof(msg))
				error("Write FIFOpet.\n");
			
			//Lo cerramos despues de la escritura
			close(fd_pet);
			
			//Apertura del archivo FIFO personalizado. Lo abrimos solo de lectura para que se bloquee mientrass no reciba respuesta del servidor
			int fd_personalizado;
			if ((fd_personalizado = open(nombre_fifo_personalizado, O_RDONLY)) == -1)
				error("\nError en el open del personalizado.\n");
												
			int respuesta, leidos;				 
			if(leidos = read(fd_personalizado, &respuesta, sizeof(int) != 0)){
				if (leidos == -1){
					perror("\nError lectura fifo personalizado.\n");
					exit(-1);
				}
			}
			//Lo cerramos despues de la lectura, q sino se nos olvida
			close(fd_personalizado);
			//Como ya no nos sirve de nada, lo borramos
			if(unlink(nombre_fifo_personalizado) == -1)
				error("Error al cerrar el FIFO personalizado.\n");
			else 
				printf("Eliminado el FIFO personalizado.\n");
										
			if (respuesta ==-1)
				printf("Cliente %d: FALLO.\n", PID);
			else if (msg.metadato == 'i')					
				printf("\nCliente %d: El numero de inodo es: %d.\n", PID, respuesta);					
			else if (msg.metadato == 's') 
				printf("\nCliente %d: El tamanio del archivo es: %d.\n",PID, respuesta);
									
			//finaliza la ejecucion el hijo
			exit(0);
		}				
	}
	
	//Cuando ejecutamos execl no hay vuelta atras, (se sustituye el proceso actual por el de execl. Por eso hacemos un fork. 
	if ((PID = fork()) == -1)
		error("Fork.\n");
	if(PID == 0){
		PID = getpid();
		printf("mi pid: %d.\n", PID);
		if ((fd_pet = open(nombre_fifo, O_RDONLY)) == -1)
			error("Open fifopet.\n");
		dup2(fd_pet,STDIN_FILENO);		
		if (execl("servidor","servidor", argv[2], NULL) != -1)
			error("Error en el execl.\n");		
		
		exit(0);
	}
			
	// El padre espera a que terminen todos los clientes y el servidor
	// para borrar el fifo de peticiones. Los fifos privados se borran
	// en el sigchld handler.
	// No se por qué a veces no se recibe alguna sigchld, por tanto se 
	// queda en este bucle esperando y no se borra fifo_peticiones
	while(n_hijos_finalizados<numero+1)	;
	if(unlink(nombre_fifo) == -1)
		error("Unlink.\n");
	printf("FIN TODO DESDE  LANZADOR\n");
												
	return 0;
}


	
	
	



