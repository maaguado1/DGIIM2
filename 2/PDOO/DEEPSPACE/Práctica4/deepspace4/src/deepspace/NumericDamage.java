
package deepspace;

import java.util.ArrayList;

class NumericDamage extends Damage{

    private int nWeapons;

    NumericDamage(int w, int s){
        super(s);
        nWeapons=w;

    }

    NumericDamage(NumericDamage n){
        super(n.getNShields());
        nWeapons= n.getNWeapons();
    }

    public int getNWeapons(){
        return nWeapons;
    }


    @Override
    public void discardWeapon(Weapon w){
        if (nWeapons>0)
            nWeapons --;
    }


    @Override
    public boolean hasNoEffect(){
        if (nWeapons==0  && getNShields()==0)
            return true;
        else
            return false;
    }

    @Override
    public NumericDamage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s){

        int min1;
        int min2;
        min1= (s.size() < getNShields())? s.size(): getNShields();
        min2=(w.size() < nWeapons) ? w.size():nWeapons;
        NumericDamage ajustado = new NumericDamage(min2, min1);
        return ajustado;
    }



    @Override
     NumericDamageToUI getUIversion(){
        return new NumericDamageToUI(this);
    }

    @Override
    public String toString(){
    String message = "[Damage]: Number of Shields: " + getNShields()
            + ", Number of Weapons: " + getNWeapons();       
    return message;
    }
};


