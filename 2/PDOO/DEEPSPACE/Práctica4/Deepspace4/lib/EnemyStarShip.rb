#encoding: utf-8

require_relative 'Loot'
require_relative 'Damage'
require_relative 'ShotResult'
require_relative  'EnemyToUI'

module Deepspace
  
# Represents the enemy of the game: an enemy star ship
#
# @author María Aguado Martínez
  
class EnemyStarShip
  
  # Constructors
	# ==========================================================================
	
  
  # Class initializer
	# @param _name [String] name of the ship
	# @param _ammo [Float] parametrizes ammunition power
	# @param _shield [Float] parametrizes shield power
	# @param _loot [Loot] associated loot
	# @param _damage [Damage] associated damage
  
  def initialize(_name, _ammo, _shield, _loot, _damage)
    @name =_name
    @ammoPower=_ammo
    @shieldPower=_shield
    @loot = _loot
    @damage = _damage
    
  end
  
  
  
   # Class copy initializer
	# @param d Enemy from which we make the copy
  
  def self.newCopy(d)
    return new(d.name, d.ammoPower, d.shieldPower, d.loot, d.damage)
  end
  
  
  # Getters
	# ==========================================================================

  attr_reader :name
  attr_reader :ammoPower
  attr_reader :shieldPower
  attr_reader :loot
  attr_reader :damage
  
  
  
  # Return star ship's shieldPower
	# @return [Float] the protection it assures
  def protection
    return @shieldPower
    
  end
  
  # Return star ship's ammoPower
	# @return [Float] the damage it can do
  def fire
    return @ammoPower
  end
  
  
  
  # Returns whether the star ship receives a certain shot or not
	# @param shot [Float] power of shot taken
	# @return [ShotResult] RESIST, if shieldPower >= shot;
	#     
  def receiveShot(shot)
    if (protection < shot)
      return ShotResult::DONOTRESIST
    else
      return ShotResult::RESIST
    end
  end
  
  
  
  # String representation, UI version
	# ==========================================================================

  def getUIversion
    return EnemyToUI.new(self)
  end
  
  
  def to_s
    return "[EnemyStarship]: Name: " + name
                + ", Protection: " + protection()
                + ", Fire: " + fire()
                + ", Loot: " + loot.to_s
                + ", Damage: " + damage.to_s;
  end
end



end