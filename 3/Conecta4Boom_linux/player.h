#ifndef PLAYER_H
#define PLAYER_H

#include "environment.h"

class Player{
    public:
      Player(int jug);
      Environment::ActionType Think();
      void Perceive(const Environment &env);
      double Poda_AlfaBeta(const Environment &estado, int jugador, int profundidadactual, int profundidadmaxima, Environment::ActionType &accion, double alpha, double beta);
      bool Tablerovacio();
    private:
      int jugador_;
      Environment actual_;
      bool inicio;
};
#endif
