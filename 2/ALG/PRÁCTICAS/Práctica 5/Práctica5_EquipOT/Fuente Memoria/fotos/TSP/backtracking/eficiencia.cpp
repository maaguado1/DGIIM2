#include <algorithm>
#include <chrono>
#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto
{
  float x;
  float y;
};

struct Arista
{
  int inicio;
  int final;
};

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: F U N C I O N E S    C O M U N E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Generacion de datos para el problema
//==============================================================================

double random_double(double min, double max)
{

  return (rand() / (double)RAND_MAX) * (max - min) + min;
}
/**
 * @brief Genera los puntos necesarios para el problema
 * @param num_points, la cantidad de puntos con la que queremos trabajar
 * @return un vector de puntos, cuyas coordenadas estan en el intevalo [0, num_points]
 * */
vector<Punto> generarProblema(int n)
{
  vector<Punto> nodos;
  srand(time(NULL));
  for (int i = 0; i < n; i++)
  {
    double x = random_double(1, n);
    double y = random_double(1, n);
    Punto p = {x, y};
    nodos.push_back(p);
  }

  return nodos;
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */
int distancia(Punto uno, Punto dos)
{
  return rint(sqrt(pow((dos.x - uno.x), 2) + pow((dos.y - uno.y), 2)));
}

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T)
{
  int n = T.size();
  for (int i = 0; i < n; i++)
  {
    cout << "\n| ";
    for (int j = 0; j < n; j++)
    {
      cout << T[i][j] << " ";
    }

    cout << "  |";
  }
  cout << "\n\n";
}

void salidavec(vector<int> vector)
{
  cout << "\nsacando...";
  for (int i = 0; i < vector.size(); i++)
  {
    cout << vector[i] << " ";
  }
  cout << "\n";
}

/**
 * @brief Genera una matriz con las distancias entre los puntos
 * @param nodos, los puntos sobre los que calculamos las distancias
 * @return una matriz con las distancias entre los puntos
 *
 * Matriz[i][j] := distance(pi, pj)
 *
 * */
vector<vector<int>> genera_matriz(vector<Punto> nodos)
{
  vector<vector<int>> T;
  int n = nodos.size();
  for (int i = 0; i < n; i++)
  {
    vector<int> fila;
    for (int j = 0; j < n; j++)
    {
      if (j < i)
        fila.push_back(T[j][i]);
      else if (j == i)
        fila.push_back(0.0);
      else
        fila.push_back(distancia(nodos[i], nodos[j]));
    }
    T.push_back(fila);
  }
  return T;
}

/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

int calcular_valor(vector<int> solucion, vector<vector<int>> T)
{
  int valor = 0;
  for (int i = 0; i < solucion.size() - 1; i++)
  {
    valor += T[solucion[i]][solucion[i + 1]];
  }
  return valor;
}

vector<int> append(vector<int> &uno, vector<int> extra)
{
  for (int i = 0; i < extra.size(); i++)
  {
    uno.push_back(extra[i]);
  }
  return uno;
}

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: B A C K T R A C K I N G    : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//
/**
  * @brief Calcula el algoritmo óptimo mediante una resolución por backtracking.
  * @param v, vector que indica si cada nodo está visitado o no
  * @param T, la matriz de distancias de los puntos con los que trabajamos
  * @param posicion, el número del nodo en el que estamos
  * @param n , número de nodos
  * @param nivel, el nivel del árbol de soluciones en el que estamos
  * @param costemin, el coste mínimo obtenido hasta la fecha
  * @param solucionparcial, la solucion que estamos construyendo
  * @param solucion, la mejor solucion que hemos obtenido hasta la fecha
  * @pre T.size() > 0
  * */
void backtracking(vector<bool> &v, int posicion, vector<vector<int>> T, int n, int nivel, int costeactual, int &costemin, vector<int> solucionparcial, vector<int> &solucion)
{

  if (nivel == n)
  {
    if (costeactual + T[posicion][0] < costemin)
    {
      costemin = costeactual + T[posicion][0];
      solucion = solucionparcial;
    }
    return;
  }

  for (int i = 0; i < n; i++)
  {
    if (!v[i])
    {

      v[i] = true;
      solucionparcial.push_back(i);
      backtracking(v, i, T, n, nivel + 1,
                   costeactual + T[posicion][i], costemin, solucionparcial, solucion);

      // Mark ith node as unvisited
      solucionparcial.pop_back();
      v[i] = false;
    }
  }
}

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: P R O G R A M A C I Ó N   D I N Á M I C A   : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int VISITADO_TODAS;
vector<int> programaciondinamica(vector<vector<int>> T, int pos, int mascara)
{

  vector<int> respuesta;
  respuesta.push_back(pos);
  if (mascara == VISITADO_TODAS)
  {
    respuesta.push_back(0);
    return respuesta;
  }

  int rep = INT_MAX;
  vector<int> uno, otro;

  for (int nodo = 0; nodo < T.size(); nodo++)
  {
    //Si la ciudad no está visitada
    if ((mascara & (1 << nodo)) == 0)
    {

      //Comprobamos todas las combinaciones con dicha ciudad
      uno = programaciondinamica(T, nodo, mascara | (1 << nodo));
      int nuevaresp = T[pos][nodo] + calcular_valor(uno, T);
      if (nuevaresp < rep)
      {
        rep = nuevaresp;
        otro = uno;
      }
    }
  }
  append(respuesta, otro);
  return respuesta;
}

//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: C E R C A N Í A  : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

// Algoritmo basado en cercanía.
/**
 * @brief Calcula la mejor solucion para un conjunto de puntos, por Shortest Neighbor First
 * @param T, la matriz de distancias de los puntos con los que trabajamos
 * @pre T.size() > 0
 * @return el camino cuya distancia es la mínima, si todo sale bien o un vector vacío, si ocurre algún error.
 * */
vector<int> solucion_cercania(vector<vector<int>> distancias)
{
  // Creamos el vector que contendrá el recorrido.
  vector<int> solucion;      //O(1)
  int n = distancias.size(); //O(1)
  int distancia_minima = 0;  //O(1)
  int distancia = 0;         //O(1)
  // Seleccionamos la primera ciudad de forma aleatoria
  int posicion = rand() % n; //O(1)
  int i;                     //O(1)
  // Introducimos la primera ciudad en el vector solución
  solucion.push_back(posicion); //O(1)

  //Mientramos no recorramos todas las ciudades, continuamos escogiendo.
  while (solucion.size() < distancias.size())
  {                             //O(n-1)
    i = posicion;               //O(1)
    distancia_minima = INT_MAX; //O(1)
    //Veamos quién es la ciudad más cercana a la actual(i)
    for (int j = 0; j < distancias.size(); j++)
    { //O(n)
      //Comprobamos si dicha ciudad se encuentra ya en el vector solución. Si no es así, se mira si la distancia es menor que la distancia mínima actual. Esta parte se corresponde con la eliminación de la ciudad del conjunto de candidatos una vez ha sido escogida.
      if (find(solucion.begin(), solucion.end(), j) == solucion.end())
      {                               //O(1)
        distancia = distancias[i][j]; //O(1)
        // Si la distancia es menor, dicha distancia pasa a ser la mínima y dicha ciudad pasa a ser la escogida.
        if (distancia < distancia_minima)
        {                               //O(1)
          posicion = j;                 //O(1)
          distancia_minima = distancia; //O(1)
        }
      }
    }
    // Una vez encontrada dicha ciudad, la añadimos al vector solución y esta pasa a ser la ciudad actual.
    solucion.push_back(posicion); //O(1)
  }
  return solucion; //O(1)
}










//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: B R A N C H & B O U N D  : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//


struct nodo{
	vector<int> visitadas;
	int CI;
	int valor_actual;
	
	bool operator< (nodo otro) const{
		return CI < otro.CI;
	}
};

vector<int> obtener_arcos(vector<vector<int>> T){
	vector<int> mejor_arco;
	for (int i=0; i < T.size(); i++){
		int min = INT_MAX;
		for(int j=0; j<T.size(); j++)
			if(T[i][j] < min && i!=j){
				min=T[i][j];
			}
		mejor_arco.push_back(min);
	}
	return mejor_arco;
}

int CI(vector<int> visitadas, vector<int> arcos, int valor_actual, int n){
	int suma = valor_actual;
	for (int i=1; i < n; i++)
		if (find(visitadas.begin(),visitadas.end(),i)==visitadas.end())
			suma += arcos[i];
			
	suma = arcos[0];
	return suma;	
}

int podar(multiset<nodo> &s, int min, int podas){
	multiset<nodo>::iterator it;
	while (!s.empty() && (*s.rbegin()).CI >= min){
		it = s.end();
		it--;
		s.erase(it);
		podas++;
	}
	return podas;
}

vector<int> solucion_ByB(vector<vector<int>> T, int& podas, int& expansiones, int& max_size){
	vector<int> solucion, visitadas;
	multiset<nodo> nodos_vivos;
	vector<int> arcos = obtener_arcos(T);
	visitadas.push_back(0);
	nodo inicial={visitadas,0,0};
	nodos_vivos.insert(inicial);
	solucion = solucion_cercania(T);
	int C = calcular_valor2(solucion,T);
	
	
	while(!nodos_vivos.empty()){
		nodo x=*nodos_vivos.begin();
		nodos_vivos.erase(nodos_vivos.begin());
		
		if (x.visitadas.size() == T.size()){
			x.valor_actual+=T[x.visitadas.front()][x.visitadas.back()];
			if (x.valor_actual < C){
				solucion = x.visitadas;
				C=x.valor_actual;
				podas = podar(nodos_vivos,C,podas);
			}
			
		}else{
			nodo y;
			expansiones++;
			
			for(int i=1; i < T.size(); i++){
				if (find(x.visitadas.begin(), x.visitadas.end(), i) == x.visitadas.end()){
					y=x;
					y.valor_actual+=T[i][x.visitadas.back()];
					y.visitadas.push_back(i);
					y.CI=CI(y.visitadas,arcos,y.valor_actual,T.size());
					if (y.CI < C)
						nodos_vivos.insert(y);
				}	
			}
			if (max_size < nodos_vivos.size())
				max_size = nodos_vivos.size();
		}
	}
	return solucion;	
}	
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    printf("Error, modo de ejecución; ./eficiencia <tamaño>");
    exit(-1);
  }

  clock_t t_antes;   // Valor del reloj antes de la ejecución
  clock_t t_despues; // Valor del reloj después de la ejecución

  string algoritmo;
  int tamanio;
  vector<vector<int>> matriz;
  vector<int> solucion1, solucion2, solucion3, solucion4;
  vector<bool> v;
  int minimo = INT_MAX;
  double t1, t2, t3, t4;
  int valor1, valor2, valor3, valor4;

  tamanio = atoi(argv[1]);
  //Generamos los problemas
  vector<Punto> puntos = generarProblema(tamanio);

  //Calculamos la matriz de distancias y aplicamos el algoritmo indicado.
  matriz = genera_matriz(puntos);

  
  cout << "\nCalculando solución por cercanía..." << endl;
  t_antes = clock();
  solucion1 = solucion_cercania(matriz);
  solucion1.push_back(solucion1[0]);
  t_despues = clock();


  valor1 = calcular_valor(solucion1, matriz);
  t1 = (double)(t_despues - t_antes) / CLOCKS_PER_SEC;
  
  
  cout << "\nCalculando solución por programación dinámica..." << endl;
  VISITADO_TODAS = (1 << matriz.size()) - 1;

  t_antes= clock();
  solucion2 = programaciondinamica(matriz, 0, 1);
  t_despues = clock();
  valor2 = calcular_valor(solucion2, matriz);
  t2=(double)(t_despues - t_antes) / CLOCKS_PER_SEC;
  
  
  cout << "\nCalculando solución por backtracking..." << endl;
  for (int i =0; i < tamanio; i ++){
    v.push_back(false);
  }
  v[0] = true;
  solucion3.push_back(0);
  vector<int> solparcial = solucion3;
  t_antes = clock();
  backtracking(v, 0, matriz, tamanio, 1, 0, minimo, solparcial, solucion3);
  t_despues = clock();
  valor3=minimo;
  solucion3.push_back(solucion3[0]);
  t3=(double)(t_despues - t_antes) / CLOCKS_PER_SEC;
  


  cout << "\nCalculando solución por branch and bound..." << endl;
  t_antes = clock();
  solucion4=solucion_ByB(matriz,podas,expansiones,max_size);
  t_despues = clock();
  valor3=calcular_valor(solucion4, matriz);
  t4=(double)(t_despues - t_antes) / CLOCKS_PER_SEC;
  
  //Mostramos los datos

  
  cout << "\nLa solución obtenida por cercanía es.... (valor = " << valor1 << "; tiempo = " << t1 << ")" << endl;
  salidavec(solucion1);
  
  
  cout << "\nLa solución obtenida por programación dinámica es.... (valor = " << valor2 << "; tiempo = " << t2 << ")" << endl;
  salidavec(solucion2);

  
  cout << "\nLa solución obtenida por backtracking es.... (valor = " << valor3 << "; tiempo = " << t3 << ")" << endl;
  salidavec(solucion3);
  

   cout << "\nLa solución obtenida por branch and bound es.... (valor = " << valor4 << "; tiempo = " << t4 << ")" << endl;
  salidavec(solucion4);
  
}
