#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>

#define SIZE=1024;

int main (int argc, char *argv[]){
    int fd, fd3, leidos;
    int mipid =getpid();
    char nombre[50];
    FILE * temporal = tmpfile();
    sprintf (nombre, "fifoprox%s", mipid );
    mkfifo (nombre, S_IRWXU);
    char buffer[SIZE];

    if ((fd = open (nombreinput, O_RDONLY))<0){
    perror ("\nError al abrir el FIFO del proxy");
    exit (-1);
  }

    dup2(fd, STDIN_FILENO);

    //Leo del descriptor, guardo en tmp

    while ((leidos = read(STDIN_FILENO, buffer, SIZE)) != 0){

        if ( leidos == -1){
            perror ("\nError en el read del proxy");
            return -1;
        }

        if ((fwrite(buffer, 1, leidos, temporal)) != leidos){
            perror ("\nError en el write del proxy");
            return -1;
        }
    }

    //PONEMOS EL BLOQUEO
    if ((fd3 = open ("bloqueo", O_RDWR))<0){
    perror ("\nError al abrir el FIFO del proxy");
    exit (-1);
  }
    struct flock cerrojo;
    int fd;
    cerrojo.l_type = F_WRLCK;
    cerrojo.l_whence = SEEK_SET;
    cerrojo.l_start =0;
    cerrojo.l_len =0;

    if (fcntl (fd3, F_SETLK, &cerrojo) == -1){
        perror ("\nError en el bloqueo del proxy");
            return -1;
    }
    

    //ESCRIBIMOS EN PANTALLA
    fseek(temporal, 0, SEEK_SET);

    while ((leidos = fread(buffer, 1, SIZE, temporal) != 0){

        if ( leidos == -1){
            perror ("\nError en el read del temporal");
            return -1;
        }

        if ((write(STDIN_FILENO, buffer, leidos)) != leidos){
            perror ("\nError en el write del temporal");
            return -1;
        }
    }
    cerojo.l_type =F_UNLCK;
    
    if (fcntl (fd3, F_SETLK, &cerrojo) == -1){
        perror ("\nError en el desbloqueo del proxy");
            return -1;
    }
    
    close (fd3);
    close (temporal);
    if ((unlink(nombre)==-1)){
        perror ("\nError al eliminar el fifo de entrada");
        exit (-1);
    }


    return 0;
}