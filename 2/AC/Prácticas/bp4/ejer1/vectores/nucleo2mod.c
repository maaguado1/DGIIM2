/*
 * Nucleo original
 */
#include "nucleo.h"

extern int R[];
extern int M;
extern int N;

inline void core()
{
    int i, j, x1, x2;

    for (j = 0; j < 40000; j++)
    {
        x1 = 0;
        x2 = 0;
        for (i = 0; i < 5000; i += 4)
        {
            x1 += 2 * s[i].a + j;
            x1 += 2 * s[i + 1].a + j;
            x1 += 2 * s[i + 2].a + j;
            x1 += 2 * s[i + 3].a + j;
        }
        for (i = 0; i < 5000; i += 4)
        {
            x2 += 3 * s[i].b - j;
            x2 += 3 * s[i + 1].b - j;
            x2 += 3 * s[i + 2].b - j;
            x2 += 3 * s[i + 3].b - j;
        }

        if (x1 < x2)
            R[j] = x1;
        else
            R[j] = x2;
    }
}
