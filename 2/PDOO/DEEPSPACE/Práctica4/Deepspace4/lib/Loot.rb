#encoding:utf-8

require_relative 'LootToUI'

module Deepspace

  # Class that represents the loot gained by defeating an enemy ship
  class Loot
    
    
    
 # Constructors
	# ==========================================================================

	# Class initializer
	# @param _nSupplies [Integer] number of supplies given by a loot
	# @param _nWeapons [Integer] number of weapons given by a loot
	# @param _nShields [Integer] number of boosters given by a loot
	# @param _nHangars [Integer] number of hangars given by a loot
	# @param _nMedals [Integer] number of medals given by a loot
	
    def initialize(sup, wea, shi, han, med, ef=false, city=false)
    @nSupplies = sup;
    @nWeapons = wea;
    @nShields= shi;
    @nHangars= han;
    @nMedals = med;
    @efficient = ef;
    @spaceCity= city;

  end
  
  
  # Getters: 
  
  def nSupplies
    @nSupplies
  end
  
  def nWeapons
    @nWeapons
  end
  
  def nShields
    @nShields
  end
  
  def nHangars
    @nHangars
  end
  
  def nMedals
    @nMedals
  end

  def efficient
    @efficient
  end
  
  def spaceCity
    @spaceCity
  end
  
  
  def to_s
    mensaje = "[Loot] -> nSupplies: #{@nSupplies}, nWeapons: #{@nWeapons}, nShields: #{@nShields}, nHangars: #{@nHangars}, nMedals #{@nMedals}, Get Efficient #{@getEfficient}, spaceCity #{@spaceCity}"
    return mensaje
  end
  
  
  def getUIversion
    return LootToUI.new(self)
  end
  
  
end # class Loot

end #module Deepspace

