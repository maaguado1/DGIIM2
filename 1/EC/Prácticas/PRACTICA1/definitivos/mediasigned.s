# suma.s: Sumar los elementos de una lista
#         llamando a función, pasando argumentos mediante registros
#         retorna: código retorno 0, comprobar suma en %eax mediante gdb/ddd

# SECCIÓN DE DATOS (.data, variables globales inicializadas)
.section .data

	.macro linea 
		.int -2,3,4,1
	.endm
lista: .irpc i, 12345678
		linea
	.endr


	
longlista:
	.int (.-lista)/4 # .= contador posiciones. Aritmética de etiquetas.
resultado:
	.quad -1
formato:
	.ascii "suma = %d / 0x%x\n"

media:
	.int -1
resto:
	.int -1


# SECCIÓN DE CÓDIGO (.text, instrucciones máquina)
.section .text


main: .global main

	mov $lista, %ebx    # dirección del array lista
	mov longlista, %ecx # número de elementos a sumar
	call suma           # llamar suma(&lista, longlista);
	idiv %ecx
	mov %eax, media
	mov %ecx, resto


	push media      # apila resultado
#	push resultado      # apila resultado
	push $formato       # apila formato
	xor %eax, %eax      # number of arguments in registers
	call printf         # llamada a función printf(&formato, resultado)
	add $12, %esp       # dejar pila intacta
			    # void _exit(int status);
	mov $1, %eax 		# exit: servicio 1 kernel Linux
	mov $0, %ebx 		# status: código a retornar (0=OK)
	int $0x80    		# llamar _exit(0);







# SUBRUTINA: int suma(int* lista, int longlista);
#            entrada: 1) %ebx = dirección inicio array
#                     2) %ecx = número de elementos a sumar
#            salida:     %eax = resultado de la suma






suma:
	push %edx    # preservar %edx (se usa aquí como índice)
	mov $0, %eax # poner a 0 acumulador
	mov $0, %edi
	mov $0, %edx
	mov $0, %esi # poner a 0 índice
	mov $0, %ebp

bucle:
	mov (%ebx, %esi, 4), %eax  			#se almacena el elemento i-ésimo en eax
	cdq 						#por defecto: transforma eax en edx:eax 
							#(parejita feliz), para salvaguardar signo

	add %eax, %ebp 				#guardamos en el acumulador (1) la suma
	adc %edx, %edi
	inc %esi
	cmp %esi, %ecx
	
	jne bucle
	
	mov %ebp, %eax
	mov %edi, %edx
	
	pop %esi
	ret













	
