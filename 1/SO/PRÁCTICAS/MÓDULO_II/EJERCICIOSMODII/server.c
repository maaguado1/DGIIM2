//Programa: server.c

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>


int gestionarPeticion(char* nombrearchivo){

    int fd, PID;
    //Creo el FIFO y lo abro
    mkfifo (nombrearchivo, S_IRWXU);
    if ((fd = open (nombrearchivo, O_WRONLY))<0){
    perror ("\nError al abrir el FIFO del servidor");
    exit (-1);
    }

    //Creo un fork

    PID=fork();

    
    if (PID==0){

        int mipd=getpid();
        if (! (resultado =write (fd, ,mipd, sizeof(int)))) {
            perror ("Cliente: error al escribir peticion en fifo conocido del servidor");
            return -1
        }

        //En el caso del hijo, ejecuto el proxy
        if ((execl ( "./proxy", "./proxy", NULL)) <0){
        perror ("\nError en al crear el proceso proxy");
        return -1
        }


        //ESCRIBIR LA VUELTA (LUEGO)

    }
    else {
        printf ("\nSoy el padre no hago nada");
        //Escribo el PID del creado en el fifo de salida
        
    }
    return 0

}

void manejador (){
    printf ("\nMe han enviado una señal");
    wait(NULL);
}

int main (int argc, char* argv[]){

  if (argc != 2){
    printf ("\nError de argumentos");
    exit (-1);
  }

    //MANEJADOR DE INTERRUPCIONES
    struct sigaction nueva;
    nueva.sa_handler = manejador;
    nueva.sa_flags=SA_RESTART;
    sigemptyset(&nueva.sa_mask);

    sigaction (SIGCHILD, nueva);
    

    
    //CREACIÓN DE ARCHIVO CERROJO PARA LA SALIDA

    flock nuevocerrojo;
    creat ("bloqueo", S_IRWXU);
    

    
  

  //TRATAMIENTO DE FIFOS
  char[50] nombreoutput;
  char[50] nombreinput;
  int peticion;
    bool nopeticiones = true;
  sprintf(nombreinput, "/tmp/%de", argv[1]);
  sprintf(nombreoutput,"/tmp/%ds", argv[1]);
  char[100] mensaje;
  int fd, fd2;
  printf ("\nEstoy creando los archivos FIFO (Soy server)");
  mkfifo (nombreinput, S_IRWXU);
    


  if ((fd = open (nombreinput, O_RDONLY))<0){
    perror ("\nError al abrir el FIFO del servidor");
    exit (-1);
  }


while (nopeticiones){ //Evalúo hasta que haya una petición
  if (read(fd, peticion, sizeof(int))){
      printf ("\nHe recibido una petición del proceso %s, voy a gestionarla (pasarle el proxy)", peticion);
      gestionarPeticion(nombreoutput);

}



  