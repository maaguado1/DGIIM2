/* 
 * @file instrucciones.cpp 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo fuente de la clase @c instrucciones
 */

#include <iostream>
#include <string>
#include "instrucciones.h"
#include <vector>

#include <stack>
#include <sstream>
 
 using namespace std;

instrucciones::instrucciones(){
    acciones nueva;
    acc=nueva;
    ArbolBinario<string> nuevo(" ");
    datos=nuevo;
}

instrucciones::instrucciones(const instrucciones& orig){
    datos=orig.datos;
    acc=orig.acc;
}



instrucciones::instrucciones(const acciones& acci){
    ArbolBinario<string> nuevo;
    datos=nuevo;
    acc = acci;
}


instrucciones instrucciones::operator+(const instrucciones& otro)const{
    instrucciones suma=*this;
    instrucciones uno = *this;
    instrucciones dos = otro;

    ArbolBinario<string> fusion(" ");
    fusion.Insertar_Hi(fusion.getRaiz(), uno.datos );
    fusion.Insertar_Hd(fusion.getRaiz(), dos.datos);
    suma.datos = fusion;
    return suma;
}

instrucciones& instrucciones::operator=(const instrucciones& orig){
    datos = orig.datos;
    acc = orig.acc;
    ings = orig.ings;
    return *this;
}

void instrucciones::setIngs(const ingredientes& ing){
    ings = ing;
}


std::ostream & operator<<(std::ostream & os, const instrucciones & nombres){
    ArbolBinario<string>::postorden_iterador it= nombres.datos.beginpostorden();
    ArbolBinario<string>::postorden_iterador ot;
    vector<string> ings;
    while (it != nombres.datos.endpostorden()){
        //Verifico si es nodo hoja o q es
        if (it.hi() != ArbolBinario<string>::postorden_iterador()){
            char a =nombres.acc[*it];
            if (ings.size() <= (a-48)){
                cout << *it ;

                for (int i =0; i < ings.size(); i++){ //No entra en este bucle y no se pq
                    cout << " "<< ings[i];
                }
                cout << endl;
                ings.erase(ings.begin(), ings.end());
            }
            else{
                cout << *it << endl;
            }
            
        }
        
        //Estoy en un hijo, osea q es ingrediente, me voy al padre y guardo los hijos que tenga como ingredientes
        else{

            if(it.padre().hi() != ArbolBinario<string>::postorden_iterador() && !(nombres.getAcciones()).esta(*it.padre().hi())){
                ings.push_back(*(it.padre().hi())); //Lo que metemos es una etiqueta, operador* del nodo, es un string.
            }
            //PROBLEMA: hay nodos en los que ambos lados son una accion, o solo un nodo
            if(it.padre().hd()!= ArbolBinario<string>::postorden_iterador() && !(nombres.getAcciones()).esta(*it.padre().hd()) ){
                ings.push_back(*(it.padre().hd()));
                if ((nombres.getIngs()).Esta((nombres.getIngs()).get( *(it.padre().hd())))){
                    ++it;
                }
                
            }
                
        }
        //Subo al padre
        ++it;
        
    }
    return os;
}

std::istream & operator>>(std::istream & is, instrucciones & nombres){
    stack<ArbolBinario<string>> pilaingredientes;

    string linea;
    string ingr1;
    string ingr2, medio;
    string accion;
    char ari;

    //LECTURA DE DATOS INICIALES PARA EL BUCLE
    stringstream ss;
    bool noing = false;



    while(is && is.peek()!=EOF){

    
        getline(is, linea);
        ss.str(linea);
        int ingredientes=0, actuales=0;
        getline(ss, accion, ' ');
        ari = nombres.acc[accion];
        ArbolBinario<string> T(accion);
        if (ss.eof())
            noing = true;
        
            
            
        if (ari == '1'){
            
            getline(ss, ingr1, ' ');
            while (!(nombres.getIngs()).Esta((nombres.getIngs()).get(ingr1)) && !ss.eof()){
                ingr1 += " ";
                getline(ss, medio , ' ');
                ingr1 +=medio;
            }
            
            
            if (noing){

                //Inserto como hijo izquierdo el último subárbol insertado
                
                T.Insertar_Hi(T.getRaiz(),  pilaingredientes.top() );
                pilaingredientes.pop();

                pilaingredientes.push(T);
                //El último aniadido no cambia, se ha metido debajo, si el último era izquierda, se mete en hijo izquierda y se mantiene
            }

            else{
                T.Insertar_Hi(T.getRaiz(), ingr1);
                //Si el último fue a la izquierda, se añade a la derecha y se actualiza el valor de ultimoaniadido
                pilaingredientes.push(T);

            }
        
        }

        else if (ari == '2'){
            //En aridad 2 lo tenemos que coger de los anteriores
            ingredientes=2;
            
            
            
            if (noing){

                T.Insertar_Hd(T.getRaiz(),  pilaingredientes.top() );
                pilaingredientes.pop();
                T.Insertar_Hi(T.getRaiz(),  pilaingredientes.top() );
                pilaingredientes.pop();
                
                pilaingredientes.push(T);
            }
            else{
                while (actuales < ingredientes && !ss.eof()){

                    getline(ss, ingr2, ' ');
                    while (!(nombres.getIngs()).Esta((nombres.getIngs()).get(ingr2)) && !ss.eof()){
                        ingr2 += " ";
                        getline(ss, medio , ' ');
                        ingr2 += medio;
                    }
                    if (actuales == 0)
                        ingr1 = ingr2;
                    actuales ++;
                }

                if (actuales ==1){
                    T.Insertar_Hi(T.getRaiz(),  pilaingredientes.top() );
                    pilaingredientes.pop();
                    T.Insertar_Hd(T.getRaiz(),  ingr1);
                    
                    pilaingredientes.push(T);
                }

                else if (actuales == 2){
                    T.Insertar_Hi(T.getRaiz(),  ingr1);
                    T.Insertar_Hd(T.getRaiz(),  ingr2);
                    pilaingredientes.push(T);
                }
                
                
            }
        }


        //Vuelvo a leer línea y recuperar acción
        
        ss.clear();
        noing = false;
        
        
    } 
    
    nombres.datos=pilaingredientes.top();
    pilaingredientes.pop();
    return is;

}




