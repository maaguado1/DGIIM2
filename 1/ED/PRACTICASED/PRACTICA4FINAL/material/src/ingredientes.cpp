#include <assert.h>
#include <iostream>
#include <string.h>
#include "ingredientes.h"
#include "ingrediente.h"
#include <cmath>
#include <vector>

using namespace std;

ingredientes::ingredientes (){
    datos.resize(0);
    posicionesTipo.resize(0);

}





ingredientes::ingredientes(const ingredientes& original){


    datos.resize(original.size());
    posicionesTipo.resize(original.size());
    ingredientes::const_iterator i(original.begin());
    for (ingredientes::iterator it = begin(); it!=end() && i!= original.end(); ++it){
        *(it) =*i;
        ++i;
    };


    vector<int>::const_iterator cont = original.posicionesTipo.begin();
    for (vector<int>::iterator ot=posicionesTipo.begin(); ot != posicionesTipo.end()&& cont != original.posicionesTipo.end(); ++ot){
        *(ot)= *cont;
        ++cont;
    };
}


//Da el iterador equivalente a datos[posicionesTipo[i]]= datos[*it]=*pos
ingredientes::iterator ingredientes::posDatos(int it){
    ingredientes::iterator pos=begin();
    for (int i =0; i < it; i++)
        ++pos;
    return pos;

}


bool ingredientes::Esta(const ingrediente & buscar){
    bool encontrado=false;
    for (ingredientes::iterator i= begin(); i!=end() && !encontrado; ++i){
        if (*i==buscar)
            encontrado=true;
        

    }
    return encontrado;
}

void ingredientes::insertar(const ingrediente& nuevo){
    bool posencontrada=false;
    ingredientes::iterator pos2;
    vector<int>::iterator pos1;


    if (size()>0){
        if (!Esta(nuevo)){
        ingrediente elemento(nuevo);


        //BUSCO LA POSICIÓN DEL ELEMENTO EN EL VECTOR DATOS (nombre)
       for (ingredientes::iterator i =begin();  i !=end() && !posencontrada ;++i){
            if ((*(i)).getNombre().compare( elemento.getNombre())>0){
                pos2=i;
                posencontrada=true;
            }
        }
        if (!posencontrada){
            pos2=end();
        }


      
        
        posencontrada=false;
        
        //BUSCO EL ÍNDICE DE LA POSICIÓN BUENA SEGÚN TIPO

        
        pos1=posicionesTipo.end();
        for (vector<int>::iterator i = posicionesTipo.begin();  i !=posicionesTipo.end() && !posencontrada ;i ++){
            ingredientes::iterator otro=posDatos(*i);
            if (*otro > elemento){
                pos1=i;
                posencontrada=true;
            }
        }

        if (!posencontrada){
            pos1=posicionesTipo.end();
        }


        //ACTUALIZO INDICES
        for (vector<int>::iterator i= posicionesTipo.begin(); i !=posicionesTipo.end(); ++i){
            if ((*(posDatos(*i))).getNombre()>elemento.getNombre())
                (*i)++;
            
        }

        //CONSIGO EL NUMERO DE POSICION EN DATOS, para meterlo en posicionesTipo
        int posiciontipo=0;
        for (ingredientes::iterator i=begin(); i!= end() && i!= pos2; ++i){
                posiciontipo++;

        }

        datos.insert(pos2.it, elemento);
        posicionesTipo.insert(pos1, posiciontipo);
            
        }
    }
    else{
        datos.push_back(nuevo);
        posicionesTipo.push_back(0);
    }
    
   
        
	

}
 



   
void ingredientes::borrar (string nombre){
    ingredientes::iterator pos=posicionNombre(nombre);
    if (pos>= begin() && pos!=end()){
        
        
        vector<int>::iterator indice= posicionTipo(pos);
       
        for (vector<int>::iterator i= posicionesTipo.begin(); i !=posicionesTipo.end(); i++){
            if ((*i)>(*indice))
                (*i)--;
            
        }

        datos.erase(pos.it);
        posicionesTipo.erase(indice);
          
    }

 
}

    
    
    


vector<string> ingredientes::getTipos(){
    
    vector<string> lista;
    
    if (size()!=0){
        string tipo= (*(posDatos(*posicionesTipo.begin()))).getTipo();
        lista.push_back(tipo);

        for (vector<int>::iterator pos = posicionesTipo.begin(); pos != posicionesTipo.end(); pos++){
            if (((*(posDatos(*pos))).getTipo()) != tipo ){

                tipo= (*(posDatos(*pos))).getTipo();
                lista.push_back(tipo);

            }
        }
    }
   
    return lista;
}
    
        
     
        
        
        

    

ingredientes ingredientes::getIngredienteTipo(string tipo){
    ingredientes nuevo;
    for (ingredientes::iterator i=begin(); i !=end(); ++i){
        if ((*(i)).getTipo().compare(tipo)==0){
            nuevo.insertar(*i);
        }
    }
    
    return nuevo; 

}
    
 

ingrediente ingredientes::get(const string& nombre) const {
    ingrediente buscado;
    for (ingredientes::const_iterator i=begin(); i !=end(); ++i){
        if ((*(i)).getNombre().compare(nombre)==0){
            buscado=*i;
        }
    }
    
    return buscado;

}



const ingrediente& ingredientes::operator[](int i)const {
    assert (i>=0);
    assert(i<size());
    return datos[i];
}

ingrediente& ingredientes::operator[](int i) {
    assert (i>=0);
    assert(i<size());
    return datos[i];
}




ingrediente ingredientes::getIngrediente(const int i){
    ingrediente copia;
    if (i<size())
        copia=datos[i];
    return copia;
}






ingredientes::iterator ingredientes::posicionNombre(string nombre){
    ingredientes::iterator indice=end();
    bool encontrada=false;
    for (ingredientes::iterator i =begin(); i !=end()&&!encontrada; ++i ){
            if ((*i).getNombre().compare(nombre)==0){
                
              indice=i;
              encontrada=true;
            }
            
        }
    return indice;
    
}

vector<int>::iterator ingredientes::posicionTipo (ingredientes::iterator pos ){
    vector<int>::iterator indice=posicionesTipo.end();
    bool encontrada=false;
    int contador=0;
    if (size()!=0){
        for (ingredientes::iterator j=begin(); j!= end() && !encontrada; ++j){
            contador++;
            if (j==pos)
                encontrada =true;

        }
        encontrada =false;
        for (vector<int>::iterator i=posicionesTipo.begin(); i !=posicionesTipo.end() &&!encontrada; ++i ){
                if (*i==contador){

                  indice=i;
                  encontrada=true;
                }

            }

    
    }
    return indice;

}




ingredientes& ingredientes::operator=(const ingredientes& original){
   if (this != &original){
       datos=original.getDatos();
       posicionesTipo=original.getIndices();
   }
   return *this;
}


   

void ingredientes::mostrarIndices(){
    cout << "\nTengo " << posicionesTipo.size() << endl;
    for (int i=0; i < posicionesTipo.size(); i ++)
        cout << posicionesTipo[i] << " ";
}



std::ostream& ingredientes::ImprimirPorTipo(std::ostream& os){
    if (size()>0){
    for (vector<int>::iterator it=posicionesTipo.begin(); it!= posicionesTipo.end(); it++){
        os << *(posDatos(*it))<< endl;
    }
    
    }
    return os;
}

  


vector<double> ingredientes::getMedias(){
    vector<double> medias;
    medias.resize(5);
    for (int i =0; i < 5; i ++)
        medias[i]=0;
    if (size()!=0){
        int media=0;
        for (int i =0; i< size(); i ++)
            media += datos[i].getCalorias();
        media=media/size();
        medias[0]=media;

        media=0;
        for (int i =0; i< size(); i ++)
            media += datos[i].getHc();
        media=media/size();
        medias[1]=media;

        media=0;
        for (int i =0; i< size(); i ++)
            media += datos[i].getProteinas();
        media=media/size();
        medias[2]=media;


        media=0;
        for (int i =0; i< size(); i ++)
            media += datos[i].getGrasas();
        media=media/size();
        medias[3]=media;

        media=0;
        for (int i =0; i< size(); i ++)
            media += datos[i].getFibra();
        media=media/size();
        medias[4]=media;     
    }
        return medias;
}




vector<double> ingredientes::desviaciones(){
    vector<double> desviaciones;
    desviaciones.resize(5);
    for (int i =0; i <5; i ++)
        desviaciones[i]=0;
    if (size()!=0){
        vector<double> medias;
        medias=getMedias();
        double desviacion=0;
        for (int i =0; i< size(); i ++)
            desviacion += pow(datos[i].getCalorias(),2);
        desviacion=desviacion/size();
        desviaciones[0]=desviacion;

        desviacion=0;
        for (int i =0; i< size(); i ++)
            desviacion += pow(datos[i].getHc(),2);
        desviacion=desviacion/size();
        desviaciones[1]=desviacion;

        desviacion=0;
        for (int i =0; i< size(); i ++)
            desviacion += pow(datos[i].getProteinas(),2);
        desviacion=desviacion/size();
        desviaciones[2]=desviacion;


        desviacion=0;
        for (int i =0; i< size(); i ++)
            desviacion += pow(datos[i].getGrasas(), 2);
        desviacion=desviacion/size();
        desviaciones[3]=desviacion;

        desviacion=0;
        for (int i =0; i< size(); i ++)
            desviacion += pow(datos[i].getFibra(),2);
        desviacion=desviacion/size();
        desviaciones[4]=desviacion;     

        for (int i =0; i < 5;i ++){
            desviaciones[i]=sqrt(desviaciones[i]);
        }
    }
    
    return desviaciones;
}
    
 vector<int> ingredientes::maximosValores(){
     
    vector<int> maximos;
    maximos.resize(5);
    int indice=0;
    int max=datos[0].getCalorias();
    for (int i =0; i < size(); i ++){
       if (datos[i].getCalorias() > max){
           max=datos[i].getCalorias();
           indice=i;
       }
    }
    maximos[0]=indice;
    
    
    
    indice=0;
    max=datos[0].getHc();
    for (int i =0; i < size(); i ++){
       if (datos[i].getHc() > max){
           max=datos[i].getHc();
           indice=i;
       }
    }
    maximos[1]=indice;
    
    
    indice=0;
    max=datos[0].getProteinas();
    for (int i =0; i < size(); i ++){
       if (datos[i].getProteinas() > max){
           max=datos[i].getProteinas();
           indice=i;
       }
    }
    maximos[2]=indice;
    
    
    indice=0;
    max=datos[0].getGrasas();
    for (int i =0; i < size(); i ++){
       if (datos[i].getGrasas() > max){
           max=datos[i].getGrasas();
           indice=i;
       }
    }
    maximos[3]=indice;
    
    
    indice=0;
    max=datos[0].getFibra();
    for (int i =0; i < size(); i ++){
       if (datos[i].getFibra() > max){
           max=datos[i].getFibra();
           indice=i;
       }
    }
    maximos[4]=indice;
    
    

    return maximos;
}
 
   

  vector<int> ingredientes::minimosValores(){
     
    vector<int> minimos;
    minimos.resize(5);
    int indice=0;
    int min=datos[0].getCalorias();
    for (int i =0; i < size(); i ++){
       if (datos[i].getCalorias() < min){
           min=datos[i].getCalorias();
           indice=i;
       }
    }
    minimos[0]=indice;
    
    
    
    indice=0;
    min=datos[0].getHc();
    for (int i =0; i < size(); i ++){
       if (datos[i].getHc() < min){
           min=datos[i].getHc();
           indice=i;
       }
    }
    minimos[1]=indice;
    
    
    indice=0;
    min=datos[0].getProteinas();
    for (int i =0; i < size(); i ++){
       if (datos[i].getProteinas() < min){
           min=datos[i].getProteinas();
           indice=i;
       }
    }
    minimos[2]=indice;
    
    
    indice=0;
    min=datos[0].getGrasas();
    for (int i =0; i < size(); i ++){
       if (datos[i].getGrasas() < min){
           min=datos[i].getGrasas();
           indice=i;
       }
    }
    minimos[3]=indice;
    
    
    indice=0;
    min=datos[0].getFibra();
    for (int i =0; i < size(); i ++){
       if (datos[i].getFibra() < min){
           min=datos[i].getFibra();
           indice=i;
       }
    }
    minimos[4]=indice;
    
    

    return minimos;
}

ostream & operator<<(std::ostream & os, const ingredientes & copia){
    if (copia.size()>0){
        os << "Alimento (100 gramos);Calorias (Kcal.);Hidratos de Carb.;Proteinas;Grasas;Fibra;Tipo" ;
        for (int i =0; i < copia.size(); i ++){
            os << "\n" << copia[i];
            if (!os){
                cerr<<"ERROR al copiar secuencia" <<endl;
                exit(1);
         
            }
            
        }
        cout << "Secuencia copiada" << endl;
    }
    return os;
            
}


istream & operator>>(std::istream & is, ingredientes & nueva){
    ingrediente nuevo;
    string linea;

    getline(is, linea);
    is >> nuevo;

    while(is && is.peek()!=EOF){
        nueva.insertar(nuevo);
        is >> nuevo;
          
        if (!is){
            cerr<<"Secuencia acabada" <<endl;
            break;
        }
    }
  
    return is;

}
