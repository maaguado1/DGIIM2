/**
 * @file ingredientes.h
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c ingredientes
 */



#ifndef INGREDIENTES_H

#define INGREDIENTES_H


#include "ingrediente.h"
#include <iostream>
#include <vector>
#include <string>
#include <assert.h>

/**
 *  @brief T.D.A. Ingredientes
 *
 *
 * Una instancia @c Ingredientes es un conjunto de datos formado por dos array 
 * dinámicos: 
 * -Un array de ingrediente {v[0],v[1],...,v[n-1]}
 * -Un array de Posiciones, ordenado por tipo donde cada valor
 * posicionesTipo[i] indica la posición dentro de v del ingrediente i según tipos.
 *
 * donde  v[i] es el valor almacenado en la posición  i del vector
 *
 * La eficiencia en espacio es @e O(n).
 *
 *
 */


class ingredientes {
    /**
       * 
       * Un objeto válido @e de Ingredientes debe cumplir 
       * - @c vector.nelementos>=0
       * - @ posicionesTipo.nelementos>=0
       * 
       * 
       *
    */
    
private:
    
    
    /**
     * @brief Vector que almacena los ingredientes.
     *
     * Este array dinámico, elemento de la clase Vector_Dinamico<Ingrediente>,
     * incluye los ingredientes que forman parte de la secuencia de datos.
     * Los ingredientes que contiene se encuentran ordenados por orden alfabético de
     * nombre, y se mantienen así en todo momento.
     */
   vector<ingrediente> datos;
   
   
   /**
    * @brief Vector auxiliar de índices.
     *
     * Este vector contiene enteros que apuntan a posiciones de @a datos. El motivo de
     * este vector es mantener la segunda ordenación (alfabéticamente por tipo). 
     * El vector se encuentra ordenadode tal forma que al recorrer las posiciones de @a datos a las que apunta 
     * @a indice, se obtiene la ordenación Alfabética por Tipo, y a igualdad de tipo,
     * por nombre.
    * @note Para ordenar los elementos por tipo se utiliza el operador relacional de la 
    *  clase Ingrediente.
     */
   
   
   vector<int> posicionesTipo;
   
   
public:
    

    /**
     * @brief Constructor por defecto.
     * @param n, número de elementos
     * @note
     *   Inicializa todo vacío, en el caso de que no haya parámetro
     */
    ingredientes();
    
    /**
     * @brief Constructor de copia
     * @param const ingredientes& original, Vector de ingredientes original
     * @note
     *   inicializa el nuevo conjunto de Ingredientes con lo que haya en el vector original
     * 
     */
   
    ingredientes(const ingredientes& original);
    
    
    
    
    /**
     * @brief Inserta un nuevo ingrediente.
     * @param nuevo Ingrediente a insertar
     * @note
     *   Busca la posicion correcta por la ordenacion 1 (nombre) y 
     * coloca el índice en la posicion correcta por la ordenacion
     * 2 (tipo). 
     * 
     * @pre Si @a nuevo está ya insertado, no se inserta.
     * @post El vector @a datos e @a índice incluyen el nuevo ingrediente y su índice asignado.
     */
    void insertar(const ingrediente& nuevo);
    
    void ordenar();
    
    /**
     * @brief Borrar un elemento
     * Busca el ingrediente cuyo nombre sea @a nombre, lo borra y ajusta tanto el vector de datos como las posiciones necesarias de PosicionesTipo.
     * @param std::string nombre, el nombre del ingrediente.
     *   
     */
    void borrar (string nombre);
    
    
    
    
     /**
     * @brief Crea un vector con los tipos de ingredientes
     * @param none
      * @retval "Vector_Dinamico<string>" array dinámico de strings con los tipo 
      * por orden alfabético.
     * 
     */
    
    vector<string> getTipos();
    

    bool Esta(const ingrediente & buscar);


    void mostrarIndices();
    
     /**
     * @brief Devuelve todos los ingredientes de un tipo
     * @param "std::string" tipo El tipo a analizar
     * @retval Ingredientes, devuelve otro elemento de la clase Ingredientes, restringido el tipo
      * 
     */
    ingredientes getIngredienteTipo(string tipo);
    
    
     /**
     * @brief Consulta el tamaño del vector
     * Llama a datos.size(), pues el tamaño de los dos vectores de la clase siempre coincide.
     * @retval int Número de ingredientes
     */
    inline int size() const {return datos.size();};
    
    
    
     /**
     * @brief Consulta la información de un ingrediente dado su nombre
     * Este método busca y devuelve el ingrediente cuyo nombre coincida con el	 * argumento dado. 
      * En caso de que no se encuentre, devuelve @c "Undefined", un objeto @a ingrediente creado por el constructor sin parámetros. 
      * @param Nombre del ingrediente
      * @retval Ingrediente El ingrediente cuyo nombre se pasa por parámetro.
     */
    
    ingrediente get(const string& nombre)const ;
    
    
     /**
     * @brief Consulta la información de un ingrediente dado su nombre
     * Este método busca y devuelve el ingrediente cuya posición coincida con el	 
     * argumento dado. 
     * En caso de que no se encuentre, devuelve @c "Undefined", un objeto @a ingrediente creado por el constructor sin parámetros. 
     * @param i Posición del ingrediente
     * @retval Ingrediente El ingrediente cuya posición se pasa por parámetro.
    */
    ingrediente getIngrediente(const int i);
    
    
    /**
     * @brief Consulta del vector de ingredientes.
     * @retval Vector_Dinamico<Ingrediente> valor de datos.
    */
    vector<ingrediente> getDatos()const{return datos;};
    
    
    /**
     * @brief Consulta del vector de posiciones.
     * @retval Vector_Dinamico<Ingrediente> valor de posicionesTipo.
    */
    vector<int> getIndices()const{return posicionesTipo;};
    
    
     /**
     * @brief Sobrecarga del operador de asignacion
     * Asigna a todos los miembros los miembros del parámetro (lado derecho)
     * @param original Objeto de la clase Ingredientes
     * @retval Ingredientes&, referecia al objeto
     * 
     */
    ingredientes& operator=(const ingredientes& original);
    
    
    void MostrarIndices();
    /**
     * @brief operador[] constante.
	 *
     * Este método devuelve el ingrediente en la posición @a i, en el vector 
     * @a datos. En caso de ser un índice inválido aborta el programa.
     * @pre @a pos debe ser un índice válido, esto es, positivo y menor que 
     * @c size().
     * @param "int i" La posición a la que se va a acceder.
     * @retval "Ingrediente " Ingrediente en la posición @a i.
     */
    
    const ingrediente&  operator[](int i)const;
    
    
    /**
     * @brief operador[] no constante.
     *
     * Exactamente la misma implementación que @c ingredientes::operator[].
     *
     * @see ingredientes::operator[]
     */
    
    ingrediente& operator[](int i);
    
    

    
   
    
    
      
    
    
    
   
    /**
     * @brief Calcula las medias de un vector de ingredientes
     * Calcula las medias de: calorías, hidratos, proteínas, grasa y fibra
     * Los valores resultantes se guardan en un array de 5 elementos.
     * @return "Vector_Dinamico<double>" Un vector de valores, que corresponden a la media de: calorías, hidratos, 
     * proteínas, grasa y fibra.
     * 
     */
    
    vector<double> getMedias();

    
    
    /**
     * @brief Calcula las desviaciones de un vector de ingredientes
     * Calcula las desviaciones de: calorías, hidratos, proteínas, grasa y fibra
     * Utiliza el método @c getMedias(), pues se necesita para el cálculo.
     * Los valores resultantes se guardan en un array de 5 elementos.
     * @return "Vector_Dinamico<double>" Un vector de valores, que corresponden a las desviaciones típicas de: calorías, hidratos, 
     * proteínas, grasa y fibra.
     * 
     */
    vector<double> desviaciones();
    
    
    
    
    /**
     * @brief Calcula los máximos valores de un vector de ingredientes
     * Calcula los valores máximos de: calorías, hidratos, proteínas, grasa y fibra
     * Los valores resultantes se guardan en un array de 5 elementos.
     * @return "Vector_Dinamico<double>" Un vector de valores, que corresponden a los valores máximos de: calorías, hidratos, 
     * proteínas, grasa y fibra.
     * 
     */
    vector<int> maximosValores();
    
    vector<int> minimosValores();
    
   /**
    * @brief Imprime los ingredientes por orden alfabético de tipo.
     *
    * Este método imprime por el buffer @a os los ingredientes en orden alfabético, según su tipo.
    * Si existiese igualdad de tipo, se usaría el nombre.
     *
    * @param "std::ostream& os" El buffer por el que se dará salida a los datos.
    * @retval "std::ostream&" El mismo buffer @a os.  
    
    */
    std::ostream& ImprimirPorTipo(ostream& os);
    

  /**
   * @brief Implementación y especificación de la clase iterator al tipo ingredientes
   * Recorre un objeto del tipo ingredientes mediante un iterador no constante
  */
  class iterator{
    private:
      vector<ingrediente>::iterator it;
    public:
    iterator():it(0){};
    iterator(const iterator& v): it(v.it){};
    ~iterator(){};

    inline ingredientes::iterator & operator=(const ingredientes::iterator& orig){it=orig.it; return *this;};
    inline ingrediente& operator*()const {return *it;};
    inline ingredientes::iterator& operator++(){it++; return *this;};
    inline ingredientes::iterator& operator+(const int s){for (int i =0; i < s; i ++) {it++;} return *this;};
    inline ingredientes::iterator& operator--(){it--; return *this;};
    inline bool operator!=(const ingredientes::iterator& v) const {return it!=v.it;};
    inline bool operator>=(const ingredientes::iterator& v) const {return it>=v.it;};
    inline bool operator==(const iterator &v) const {return it==v.it;};
    friend class ingredientes;
    friend class const_iterator;

  };


  
  /**
   * @brief Implementación y especificación de la class const_iterator al tipo ingredientes
   * Recorre un objeto del tipo ingredientes mediante un iterador de tipo constante
  */
  class const_iterator{
    private:
      vector<ingrediente>::const_iterator it;
    public:
    const_iterator(){};
    const_iterator(const const_iterator& v){it=v.it; };
    ~const_iterator(){};

    inline ingredientes::const_iterator & operator=(const ingredientes::const_iterator& orig){it=orig.it; return *this;};
    inline const ingrediente& operator*() const {vector<ingrediente>::const_iterator nulo; assert(it!=nulo); return *it;};
    inline ingredientes::const_iterator& operator++(){assert (it.base() !=0); it++; return *this;};
    inline ingredientes::const_iterator& operator+(const int s){for (int i =0; i < s; i ++) {++it;} return *this;};
    inline ingredientes::const_iterator& operator--(){assert (it.base() !=0); it--; return *this;};
    inline bool operator!=(const const_iterator& v) const {return it!=v.it;};
    inline bool operator==(const const_iterator &v) const {return it==v.it;}
    friend class ingredientes;

  };
  /**
   * @brief Iterador begin de la clase ingredientes
   * @return Un iterador que apunta al primer ingrediente del objeto ingredientes apuntado por el iterador de la clase
  */
  inline iterator begin(){ingredientes::iterator i; i.it=datos.begin(); return i;};
  /**
   * @brief Iterador end de la clase ingredientes
   * @return Un iterador que apunta al último ingrediente del objeto ingredientes apuntado por el iterador de la clase
  */
  inline iterator end(){ingredientes::iterator i; i.it=datos.end(); return i;};
  /**
   * @brief Iterador constante begin de la clase ingredientes
   * @return Un iterador constante que apunta al primer ingrediente del objeto ingredientes apuntado por el iterador de la clase
  */  
  inline const_iterator begin() const {ingredientes::const_iterator i; i.it=datos.begin(); return i;};
  /**
   * @brief Iterador constante end de la clase ingredientes
   * @return Un iterador constante que apunta al último ingrediente del objeto ingredientes apuntado por el iterador de la clase
  */
  inline const_iterator end() const {ingredientes::const_iterator i; i.it=datos.end(); return i;};
  /**
   * @brief Iterador de la clase ingredientes que recorre el vector posDatos
   * @param it Posición que ocupa el ingrediente al cual queremos referenciar
   * @return Una referencia al ingrediente que ocupa la posición it del vector posDatos de ingredientes
  */
  inline iterator posDatos(int it);





       /**
     * @brief Consulta la posicion de un nombre en el vector de nombre @c datos 
        Busca y devuelve el elemento cuyo nombre coincida.
     * @param string nombre Nombre del ingrediente a consultar
     * @retval "iterator" Posicion del ingrediente buscado en el vector de nombres
     * 
     */
    ingredientes::iterator posicionNombre(string nombre);






     /**
     * @brief Consulta la posicion en el vector de posiciones de una determinada posicion en el de nombres.
     * Busca dentro de @c posicionesTipo el índice que coincida con el parámetro, devuelve la 
     * posición de ese índice dentro del array PosicionesTipo.
     * @param i Posición a consultar
     * @retval "int" Posicion en el vector de posiciones
     * 
     */
    
    vector<int>::iterator posicionTipo (ingredientes::iterator pos );







/**
    * @brief Sobrecarga del operador de salida de flujo.
     *
    * Esta función envía al buffer @a out el objeto @a i que se le 
    * pase como parámetro.
    *@note Utiliza el operador sobrecargado "<<" de la clase Ingrediente    
    *
    * Se implementa recorriendo el vector datos y enviando al flujo cada elemento.
    * Por lo tanto son enviados por orden alfabético de nombre.
    *
    * @param "std::ostream& out" Flujo de salida de datos por dónde se enviará 
    * @a i.
    * @param "const Ingredientes& i" Objeto de la clase que se enviará por @a out.
    *
    * @retval "std::ostream&" El flujo de salida tras el envío de datos.
     */


  friend ostream& operator<<(std::ostream & os, const ingredientes & i);

/**
    * @brief Sobrecarga del operador de entrada de flujo.
     *
     * Esta función realiza los siguientes pasos: Lee una línea, verifica que no se ha alcanzado
     * el fin del flujo (@c std::EOF), inserta elemento y repite el bucle.
     *
     * Una vez concluido el proceso, toda la información se debe encontrar en el
     * objeto ingredientes @a i.
     *
     * @param "std::istream& is" Flujo de entrada.
     * @param "ingredientes& i" Objeto donde se almacenará la información.
     *
     * @retval "std::istream&" El Flujo de entrada tras la operación.
     * 
     */
  friend istream& operator>>(std::istream & is, ingredientes & i);


};

#endif /* INGREDIENTES_H */
