
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author maguado
 */
public class SpaceStation {
    
    private static final int MAXFUEL=100;
    private static final float SHIELDLOSSPERUNITSHOT=0.1f;
    
    private String name;
    private int nMedals;
    
    private float ammoPower;
    private float fuelUnits;
    private float shieldPower;
    
    private ArrayList<Weapon> weapons;
    private ArrayList<ShieldBooster> shieldBoosters;
    private Hangar hangar;   
    private Damage pendingDamage;
    
    
    SpaceStation(String n, SuppliesPackage s){
        name = n;
        ammoPower=1.0f; 
        fuelUnits= 1.0f;
        shieldPower=1.0f;
        nMedals=0;
        weapons=new ArrayList<Weapon>();
        shieldBoosters=new ArrayList<ShieldBooster>();
        hangar=null;
        pendingDamage= null;
        
        receiveSupplies(s);
        
    }
    
    SpaceStation(SpaceStation s){
        name = s.getName();
        nMedals=s.getNMedals();
        
        ammoPower= s.getAmmoPower();
        fuelUnits= s.getFuelUnits();
        shieldPower=s.getShieldPower();
        
        for (Weapon w: s.getWeapons())
            weapons.add(w);
        for (ShieldBooster m:s.getShieldBoosters())
            shieldBoosters.add(m);
        hangar=s.getHangar();
        pendingDamage=s.getPendingDamage();
        
    }
    
    
    private void assignFuelValue(float f){
        if (f<MAXFUEL)
            fuelUnits = f;
        
    }
    
    private void cleanPendingDamage(){
        if (pendingDamage.hasNoEffect())
            pendingDamage=null;
        
    }
    
    
    public void cleanUpMountedItems(){
        int i=0;
                
        while ( i<weapons.size()) {
            if (weapons.get(i).getUses()==0)
                weapons.remove(i);
            else
                i++;
        }
        i=0;
        while ( i<shieldBoosters.size()) {
            if (shieldBoosters.get(i).getUses()==0)
                shieldBoosters.remove(i);
            else
                i++;
        }
        
        
    }
    
    
    
   public  void discardHangar(){
        
        hangar =null;
        
    }
    
    public void discardShieldBooster(int i){
        int size = shieldBoosters.size();
        if (i>=0 && i <size){
            shieldBoosters.remove(i);
            if (pendingDamage != null){
                pendingDamage.discardShieldBooster();
                cleanPendingDamage();
            }
        }
    }
    
    public void discardShieldBoosterInHangar(int i ){
        if (hangar != null){
            hangar.removeShieldBooster(i);
        }
        
        
    }
    
    
    public void discardWeapon(int i){
        int size = weapons.size();
        if (i>=0 && i <size){
            Weapon w= weapons.remove(i);
            if (pendingDamage != null){
                pendingDamage .discardWeapon(w);
                cleanPendingDamage();
            }
        }
        
        
    }
    
    
    public void discardWeaponInHangar(int i){
        if (hangar!= null)
            hangar.removeWeapon(i);
        
        
    }
    
    public float fire(){
        
        float factor = 1;
        
        for (Weapon w: weapons){
            factor *= w.useIt();
        }
        return factor;
    }
    
    
    public float getAmmoPower(){
        return ammoPower;
    }
    
    
    public float getFuelUnits(){
        return fuelUnits;   
    }
    
    public Hangar getHangar(){
        return hangar; 
    }
    
    
    
    public String getName(){
        
        return name;
        
    }
    
    public int getNMedals(){
        return nMedals;
        
    }
    
    
    
    public Damage getPendingDamage(){
        
        return pendingDamage;
    }
    
    
    public  ArrayList<Weapon> getWeapons(){
        return weapons;  
    }
    
    
    public  ArrayList<ShieldBooster> getShieldBoosters(){
        return shieldBoosters;
    }
    
    
    public float getShieldPower(){
        return shieldPower; 
    }
    
    
    public float getSpeed(){
        return (fuelUnits / MAXFUEL);  
    }  
    
    public SpaceStationToUI getUIversion(){
        return new SpaceStationToUI(this);
    }
    
    
    
    
    public void mountShieldBooster(int i){
        if (hangar!= null){
            ShieldBooster s = hangar.removeShieldBooster(i) ;
            if (s != null)
               shieldBoosters.add(s);
        }
            
 
    }
    
    
    public void mountWeapon(int i ){
        if (hangar!= null){
            Weapon w = hangar.removeWeapon(i);
            if (w != null)
               weapons.add(w);
        }
}
    
    
    public void move(){
        if (fuelUnits-getSpeed() >=0)
            fuelUnits-=getSpeed();
    }
    
    
    
    public float protection(){
        
        float factor = 1;
        
        for (ShieldBooster s: shieldBoosters){
            factor *= s.useIt();
        }
        return factor;
    }
    
    public void receiveHangar(Hangar h){
        if (hangar == null)
            hangar = h;
    }
    
    
    public boolean receiveShieldBooster(ShieldBooster s){
        if (hangar != null){
            return hangar.addShieldBooster(s);
        }
        else
            return false;
    }
    
    
    public ShotResult receiveShot(float shot){
       float myProtection= protection();
       if (myProtection >= shot){
           shieldPower -= SHIELDLOSSPERUNITSHOT*shot;
           shieldPower = (shieldPower > 0f)?shieldPower:0f;
           return ShotResult.RESIST;
       }
       else{
           shieldPower = 0f;
           return ShotResult.DONOTRESIST;
       }
          
    }
    
    
    public  void receiveSupplies(SuppliesPackage s){
        fuelUnits+=s.getFuelUnits();
        ammoPower+=s.getAmmoPower();
        shieldPower+= s.getShieldPower();
        
    }
    
   
    public boolean receiveWeapon(Weapon w){
        if (hangar!= null){
            return hangar.addWeapon(w);
            
        }
        else
            return false;
    }
    
    public void setLoot(Loot l){
        CardDealer dealer = CardDealer.getInstance();
        int h = l.getNHangars();
        if (h>0){
            
            Hangar hangar = dealer.nextHangar();
            receiveHangar(hangar);
        }
        int elements = l.getNSupplies();
        SuppliesPackage sup;
        for (int i =0; i < elements; i ++){
            sup = dealer.nextSuppliesPackage();
            receiveSupplies(sup);
        }
        
        elements = l.getNWeapons();
        Weapon weap;
        for (int i =0;i < elements; i ++){
            weap = dealer.nextWeapon();
            receiveWeapon(weap);
            
        }
        
        elements = l.getNShields();
        ShieldBooster sh;
        for (int i =0;i < elements; i ++){
            sh = dealer.nextShieldBooster();
            receiveShieldBooster(sh);
            
        }
        
        int medals = l.getNMedals();
        nMedals += medals;
           
    }
    
    
    public void setPendingDamage(Damage d){
        Damage otro = d.adjust(weapons, shieldBoosters);
        pendingDamage=otro;
    }
    
    public boolean validState(){
        if (pendingDamage == null || pendingDamage.hasNoEffect())
            return true;
        else
            return false;
        
    }
    
    public String toString(){
        return  "SpaceStation(\n" + 
                "\tammoPower = " + ammoPower + "\n" +
                "\tfuelUnits = " + fuelUnits + "\n" +
                "\tname = " + name + "\n" +
                "\tnMedals = " + nMedals + "\n" +
                "\tshieldPower = " + shieldPower + "\n" +
                "\tpendingDamage = " + pendingDamage + "\n" +
                "\tweapons = " + weapons + "\n" +
                "\tshieldBoosters = " + shieldBoosters + "\n" +
                "\tHangar = " + hangar + "\n" +
                ")";
    }
    
    
    
    
    
}
