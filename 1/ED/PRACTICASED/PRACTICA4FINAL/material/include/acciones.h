/* 
 * @file acciones.h 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c acciones
 */

 #include <iostream>
 #include <string>

 #include <map>

 using namespace std;


#ifndef ACCIONES_H
#define ACCIONES_H

/**
 * @brief T.D.A. acciones
 * Una instancia @c acciones se organiza en un map o conjunto de elementos formados cada uno por el nombre de la acción que representa
 * y su ariedad. La ariedad se refiere al número de ingredientes (básicos o procesados) que necesita
 * la acción para ejecutarse.
*/
 class acciones{
     private:
        map<string, unsigned char> datos;   //conjunto de pares formados cada uno por una acción y la ariedad de la acción (número de operandos necesarios)

    public:
    /**
         * @brief Constructor por defecto
         * Inicializa todo al vacío
        */
        acciones();
        /**
         * @brief Constructor de copia
         * @param const accioness& orig: conjunto de acciones a copiar en el objeto apuntado por this
        */
        acciones(const acciones& orig);

        /**
         * @brief Inserta una nueva acción en el conjunto de acciones apuntado por this
         * @param nueva: nombre de la acción a insertar
         * @param ari: ariedad de la acción a insertar
        */
        void insertar(const string & nueva, unsigned char ari);
        /**
         * @brief Borra una acción del conjunto
         * @param nombre: nombre de la acción que se quiere eliminar
        */
        void borrar(const string& nombre);
    
        /**
         * @brief Verifica que la acción está en el conjunto 
         * @param nombre: nombre de la acción que se quiere buscar
        */
        bool esta (const string& nombre);
        /**
         * @brief Operator =:permite copiar un conjunto de acciones
         * @param otra: nombre de la acción a la que se quiere acceder
        */
        inline void operator=(const acciones& otra){datos=otra.datos;}
        
        /**
         * @brief Operator [] (tipo constante): permite acceder a un elemento del conjunto de acciones
         * @param nombre: nombre de la acción a la que se quiere acceder
         * @return La ariedad de la acción especificada
        */
        const unsigned char& operator[](const string& nombre)const;
         /**
         * @brief Operator []: permite acceder a un elemento del conjunto de acciones
         * @param nombre:nombre de la acción a la que se quiere acceder
         * @return La ariedad de la acción especificada
        */
       unsigned char& operator[](const string& nombre);
        /**
         * @brief Operador de salida para acciones
         * @param os: flujo de salida
         * @param nombres: objeto del tipo acciones que se quiere imprimir
        */
        friend std::ostream & operator<<(std::ostream & os, const acciones & nombres);
        /**
         * @brief Operador de entrada para acciones
         * @param is: flujo de entrada
         * @param nombres: objeto del tipo acciones que se quiere introducir
        */
        friend std::istream & operator>>(std::istream & is, acciones & nombres);
        /**
         * @brief Número de acciones que hay en el objeto apuntado por this
         * @return Número de acciones que hay en el conjunto apunntado por this
        */
        inline int size() const {return datos.size();}

        /**
         * @brief Implementación y especificación del iterador para el TDA acciones
         * Recorre un objeto del tipo acciones
        */
        class iterator{
            private:

                map<string,unsigned char>::iterator it;

            public:
                iterator():it(0){};
                iterator(const acciones::iterator &i):it(i.it){};
                ~iterator(){};
                inline acciones::iterator& operator=(const acciones::iterator& orig){it=orig.it; return *this;};
                inline pair<string, unsigned char> operator*()const {return (*it);};
                inline acciones::iterator & operator++(){it++; return *this;};
                inline acciones::iterator & operator--(){it--; return *this;};
                inline bool operator==(const acciones::iterator &i)const {return it==i.it;};
                inline bool operator!=(const acciones::iterator &i)const {return it!=i.it;};
                friend class const_iterator;
                friend class acciones;


        };

        /**
         * @brief Implementación y especificación del iterador constante para el TDA acciones
         * Recorre un objeto del tipo acciones, teniendo en cuenta que sus elementos se referencian de manera constante
        */
        class const_iterator{

            private:
                map<string, unsigned char>::const_iterator it;

            public:

                const_iterator():it(0){};
                const_iterator(const  acciones::const_iterator &i):it(i.it){};
                ~const_iterator(){};
                inline const_iterator & operator++(){it++; return *this;};
                inline const_iterator & operator--(){it--; return *this;};
                inline acciones::const_iterator& operator=(const acciones::const_iterator& orig){this->it=orig.it; return *this;};
                inline const pair<string, unsigned char> operator*()const {return (*it);};
                inline bool operator==(const acciones::const_iterator &i)const{return it==i.it;};
                inline bool operator!=(const acciones::const_iterator &i)const {return it!=i.it;};
                friend class acciones;


        };

        /**
         * @brief Iterador begin() para el TDA acciones
         * @return Un iterador que apunta a la primera acción del conjunto
        */
        inline iterator begin(){acciones::iterator i; i.it=datos.begin(); return i;};
        /**
         * @brief Iteradir end() para el TDA acciones
         * @return Un iterador que apunta a la última acción del conjunto
        */
        inline iterator end(){acciones::iterator i; i.it=datos.end(); return i;}
        /**
         * @brief Iterador constante begin() para el TDA acciones
         * @return Un iterador constante que apunta al primer elemento del conjunto
        */
        inline const_iterator begin()const {acciones::const_iterator i; i.it=datos.begin(); return i;}
        /**
         * @brief Iterador constante end() para el TDA acciones
         * @return Un iterador constante que apunta al último elemento del conjunto
        */
        inline const_iterator end()const {acciones::const_iterator i; i.it=datos.end(); return i;}


 };

 #endif