### 1) Basado en la cercanía entre las ciudades

DIvidiremos esta sección en varias apartados:
-*Componentes del método voraz.*
-*El pseudocódigo del problema.*
-*Eficiencia.*

**Componentes del método voraz.**
Esta manera de resolver el problema del viajante de comercio(TSP), se basa en una idea muy sencilla y es la siguiente:
Dada una ciudad *v<sub>i</sub>*, la siguiente ciudad que se escogerá será la ciudad *v* que más cerca se encuentre de *v<sub>i</sub>*
Dicho esto, pasamos a ver qué tipo de componentes tiene este método voraz:

1) Lista de candidatos:
La lista de candidatos será,lógicamente, los nodos del grafo original, que contiene la posicion en coordenadas de cada ciudad.

2)Lista de candidatos utilizados:
La lista de candidatos utilizados serán la lista de nodos(o ciudades) que hemos visitado previamente.

3)La funcion solución:
El algoritmo terminará cuando el número de nodos(o ciudades) que hemos visitado sea igual al número de nodos del grafo, es decir, cuando se hayan recorrido todos los nodos del gráfico original.

4)Función de selección:
Se selecciona un nodo *v* tal que la arista *a* que lo une con el último nodo visitado tiene coste mínimo, es decir,de manera que la distancia euclídea entre el nodo ya visitado y *v* sea mínima.

5)Criterio de factibilidad:
No se pueden formar ciclos, salvo al final del recorrido cuando tengamos que unir el último nodo y el primero, para cerrar dicho circuito.

6)Función objetivo:
Encontrar un ciclo hamiltoniano minimal del grafo.

**El pseudocódigo del problema.**
Hagamos una aclaración antes:
-Cuando nos refiramos a un nodo del grafo, estaremos haciendo referencia a una ciudad.

Veamos que estructura tiene dicho problema:

Algoritmo  cercania(G=(V,A));

i=s= seleccionamos un nodo cualquiera de V.

C=V \ {s} Los candidatos serán los nodos del grafo quitando el nodo inicial seleccionado.

Los candidatos serán los nodos del grafo quitando el nodo inicial seleccionado.

S={s} La solución a devolver estará formada por una secuencia de nodos, que darán lugar al recorrido. Añadimos el primer nodo seleccionado.

El bucle será de la siguiente manera:
Repetir hasta que S.size() = V.size() :

v=seleccionamos un nodo en C donde *a(s,v)* tiene peso mínimo (la distancia euclídea entre el nodo *s* y el nodo *v* seleccionada es la menor de las distancias entre el nodo *s* y el resto de nodos de C).

S=S U {v}. A la solución le añadimos el nodo v seleccionada.

C=C \ {v}. Eliminamos el nodo *v* seleccionado del conjunto de candidatos.

Si calculamos la distancia total del circuito, hay que tener en cuenta que el recorrido hay que cerrarlo, luego la distancia entre el último nodo seleccionado y el primero se tiene que considerar.

s=v Actualizamos el valor de *s* a *v*(ahora, el nodo actual es *v*).
Fin-Repetir.

Devolver S. Devolvemos la solución.

Veamos la implementación de este algoritmo greedy:
~~~
/**
 * @brief Calcula la mejor solucion para un conjunto de puntos, por Shortest Neighbor First
 * @param T, la matriz de distancias de los puntos con los que trabajamos
 * @pre T.size() > 0
 * @return el camino cuya distancia es la mínima, si todo sale bien o un vector vacío, si ocurre algún error.
 * */
vector<int> solucion_cercania(vector<vector<float>> distancias){
	// Creamos el vector que contendrá el recorrido.
	vector<int> solucion;			 O(1)
	int n = distancias.size();		 O(1)
	int distancia_minima = 0;		 O(1)
	int distancia = 0;				 O(1)
	// Seleccionamos la primera ciudad de forma aleatoria
	int posicion= rand() % n;		 O(1)
	int i;						     O(1)
	// Introducimos la primera ciudad en el vector solución
	solucion.push_back(posicion);	 O(1)

	//Mientramos no recorramos todas las ciudades, continuamos escogiendo.
	while(solucion.size() < distancias.size()){ 	    O(n-1)
		i=posicion;							            O(1)
		distancia_minima=INT_MAX;			            O(1)
		//Veamos quién es la ciudad más cercana a la actual(i)
		for(int j=0; j< distancias.size(); j++){ 		O(n)
			//Comprobamos si dicha ciudad se encuentra ya en el vector solución. Si no es así, se mira si la distancia es menor que la distancia mínima actual. Esta parte se corresponde con la eliminación de la ciudad del conjunto de candidatos una vez ha sido escogida.
			if(find(solucion.begin(),solucion.end(),j) == solucion.end()){ O(1)
				distancia=distancias[i][j];		 O(1)
				// Si la distancia es menor, dicha distancia pasa a ser la mínima y dicha ciudad pasa a ser la escogida.
				if(distancia<distancia_minima){	 O(1)
					posicion=j;				     O(1)
					distancia_minima=distancia;  O(1)
				}
			}
		}
		// Una vez encontrada dicha ciudad, la añadimos al vector solución y esta pasa a ser la ciudad actual.
		solucion.push_back(posicion);         	  O(1)
	}
	return solucion;							  O(1)
}
~~~
Vemos que el algoritmo nos devuelve el vector resultado, en el que se almacena el orden en el que se visitarán las ciudades, es decir, el recorrido que realizará el comerciante.Cabe destacar que la primera ciudad se selecciona aleatoriamente, de ahí a que utilicemos "rand()" al escoger la prime posición. Además, respecto a lo que se ha dicho antes de cerrar el circuito, en el fichero solución que genera el programa, hemos incluido como último nodo el primero, para remarcar que el recorrido es cerrado y que la distancia entre el último nodo seleccionado y el primero debe tenerse en cuenta.

**Eficiencia.**
-*Eficiencia Teórica*
La eficiencia teórica de este algoritmo greedy es fácil de ver que es O(n²), pues al final se reduce a O(n-1)*O(n)=O(n²).

-*Eficiencia Empírica y Eficiencia Híbrida*
Para estudiar la eficiencia empírica del algoritmo voraz, hemos generado ,mediante un programa, matrices de distancias aleatorias dado un tamaño *n*.Dicho *n* está comprendido entre 10 y 200. Debido a que los datos son aleatorios, al representarlos en la gráfica se ven algo dispersos, pero mediante el coeficiente de correlación lineal, veremos que el ajuste realizado es prácticamente perfecto:

![Gráfica que muestra la eficiencia empírica](/home/joseja/Escritorio/Practica_3_ALG/EFICIENCIA/eficiencia_cercania.png) 

Seguidamente, le hemos realizado un ajuste cuadrático:
~~~
FIT:    data read from "optc.dat"
        format = z
        #datapoints = 39
        residuals are weighted equally (unit weight)

function used for fitting: f(x)
	f(x) = a*x*x + b*x + c
fitted parameters initialized with current variable values

iter      chisq       delta/lim  lambda   a             b             c            
   0 1.3783099600e+10   0.00e+00  1.08e+04    1.000000e+00   1.000000e+00   1.000000e+00
   8 1.8835807715e-07  -1.92e-03  1.08e-04    4.231858e-08  -3.383623e-06   8.237585e-05

After 8 iterations the fit converged.
final sum of squares of residuals : 1.88358e-07
rel. change during last iteration : -1.91505e-08

degrees of freedom    (FIT_NDF)                        : 36
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 7.23337e-05
variance of residuals (reduced chisquare) = WSSR/ndf   : 5.23217e-09

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a               = 4.23186e-08      +/- 4.093e-09    (9.673%)
b               = -3.38362e-06     +/- 8.839e-07    (26.12%)
c               = 8.23759e-05      +/- 4.045e-05    (49.1%)

correlation matrix of the fit parameters:
                a      b      c      
a               1.000 
b              -0.973  1.000 
c               0.795 -0.898  1.000 

~~~

Luego, como se puede apreciar, la función cuadrática que se ajusta a dichos datos es:
f(x) = 4.23186*10⁻⁸ x² -3.38362*10⁻⁶ x + 8.23759*10⁻⁵

![ ](/home/joseja/Escritorio/Practica_3_ALG/EFICIENCIA/eficiencia_hibrida_cercania.png  "Ajuste híbrido mediante función cuadrática")

Veamos qué tanto se ajusta dicha función a la nube de puntos. Usaremos dos fórmulas, explicadas a continuación:

**Regresión**:
$$\sigma^2_y = \sigma^2_{ey} + \sigma^2_{ry}$$
dónde 
$$\sigma^2_{ey} = \sum_{i=1}^kf_{ij}(y_j-y)^2$$

**Correlación**:
$$\mu^2_{Y/X} = \frac{\sigma^2_{ey}}{\sigma^2_y} = 1-\frac{\sigma^2_{ry}}{\sigma^2_y}$$

Vemos que el coeficiente de correlación en este caso es:
$$\mu^2_{Y/X} =  1 - (5.23217*10⁻⁹)/(8,56481840445231*10⁻⁶ ) = 0,999389109056033$$

Por lo tanto, como el coeficiente de correlación está muy próximo a 1, el ajuste es muy bueno. La función cuadrática se ajusta de forma casi perfecta a los datos.

Veamos que ocurre si ajustamos dicha nube de puntos mediante una función logarítmica:

~~~
FIT:    data read from "optc.dat"
        format = z
        #datapoints = 39
        residuals are weighted equally (unit weight)

function used for fitting: g(x)
	g(x) = a*log(x) + b
fitted parameters initialized with current variable values

iter      chisq       delta/lim  lambda   a             b            
   0 8.7565788414e-06   0.00e+00  2.40e-06    4.231858e-08  -3.383623e-06
   5 1.9239629001e-06  -2.08e-07  2.40e-11    3.417861e-04  -1.189239e-03

After 5 iterations the fit converged.
final sum of squares of residuals : 1.92396e-06
rel. change during last iteration : -2.0769e-12

degrees of freedom    (FIT_NDF)                        : 37
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.000228033
variance of residuals (reduced chisquare) = WSSR/ndf   : 5.1999e-08

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
a               = 0.000341786      +/- 4.867e-05    (14.24%)
b               = -0.00118924      +/- 0.0002191    (18.42%)

correlation matrix of the fit parameters:
                a      b      
a               1.000 
b              -0.986  1.000 
~~~

La función logarítmica que obtenemos es:
f(x) = 0.000341786*log(x) - 0.00118924

![ ](/home/joseja/Escritorio/Practica_3_ALG/EFICIENCIA/cercania_logaritmica.png  "Ajuste híbrido mediante función cuadrática")

No cabe duda de que no se ajuste de manera correcta a los datos del problema.









