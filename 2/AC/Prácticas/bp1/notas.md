### SEMINARIO 1: Herramientas de programación paralela

##### 1. Intro: OpenMP

Es un conjunto de herramientas que nos permiten usar multiprocesamiento, ayuda a crear código paralelo (paradigma de variables compartidas).

(Diapos 1-12)

- PORTABILIDAD:

  Las directivas se establecen en tiempo de compilación (las sustituye por código), y las funciones en tiempo de ejecución: puede dar lugar a problemas.

  Para evitarlos usamos #ifdef, si queremos que el código sea portable, necesitamos cuidar las llamadas en tiempo de ejecución (si estamos en monocore y no lo hemos previsto nos va a dar fallo)-> **Compilación condicionada(_OPENMP).**

##### 2.- Directivas:

- Parallel: crea tantas hebras como core lógicos haya disponibles. 

  OJO! No podemos determinar el código que ejecuta cada hebra, todas hacen lo mismo (se podría controlar con el flujo, pero no como tal).

  - OMP_DYNAMIC
  - OMP_NUM...: para fijar número de threads (obv).

- De trabajo compartido: 

  - Sections: cada una de las hebras hará una función. No se puede especificar qué hebra hace cada función. 

    - Si tenemos más hebras que tareas, las que sobren se van a la barrera implícita a esperar a las demás.
    - Si tenemos menos hebras que tareas, se empiezan a ejecutar las secciones, y la primera hebra que acabe empieza la siguiente sección.

    

  - Single: Pueden existir trozos de código no paralelizables, se usa _en un bloque paralelo_ cuando se interacciona con el usuario

  - For/bucle: reparte las iteraciones del bucle entre las hebras disponibles (OMP_NUM_THREADS), las hebras vienen de la creación de parallel. Si el bucle for tiene dependencia, no se puede hacer paralelismo.

- De trabajo compartido combinado.

- Barrier: No tiene código asociado, obliga a crear una barrera.

- Critical: Entran en exclusión mutua, entran y ejecutan pero ordenaditas jej

  (En el ejemplo, si no tuviéramos lo de critical, las hebras podrían acceder a suma, y el valor cambiaría).

  ​	



!!Barrera implícita: las hebras se "esperan" unas a otras al final de la directiva, porque pueden tener tiempos de ejecución distintos.



### Ejercicios

##### Ejercicio 6:

Para calcular los MIPS, buscamos la llamada a clock_get_time(), y el número de instrucciones será:

(Instrucciones entre clockgettime y el bucle)+num_iteraciones*(instrucciones bucle)+ inst entre bucle y clock_settime()

Para los Flops buscamos los registros xmm.

##### Ejercicio 7

Dividimos el vector en 4, 



##### Ejercicio 8

Usamos 

~~~
#pragme omp parallel sections
section
for (i=0; i <N; i+=4)

section
for (i=1; i <N; i +=4)

section
... y así
~~~

Otra solución: iterar sobre índices distintos.

- Uso de omp_get_wtime(), funciona igual que clock_gettime(), pero es específica de openmp.







