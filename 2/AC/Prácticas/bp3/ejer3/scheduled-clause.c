#include <stdio.h>
#include <stdlib.h>

#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_thread_num() 0
#endif

int main(int argc, char **argv) {
	int i, n=16,chunk,a[n],suma=0;
	omp_sched_t kind;
	int modifier;
	if(argc < 3) {
		fprintf(stderr,"\nFalta iteraciones o chunk \n");
		exit(-1);
	}
	n = atoi(argv[1]); if (n>200) n=200; chunk = atoi(argv[2]);
	
	for (i=0; i<n; i++) a[i] = i;

	#pragma omp parallel for firstprivate(suma) \
	lastprivate(suma)  schedule(dynamic,chunk)
	for (i=0; i<n; i++)
	{ 	suma = suma + a[i];
		printf(" \nthread %d suma a[%d]=%d suma=%d ", omp_get_thread_num(),i,a[i],suma);

		if (i ==0)
		{
		omp_get_schedule(&kind, &modifier);
		printf ("\n DENTRO DEL FOR \n Variable dyn_var (Hay ajuste dinámico): %s \n Variable nthreads-var (Número de threads): %d \n Variable thread-limit-var (Número máximo de threads): %d \n Variable run-sched-var (Planificación): tipo %d || chunk %d\n", omp_get_dynamic()?"true":"false", omp_get_max_threads(), omp_get_thread_limit(), kind, modifier);

		}
	}

	printf("Fuera de 'parallel for' suma=%d\n",suma);
	printf ("\n\nFUERA DEL FOR: \nVariable dyn_var (Hay ajuste dinámico): %s \n Variable nthreads-var (Número de threads): %d \n Variable thread-limit-var (Número máximo de threads): %d \n Variable run-sched-var (Planificación): tipo %d || chunk %d\n", omp_get_dynamic()?"true":"false", omp_get_max_threads(), omp_get_thread_limit(), kind, modifier);
}
