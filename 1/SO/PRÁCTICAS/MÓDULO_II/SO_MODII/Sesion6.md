## MÓDULO II: Sesión 6

### Control de archivos y archivos proyectados en memoria

## 1. La función fcntl

Permite ajustar las banderas de control de acceso de un descriptor.

~~~
#include <unistd.h>
#include <fcntl.h>
int fcntl(int fd, int orden, /* argumento_orden */);

~~~

Posibles argumentos para ORDEN:

| ORDEN | Descripción |
| ----- | ------|
| F_GETFL | Devuelve las banderas de control asociadas|
| F_SETFL | Ajusta las banderas que se pasen como argumento|
| F_GETFD | Devuelve la bandera "close-on-exec", si es 0 es que está desactivada (valor por defecto) |
| F_SETFD | Activa la "close-on-exec", según el parámetro (0/1)|
| F_DUPFD | Duplica el descriptor de **fd**, el tercer argumento (entero): el descriptor duplicado tiene que ser >= que entero. Devuelve el descriptor duplicado |
| F_SETLK | Bloquea el archivo (si tiene éxito)|
| F_SETLKW | Establece cerrojo y bloquea al que llama hasta que se consigue |
| F_GETLK | Consulta si hay bloqueo |


### 1.A Consultar/modificar banderas de estado de un archivo abierto

Se puede recuperar las banderas de estado y modo de acceso (argumentos de open) de un archivo: `F_GETFL`.
Consulta:
- Escrituras sincronizadas: **O_SYNC**
- Modo de acceso, se aplica una máscara O_ACCMODE: para comparar con **O_RDONLY**, **O_WRONLY**, **O_RDWR**

Modificación:
Se pueden modificar las banderas: **O_APPEND, O_NONBLOCK, O_NOATIME, O_ASYNC, O_DIRECT**.

Método:

~~~

int bandera;
bandera = fcntl(fd, F_GETFL);
if (bandera == -1)
perror(“fcntl”);
bandera |= O_APPEND;
if (fcntl(fd, F_SETFL, bandera) == -1)
perror(“fcntl”);
~~~

### 1.B Duplicar descriptores de archivos

Se pueden duplicar descriptores, para que se tengan en el mismo proceso dos descriptores apuntando al mismo archivo abierto, mismo modo, puntero lectura-escritura (dup y dup2).

Redireccionar la salida estándar de un proceso hacia un archivo:
~~~
int fd = open (“temporal”, O_WRONLY); //Abrir el archivo
close (1);    //Cerramos salida estándar
if (fcntl(fd, F_DUPFD, 1) == -1 ) perror (“Fallo en fcntl”); //Duplicamos fd en el descriptor número 1
char bufer[256];
int cont = write (1, bufer, 256); //Escribe en archivo temporal
~~~


##### EJERCICIO 1:

~~~

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>


int main(int argc, char *argv[]){

	if (argc <3 ){
		printf ("\nError en el número de argumentos");
	}
	else{
		char *str_command=argv[1];
		char *str_file=argv[3];

		if ( argv[2] == "<" ){
			int fd = open ("temporal", O_RDONLY);
			close(STDOUT_FILENO);
			if (fcntl(fd, F_DUPFD, STDOUT_FILENO) == -1 )
				perror (“Fallo en fcntl”);

		}
		else if (argv[2] == ">"){
			int fd = open ("temporal", O_WRONLY|O_CREAT);
			close(STDIN_FILENO);
			if (fcntl(fd, F_DUPFD, STDIN_FILENO) == -1 )
				perror (“Fallo en fcntl”);

		}

		if( (execlp(str_command, "", NULL) < 0)) {

                        perror("Error en el execlp\n");
                        exit(-1);
                }

                //Cerramos el fichero
                close(fd);
      }
	}
~~~

##### EJERCICIO 2:

~~~
/*
 Reescribir el programa que implemente un encauzamiento de dos órdenes pero
utilizando fcntl. Este programa admitirá tres argumentos. El primer argumento y el tercero
serán dos órdenes de Linux. El segundo argumento será el carácter “|”. El programa deberá
ahora hacer la redirección de la salida de la orden indicada por el primer argumento hacia el
cauce, y redireccionar la entrada estándar de la segunda orden desde el cauce. Por ejemplo,
para simular el encauzamiento ls|sort, ejecutaríamos nuestro programa como:
*/

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>


int main(int argc, char *argv[]){


	if (argc <3 ){
		printf ("\nError en el número de argumentos");
	}
	else{
		int fd[2];
		pipe(fd);
		pid_t PID;
		char *str_command1=argv[1];
		char *str_command2=argv[3];
		char *str_cauce=argv[2];

		if ((PID=fork())<0){

			perror("\Error en el fork");
			exit -1;
		}
		if (PID ==0){
			close(fd[0]);
			if (fcntl(fd[1], F_DUPFD, STDOUT_FILENO) == -1 )
				perror (“Fallo en fcntl”);
			execlp(str_command1, "", NULL);
		}


		else{
			close(fd[1]);
			if (fcntl(fd[0], F_DUPFD, STDIN_FILENO) == -1 )
				perror (“Fallo en fcntl”);
			execlp(str_command2, "", NULL);

		}


        }
}

~~~


### 1.C Bloquear archivos

Se necesita controlar accesos a los archivos. Se tienen dos APIs:
- `flock()` bloquea archivo completo con cerrojo
- `fcntl()` bloquea regiones del archivo, es el que nos interesa


Se siguen los pasos siguientes:
- Posicionar un cerrojo
- Realizar E/S
- Desbloquear archivo

ATENCIÓN: problema, cuando el búfer de entrada se llena antes de situar un cerrojo, o el búfer de salida se limpia después de eliminar un cerrojo. Para solucionar:
- Se usan read() y write(), en vez de stdio.
- Se limpia el stream stdio antes y después de poner o quitar un cerrojo.
- Se deshabilita el búfer `setbuf()`.
Dos tipos de bloqueo: **consultivo**, si un proceso puede ignorar el cerrojo, u **obligatorio**.

#### a) Bloqueo de registros

Usamos la estructura **flock**:

~~~
struct flock {
short l_type; /* Tipo de cerrojo: F_RDLCK(lectura), F_WRLCK(escritura), F_UNLCK(eliminar) */
short l_whence; /*Interpretar l_start: SEEK_SET(inicio),SEEK_CURR(posición actual),SEEK_END(final)*/
off_t l_start; /* Desplazamiento donde se inicia el bloqueo */
off_t l_len; /* Numero bytes bloqueados: 0 significa “hasta EOF”  0 si se bloquean todos desde l_whence hasta el final*/
pid_t l_pid; /* Proceso que previene nuestro bloqueo(solo F_GETLK)*/
};
~~~

ATENCIÓN: si queremos situar un cerrojo de lectura/escritura, el archivo se abre en modo lectura/escritura, (respectivamente, o ambos `O_RDWR`).
Para bloquear todo: l_whence=SEEK_SET, l_start=l_len=0.

| Órdenes | Descripción |
| ----- | ----- |
| F_SETLK | Adquiere o libera un cerrojo sobre los bytes de `flockstr`. Si algún proceso tiene un cerrojo incompatible-> EAGAIN |
| F_SETLKW | Análogo al anterior, en vez de EAGAIN, se bloquea. (Cuidado con SA_RESTART, falla con EINTR) |
|F_GETLK| Comprueba si se puede adquirir un cerrojo (l_type=read/write). La estructura flock almacena info sobre si se puede adquirir (l_type=F_UNLCK), si hay algún bloqueo incompatible, retorna info sobre algún bloqueo |


Cosas a tener en cuenta cuando se adquiere/libera un cerrojo:

- Desbloquear región siempre es exitoso.
- Un proceso sólo puede tener un tipo de cerrojo activo. Si se sitúa uno nuevo, se cambia el cerrojo automáticamente (puede devolver error).
- Un proceso no se puede bloquear a si mismo.
- Si se pone un cerrojo en medio de otro de otro tipo, se crean 3, uno a cada lado y el del medio(pensar permutaciones).
- Cerrar el descriptor la lía un poquito.
- Interbloqueos: se evita comprobando en cada solicitud de bloqueo, si da, hace que uno de los dos dé fallo. Error EDEADLK.
- Inanición: un proceso intenta realizar escritura cuando los demás están con lectura. En Linux el orden de servicio no está determinado, ni los escritores ni los lectores tienen prioridad sobre los otros.
- Los cerrojos no son hereditarios por fork(), pero se mantienen con exec() y a través de los hilos.
- Los cerrojos pueden estar asociados a inodos.

Para usar un bloqueo obligatorio, tenemos que activarlo desde el sistema de archivos que lo contienen y desde cada archivo.
- Por SA: `mount -o mand /dev/sda1 /pruebafs`
- Desde un programa: especificamos la bandera `MS_MANDLOCK` en mount o con `chmod`
- Sobre un archivo: `setgroupid`=1 y `group-execute`=0
- Desde el shell: `chmod g+s,g-x /pruebafs/archivo`

**¿Qué pasa si aparece un conflicto de cerrojos con bloqueos obligatorios?**
Depende de si el archivo se abre de manera bloqueante. Si se abre con `O_NONBLOCK`, falla con el error EAGAIN. Si no se ha especificado el no-bloqueo, se provoca un interbloqueo, el kernel lo soluciona haciendo que uno falle.

#### b) El archivo /proc/locks

Sirve para ver los cerrojos en el sistema. Tiene 8 campos:
- Número de cerrojo
- Tipo de cerrojo(POSIX=fcntl(), FLOCK=flock())
- Modo de cerrojo (ADVISORY//MANDATORY)
- Tipo de cerrojo, write/read
- PID del proceso que lo mantiene
- Tres números separados por “:” que identifican el archivo sobre el que se mantiene el cerrojo: el número principal y secundario del dispositivo donde reside el sistema de archivos que contiene el archivo, seguido del número de inodo del archivo.
- Byte de inicio de bloqueo.
- Byte final


#### c) Ejecutar una instancia de un programa

Para ejecutar programas solo una vez, se crea un archivo aparte (normalmente /var/run), y se establece un cerrojo de escritura sobre él. Los demonios crean un archivo .pid




##### EJERCICIO 3:
~~~
/*Construir un programa que verifique que, efectivamente, el kernel comprueba que
puede darse una situación de interbloqueo en el bloqueo de archivo
*/
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>




int main(int argc, char* argv[])
{
  struct flock cerrojo, cerrojo2;
  int fd;
  if ((fd=open(argv[1], O_RDWR)) == -1) {
    perror("open fallo");
    return 0;
  }
  cerrojo.l_type =F_WRLCK;
  cerrojo.l_whence =SEEK_SET;
  cerrojo.l_start =0;
  cerrojo.l_len =1;




  /* bloqueamos ambas zonas */
  if (fcntl(fd, F_SETLK, &cerrojo) == -1) {
    print("No he podudi poner el primer cerrijo");
    return 0;
  }
  printf("lalalaprocesooooo");
  for (i=0; i<10; i ++)
    sleep(1);




  /* Una vez finalizado el trabajo, desbloqueamos el archivo entero */
  cerrojo.l_type =F_UNLCK;
  cerrojo.l_whence =SEEK_SET;
  cerrojo.l_start =0;
  cerrojo.l_len =0;
  if (fcntl(fd, F_SETLKW, &cerrojo) == -1) perror("Desbloqueo");

  return 0;
}

~~~


##### EJERCICIO 4:
~~~
/*Construir un programa que se asegure que solo hay una instancia de él en
ejecución en un momento dado. El programa, una vez que ha establecido el mecanismo para asegurar que solo una instancia se ejecuta, entrará en un bucle infinito que nos permitirá comprobar que no podemos lanzar más ejecuciones del mismo. En la construcción del mismo, deberemos asegurarnos de que el archivo a bloquear no contiene inicialmente nada escrito en una ejecución anterior que pudo quedar por una caída del sistem
*/
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>




int main(int argc, char* argv[])
{
  struct flock cerrojo, cerrojo2;
  int fd;
  char cadena[100];
  sprintf (cadena, "%s.txt", argv[0]);
  if ((fd=open(cadena, O_CREAT|O_RDWR)) == -1) {
    perror("open fallo");
    return 0;
  }
  cerrojo.l_type =F_WRLCK;
  cerrojo.l_whence =SEEK_SET;
  cerrojo.l_start =0;
  cerrojo.l_len =0;




  /* bloqueamos ambas zonas */
  if (fcntl(fd, F_SETLK, &cerrojo) == -1) {
    printf("No he podudi poner el primer cerrijo");
    return 0;
  }
  printf("lalalaprocesooooo");
  while (1){
    sleep(1);
  }





  /* Una vez finalizado el trabajo, desbloqueamos el archivo entero */
  cerrojo.l_type =F_UNLCK;
  cerrojo.l_whence =SEEK_SET;
  cerrojo.l_start =0;
  cerrojo.l_len =0;
  if (fcntl(fd, F_SETLKW, &cerrojo) == -1) perror("Desbloqueo");

  return 0;
}

~~~


## Archivos proyectados en memoria con mmap()

Para acceder a archivos: se duplica en el esp. de direcciones del proceso lo que se quiere gestionar.
**mmap()**, propósitos:
- Archivo regular (suministrar E/S)
- Archivos especiales, anónimas.
- `shm_open`, compartir memoria.

~~~

#include <sys/mman.h>
void *mmap(void *address, size_t length, int prot, int flags,
int fd, off_t offset);
Retorna: dirección inicial de la proyección,si OK; MAP_FAILED, si error.
~~~

ARGUMENTOS:
- **adress**: dirección de inicio dentro del proceso donde se duplica el descriptores
- **len**: número de bytes desde offset.
- **prot**: tipo de protección

| Valor de prot | Descripción |
| --- | --- |
|PROT_READ | Se pueden leer |
| PROT_WRITE| Se pueden escribir |
| PROT_EXEC | Se pueden ejecutar |
| PROT_NONE | No se pueden acceder a los datos |


- **flags**: Se tiene que poner el shared/private.


| Flag | Significado |
| --- | --- |
| MAP_PRIVATE | Solo son visibles para el proceso y no modifican el objeto de donde viene la proyección |
| MAP_SHARED | Las modificaciones son visibles a los procesos que comparten la proyección, se modifica el objeto. Si la actualización tiene que ser inmediata: `msync()` |
| MAP_FIXED | La dirección address es un requisito, falla si no se puede. Si hay una proyección ya, se solapa |
| MAP_ANONYMOUS | Crea un mapeo anónimo |
| MAP_LOCKED | Bloquea las páginas de memoria |
|MAP_NORESERVE | Controla la reserva de espacio de intercambio |
| MAP_POPULATE | Lee de forma adelantada el archivo |
| MAP_UNITIALIZED | No pone a 0 las proyecciones anónimas |



- **fd**: descriptor de archivo a proyectar (se cierra después de mmap).
- **offset**: (normalmente 0). Len y offset tienen estar alineados con páginas

PASOS:
1) Obtener el descriptor con permisos que dependen del tipo de proyección.
2) Pasar el descriptor a mmap().
3) Hacemos lo que tengamos que hacer.
4) Eliminamos proyección

~~~
#include <sys/mman.h>
int munmap(void *address, size_t length);

//Retorna: 0,si OK; -1, si error.

~~~
(adress =dirección que devolvió mmap, len=tamaño de la región)


**Cositas a tener en cuenta**
- El mapeo tiene que entrar en el esp. de dir.
- La visibilidad de los cambios es diferente que con read/write (más automática)
- Se desperdicia espacio (num paginas - tamaño archivo)
- No todos se pueden mapear (terminal o socket se hacen mediante read/write)

**Otras funciones**

- `mremap()`: extiende una proyección
- `mprotect()`: cambia la protección
- `madvise()`: consejos sobre como manejar la e/s de páginas
- `remap_file_pages()`: crea mapeos no lineales
- `mlock()`: ancla páginas en memoria
- `mincore()`: informa de las páginas en RAM


#### Proyecciones anónimas

Es un mapeo sin archivo de partida, se inicializa todo a 0.
CÓMO INICIALIZAR:
- MAP_ANON + fd = -1 + memoria a 0
- Pseudo-dispositivo /dev/zero (se hace como un archivo normal)


#### Tamaño de la proyección

Cuando no coincide con un múltiplo del tamaño de página

##### EJERCICIO 5


~~~
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>


int main(int argc, char *argv[]){
	int fd1, fd2, i;
	char *archivo1, *archivo2, *ptr1, *ptr2;
	struct stat nopuc;
	int tamanio;
	archivo1= argv[1];
	archivo2=argv[2];

	fd1 = open(archivo1, O_RDONLY);

	if (fd1 == -1) {
              perror("Fallo al abrir el archivo de origen\n");
              exit(2);
           }

  	if  (stat(fd1,&nopuc) < 0)    
        	return 1;


	if (!S_ISREG (nopuc.st_mode)) {
                printf ("El fichero de origen no es un archivo regular\n");
                return 1;
        }

	tamanio = nopuc.st_size;

	fd2 = open(archivo2, O_RDWR|O_CREAT|O_EXCL, S_IRWXU);

	if (fd2 == -1) {
              perror("Fallo al abrir el archivo de salida\n");
              exit(2);
           }

  	ftruncate(fd2, tamanio);

  	ptr1 = (char*)mmap(NULL, tamanio, PROT_READ, MAP_SHARED, fd1, 0);
	if (ptr1 == MAP_FAILED) {
		printf("Fallo el mapeo\n");
		return 1;
	}



	ptr2 = (char*)mmap(NULL, tamanio, PROT_WRITE, MAP_SHARED, fd2, 0);

	if (ptr2 == MAP_FAILED) {
	    printf("Fallo el mapeo\n");
	    return 1;
	  }
	memcpy(ptr1, ptr2, tamanio);
	munmap(ptr1, tamanio);
	munmap(ptr2, tamanio);

 	close(fd1);
 	close(fd2);



	  return 0;

}
~~~
