
#encoding:utf-8

require_relative 'Weapon'
require_relative 'Damage'
require_relative 'ShieldBooster'


require_relative'Loot'
require_relative 'SuppliesPackage'
require_relative 'SpaceStation'

=begin
EJERCICIO 1:

Modificaciones en la clase Dice:
1. He creado dos atributos de clase con las probabilidades de perder el botín @ENEMYLOSES, y de perder x elementos@LOSTELEMENTPROB(x del 1 al 4). 
2. He creado también en Dado un método de instancia (enemyLoses) que determina si el botín se ve reducido o no, generando un número aleatorio entre 0 y 1 
y comparándolo con @ENEMYLOSES. 
3. He creado además otro método de instancia (lostNElements) que me indica cuántos elementos se quitan, utilizando de nuevo el generador de la clase para
obtener un número aleatorio que se compara con @LOSTELEMENTPROB.

Modificaciones en la clase SpaceStation:

1. He modificado el método setLoot incluyendo en la ejecución del mismo un dado.
2. Determino con un valor booleano si se pierden elementos o no.
3. Para cada item del botín
  - Si resulta que sí que se pierden lanzo el dado otra vez para determinar (para cada componente del botín)
cuántos elementos se pierden.
  - Si no, añado directamente el número del botín



=end
module Deepspace
  
  class Examen
    
    
    
    def self.principal
      
      armas = [WeaponType::LASER, WeaponType::PLASMA, WeaponType::LASER, WeaponType::MISSILE]
      damage = Damage.newSpecificWeapons(armas, 4)
      
      puts "Ejercicio 2. Prueba de Damage: #{damage.to_s}"
      
      armasestacion = [Weapon.new("Arma3", WeaponType::LASER, 2), Weapon.new("Arma4", WeaponType::MISSILE, 2)]
      escudosestacion = [ShieldBooster.new("Escudo1", 1, 1), ShieldBooster.new("Escudo2", 1, 1)]
      
      damage = damage.adjust(armasestacion, escudosestacion)
      puts "Ejercicio 2.2 Prueba de adjust #{damage.to_s}"
      
      
      
      
      #PRUEBA DEL EJERCICIO 1
      supplies1 = Deepspace::SuppliesPackage.new(3, 3, 3)
      spaceStation1 = Deepspace::SpaceStation.new("Maria", supplies1)
      botin1 = Deepspace::Loot.new(2, 2, 2, 2, 2)
      spaceStation1.setLoot(botin1)
      
      
    end
    
    
    
    
    
    
  end
  
  
  Examen.principal
  
  
  
  
end

