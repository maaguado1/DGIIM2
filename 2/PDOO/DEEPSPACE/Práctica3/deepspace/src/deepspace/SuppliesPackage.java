/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deepspace;

/**
 *
 * @author maguado
 */
class SuppliesPackage {
    private float ammoPower, fuelUnits, shieldPower;
    
    SuppliesPackage( float amo, float fue, float shie){
        this.ammoPower = amo;
        this.fuelUnits = fue;
        this.shieldPower = shie;
                
    }
    
    SuppliesPackage (SuppliesPackage s){
        this.ammoPower = s.ammoPower;
        this.fuelUnits = s.fuelUnits;
        this.shieldPower = s.shieldPower;
    }
    
    public float getAmmoPower(){
        return ammoPower;
        
    }
    
    public float getFuelUnits(){
        return fuelUnits;
        
    }
    
    public float getShieldPower(){
        return shieldPower;
    }
    
   public String toString(){
       return "SUPPLIES PACKAGE:\n ammoPower=" + this.getAmmoPower() + " fuelUnits="+ this.getFuelUnits() + " shieldPower=" + this.getShieldPower();
   }
}

