/*
 * EXPLICACIÓN DE LOS CAMBIOS:
En primer lugar, he creado una vista ElementView que tenga la misma funcionalidad que WeaponView y ShieldView pero que solamente muestre los usos.
También he añadido un Panel que se llama ElementPanelView y que muestre un array de CombatElement en forma de ElementView.

Además, en el MainWindow2 actualizo para usar un controlador nuevo, Controller2 y guardo dos atributos con las dos vistas de los paneles.
En el UpdateView simplemente actualizo los valores de los dos paneles mediante el SetArray.

En el controller nuevo, he añadido tres métodos:
- Transfer
- addElement
- start

que realizan las funcionalidades pedidas en el enunciado delegando en las del modelo.

Por último, en el Modelo he creado también esos métodos y además he añadido un método en el dado que indique si loq ue hay que añadir es un Weapon o un Shield, con igual
probabilidad.


 */
package main;

import View.GUI.MainWindow;

import deepspace.GameUniverse;

import View.GUI.MainWindow2;
import controller.Controller2;

/**
 *
 * @author Profe
 */
public class ExamenP5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameUniverse model = new GameUniverse();
        MainWindow2 view = MainWindow2.getInstance();
//        View view = MainWindow_v2.getInstance();
        Controller2 controller = Controller2.getInstance();
        controller.setModelView (model,view);
        controller.start();
    }
    
}
