#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#ifdef _OPENMP
  #include <omp.h>
#else
  #define omp_get_thread_num() 0
  #define omp_get_wtime() (1.0*clock()/CLOCKS_PER_SEC)
#endif

#define _CHUNK


void SalidaMatrizVector(double** T, double *v, int n){
  for (int i = 0; i < n; i++)
    {
    	printf("\n| ");
      for (int j = 0; j< n; j ++){
    printf(" %f " ,T[i][j]);
  }

  printf("  |  | %f |", v[i] );

  }
  printf("\n\n ");
}


int main (int argc, char *argv[]){


	if (argc <2){

		printf ("Falta argumento\n");
		exit (-1);
	}
	int i, j;


  int chunk;
  omp_sched_t kind;


	unsigned int N = atoi(argv[1]);
	double **M1, *v1, *v2, t1, t2, ncgt;

	M1= (double**) malloc(N*sizeof(double*));
	v1= (double*)malloc(N*sizeof(double));
	v2 = (double*) malloc (N*sizeof(double));


	if (M1 == NULL || v1== NULL || v2== NULL){
		printf ("\nError en la reserva de memoria");
		exit(-1);

	}

	for (i=0; i < N; i++){
		M1[i]=(double*) malloc(N*sizeof(double));


		if (M1[i] == NULL){
			printf ("Error en la reserva de memoria 2");
			exit(-1);

		}
	}

    omp_get_schedule(&kind, &chunk);
    printf ("\nPLANIFICACION UTILIZADA(run-sched-var):\nChunk: %d\n\n", chunk);


		//Inicialización de la matriz
	#pragma omp parallel for private(i,j) schedule(runtime)
	for (int i=0; i < N; i ++){
		v1[i]= i;
		v2[i]=0.0;

		for (int j=0; j < N; j++){
			if (j>=i){
				M1[i][j] = j-i +1;
			}
			else{
				M1[i][j]=0;
			}

		}

	}

	t1 = omp_get_wtime();


	//Cálculo
	double sumai;

	#pragma omp parallel for private(i,j) shared(v2)  schedule(runtime)
  for (int i=0; i < N; i ++){

		sumai =0;

		for (int j=i; j < N; j++){
			sumai += M1[i][j]*v1[j];

		}

			v2[i] += sumai;
	}


	t2 = omp_get_wtime();
	ncgt = t2-t1;

	//Salida

	if (N <= 11){
		printf("Tiempo:%11.9f\n",ncgt);

		printf ("\nResultado: [ ");

		for (i=0; i<N;i++){

				printf (" %f ", v2[i]);


			}
		printf (" ]\n");

	}
	else {

		printf ("  ");
		printf("Tiempo:%11.9f\t / Tamaño Vectores:%u\t/ V2[0]=%8.6f / / V3[%d]=(%8.6f) /\n", ncgt,N,v2[0],N-1,v2[N-1]);




	}

	free(v1); // libera el espacio reservado para v1
	free(v2); // libera el espacio reservado para v2

	for (i=0; i < N; i++){
		free(M1[i]);
}
	 // libera el espacio reservado para v3
	 free(M1);
  return 0;


}
