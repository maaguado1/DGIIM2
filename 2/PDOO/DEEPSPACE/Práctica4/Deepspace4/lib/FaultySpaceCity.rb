#encoding:utf-8

require_relative 'SpaceCity'


module Deepspace
class FaultySpaceCity < SpaceCity
  
   
  def initialize (station, collaborator)
    super
  end
  
  
  def fire()
    fuego = super 
    for c in @collaborators
      fuego += c.fire()
    end
    
    estacion = rand(@collaborators.length)
    fuego += @collaborators.at(estacion).fire()
    return fuego
  end
  
  
  
 
  
  
  
end
end

