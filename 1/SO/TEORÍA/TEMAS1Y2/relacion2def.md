## TEMA 2: PROCESOS Y hebras

### Relación de problemas

#### 1. Cuestiones generales sobre procesos y asignación de CPU:

##### a) ¿Cuáles son los motivos que pueden llevar a la creación de un proceso?

Un proceso se puede crear en:

- Nuevo proceso por lotes, el sistema operativo lee la siguiente secuencia de mandatos de control de trabajo cuando está listo para procesar un nuevo trabajo.
- Sesión interactiva, cuando un nuevo usuario entra.
- El propio sistema operativo puede crear proceso para realizar una función y evitar tiempo de espera al usuario.
- Un proceso puede crear otro, por modularidad o para explotar el paralelismo.


##### b) ¿Es necesario que lo último que haga todo proceso antes de finalizar sea una llamada al sistema para finalizar de forma explícita, por ejemplo exit()?

Una llamada al sistema es una petición explícita al SO, pero no todos los procesos llegan a la llamada exit() antes de finalizar. Por ejemplo,
una excepción no recuperable pasará por el sys_exit(), pero no por el exit().

##### c) Cuando un proceso pasa a estado “BLOQUEADO”, ¿Quién se encarga de cambiar el valor de su estado en el descriptor de proceso o PCB?

La operación de cambio de contexto (Context_switch ) se encarga de cambiar el PID y el estado y encolar  el proceso actual.


##### d) ¿Qué debería hacer cualquier planificador a corto plazo cuando es invocado pero no hay ningún proceso en la cola de ejecutables?
Siempre hay una tarea "doomie", __tarea tonta__, que cuando no hay trabajos, entra y vuelve en bucle. Es una tarea meramente estadística, que siempre está ahí.



##### e) ¿Qué algoritmos de planificación quedan descartados para ser usados en sistemas de tiempo compartido?

Los no apropiativos, pues podría pasar que una tarea no finalice nunca, junto con el First Come First Served y el de prioridades, pues los usuarios según llegan se activan, no se encolan, y todos los usuarios tienen la
misma prioridad.


#### 2. Cuestiones sobre el modelo de procesos extendido.

##### a) ¿Qué pasos debe llevar a cabo un SO para poder pasar un proceso de reciente creación de estado “NUEVO” a estado “LISTO”?

El sistema operativo mueve un estado de nuevo a listo cuando éste se encuentra preparado para ejecutar uno nuevo. Se tiene que cargar el programa en memoria principal, pues se encuentra en almaceamiento secundario.

##### b) ¿Qué pasos debe llevar a cabo un SO para poder pasar un proceso ejecutándose en CPU a estado “FINALIZADO”?

El sistema tiene que hacer un `sys_exit()`, como terminación de procesos. Se requiere:

- Liberar los recursos.
- Actualizar el valor del PCB: `PCB.ESTADO_FINALIZACION=EXIT_SUCCESS`
- Enviar una señal de finalización al padre `sigchild`
- Colocar los procesos hijos del proceso finalizado como hijos de INIT.

##### c) Hemos explicado en clase que la función context_switch() realiza siempre dos funcionalidades y que además es necesario que el kernel la llame siempre cuando el proceso en ejecución pasa a estado “FINALIZADO” o “BLOQUEADO”. ¿Qué funcionalidades debe realizar y en qué funciones del SO se llama a esta función?

El context_switch() se ha usado en rutinas de E/S, en el timeout y en el `sys_exit()`. En los cambios de estado solamente cuando se pasa a finalizado o bloqueado porque es cuando se añade un proceso nuevo.
Hay que tener cuidado con adaptar dónde se encola el proceso anterior:
- Si es un timeout, se encola en LISTOS.
- Si el proceso está esperando a una operación de E/S, se encola en BLOQUEADOS.
- Si el proceso ha llamado a `sys_exit()`, se encola en FINALIZADOS.

Después se llamaría a dispatch() y a schedule().

#### 5. Responda a las siguientes cuestiones relacionadas con el concepto de hebra

##### a) a) ¿Qué elementos de información es imprescindible que contenga una estructura de datos que permita gestionar hebras en un kernel de SO? Describa las estructuras task_t y la thread_t.

El kernel requiere la siguiente información:

- Contexto de los registros PC, SP, PSW.
- Pila de ejecución.
- Estado.


Analizamos `task_t`:

Task es la estructura de datos que gestiona los procesos en Linux.
Contiene (9+4):
- El estado de ejecución del proceso.
- La información de planificación
- Identificadores
- Comunicación entre procesos
- Enlaces
- Tiempos y temporizadores
- Sistema de archivos
- Espacio de direcciones
- Contexto específico del procesador

- Ejecutando
- Interrumpible/ininterrumpible
- Detenido
- Zombie


Ahora `thread_t`: me lo invento no lo se jejeje

Linux proporciona una solución única en la que no diferencia hilos y procesos. Los hilo a nivel de usuario se asocian con procesos a nivel de núcleo. Múltiples hilos de nivel de usuario que constituyen un único proceso a nivel de usuario se asocian con procesos Linux a nivel de núcleo y comparten el ID de grupo. Esto permite a estos procesos compartir recursos sin cambiar el contexto.
La llamada al sistema `fork()` en Linux se implementa con la llamada `clone()`.

**LLAMADA CLONE**
En Linux se crea un proceso copiando los atributos del proceso actual, es decir, un nuevo proceso se puede clonar de forma que comparte recursos (archivos, manejadores de señales y memoria virtual)
Aunque los procesos clonados comparten el mismo espacio de memoria, no pueden compartir la pila, por tanto es necesario crea espacios de pila separados.

##### b) En una implementación de hebras con una biblioteca de usuario en la cual cada hebra de usuario tiene una correspondencia N:1 con una hebra kernel, ¿Qué ocurre con la tarea si se realiza una llamada al sistema bloqueante, por ejemplo read()?

Estaríamos en un enfoque combinado ULT-UKT, esto quiere decir que una llamada bloqueante no bloquea el proceso entero. La hebra kernel llamaría al read().

##### c) ¿Qué ocurriría con la llamada al sistema read() con respecto a la tarea de la pregunta anterior si la correspondencia entre hebras usuario y hebras kernel fuese 1:1?

Si una hebra se bloquea, el planificador sigue igual, la hebra pasa a bloqueado y el planificador selecciona otra. Por tanto, la tarea sigue en estado 'Ejecutando'



#### 6. ¿Puede el procesador manejar una interrupción mientras está ejecutando un proceso sin hacer context_switch() si la política de planificación que utilizamos es no apropiativa?¿Y si es apropiativa?

Durante el manejo de la interrupción, el procesador dejará de ejecutar ese proceso y pasará al manejador de la interrupción por tanto se necesitará hacer el context_switch(). Sea o no apropiativa, en las interrupciones se realiza un cambio de contexto, pues el proceso no se está ejecutando.


#### 7. Suponga que es responsable de diseñar e implementar un SO que va a utilizar una política de planificación apropiativa (preemptive). Suponiendo que el sistema ya funciona perfectamente con multiprogramación pura y que tenemos implementada la función Planif_CPU(), ¿qué otras partes del SO habría que modificar para implementar tal sistema? Escriba el código que habría que incorporar a dichas partes para implementar apropiación(preemption).


El scheduler ya está implementado (`planif_CPU()`), habría que implementar el dispatch, que se encarga de cambiar de un proceso a otro.
El dispatcher actualiza el PCB antiguo, mueve el PCB antiguo a la cola deseada, actualiza el PCB nuevo seleccionado por el scheduler, actualiza las estructuras de datos de gestión de memoria y restaura los valores de contexto del PCB nuevo al procesador.

~~~
context_switch(){
  if (planif_CPU)
  then
    DISPATCH
}


~~~

La planificación apropiativa se realiza por interrupciones, es decir, primero el proceso realiza RSI, y antes del pop y ret del final de la rutina ejecuta `if (planif_CPU)`.



#### 8. Para cada una de las siguientes llamadas al sistema explique si su procesamiento por parte del SO requiere la invocación del planificador a corto plazo (Planif_CPU)

- **Crear un proceso** No requiere de la planificación a corto plazo, sino de la de a largo plazo, que se encarga de decidir qué trabajos son admitidos en el sistema, y posteriormente convertidos en procesos.

- **Abortar un proceso** Sí que se requiere la planificación a corto plazo, pues cuando se realiza sys_exit, aunque sea forzosa, se deberá establecer qué proceso se ejecuta inmediatamente después (labor de Planif_CPU).

- **Bloquear un proceso, read(), o wait()** Sí que se requiere, pues el planificador a corto plazo actúa en las llamadas al sistema y en los bloqueos, pues un proceso deja de ejecutarse y se tiene que decidir cuál va después.

- **Desbloquear un proceso, RSI, exit()** También (esto aburre ya)

- **Modificar la prioridad de un proceso** No necesita el planificador a corto plazo, pues no se decide si el proceso se ejecuta o no, simplemente es un cambio en los atributos de gestión del proceso.



#### 9. En el algoritmo de planificación FCFS, el índice de penalización, (M+r)/r, ¿es creciente,decreciente o constante respecto a r (ráfaga de CPU: tiempo de servicio de CPU requerido por un proceso)? Justifique su respuesta

Es decreciente:
- para procesos cortos, es decir con una r pequeña, el índice de penalización crece bastante.

- En procesos largos, limitados por la CPU, el índice de penalización no crece tanto.


Claramente esta situación es debida a que los procesos cortos se ven perjudicados en una planificación FCFS, al tener que pasar varias veces por la espera de la cola de listos (si están limitados por E/S).




#### 10.  multiprogramado que utiliza el algoritmo Por Turnos (Round-Robin, RR). Sea S el tiempo que tarda el despachador en cada cambio de contexto. ¿Cuál debe ser el valor de quantum Q para que el porcentaje de uso de la CPU por los procesos de usuario sea del 80%?

S: tiempo de context_switch()
Q: quantum

**Se hace Tiempo total de trabajo(Q)/Tiempo de ejecución(Q+S)**
