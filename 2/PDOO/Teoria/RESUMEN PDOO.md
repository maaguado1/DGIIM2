## RESUMEN PDOO

### 1. Atributos y métodos 

Si queremos que un atributo sea constante: Java `final`, Ruby `MAYUSCULAS`.

**Atributos de instancia** : Son variables que están asociadas a cada objeto, y cada instancia tiene su espacio de atributos.

​	Java: `private int a_instancia;`

​	Ruby (en el constructor): `@de_instancia`

**Métodos de instancia**: Son funciones que están asociadas a cada objeto de la clase. 

​	Java: ` public int get_instancia();`

​	Ruby: `def mde_instancia`

**Atributos de clase**: Almacenan información que está asociada a la propia clase.

​	Java: `static private int a_clase;`

​	Ruby (fuera  del constructor): `@@de_instancia`

**Métodos de clase:** Son funciones asociadas a la propia clase

​	Java: `static private int getClase();`

​	Ruby: `def self.clase`

**(RUBY) Atributos de instancia de la clase**: 

​	Ruby (fuera  del constructor): `@instancia_clase`

#### A) Acceso

- No se puede acceder a atributos o métodos de instancia desde un método de la clase.

  - Se tendría que solicitar ese elemento a una instancia concreta

- RUBY:  

  - Los atributos de clase (@@) 
    - Son accesibles desde el ámbito de instancia.
    - Se comparten entre subclases
  - Los atributos de instancia de la clase (@) no lo son.
    - Para acceder creamos un getter: `self.class.getEdad()`.
  - El propio objeto se referencia con ` self`
  - Todos los atributos son `private`.

- JAVA:

  - El objeto se referencia con `this`.

  - Hay niveles de accesibilidad, se accede a los privados desde:

    - Desde una instancia a otra instancia de la misma clase.

    - Desde el ámbito de clase a una instancia de esa clase.

    - Desde el ámbito de instancia a una clase de la que se es instancia.

      

### 2. Constructores

Sirven para crear (reservar e inicializar) objetos.

#### JAVA

- Tienen el mismo nombre de la clase, no devuelven nada.

- Admite sobrecarga

  - Se pueden llamar entre ellos: 

    `this.NombreClase(parametrosconstructor)`

#### RUBY

- Se llama initialize, es un método de instancia privado.
- No se puede sobrecargar, para añadir varios:
  - Se crean métodos de clase que hagan de constructor.
  - Se pasa un número variable de argumentos a initialize

