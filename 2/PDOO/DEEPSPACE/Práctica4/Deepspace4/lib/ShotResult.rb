#encoding:utf-8

module Deepspace
  
  
# Enum that represents a shot result received by an enemy ship or a space station
module ShotResult
    DONOTRESIST=:donotresist;
    RESIST=:resist;
end # enum

end # module Deepspace