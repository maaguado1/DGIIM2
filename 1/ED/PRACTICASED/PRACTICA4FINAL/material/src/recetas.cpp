/**
 * @file receta.cpp
 * @brief implementación del TDA receta
 * @authors María Aguado y Amparo Almohalla
 */



#include <assert.h>
#include <iostream>
#include <string>
#include <sstream>
#include "recetas.h"
#include "ingredientes.h"




using namespace std;


recetas::recetas(){

    map<string, receta> uno;
    datos=uno;
}


recetas::recetas(const recetas& orig){
    datos=orig.datos;
}



const receta& recetas::operator[](const string& c)const{
    map<string,receta>::const_iterator it = datos.find(c);
    return (*it).second;
}



receta& recetas::operator[](const string& c){
    return datos[c];
}

void recetas::setValores(const ingredientes& valores){
    for (recetas::iterator it = begin(); it!= end(); ++it)
        (*it).setValores(valores);
}



void recetas::insertar(const receta& nueva){
    string codigo=nueva.getCode();
    pair<string, receta> nuevo= make_pair(codigo, nueva);
    datos.insert(nuevo);
}


void recetas::borrar(const string& codigo){
    if (!datos.empty())
        datos.erase(codigo);
}

float recetas::getCalorias(){
    float tot=0;
    for (recetas::iterator it = begin();it != end(); ++it)
        tot += (*it).getCalorias();
    return tot;
}









std::ostream & operator<<(std::ostream & os, const recetas & copia){
    if (copia.size()>0){
        for (recetas::const_iterator it = copia.begin(); it != copia.end(); ++it){
            os << (*it);
            if (!os){
                cerr<<"ERROR al copiar secuencia" <<endl;
                exit(1);
         
            }
            
        }
        cout << "Secuencia copiada" << endl;
    }
    return os;
            
}


std::istream & operator>>(std::istream & is, recetas & nueva){
    receta nuevo;
    is >> nuevo;
    nueva.insertar(nuevo);
    while(is.peek()!=EOF){
        is >> nuevo;
          
        if (!is){
            cerr<<"Secuencia acabada" <<endl;
            break;
        }

        nueva.insertar(nuevo);
    }
  
    return is;

}