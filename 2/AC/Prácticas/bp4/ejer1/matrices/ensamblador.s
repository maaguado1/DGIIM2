	.file	"ensamblador.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Faltan n\302\272 componentes del vector"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"\n| "
.LC3:
	.string	" %f "
.LC4:
	.string	" | "
.LC5:
	.string	"\n\n "
.LC6:
	.string	"%11.9f"
.LC8:
	.string	"Tiempo:%11.9f\n"
.LC9:
	.string	"\nResultado: "
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB41:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	movq	%fs:40, %rax
	movq	%rax, 40(%rsp)
	xorl	%eax, %eax
	cmpl	$1, %edi
	jle	.L42
	movq	8(%rsi), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	movl	$10000, %ebp
	call	strtol@PLT
	cmpl	$10000, %eax
	movq	%rax, %r14
	cmovbe	%eax, %ebp
	testl	%eax, %eax
	je	.L3
	leaq	M1(%rip), %r12
	leaq	M2(%rip), %r13
	leaq	M3(%rip), %rbx
	xorl	%r9d, %r9d
	movl	$2, %r10d
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%r10d, %edx
	xorl	%eax, %eax
	subl	%r9d, %edx
	.p2align 4,,10
	.p2align 3
.L5:
	pxor	%xmm0, %xmm0
	leal	-1(%rdx), %ecx
	movq	$0x000000000, (%r8,%rax,8)
	cvtsi2sd	%ecx, %xmm0
	movsd	%xmm0, (%rsi,%rax,8)
	pxor	%xmm0, %xmm0
	cvtsi2sd	%edx, %xmm0
	addl	$1, %edx
	movsd	%xmm0, (%rdi,%rax,8)
	addq	$1, %rax
	cmpl	%eax, %ebp
	ja	.L5
	addl	$1, %r9d
	addq	$80000, %rsi
	addq	$80000, %rdi
	addq	$80000, %r8
	cmpl	%r9d, %ebp
	ja	.L4
	movl	%ebp, %r15d
	movq	%rsp, %rsi
	xorl	%edi, %edi
	shrl	$4, %r15d
	call	clock_gettime@PLT
	leal	-1(%r15), %esi
	leaq	128+M3(%rip), %rax
	movl	%r15d, %r8d
	sall	$4, %r8d
	xorl	%r9d, %r9d
	salq	$7, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rcx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L15:
	testl	%r15d, %r15d
	je	.L13
	movsd	(%r12,%rdi,8), %xmm0
	movq	%rcx, %rdx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L8:
	movsd	(%rdx), %xmm1
	subq	$-128, %rax
	subq	$-128, %rdx
	mulsd	%xmm0, %xmm1
	addsd	-128(%rax), %xmm1
	movsd	%xmm1, -128(%rax)
	movsd	-120(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-120(%rax), %xmm1
	movsd	%xmm1, -120(%rax)
	movsd	-112(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-112(%rax), %xmm1
	movsd	%xmm1, -112(%rax)
	movsd	-104(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-104(%rax), %xmm1
	movsd	%xmm1, -104(%rax)
	movsd	-96(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-96(%rax), %xmm1
	movsd	%xmm1, -96(%rax)
	movsd	-88(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-88(%rax), %xmm1
	movsd	%xmm1, -88(%rax)
	movsd	-80(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-80(%rax), %xmm1
	movsd	%xmm1, -80(%rax)
	movsd	-72(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-72(%rax), %xmm1
	movsd	%xmm1, -72(%rax)
	movsd	-64(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-64(%rax), %xmm1
	movsd	%xmm1, -64(%rax)
	movsd	-56(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-56(%rax), %xmm1
	movsd	%xmm1, -56(%rax)
	movsd	-48(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-48(%rax), %xmm1
	movsd	%xmm1, -48(%rax)
	movsd	-40(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-40(%rax), %xmm1
	movsd	%xmm1, -40(%rax)
	movsd	-32(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-32(%rax), %xmm1
	movsd	%xmm1, -32(%rax)
	movsd	-24(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-24(%rax), %xmm1
	movsd	%xmm1, -24(%rax)
	movsd	-16(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-16(%rax), %xmm1
	movsd	%xmm1, -16(%rax)
	movsd	-8(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	-8(%rax), %xmm1
	movsd	%xmm1, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L8
.L13:
	cmpl	%r8d, %ebp
	jbe	.L9
	movsd	(%r12,%rdi,8), %xmm1
	movl	%r8d, %eax
	.p2align 4,,10
	.p2align 3
.L10:
	movsd	(%rcx,%rax,8), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%rbx,%rax,8)
	addq	$1, %rax
	cmpl	%eax, %ebp
	ja	.L10
.L9:
	addq	$1, %rdi
	addq	$80000, %rcx
	cmpl	%edi, %ebp
	ja	.L15
	addl	$1, %r9d
	addq	$80000, %rbx
	addq	$80000, %r12
	addq	$80000, %rsi
	cmpl	%r9d, %ebp
	ja	.L7
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	call	clock_gettime@PLT
	movq	24(%rsp), %rax
	subq	8(%rsp), %rax
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm0
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	cmpl	$11, %r14d
	cvtsi2sdq	%rax, %xmm1
	divsd	.LC7(%rip), %xmm0
	addsd	%xmm1, %xmm0
	jbe	.L43
	leaq	.LC6(%rip), %rsi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk@PLT
.L23:
	xorl	%eax, %eax
	movq	40(%rsp), %rdi
	xorq	%fs:40, %rdi
	jne	.L44
	addq	$56, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L43:
	.cfi_restore_state
	leaq	.LC8(%rip), %rsi
	leaq	M1(%rip), %r12
	leaq	.LC3(%rip), %r13
	movl	$1, %edi
	movl	$1, %eax
	xorl	%r14d, %r14d
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC2(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	movsd	(%r12,%rbx,8), %xmm0
	movq	%r13, %rsi
	movl	$1, %edi
	movl	$1, %eax
	addq	$1, %rbx
	call	__printf_chk@PLT
	cmpl	%ebx, %ebp
	ja	.L16
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %edi
	addl	$1, %r14d
	addq	$80000, %r12
	call	__printf_chk@PLT
	cmpl	%r14d, %ebp
	ja	.L17
	leaq	.LC5(%rip), %rsi
	leaq	M2(%rip), %r12
	leaq	.LC3(%rip), %r13
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC2(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movsd	(%r12,%rbx,8), %xmm0
	movq	%r13, %rsi
	movl	$1, %edi
	movl	$1, %eax
	addq	$1, %rbx
	call	__printf_chk@PLT
	cmpl	%ebx, %ebp
	ja	.L18
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %edi
	addl	$1, %r14d
	addq	$80000, %r12
	call	__printf_chk@PLT
	cmpl	%r14d, %ebp
	ja	.L19
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	leaq	M3(%rip), %r12
	leaq	.LC3(%rip), %r13
	xorl	%r14d, %r14d
	call	__printf_chk@PLT
	leaq	.LC9(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC2(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	__printf_chk@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	movsd	(%r12,%rbx,8), %xmm0
	movq	%r13, %rsi
	movl	$1, %edi
	movl	$1, %eax
	addq	$1, %rbx
	call	__printf_chk@PLT
	cmpl	%ebx, %ebp
	ja	.L21
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %edi
	addl	$1, %r14d
	addq	$80000, %r12
	call	__printf_chk@PLT
	cmpl	%ebp, %r14d
	jb	.L22
.L24:
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L23
.L3:
	movq	%rsp, %rsi
	xorl	%edi, %edi
	call	clock_gettime@PLT
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	call	clock_gettime@PLT
	movq	24(%rsp), %rax
	subq	8(%rsp), %rax
	leaq	.LC8(%rip), %rsi
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm0
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	cvtsi2sdq	%rax, %xmm1
	movl	$1, %eax
	divsd	.LC7(%rip), %xmm0
	addsd	%xmm1, %xmm0
	call	__printf_chk@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	leaq	.LC5(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	leaq	.LC9(%rip), %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk@PLT
	jmp	.L24
.L44:
	call	__stack_chk_fail@PLT
.L42:
	leaq	.LC0(%rip), %rdi
	call	puts@PLT
	orl	$-1, %edi
	call	exit@PLT
	.cfi_endproc
.LFE41:
	.size	main, .-main
	.comm	M3,800000000,32
	.comm	M2,800000000,32
	.comm	M1,800000000,32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	1104006501
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
