## MÓDULO II: Sesión 2

### Llamadas al sistema para el SA (II)
## 1. Llamadas para los Permisos

### 1.1. Llamada al sistema umask:

Establece la máscara de usuario, usada por open para establecer los permisos iniciales.
Funcionamiento: los permisos en la máscara se desactivan de mode.
SINTAXIS:

~~~
#include <sys/types.h>
#include <sys/stat.h>

mode_t umask(mode_t mask);
~~~

### 1.2. Llamada al sistema chmod y fchmod:

Nos dejan cambiar los permisos sobre un archivo (chmod), o sobre un archivo abierto por open (fchmod).

SINTAXIS:
~~~
#include <sys/types.h>
#include <sys/stat.h>
int chmod(const char *path, mode_t mode);
int fchmod(int fildes, mode_t mode);
~~~

PARÁMETROS:

| Valor | Descripción |
| ---- | ---- |
| S_ISUID | Asigna el UID del propietario al UID efectivo |
| S_ISGID | Asigna el GID del propietario al GID efectivo |
|S_ISVTX | Activa sticky bit (borrado restringido, es decir, un proceso no privilegiado no puede borrar o renombrar archivos del directorio salvo que tenga permiso de escritura y sea propietario). |
| S_(tabla del open) | tabla open sesion 1 |

VALOR DEVUELTO:
Exito 0, error -1.

## Funciones de manejo de Directorio

SINTAXIS:
~~~
#include <sys/types.h>
#include <dirent.h>

DIR *opendir(char *dirname) //Devuelve un puntero al stream
struct dirent *readdir(DIR *dirp) //Lee la entrada del puntero
                                  // de lectura. Devuelve punteros
                                  // a un struct dirent
int closedir(DIR *dirp)           //cierra directorio (exito 0, error -1)
void seekdir(DIR *dirp, log loc)  // sitúa el puntero de lectura
long telldir(DIR *dirp)           // devuelve la posición del puntero de lectura
void rewinddir(DIR *dirp)         // posiciona el puntero al principio
~~~

ESTRUCTURAS:
~~~
typedef struct _dirdesc {
  int dd_fd;
  long dd_loc;
  long dd_size;
  long dd_bbase;
  long dd_entno;
  long dd_bsize;
  char *dd_buf;
} DIR;

//La estructura struct dirent:
#include <sys/types.h>
#include <dirent.h>

struct dirent {
long d_ino; /* número i-nodo */
char d_name[256]; /* nombre del archivo */
};

~~~

### La orden nftw()

SINTAXIS:
~~~
#define _XOPEN_SOURCE 500
#include <ftw.h>
int nftw (const char *dirpath, int (*func) (const char *pathname, const struct stat *statbuf,
int typeflag, struct FTW *ftwbuf), int nopenfd, int flags);

~~~

DESCRIPCIÓN:
Llama a la función func para cada archivo del árbol.
- **nopenfd**: número máximo de descriptores, si se pasa (cierra y abre).
- **flags**:


|Flag | Descripción |
|--- |----|
|FTW_DIR | hace chdir en cada directorio|
| FTW_DEPTH | hace recorrido postorden, llama a func sobre los archivos antes de ejecutar func sobre el directorio|
| FTW_MOUNT | No cruza punto de montaje |
| FTW_PHYS | desreferencia enlaces simbólicos |

Parámetros de la función func:
- **pathname**: nombre del archivo (relativo o absoluto)
-  ** statbuf**: puntero a la estructura stat
- **typeflag**: da info adicional sobre el archivo:

| Flag | Descripción |
|---- | --- |
| FTW_D | Es un directorio |
| FTW_DNR | posorden de un directorio, el actual es un directorio ya recorrido |
| FTW_F | No es directorio o enlace simbólico |
| FTW_NS | stat ha fallado |
|FTW_SL | Enlace simbólico (nftw con FTW_PHYS) |
|FTW_SLN | Enlace simbólico perdido (no FTW_PHYS)|

 - **ftwbuf** puntero a estructura definida por

 ~~~
 struct FTW {
   int base; /* Desplazamiento de la parte base del pathname */
   int level; /*Profundidad del archivo dentro recorrido del arbol */
 };
 ~~~
La función func devuelve un valor entero: 0 (nftw continúa), otro (nftw para inmediatamente)

DEVOLUCIÓN:

nftw devuelve lo que devuelva func, si siempre 0, 0. Si func devuelve en algún momento otro distinto, devuelve ese.
