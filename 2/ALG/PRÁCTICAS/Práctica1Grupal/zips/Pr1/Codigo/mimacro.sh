#!/bin/bash
contador=1
echo "" >> salidaSeleccionOpti.dat
while [ $contador -le 26 ]
do
	echo " $contador " >> salidaSeleccionOpti.dat
	./seleccionOpti $contador >> salidaSeleccionOpti.dat
	((contador+=1))
done
cat salidaSeleccionOpti.dat
