/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import View.View;
import java.util.ArrayList;
import model.AppState;

import model.BankCheckToUI;
import model.Model;
import model.ModelToUI;


/**
 *
 * @author maguado
 */
public class Controller {
    
    private static Controller instance = null;
    private View view;
    private Model model;
    
    public static Controller getInstance(){
         if (instance==null)
             instance = new Controller();
         return instance;
         
     }
    
    private Controller(){
        
    }
    
    
    public void setModelView (Model modelo, View vista){
        model = modelo;
        view = vista;
    }
    
    public void start(){
        model.init(view.getNames());
        view.updateView();
        view.showView();
    }
    
    public void finish(int i ){
        if (view.confirmExitMessage())
            System.exit(i);
    }
    
    public AppState getAppState(){
        return model.getAppState();
    }
    
    
     public ModelToUI getModelToUI () {
        return model.getUIversion();
    }

      public void getNewBankChecks(int howMany) {
        for (int i = 0; i < howMany; i++)
            model.getAnotherBankCheck();
    }

    public void next() {
        model.next();
    }

     
    public void spendBankChecks(ArrayList<Integer> selectedChecks){
        for (int i =selectedChecks.size()-1; i >=0; i --){
            model.spendBankCheck(selectedChecks.get(i));
        }
    }
    
   
    
    
}
