/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deepspace;

/**
 *
 * @author maguado
 */
class ShieldBooster {
    private String name;
    private float boost;
    private int uses;
    
    ShieldBooster(String n, float b, int u){
        this.name = n;
        this.boost = b;
        this.uses = u;
    }
    
    ShieldBooster(ShieldBooster s){
        this.name = s.name;
        this.boost = s.boost;
        this.uses = s.uses;
    }
    
    
    
    public float getBoost(){
        return boost;
       
    }
    
    public int getUses(){
        return uses;
    }
    
    public float useIt(){
        if (uses > 0){
            uses --;
            return boost;
        }
        else
            return 1.0f;
              
    }
    
    public String toString(){
        return "\nSHIELDBOOSTER: \n Name= " + this.name + " Boost= " + this.getBoost() + " Uses= " + this.getUses();
    }
    
    ShieldToUI getUIversion(){
        return new ShieldToUI(this);
    }
}
