#Asignación de tareas

####Descripción del problema

<div style="text-align: justify"> Supongamos que disponemos de *n* trabajadores y *n* tareas. Llamamos *c<sub>i,j</sub>* > 0 al coste de asignarle la tarea *j* al trabajador *i* (por lo tanto, cuando representemos los datos en una matriz de costos, las columnas se corresponderán con las tareas y las filas con los trabajadores). Una asignación válida es aquella en la que a cada trabajador le corresponde una tarea y cada tarea la realiza un trabajador diferente. Dada una asignación válida, definimos el coste de dicha asignación como la suma total de los costes individuales. Nuestro trabajo consistirá en diseñar e implementar un algoritmo que use la técnica *greedy* (un algoritmo *voraz* ) para obtener una asignación de tareas a trabajadores óptima. </div> 

#### Datos del problema

A lo largo de la resolución del problema usaremos la siguiente notación:
- *n* es el ** número de trabajadores **, que coincide con el ** número de tareas**.
- *C* es la **matriz de costos**, de tamaño *n* x *n*, en el que el elemento *c<sub>i,j</sub>* de la matriz es el cote de asignar la tarea *j* al trabajador *i*.
- *a* es el **vector de asiganciones**, de tamaño *n*. La tarea asignada al trabajador *i* estará en el elemento *a<sub>i</sub>* del vector.
- *W<sub>a</sub>* es el coste de una cierta asigción *a*, que se calcula mediante:
$$ W_a =\sum_{i=0}^{n-1}c_{i,a_j}$$

#### Diseño del algoritmo
Para resolver este problema mediante un algoritmo *greedy* hemos visto dos heurísticas:
1. Aisgnar cada tarea al mejor trabajador posible
2. Asignar cada trabajador a la mejor tarea posible

Aunque ninguna de las dos es óptima, ambas utilizan la técnica *greedy*. En nuestro caso, hemos escogido la segunda heurística. 

A la hora de diseñar nuestro algoritmo para la resolución del problema, tenemos que asegurarnos de que se cumplan las 6 características de los algoritmos *greedy*. En nuestro caso, estos elementos son los siguientes:
1. **Lista de candidatos** : Las tareas.
2. **Lista de candidatos utilizados**: Las tareas que ya han sido asignadas, y que por tanto tendrán su casilla marcada como *false* en el vector *disponibles*.
3. **Función solución** : Todas las tareas han sido asignadas.
4. **Función de selección** : La tarea de menor coste para cada trabajador.
5. **Criterio de factibilidad**: La tarea que vayamos a asignar debe estar disponible, no haber sido asignada anteriormente, ya que cada tarea sólo se puede asignar a un trabajador.
6. **Función objetivo**: Minimizar el coste total de asignar cada trabajador a cada tarea.

Por tanto, el pseudocódigo de nuestro algoritmo (el código completo lo podemos encontrar en `Trabajadores > trabajadores.cpp` ) es el siguiente:

	ALGORITMO P = AsignaciónTareasHeurística2(Matriz C, n)
	U={1,...n}     //Candidatos, tareas a asignar
	X = {0}<sub>n</sub>        //Vector de tareas asignadas, solución a devolver
	Para i=desde 1 hasta n, hacer:
		j = Seleccionar tarea j en U donde C[i][j] sea mínimo
		U = U\{j}
		X(i) = j
	FIN
	Devolver X
	

#### Descripción del algoritmo
Para resolver nuestro problema, realizaremos los siguientes pasos en el orden en el que se indican:
1. Recorremos la matriz C por filas, y asignamos al primer trabajador la tarea de menor coste.
2. Inhabilitamos la columna (ya que cada tarea se asigna a un sólo trabajador) haciendo uso de un vector auxiliar -que hemos llamado *d*- de trabajos disponibles. También podríamos sobreescribir la matriz C colocando todos los componentes de esa misma columna a 0 (pues los costes son siempre estrictamente positivos).
3. Realizamos de nuevo este procedimiento hasta que todos los trabajadores tengan una tarea asignada, con la salvedad de que en el paso (2) hemos de comprobar si la tarea está disponible, ya sea consultado el vector de trabajos disponibles o si los elementos de la matriz no son ceros en el caso de usar la otra alternativa.

Visualicemos los pasos descritos anteriromente:
<center>![visuAlgoritmo](/home/amparo/Escritorio/Alg/pr3/visuAlgoritmo.png  "visuAlgoritmo")</center>

En nuestro código este procedimiento lo realizan las siguientes funciones:
- **optima** : función para calcular la asignación especificada anteriormente.
- **mejor_tarea** : función para calcular la mejor tarea ( la de menor coste ) que se encuentre disponible, haciendo uso de un vector *disponibles*, de un conjunto de tareas.

#### Eficiencia del algoritmo
###### Eficiencia teórica
Dado que en nuestro código hemos implementado diferentes funciones para hacer la ejecución del programa principal más compacta y visual, para realizar el estudio de la eficiencia teórica debemos analizar la eficiencia de cada una de nuestras funciones. Rápidamente nos damos cuenta de que todas las funciones especificadas en el apartado de *Generación de números aleatorios* son O(1), luego no afectarán a la eficiencia de nuestro algoritmo salvo que sean utilizadas en bucles, como veremos a continuación.

En la siguiente sección, *Generación de datos del problema*, tenemos que pararnos un poco más. La única función de este apartado es `generar_matriz`, que devuelve la matriz de costes aleatorios para trabajadores y tareas. Para ello usa dos bucles anidados, ambos de coste O(n), siendo *n* el orden de la matriz, es decir, el tamaño de nuestro problema, luego esta función es O(n^2). Por lo tanto nuestro algoritmo ya será como mínimo, de orden cuadrático.

Procedemos a la siguiente sección `Cálculos del problema`, donde tenemos varias funciones que analizar:
1. **calcular_coste** : es O(n) ya que recorre el vector asignacion mediante un bucle, y como hemos visto antes, el vector asignación es de tamaño *n* .
2. **mejor_tarea** : recorre el vector tareas_disponibles que es de tamaño *n* mediante un bucle, luego como mínimo es O(n). Luego realiza otro bucle comparando con los demás costes disponibles y dentro del bucle realizamos operaciones de coste O(1), por lo que esta función también es O(n).
2. **optima**: dentro del bucle de *n* iteraciones invoca a la función mejor_tarea descrita previamente, y que ya hemos visto que esta función es O(n). Por eso concluimos que esta función es O(n^2).

En la última sección antes del programa principal, tenemos una única función llamada `mostrar_matriz`. Es fácil ver que esta función es O(n^2).

Dentro del programa principal, lo único que se hace es llamar secuencialmente a las  funciones ya implementadas y descritas anteriormente y luego mostrar los resultados. En conclusión, nuestro algoritmo es **O(n^2)**, por lo que no sólo es más fácil de implementar que un algoritmo de fuerza bruta, si no que es más eficiente y tiene tiempos de ejecución bastante menores.


###### Eficiencia empírica
Los tamaños de prueba para ejecutar el algoritmo han ido desde 100 hasta 4000, cada vez con incremento de 200. A su vez cada iteración la heos hecho 100 veces y hemos calculado la media, con el fin de eliminar los mejores y peores casos. Para automatizar este proceso tan repetitivo hemos creado una macro, que la podemos consultar en `Trabajdores > miMacro.sh `.

Los datos obtenidos, calculando por un lado el tiempo y por otro la distancia han sido estos:

<center> ![salidaTrabajadoresTiempo](/home/amparo/Escritorio/Alg/trabajadores/salidaTrabajadoresTiempoo.png  "salidaTrabajadoresTiempo")</center>

<center>![salidaTrabajadoresCoste](/home/amparo/Escritorio/Alg/trabajadores/salidaTrabajadoresCoste.png  "salidaTrabajadoresCoste")</center>

###### Eficiencia híbrida
Aplicando lo aprendido en las prácticas anteriores, es fácil observar que la función que mejor se ajusta a nuestros datos es una de tipo cuadrático, especialmente para los datos que se corresponden con el tiempo de ejecución del algoritmo.
$$f(x) = a_0*x^2 + a_1*x + a_2 $$

	After 10 iterations the fit converged.
	final sum of squares of residuals : 3.8816e+07
	rel. change during last iteration : -1.40737e-10

	degrees of freedom    (FIT_NDF)                        : 17
	rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 1511.06
	variance of residuals (reduced chisquare) = WSSR/ndf   : 2.28329e+06

	Final set of parameters            Asymptotic Standard Error
	=======================            ==========================
	a0              = 0.0309058        +/- 0.0002851    (0.9225%)
	a1              = -0.684033        +/- 1.177        (172.1%)
	a2              = 1015.97          +/- 1018         (100.2%)

	correlation matrix of the fit parameters:
		        a0     a1     a2     
	a0              1.000 
	a1             -0.969  1.000 
	a2              0.748 -0.867  1.000 
	
<center> ![tiempoAjustado](/home/amparo/Escritorio/Alg/trabajadores/tiempoAjustado.png  "tiempoAjustado")</center>