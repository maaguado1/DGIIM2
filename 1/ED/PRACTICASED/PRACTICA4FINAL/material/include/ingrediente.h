
/**
 * @file ingrediente.h 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c ingrediente
 */

#ifndef INGREDIENTE_H
#define INGREDIENTE_H


#include<string>
#include <iostream>
using namespace std;

/**
 *  @brief Clase usada para representar un Ingrediente.
 *
 *
 * Una instancia @c Ingrediente es un conjunto de datos
 * sobre un ingrediente: 
 * -string con el nombre
 * -double con las calorías por 100 gr
 * -double con el porcentaje de hidratos de carbono 
 * -double con el porcentaje de proteínas 
 * -double con el porcentaje de grasas 
 * -double con el porcentaje de fibra 
 * -Tipo de ingrediente (string)
 *  
 * La eficiencia en espacio es @e O(n).
 *
 *
 */


class ingrediente{
     /**
       * 
       * Un objeto válido @e v del TDA Ingredientes debe cumplir 
       * - @c calorias, hidratos, proteinas, grasas y fibra>=0
       * - @c nombre, tipo 
       * 
      * @f$(>=0)@f$.
       * 
       *
    */
private:
    /**
    * @brief Nombre del ingrediente.
    */
    string _nombre;
    
        /**
    * @brief Calorías(100g) del ingrediente.
    */
    float _calorias;
    
        /**
    * @brief Hidratos(100g) del ingrediente.
    */
    float _hidratos;
      /**
    * @brief Proteína(100g) del ingrediente.
    */  
    
    float _proteina;
    /**
    * @brief Grasa(100g) del ingrediente.
    */    
    
    float _grasa;
     /**
    * @brief Fibra(100g) del ingrediente.
    */   
    float _fibra ;
    
        /**
    * @brief Tipo del ingrediente.
    */
    string _tipo;
   
public:
    
// ---------------  Constructores ----------------
    /**
     * @brief Constructor por defecto
     *   Inicializa 
     *  -@a _nombre a @c " "
     *  -@a _tipo a @c " "
     *  -@a _calorias a @c 0
     *  -@a _hidratos a @c 0
     *  -@a _proteína a @c 0
     *  -@a _grasa a @c 0
     *  -@a _fibra a @c 0
     */
   
    ingrediente();
    
    
    /**
     * @brief Constructor de copia.
     *
    * Este constructor toma como parámetro un objeto de tipo @a ingrediente y copia
    * todos los datos miembros al nuevo ingrediente.
    *
    * @param original El ingrediente a copiar.
    */
    
    ingrediente (const ingrediente& original);
 

    
    //FUNCIONES DE CONSULTA
    /**
     * @brief Consulta del nombre 
     * @retval string Nombre del ingrediente.
     * 
     */  
    
    string getNombre ()const {return _nombre;}
    
    /**
     * @brief Consulta de las calorías
     * @retval double Calorías del ingrediente(100g)     * 
     */  
    inline float getCalorias ()const{return _calorias;}
    

    /**
     * @brief Consulta de los hidratos
     * @retval double Hidratos del ingrediente(100g)
     * 
     */  
    inline float getHc ()const{return _hidratos;}
    
    /**
     * @brief Consulta de la proteína
     * @retval double Proteína del ingrediente (100g)
     * 
     */  
    
    inline float getProteinas ()const{return _proteina;}
    
    /**
     * @brief Consulta de la grasa
     * @retval double Grasa del ingrediente(100g)
     * 
     */  
    
    inline float getGrasas ()const{return _grasa;}
    
    
    /**
     * @brief Consulta de la fibra
     * @retval double Fibra del ingrediente (100g)
     * 
     */  
    
     inline float getFibra () const{return _fibra;}
    

    /**
     * @brief Consulta del tipo
     * @retval string Tipo del ingrediente
     * 
     */  
    
    inline string getTipo () const{ return _tipo;}

    /**
     * @brief Establece el nombre del objeto apuntado por this
     * @param nombre Nombre a adjudicar
     * */
    inline void setNombre(string nombre){this->_nombre = nombre;};

    /**
     * @brief Establece las calorías del objeto apuntado por this
     * @param calorias Calorías a adjudicar
     * */
    inline void setCalorias(double calorias){this->_calorias=calorias;};

    /**
     * @brief Establece la fibra del objeto apuntado por this
     * @param fibra Fibra a adjudicar
     * */
    inline void setFibra(double fibra){this->_fibra=fibra;};
    
    /**
     * @brief Establece la grasa del objeto apuntado por this
     * @param grasa Grasa a adjudicar
     * */
    inline void setGrasa(double grasa){this->_grasa=grasa;};

    /**
     * @brief Establece las proteínas del objeto apuntado por this
     * @param proteina Proteínas a adjudicar
     * */
    inline void setProteinas(double proteina){this->_proteina=proteina;};

    /**
     * @brief Establece los hidratos de carbono del objeto apuntado por this
     * @param hidratos Hidratos de carbono a adjudicar
     * */
    inline void setHidratos(double hidratos){this->_hidratos=hidratos;};

    /**
     * @brief Establece el tipo de alimento al objeto apuntado por this
     * @param tipo Tipo de alimento a adjudicar
     * */
    inline void setTipo(string tipo){this->_tipo=tipo;};
    
    
    
    /**
     * @brief Sobrecarga del operador de asignacion
     * Asigna a todos los miembros los miembros del parámetro (lado derecho)
     * @param original Objeto de la clase Ingrediente
     * @retval Ingrediente&, referecia al objeto
     * 
     */
    ingrediente& operator=(const ingrediente& original);
    
    
    /**
     * @brief Sobrecarga del operador relacional "menor que"
     * @param original Objeto de la clase Ingrediente
     * @retval bool valor booleano que indica si el ingrediente es menor 
     * que el original
     * @note la comparación se realiza mediante el tipo de alimento, 
     * ante la igualdad, por el nombre
     */
    bool operator<(const ingrediente& original)const ;
    
    
    /**
     * @brief Sobrecarga del operador relacional "mayor que"
     * @param original Objeto de la clase Ingrediente
     * @retval bool valor booleano que indica si el ingrediente es mayor 
     * que el original
     * @note la comparación se realiza mediante el tipo de alimento, 
     * ante la igualdad, por el nombre
     */
    bool operator>(const ingrediente& original)const ;
    
    /**
     * @brief Operador relacional
     * @param original Objeto de la clase Ingrediente
     * @return valor booleano que indica si el ingrediente es igual 
     * que el original
     * @note la comparación se realiza atributo a atributo
     */
    bool operator==(const ingrediente& original);
   
    
    /**
 * @brief Sobrecarga del operador de salida. Serialización de un ingrediente para su inserción en un flujo de salida.
 * @param os Flujo de salida
 * @param i Secuencia a serializar
 * @retval El mismo flujo de salida para inserciones consecutivas
 */
friend std::ostream & operator<<(std::ostream & os, const ingrediente & i);




/**
 * @brief Sobrecarga del operador de entrada. Reconstruye un Ingrediente a partir de un serialización
 * @param is Flujo de entrada desde el que se extrae la serialización
 * @param i Diccionario reconstruido
 * @return El mismo flujo de entrada para extracciones consecutivas
 */
friend std::istream & operator>>(std::istream & is, ingrediente & i);

    
    
    
    
    
    
};
    






#endif /* INGREDIENTE_H */
