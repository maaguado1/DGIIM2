#!/bin/bash


echo "N=200"
for i in `seq 1 4`
do
	export OMP_NUM_THREADS=$i
	./pmmparalelo 200
done


echo "N=1500"
for i in `seq 1 4`
do
	export OMP_NUM_THREADS=$i
	./pmmparalelo 1500
done
