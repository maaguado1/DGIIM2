## MÓDULO II: Sesión 1

### Llamadas al sistema para el SA (I)

## 1. E/S de archivos regulares

SINTAXIS:
~~~
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

      int open(const char *pathname, int flags, mode_t mode);
      int close(int fd);
      ssize_t write(int fd, const void *buf, size_t count);
      ssize_t read(int fd, void *buf, size_t count);
      off_t lseek(int fd, off_t offset, int whence);

~~~
Todos los archivos se identifican con un descriptor (entero no negativo). Descriptor 0,1,2: entrada,salida estándar, salida de error(STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO).
La posición de lectura se mide con el offset (inicializado a 0 si no se activa el O_APPEND): se puede cambiar con `lseek`.

FLAGS PRINCIPALES DEL OPEN

| FLAG | Descripción |
| --- | --- |
| O_CREAT | Crea el archivo (regular) |
|O_DIRECTORY | Si no es directorio, falla |
| O_NOATIME | No actualiza el st_atime cuando se lee |
|O_TRUNC |  Trunca el archivo a longitud 0 |
(ver más en `man 2 write`)

## 2. Metadatos de un archivo

### 2.1 Tipos de archivo

- Archivo regular
- Archivo de directorio: contiene nombres de otros archivos y punteros a la info. Los puede leer cualquiera, escribir sólo núcleo.
- Archivo especial de caracteres: representa dispositivos.
- Archivo especial de bloques: discos duros
- FIFO: Comunicación entre procesos.
- Enlace simbólico: apunta a otro
- Socket: Comunicación de red entre procesos

 ### 2.2 Estructura stat

 ~~~
 struct stat {
 dev_t st_dev; /* no de dispositivo (filesystem) */
 dev_t st_rdev; /* no de dispositivo para archivos especiales */
 ino_t st_ino; /* no de inodo */
 mode_t st_mode; /* tipo de archivo y mode (permisos) */
 nlink_t st_nlink; /* número de enlaces duros (hard) */
 uid_t st_uid; /* UID del usuario propietario (owner) */
 gid_t st_gid; /* GID del usuario propietario (owner) */
 off_t st_size; /* tamaño total en bytes para archivos regulares */
 unsigned long st_blksize; /* tamaño bloque E/S para el sistema de archivos*/
 unsigned long st_blocks; /* número de bloques asignados */
 time_t st_atime; /* hora último acceso */
 time_t st_mtime; /* hora última modificación */
 time_t st_ctime; /* hora último cambio */
 };
 ~~~

- **st_blocks** tamaño de fichero (bloques 512 bytes)
- **st_blksize** tamaño de bloque preferido
- **st_atime** modificado por `mknod, utime, read, write, truncate`
- **st_mtime** modificado por `mknod, utime, write`
- **st_ctime** cambiar inodos

- Comprobar tipo de fichero

| Orden | Verdadero si es... |
|---- | --- |
|S_ISLNK| Enlace simbólico |
|S_ISREG | Archivo regular |
| S_ISDIR | Directorio |
| S_ISCHR | Disp. de caracteres |
| S_ISBLK | Disp. de bloques|
|S_ISFIFO | Cauce con nombre |
| S_ISSOCK | Socket |


- Flags para el st_mode:

| Flag | Descripción || Flag | Descripción |
| --- | --- | ---- |----|---|
| S_IFSOCK | socket | | S_IWUSR| user write |
| S_IFLNK| enlace simbólico | | S_IXUSR | user exec |
|S_IFREG | Archivo regular | | S_IRWXG | group read/write/exec |
|S_IFBLK | Disp bloques || S_IRGRP | group read |
| S_IFDIR| Directorio || S_IWGRP | group write |
| S_IFCHR | Disp caracteres ||S_IXGRP| group exec |
| S_IFIFO| Cauce || S_IRWXO | other read/write/exec |
|S_IRWXU | user read/write/exec || S_IROTH | other read|
| S_IRUSR | user read || S_IWOTH | other write |
|S_IXOTH| other exec |












### 2.3 Permisos de acceso a Archivos

COSITAS PARA TENER EN CUENTA:
- Si abrimos un archivo tenemos que tener permisos de ejecucion en cualquier directorio del PATHNAME.
- Permiso de lectura de un directorio: leer el directorio. Permiso de ejecución: pasar a través del directorio (path).
- Permiso de lectura (O_RDONLY/O_RDWR en el open)
- Permiso de escritura (O_WRONLY/O_RDWR)
- Para especificar O_TRUNC necesitaos escritura.
- Para crear/borrar nuevo archivo en directorio: escritura y ejecución.
- El permiso de ejecución en archivos: si queremos ejecutar cualquier llamada `exec`, el archivo debe ser regular.
