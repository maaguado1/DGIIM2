#encoding:utf-8

require_relative 'ShieldToUI'
module Deepspace

   # Class that represents the shield boosters that can be used by space stations
  class ShieldBooster
  
    
  # Class initializer
	# @param _name [String] name of the shield booster
	# @param _boost [Float] percentage of damage absorbed by the shield
	# @param _uses [Integer] how many uses the shield booster has
    
  def initialize(nam, boo, use)
    @name = nam;
    @boost =boo;
    @uses = use;
  end
  
  
  # Copy constructor
  def self.newCopy(nuevo)
    new(nuevo.name, nuevo.boost, nuevo.uses);

  end
  
  
  # Getters
  
 
  def name
    @name
  end
  
  def boost
    @boost;
  end
  
  def uses
    @uses;
  end
  
  
  # Uses shield booster: if the booster is available (uses>0), the boost can be used
  def useIt
    valor = 1.0;
    if @uses > 0
      @uses = @uses - 1;
      valor = @boost;
    end
    valor
  end
  
  
  # String representation
  def to_s
    mensaje = "[ShieldBooster] -> Nombre: #{@name}, Boost: #{@boost}, Uses: #{@uses}"
    return mensaje;
  end
  
  def getUIversion
    return ShieldToUI.new(self)
  end
end

end
