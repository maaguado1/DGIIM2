/**
  * @file ingrediente.cpp
  * @brief Implementación del TDA Ingrediente
  *
  */


#include <assert.h>
#include <iostream>
#include <string>
#include <sstream>
#include <ingrediente.h>

using namespace std;


ingrediente::ingrediente(){
    _nombre=" ";
    _tipo=" ";
    _calorias=0;
    _hidratos=0;
    _proteina=0;
    _grasa=0;
    _fibra=0;
    
}




ingrediente::ingrediente(const ingrediente& original){
    _nombre = original._nombre;
    _tipo = original._tipo;
    _calorias=original._calorias;
    _hidratos=original._hidratos;
    _proteina=original._proteina;
    _grasa=original._grasa;
    _fibra=original._fibra;
    
}
    
ingrediente& ingrediente::operator=   (const ingrediente& original){
   if (this != &original){
    _nombre =original.getNombre();
    _tipo = original.getTipo();
    _calorias=original.getCalorias();
    _hidratos=original.getHc();
    _proteina=original.getProteinas();
    _grasa=original.getGrasas();
    _fibra=original.getFibra();
   }
   return *this;
    
}


bool ingrediente::operator <(const ingrediente& original)const {
    bool resultado=false;
    if ( _tipo.compare(original.getTipo())<0)
        resultado=true;
    else if ( _tipo.compare(original.getTipo())==0)
        ( _nombre<original.getNombre())?(resultado=true ): (resultado=false);
    return resultado;   
}


bool ingrediente::operator >(const ingrediente& original) const{
    bool resultado=false;
    if (_tipo.compare(original.getTipo())>0)
        resultado=true;
    
    else if (_tipo.compare( original.getTipo())==0)
        (_nombre>original.getNombre())?(resultado=true ): (resultado=false);
    return resultado;   
}

bool ingrediente::operator==(const ingrediente& original){
    bool igual =false;
        if (  _tipo.compare( original.getTipo())==0 &&   _nombre.compare(original.getNombre())==0 &&   _calorias==original.getCalorias() &&   _hidratos ==original.getHc() &&   _fibra==original.getFibra() &&   _grasa== original.getGrasas()&&   _proteina== original.getProteinas())
            igual=true;
    
    return igual;
}
        
    
    


std::istream & operator>>(std::istream & is,  ingrediente & i){
    string palabra;
    getline(is, palabra, ';');
    i.setNombre(palabra);
    int num;
    

    getline(is, palabra,  ';');
    num=stoi(palabra);
    i.setCalorias(num);
    
    getline(is, palabra,  ';');
    num=stoi(palabra);
    i.setHidratos(num);
    
    getline(is, palabra,  ';');
    num=stoi(palabra);
    i._proteina=num;
    
    getline(is, palabra,  ';');
    num=stoi(palabra);
    i.setGrasa(num);
    
    
    getline(is, palabra,  ';');
    num=stoi(palabra);
    i.setFibra(num);
    
    getline(is, palabra);
    i.setTipo(palabra);
    
    
    return is;
 }
  
std::ostream & operator<<(std::ostream & os, const ingrediente & i){
    os << i.getNombre() << "; " << i.getCalorias() << "; " << i.getHc() <<"; " << i.getProteinas() << "; " << i.getGrasas() <<"; " << i.getFibra() << "; " << i.getTipo()<< endl;
    if (!os){
        cerr << "ERROR al copiar el ingrediente";
        exit(1);
    }
    return os;         
     
 }
    
    
    
     
    
    


    
    
