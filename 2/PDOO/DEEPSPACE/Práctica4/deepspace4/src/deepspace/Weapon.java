package deepspace;

/*
 * Esta clase representa a las armas de las que 
 * puede disponer una estación espacial para potenciar su energía al disparar.
 */

/**
 *
 * @author maguado
 */
class Weapon implements CombatElement {
    
    private String name;
    private WeaponType type;
    private int uses;
    
    Weapon (String na, WeaponType ty, int us){
        this.name = na;
        this.type = ty;
        this.uses= us;
        
    }
    
    Weapon (Weapon otro){
        this.name = otro.name;
        this.type = otro.type;
        this.uses = otro.uses;
    }
    
    WeaponType getType(){
        return type;
    }
   
    public int getUses(){
         return uses;
     }
    
    public float power(){
         return type.getPower();
     }
    
    public float useIt(){
        float valor;
        if (uses > 0){
            uses -= 1;
            valor = power();
            
        }
        else
            valor = 1.0f;
        return valor;
    }
    

    public String toString(){
        return "\n[WEAPON:]\n name: " + this.name + " type:" + this.type.toString() + " uses: " + this.getUses();
        
    }
    
    public WeaponToUI getUIversion(){
        return new WeaponToUI(this);
    }

}
