#encoding:utf-8
=begin
Para la primera parte del ejercicio he creado una clase que hereda de SpaceCity en la que solo cambia el método fire: obtiene un número aleatorio que indica de qué estación 
se duplicará el disparo. Añade el fire() de dicha estación y lo devuelve.

=end
require_relative "Hangar"
require_relative "Weapon"
require_relative "SpaceStation"
require_relative "SpaceCity"
require_relative "NumericDamage"
require_relative  "FaultySpaceCity"


module Deepspace
  class TestP4
    def self.main     
      
      #Creamos tres estaciones
      station1=SpaceStation.new("Estacion1", SuppliesPackage.new(500,50,500));
      station2=SpaceStation.new("Estacion2", SuppliesPackage.new(500,50,500));
      station3=SpaceStation.new("Estacion3", SuppliesPackage.new(500,50,500));
      
      #Creamos los elementos a añadir
      hangar = Hangar.new(3);
      arma1 = Deepspace::Weapon.new("Pistola", Deepspace::WeaponType::PLASMA, 2)
      arma2 = Deepspace::Weapon.new("Cannon", Deepspace::WeaponType::LASER, 12)
      
      #Nos aseguramos de que tienen Hangar (no podemos asignar el mismo hangar)
      station1.receiveHangar(hangar)
      station2.receiveHangar(Hangar.newCopy(hangar))
      station3.receiveHangar(Hangar.newCopy(hangar))
      
      station1.receiveWeapon(arma1)
      station1.receiveWeapon(arma2)
      

      station2.receiveWeapon(arma1)
      station2.receiveWeapon(arma2)
      
      station3.receiveWeapon(arma1)
      station3.receiveWeapon(arma2)
    
      
      2.times do
        station1.mountWeapon(0)
        station2.mountWeapon(0)
        station3.mountWeapon(0)
      end
      
      ciudadespacial = SpaceCity.new(station1, [station2, station3])
      puts "La ciudad que he creado es: \n\n #{ciudadespacial.to_s} \nDisparando...........DISPARO: #{ciudadespacial.fire}"
      
      
      
      #Probamos la clase nueva
      falla = FaultySpaceCity.new(station1, [station2, station3])
       puts "\n\n\nLa ciudad extra que he creado es: \n\n #{falla.to_s} \nDisparando...........DISPARO: #{falla.fire}"
      
      
      
      
      
      
    end
    
    
  end
  
TestP4.main  
 
end
