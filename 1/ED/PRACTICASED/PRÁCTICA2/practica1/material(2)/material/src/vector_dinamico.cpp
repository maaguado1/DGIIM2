/**
  * @file vector_dinamico.cpp
  * @brief Implementación del TDA VectorDinámico
  *
  */
#include <cassert>
#include <vector_dinamico.h>

/* _________________________________________________________________________ */
template <class T>

Vector_Dinamico<T>::Vector_Dinamico(int num) 
{
  assert(num>=0);
  if (num>0){
    datos= new T[num];
    nelementos= num;
  }
  else{
       nelementos=0;
       datos=0;
  }
     
  
}
/* _________________________________________________________________________ */

template <class T>

Vector_Dinamico<T>::Vector_Dinamico(const Vector_Dinamico<T>& original) 
{
  nelementos= original.nelementos;
  if (nelementos>0) {
    datos= new T[nelementos];
    for (int i=0; i<nelementos;++i)
      datos[i]= original.datos[i];
  }
  else datos=0;
}
/* _________________________________________________________________________ */


template <class T>
int Vector_Dinamico<T>:: Esta(T x) const{
    int n = size();
    int inicio=0, fin=n;
    int pos;
    while (inicio<fin){
        int mitad=(inicio+fin)/2;
        if (datos[mitad]==x){
            pos=-1;
        }
        else{
            if (x> datos[mitad])
                inicio=mitad+1;
            else
                fin=mitad;
            
        }
        pos=inicio;
        return pos; 
    }
    
    
}



template <class T>
Vector_Dinamico<T>::~Vector_Dinamico() 
{
  if (nelementos>0) 
      delete[] datos;
      
  datos=0;
}
/* _________________________________________________________________________ */


template <class T>
int Vector_Dinamico<T>::size() const 
{ 
  return nelementos; 
}
/* _________________________________________________________________________ */


/* _________________________________________________________________________ */

template <class T>
void Vector_Dinamico<T>::resize(int nuevotam) {
    
    int tamanio=nuevotam;
    assert(nuevotam>=0);
    if (nuevotam>0){
        cout << "haciendo resize" ;
        T *aux = new T [tamanio];
        int minimo = (size()<tamanio) ? size():tamanio;
        for (int i=0; i<minimo; i++)
            aux[i] = datos[i];

        nelementos = tamanio;
        delete [] datos;
        datos = aux;
    }
    else{
        nelementos=0;
        delete[] datos;
        datos=0;
    }
}

/* _________________________________________________________________________ */


template <class T>
Vector_Dinamico<T>& Vector_Dinamico<T>::operator= (const Vector_Dinamico<T>& original) 
{
  if (this!= &original) {
    if (nelementos>0) delete[] datos;
    nelementos= original.nelementos;
    datos= new T[nelementos];
    for (int i=0; i<nelementos;++i)
      datos[i]= original.datos[i];
  }
  return *this;
}

