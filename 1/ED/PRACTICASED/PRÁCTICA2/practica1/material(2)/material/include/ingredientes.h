

/* 
 * File:   ingredientes.h
 * Author: maguado
 *
 * Created on 21 de octubre de 2019, 10:55
 */



#include "ingrediente.h"
#include "vector_dinamico.h"

#ifndef INGREDIENTES_H
#define INGREDIENTES_H




/**
 *  @brief T.D.A. Ingredientes
 *
 *
 * Una instancia @e v del tipo de datos abstracto @c Ingredientes sobre el
 * tipo @c Ingrediente es conjunto de datos formado por array de ingredientes.
 *  {v[0],v[1],...,v[n-1]} , y un array de Posiciones, ordenado por tipo donde cada valor
 * posicionesTipo[i] indica la posición dentro de v del ingrediente i según tipos.
 *
 * donde  v[i] es el valor almacenado en la posici�n  i del vector
 *
 * La eficiencia en espacio es @e O(n).
 *
 *
 */


class Ingredientes {
    /**
       * 
       * Un objeto válido @e v del TDA Ingredientes debe cumplir 
       * - @c vector.nelementos>=0
       * - @ posicionesTipo.nelementos>=0
       * 
       * 
       *
    */
    
private:
   Vector_Dinamico <Ingrediente> datos;
   Vector_Dinamico <int> posicionesTipo;
   
   
public:
    
    // ---------------  Constructores ----------------
    /**
     * @brief Constructor por defecto
     * @param n, número de elementos
     * @note
     *   Inicializa todo vacío, en el caso de que no haya parámetro
     */
    Ingredientes();
    
    /**
     * @brief Constructor de copia
     * @param Vector de ingredientes original
     * @note
     *   inicializa el nuevo conjunto de Ingredientes con lo que haya en el vector original
     * 
     */
   
    Ingredientes ( const Ingredientes& original);
    
    
    
    
    /**
     * @brief Destructor
     */
    
    
    ~Ingredientes();
    
    /**
     * @brief Inserta un elemento al vector
     * @param Ingrediente a insertar
     * @note
     *   Busca la posicion correcta por la ordenacion 1 (nombre) y 
     * coloca el índice en la posicion correcta por la ordenacion
     * 2 (tipo). 
     */
    void insertar(const Ingrediente& nuevo);
    
    
    /**
     * @brief Borrar un elemento
     * @param posicion del elemento
     * @note Elimina el ingrediente de una celda, ajustando los índices
     *   
     */
    void borrar (int pos);
    
    
     /**
     * @brief Crea un vector con los tipos de ingredientes
     * @param none
      * @return el vector dinámico de tipos
     * 
     */
    
    Vector_Dinamico<string> getTipos() const;
    
    void mostrarIndices();
    
     /**
     * @brief Consulta todos los ingredientes de un tipo
     * @param el tipo a analizar
     * @return devuelve otro elemento de la clase Ingredientes, restringido el tipo
      * 
     */
    Ingredientes getIngredientesTipo(string tipo);
    
    
     /**
     * @brief Consulta el tamaño del vector
     * @param none
     */
    inline int size() const {return datos.size();}
    
    
    
    
     /**
     * @brief Consulta la información de un ingrediente dado su nombre
     * @param Nombre del ingrediente
     */
    
    Ingrediente getInfo(const string& nombre);
    
    
    /**
     * @brief Consulta la información de un ingrediente dado su posición
     * @param Posicion del ingrediente según el orden de nombre
     */
    
    Ingrediente getIngrediente(const int i);
    
    
    
    /**
     * @brief Operador de consulta []
     * @param Posición a consultar
     * @return Ingrediente en esa posicion
     
     */
    
    Ingrediente  operator[](int i)const ;
    
    
    /**
     * @brief Modifica la posicion i
     * @param i: posición que se modifica. Nuevo: ingrediente a insertar
     * 
     */
    
    void setNuevo(int i, const Ingrediente& nuevo); 
    
      /**
     * @brief Verifica que el ingrediente no se encuentra en la secuencia
     * @param nuevo: ingrediente a verificar
     * 
     */
    
    
    bool estaDentro(const Ingrediente& nuevo);
    
    
       /**
     * @brief Consulta la posicion en el vector de posiciones de una determinada posicion en el de nombres
     * @param Posición a consultar
     * @return Posicion en el vector de posiciones
     * 
     */
    
    int posicionTipo (int i );
    
    
    
    int posicionNombre(string nombre);
    
   
    /**
     * @brief Calcula las estadísticas de un vector de ingredientes
     * @param none
     * @return Un vector de valores, que corresponden a la media de: calorías, hidratos, 
     * proteínas, grasa y fibra.
     * 
     */
    
    Vector_Dinamico<double> getMedias();

    Vector_Dinamico<double> desviaciones();
    
    Vector_Dinamico<int> maximosValores();
    
    Vector_Dinamico<int> minimosValores();
    
     /**
     * @brief Imprime por un flujo de salida el vector de posiciones
     * @param Flujo de salida
     * 
     */
    std::ostream& ImprimirPorTipo(ostream& os);
    
};




/**
 * @brief Serialización de un vector de ingredientes para su inserción en un flujo de salida.
 * @param os Flujo de salida
 * @param i Secuencia a serializar
 * @return El mismo flujo de salida para inserciones consecutivas
 */
std::ostream & operator<<(std::ostream & os, const Ingredientes & i);

/**
 * @brief Reconstruye una serie de Ingredientes a partir de un serialización
 * @param is Flujo de entrada desde el que se extrae la serialización
 * @param i Diccionario reconstruido
 * @return El mismo flujo de entrada para extracciones consecutivas
 */
std::istream & operator>>(std::istream & is, Ingredientes & i);




#endif /* INGREDIENTES_H */

