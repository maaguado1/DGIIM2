#include <assert.h>
#include <iostream>
#include <string.h>
#include "ingredientes.h"
#include "ingrediente.h"
#include <cmath>

using namespace std;

Ingredientes::Ingredientes (){
    datos.resize(0);
    cout << "Reservando memoria" << endl;
    posicionesTipo.resize(0);

}





Ingredientes::Ingredientes ( const Ingredientes& original){
    assert (this!=&original);

    datos.resize(original.size());
    posicionesTipo.resize(original.size());
    for (int i =0; i < original.datos.size(); i ++){
        datos[i]=original.datos[i];
        posicionesTipo[i]=original.posicionesTipo[i];
    }
}



Ingredientes::~Ingredientes(){
    cout << "Borrando... " ;
    
}


void Ingredientes::insertar(const Ingrediente& nuevo){
    bool posencontrada=false;
    int pos1=0, pos2=0;
    if (size()>0){
        if (datos.Esta(nuevo)>=0){
        Ingrediente elemento(nuevo);
        
        
        //BUSCO LA POSICIÓN DEL ELEMENTO EN EL VECTOR DATOS (nombre)
       for (int i =0;  i < size() && !posencontrada ;i ++){
            if (datos[i].getNombre().compare( elemento.getNombre())>0){
                pos2=i;
                posencontrada=true;
            }
        }
        if (!posencontrada)
            pos2=datos.size();


      
        
        posencontrada=false;
        
        //BUSCO EL ÍNDICE DE LA POSICIÓN BUENA SEGÚN TIPO

        
        pos1=datos.size();
        for (int i =0;  i < size() && !posencontrada ;i ++){
            if (datos[posicionesTipo[i]] > elemento){
                pos1=i;
                posencontrada=true;
            }
        }

        for (int i= 0; i < size(); i++){
            if (datos[posicionesTipo[i]].getNombre()>elemento.getNombre)
                posicionesTipo[i]++;
            
        }
        
        datos.resize(size()+1);
        posicionesTipo.resize(size()+1);
        for (int j = datos.size()-1; j >pos2 ; j --)
                    datos[j]= datos[j-1];
                    
        
        datos[pos2]=elemento;
        
        for (int i =size()-1; i > pos1; i --)
            posicionesTipo[i]=posicionesTipo[i-1];

        posicionesTipo[pos1]=pos2;
            
        }
    }
    
    else {
        datos.resize(1);
        posicionesTipo.resize(1);
        datos[0]=nuevo;
        posicionesTipo[0]=0;
    }
        
}
    

void Ingredientes::mostrarIndices(){
     for (int i =0; i < size(); i ++)
        cout << posicionesTipo[i]<< "\n";
    
}

   
void Ingredientes::borrar (int pos){
    cout << "\nBorro la posicion " << pos;
    assert(size()>0);
    if (pos>=0 && pos<size()){
        
        
        int indice=posicionTipo(pos);
        mostrarIndices();
        cout << "\n";
        
        mostrarIndices();
        
        
        for (int i =pos; i <size()-1; i ++)
            datos[i]=datos[i+1];  
        datos.resize(size()-1); 
        
      
        
        mostrarIndices();
          
        
        
        
        for (int i = indice; i < datos.size()-1; i ++)
            posicionesTipo[i]=posicionesTipo[i+1];

     
        posicionesTipo.resize(size()-1);
        
        for (int i= 0; i < size(); i++){
            if (posicionesTipo[i]>pos)
                posicionesTipo[i]--;
            
        }
        cout << "\nHE BOORADO BEIN \n";
    }
    
    
}

    
    
    

    
Vector_Dinamico<string> Ingredientes::getTipos() const{
    
    Vector_Dinamico <string> lista;
    
    if (size()!=0){
        int num=1;
        string tipo=datos[posicionesTipo[0]].getTipo();
        
        
        lista.resize(size());
        lista[0]=tipo;
        
        for (int i =1; i < datos.size(); i ++){
            
            if (datos[posicionesTipo[i]].getTipo().compare(tipo)!=0){

                tipo=datos[posicionesTipo[i]].getTipo();
                lista[num]=tipo;
               
                num++;
            }
        }
        lista.resize(num);
    }
   
    return lista;
}
    
        
     
        
        
        

    

Ingredientes Ingredientes::getIngredientesTipo(string tipo){
    int num=0,posinicio=0;
    bool encontrada=false;
 

    for (int i =0; i < size(); i ++){
 
        if (datos[posicionesTipo[i]].getTipo().compare(tipo)==0){

            num++;
            if (!encontrada){
                posinicio=i;
                encontrada=true;
            }
        }
    }
    Ingredientes nuevo;
    nuevo.datos.resize(num);
    nuevo.posicionesTipo.resize(num);
    for (int i =0; i <num; i ++)
        nuevo.insertar(datos[posicionesTipo[posinicio+i]]);
    return nuevo; 

}
    
 

Ingrediente Ingredientes::getInfo(const string& nombre){
    bool encontrado=false;
    Ingrediente buscado;
    for (int i =0; i < size()&& !encontrado; i ++){
        if (datos[i].getNombre().compare(nombre)==0){
            encontrado=true;
            buscado=datos[i];
        }
        
    }
    return buscado;

}



Ingrediente Ingredientes::operator[](int i)const {
    assert (i>=0);
    assert(i<size());
    return datos[i];
}






Ingrediente Ingredientes::getIngrediente(const int i){
    Ingrediente copia;
    return datos[i];
}






int Ingredientes::posicionNombre(string nombre){
    int indice=0;
    bool encontrada=false;
    for (int i =0; i < datos.size()&&!encontrada; i ++){
            if (datos[i].getNombre().compare(nombre)==0){
                
              indice=i;
              encontrada=true;
            }
            
        }
    return indice;
    
}

int Ingredientes::posicionTipo (int pos ){
    int indice=0;
    bool encontrada=false;
    if (size()!=0){
   
        
        if (pos == size()){
            cout << "EL SIZE ES " << size();
            cout << "es size";
        }
            
        else{
            for (int i =0; i < posicionesTipo.size() &&!encontrada; i ++){
                    if (posicionesTipo[i]==pos){
                        
                      indice=i;
                      encontrada=true;
                    }

                }
        }
    
    }
    cout << "El elemento correspondiente a " << pos << "en indices es " << indice << endl;
    return indice;
    
}



void Ingredientes::setNuevo(int i, const Ingrediente& nuevo){
    string nombre=nuevo.getNombre();
    if (nombre.compare(datos[i-1].getNombre())>0  && nombre.compare(datos[i+1].getNombre())<0 && nuevo >datos[posicionesTipo[(posicionTipo(i)-1)]] && nuevo  <datos[posicionesTipo[(posicionTipo(i)+1)]]){
        datos[i]=nuevo;
    }
    else {
        borrar(i);
        insertar(nuevo);
    }
}
   



std::ostream& Ingredientes::ImprimirPorTipo(std::ostream& os){
    if (size()>0){
    for (int i =0; i < size(); i ++){
        
        os << datos[posicionesTipo[i]]<< endl;
        if (!os){
            cerr<<"ERROR Imprimendo por tipos";
            exit(1);
        }
    }
    }
    
    return os;
    
}

  


Vector_Dinamico<double> Ingredientes::getMedias(){
    Vector_Dinamico<double> medias;
    medias.resize(5);
    int media=0;
    for (int i =0; i< size(); i ++)
        media += datos[i].getCalorias();
    media=media/size();
    medias[0]=media;
    
    media=0;
    for (int i =0; i< size(); i ++)
        media += datos[i].getHidratos();
    media=media/size();
    medias[1]=media;
    
    media=0;
    for (int i =0; i< size(); i ++)
        media += datos[i].getProteina();
    media=media/size();
    medias[2]=media;
    
    
    media=0;
    for (int i =0; i< size(); i ++)
        media += datos[i].getGrasa();
    media=media/size();
    medias[3]=media;
    
    media=0;
    for (int i =0; i< size(); i ++)
        media += datos[i].getFibra();
    media=media/size();
    medias[4]=media;     
          
    return medias;
}




Vector_Dinamico<double> Ingredientes::desviaciones(){
    Vector_Dinamico<double> medias;
    medias=getMedias();
    Vector_Dinamico<double> desviaciones;
    desviaciones.resize(5);
    double desviacion=0;
    for (int i =0; i< size(); i ++)
        desviacion += pow(datos[i].getCalorias(),2);
    desviacion=desviacion/size();
    desviaciones[0]=desviacion;
    
    desviacion=0;
    for (int i =0; i< size(); i ++)
        desviacion += pow(datos[i].getHidratos(),2);
    desviacion=desviacion/size();
    desviaciones[1]=desviacion;
    
    desviacion=0;
    for (int i =0; i< size(); i ++)
        desviacion += pow(datos[i].getProteina(),2);
    desviacion=desviacion/size();
    desviaciones[2]=desviacion;
    
    
    desviacion=0;
    for (int i =0; i< size(); i ++)
        desviacion += pow(datos[i].getGrasa(), 2);
    desviacion=desviacion/size();
    desviaciones[3]=desviacion;
    
    desviacion=0;
    for (int i =0; i< size(); i ++)
        desviacion += pow(datos[i].getFibra(),2);
    desviacion=desviacion/size();
    desviaciones[4]=desviacion;     
          
    for (int i =0; i < 5;i ++){
        desviaciones[i]=sqrt(desviaciones[i]);
    }
    return desviaciones;
}
    
 Vector_Dinamico<int> Ingredientes::maximosValores(){
     
    Vector_Dinamico<int> maximos;
    maximos.resize(5);
    int indice=0;
    int max=datos[0].getCalorias();
    for (int i =0; i < size(); i ++){
       if (datos[i].getCalorias() > max){
           max=datos[i].getCalorias();
           indice=i;
       }
    }
    maximos[0]=indice;
    
    
    
    indice=0;
    max=datos[0].getHidratos();
    for (int i =0; i < size(); i ++){
       if (datos[i].getHidratos() > max){
           max=datos[i].getHidratos();
           indice=i;
       }
    }
    maximos[1]=indice;
    
    
    indice=0;
    max=datos[0].getProteina();
    for (int i =0; i < size(); i ++){
       if (datos[i].getProteina() > max){
           max=datos[i].getProteina();
           indice=i;
       }
    }
    maximos[2]=indice;
    
    
    indice=0;
    max=datos[0].getGrasa();
    for (int i =0; i < size(); i ++){
       if (datos[i].getGrasa() > max){
           max=datos[i].getGrasa();
           indice=i;
       }
    }
    maximos[3]=indice;
    
    
    indice=0;
    max=datos[0].getFibra();
    for (int i =0; i < size(); i ++){
       if (datos[i].getFibra() > max){
           max=datos[i].getFibra();
           indice=i;
       }
    }
    maximos[4]=indice;
    
    

    return maximos;
}
 
   

  Vector_Dinamico<int> Ingredientes::minimosValores(){
     
    Vector_Dinamico<int> minimos;
    minimos.resize(5);
    int indice=0;
    int min=datos[0].getCalorias();
    for (int i =0; i < size(); i ++){
       if (datos[i].getCalorias() < min){
           min=datos[i].getCalorias();
           indice=i;
       }
    }
    minimos[0]=indice;
    
    
    
    indice=0;
    min=datos[0].getHidratos();
    for (int i =0; i < size(); i ++){
       if (datos[i].getHidratos() < min){
           min=datos[i].getHidratos();
           indice=i;
       }
    }
    minimos[1]=indice;
    
    
    indice=0;
    min=datos[0].getProteina();
    for (int i =0; i < size(); i ++){
       if (datos[i].getProteina() < min){
           min=datos[i].getProteina();
           indice=i;
       }
    }
    minimos[2]=indice;
    
    
    indice=0;
    min=datos[0].getGrasa();
    for (int i =0; i < size(); i ++){
       if (datos[i].getGrasa() < min){
           min=datos[i].getGrasa();
           indice=i;
       }
    }
    minimos[3]=indice;
    
    
    indice=0;
    min=datos[0].getFibra();
    for (int i =0; i < size(); i ++){
       if (datos[i].getFibra() < min){
           min=datos[i].getFibra();
           indice=i;
       }
    }
    minimos[4]=indice;
    
    

    return minimos;
}

std::ostream & operator<<(std::ostream & os, const Ingredientes & copia){
    if (copia.size()>0){
        os << "Alimento (100 gramos);Calorias (Kcal.);Hidratos de Carb.;Proteinas;Grasas;Fibra;Tipo" ;
        for (int i =0; i < copia.size(); i ++){
            os << copia[i]<< "\n";
            if (!os){
                cerr<<"ERROR al copiar secuencia" <<endl;
                exit(1);
         
            }
            
        }
        cout << "Secuencia copiada" << endl;
    }
    return os;
            
}


std::istream & operator>>(std::istream & is, Ingredientes & nueva){
    Ingrediente nuevo;
    string linea;
    getline(is, linea);
    while(is){
        is >> nuevo;
        nueva.insertar(nuevo);   
        if (!is){
            cerr<<"Secuencia acabada" <<endl;
        }
    }
  
    return is;

}

 
    
    
    
    
    
