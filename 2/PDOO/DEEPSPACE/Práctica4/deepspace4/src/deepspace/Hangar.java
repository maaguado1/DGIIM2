package deepspace;

 
import java.util.ArrayList;



/**
 *
 * @author maguado
 */



public class Hangar {
    
    
   private ArrayList<ShieldBooster> shieldBoosters;
   private ArrayList<Weapon> weapons;
   
    private int maxElements;
    
    Hangar(int capacity){
        maxElements = capacity;
        shieldBoosters= new ArrayList<>();
        weapons= new ArrayList<>();
    }
    
    Hangar(Hangar h){
        maxElements= h.maxElements;
        weapons = new ArrayList<>();
        shieldBoosters= new ArrayList<>();
        for (Weapon w:h.getWeapons()) {
            weapons.add(w);
        }
        
        for(ShieldBooster s:h.getShieldBoosters()) {
            shieldBoosters.add(s);
        }
    }
    
    public ArrayList<ShieldBooster> getShieldBoosters(){
        return shieldBoosters;
    }
    
    public ArrayList<Weapon> getWeapons(){
        return weapons;
    }
    
    public int getMaxElements(){
        return maxElements;
    }
    
    
    private boolean spaceAvailable(){
       
       return shieldBoosters.size()+weapons.size() < maxElements;
    }
    
    public boolean addWeapon(Weapon w){
        
        if (spaceAvailable()){
            return weapons.add(w);
            
        }
        else 
            return false;
    }
    
    
    public boolean addShieldBooster(ShieldBooster w){
         boolean space = false;
        if (spaceAvailable()){
            shieldBoosters.add(w);
            space = true;
        }
        return space;
    }
    
    
    public Weapon removeWeapon(int w){
        if (w>=0 && w<weapons.size())
            return weapons.remove(w);
        else 
            return  null;
       
    }
    
    public ShieldBooster removeShieldBooster(int s){
        if (s>=0 && s<shieldBoosters.size())
            return shieldBoosters.remove(s);
        else
            return null;
    }
    
    
    HangarToUI getUIversion() {
        return new HangarToUI(this);
    }
    
    
    public String toString(){
        String message = "[Hangar]: \nMaxelements: " + maxElements
                + ", Shields: " + shieldBoosters.toString()
                + ", Weapons: " + weapons.toString() + "\n";
        return message;
    }
    
    
}
