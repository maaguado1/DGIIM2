/* 
 * @file acciones.cpp 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo fuente de la clase @c acciones
 */

 #include <iostream>
 #include <string>
 #include "acciones.h"
 #include <map>

 using namespace std;



acciones::acciones(){
    map <string, unsigned char> uno;
    datos =uno;
}
        
acciones::acciones(const acciones& orig){
    datos=orig.datos;
}


bool acciones::esta (const string& nombre){
    bool presente=false;
    for (acciones::iterator it=begin(); it!= end() && !presente; ++it){
        if ((*it).first == nombre)
            presente = true;
    }
    return presente;
}   

void acciones::insertar(const string & nueva, unsigned char ari){
    datos.insert(make_pair(nueva, ari));

}


void acciones::borrar(const string& nombre){
    datos.erase(nombre);
}

const unsigned char& acciones::operator[](const string& nombre)const{
    map<string,unsigned char>::const_iterator it = datos.find(nombre);
    return (*it).second;
}


unsigned char& acciones::operator[](const string& nombre){
    return datos[nombre];
}


std::ostream & operator<<(std::ostream & os, const acciones & nombres){
    if (nombres.size()>0){
        for (acciones::const_iterator it = nombres.begin(); it != nombres.end(); ++it){
            os << (*it).first << " "<< (*it).second << "\n";
        }
        cout << "Secuencia copiada\n";

    }
    return os;
}

std::istream & operator>>(std::istream & is, acciones & nombres){
    string nombre;
    char aridad;
    is >> nombre;
    is >> aridad;
    while(is && is.peek()!=EOF){
        nombres.insertar(nombre, aridad);
        is >> nombre;
        is >> aridad;
          
        if (!is){
            cerr<<"Secuencia acabada" <<endl;
            break;
        }
    }
    return is;
}

