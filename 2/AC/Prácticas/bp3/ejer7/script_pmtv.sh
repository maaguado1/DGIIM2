#!/bin/bash

#PBS -N pmtv-OpenMP 
#PBS -q ac

export OMP_NUM_THREADS=12
export OMP_SCHEDULE="static"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="static, 1"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="static, 64"
./pmtv-OpenMP 24576


export OMP_NUM_THREADS=12
export OMP_SCHEDULE="dynamic"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="dynamic, 1"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="dynamic, 64"
./pmtv-OpenMP 24576


export OMP_NUM_THREADS=12
export OMP_SCHEDULE="guided"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="guided, 1"
./pmtv-OpenMP 24576
export OMP_SCHEDULE="guided, 64"
./pmtv-OpenMP 24576


