/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deepspace;

/**
 *
 * @author maguado
 */
public class EnemyStarShip implements SpaceFighter {
    private final String name;
    private final float ammoPower;
    private final float shieldPower;
    
    private Loot loot;
    private Damage damage;   
    
    
    
    EnemyStarShip(String n, float a, float s, Loot l, Damage d){
        name =n;
        ammoPower=a;
        shieldPower=s;
        loot=l;
        damage=d;
    }
    
    EnemyStarShip( EnemyStarShip e){
        name = e.getName();
        ammoPower= e.getAmmoPower();
        shieldPower= e.getShieldPower();
        loot = e.getLoot();
        damage= e.getDamage();
                 
    }
    
    public float getAmmoPower(){
        return ammoPower;
    }
    
    public Damage getDamage(){
        return damage;
                
    }
    
    public Loot getLoot(){
        return loot;
    }
    
    public String getName(){
        return name;
    }
    
    public float getShieldPower(){
        return shieldPower;
    }
    
    public float protection(){
        return shieldPower;
    }
    
    public float fire(){
        return ammoPower;
    }
    
    public boolean getsEfficient(){return getLoot().getEfficient();}
    
     public boolean getsCity(){return getLoot().spaceCity();}
     
    public ShotResult receiveShot(float shot){
        ShotResult result;
        if (protection() < shot)
            result= ShotResult.DONOTRESIST;
        else
            result = ShotResult.RESIST;
        return result;
    }
    
    EnemyToUI getUIversion() {
        return new EnemyToUI(this);
    }
    
    
    public String toString(){
        String message = "[EnemyStarship]: Name: " + name
                + ", Protection: " + protection()
                + ", Fire: " + fire()
                + ", Loot: " + loot.toString()
                + ", Damage: " + damage.toString();
        return message;
    }
}
