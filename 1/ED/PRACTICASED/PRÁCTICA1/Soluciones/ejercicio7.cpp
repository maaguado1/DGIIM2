
//Código para multiplicar dos matrices aleatorias bidimensionales

#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

int main(int argc, char *argv[]){

//PRECONDICIONES
if (argc!=2)
	cerr << "Número de argumentos incorrecto. \n";

int N=atoi(argv[1]);

if(N<=0)
	cerr << "N incorrecto. \n";

//GENERAMOS LAS MATRICES ALEATORIAMENTE
int **matriz1;
matriz1=new int* [N];
for(int i=0; i<N; i++)
	matriz1[i] = new int [N];
srand(time(0));
for(int i=0; i<N; i++){
	for(int j=0; j<N; j++)
		matriz1[i][j]=rand();
}

int **matriz2;
matriz2=new int* [N];
for(int i=0; i<N; i++)
	matriz2[i] = new int [N];
srand(time(0));
for(int i=0; i<N; i++){
	for(int j=0; j<N; j++)
		matriz2[i][j]=rand();
}

//CREAMOS UNA MATRIZ PARA ALMACENAR EL RESULTADO
int **resultado;
resultado = new int* [N];
for(int i=0; i<N; i++)
	resultado[i] = new int [N];

//INICIAMOS EL RELOJ Y MULTIPLICAMOS LAS MATRICES
clock_t tini, tfin;
tini = clock();

for (int i=0; i<N; i++){
	for(int j=0; j<N; j++){
		for(int z=0; z<N; z++)
			resultado[i][j] = resultado[i][j] + matriz1[i][z]*matriz2[z][j];
	}
}

tfin = clock();

cout << (tfin-tini)/(double)CLOCKS_PER_SEC << "\n";

//LIBERAMOS LA MEMORIA
for(int i=0; i<N; i++){
	delete[] matriz1[i];
	delete[] matriz2[i];
	delete[] resultado[i];
}
delete[] matriz1;
delete[] matriz2;
delete[] resultado;

}
