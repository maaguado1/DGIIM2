
package deepspace;

import java.util.ArrayList;
import java.util.*;

/**
 * Represents damages done on a spaceship after loosing a combat.
 * They indicate which elements are going to be lost after losing the combat.
 * 
 * A damage is composed by a number of shields to lose and:
 *   - A number of weapons to lose (nWeapons)
 *   - A list of weapons to lose (weapons)
 * 
 * @author maguado
 */
public abstract class Damage {
    
   
    private int nShields;
    
    Damage(int s){
        nShields=s;
    }
    
    public int getNShields(){
        return nShields;
        
    }
     
    public void discardShieldBooster(){
        if (nShields >0)
            nShields --;
        
    }
    
    
    //Ver visibilidad
    int arrayContainsType(ArrayList<Weapon> w,WeaponType t){
        boolean found = false;
        int position=-1;
        int pos =0;
        while(!found && pos<w.size()){
            if (w.get(pos).getType() == t){
                found = true;
                position = pos;
                

            }
            pos++;
        }
        return position;
    }
    
    public abstract Damage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s);
    
    public abstract void discardWeapon(Weapon w);
    
    public abstract boolean hasNoEffect();
    
    abstract DamageToUI getUIversion();

   
    @Override
    public abstract String toString();
    
}   









    
    
    
    
    

