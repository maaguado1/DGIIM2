/* SumaVectoresC.c
 Suma de dos vectores: M3 = M1 + M2

 Para compilar usar (-lrt: real time library, es posible que no sea necesario usar -lrt):
	gcc  -O2  SumaVectores.c -o SumaVectores -lrt
	gcc  -O2 -S SumaVectores.c -lrt

 Para ejecutar use: SumaVectoresC longitud

*/

#include <stdlib.h>	// biblioteca con funciones atoi(), malloc() y free()
#include <stdio.h>	// biblioteca donde se encuentra la función printf()
#include <time.h>	// biblioteca donde se encuentra la función clock_gettime()

#define GLOBAL_MATRIX // Usamos variales globales

#ifdef GLOBAL_MATRIX
#define MAX 10000	//=2^25

double M1[MAX][MAX], M2[MAX][MAX], M3[MAX][MAX];
#endif





int main(int argc, char** argv){

	int i,j,k;

	struct timespec cgt1,cgt2; double ncgt; //para tiempo de ejecución

  //Leer argumento de entrada (nº de componentes del vector)
  if (argc<2){
    printf("Faltan nº componentes del vector\n");
    exit(-1);
  }

  unsigned int N = atoi(argv[1]);	// Máximo N =2^32-1=4294967295 (sizeof(unsigned int) = 4 B)


  #ifdef GLOBAL_MATRIX
  if (N>MAX) N=MAX;
  #endif
	//Inicializar vectores
	for (i=0; i < N; i ++){


		for ( j=0; j < N; j++){
				M1[i][j] = j-i +1;
				M2[i][j] = j-i +2;
      	M3[i][j] = 0;
			}

	  }

  clock_gettime(CLOCK_REALTIME,&cgt1);
  //Calcular suma de vectores
	for ( i=0; i < N; i++){

		for ( k=0; k < N; k++){

		    for ( j =0; j < N; j ++){
          M3[i][j]+= M1[i][k] * M2[k][j];
        }
			}
		}

  clock_gettime(CLOCK_REALTIME,&cgt2);
  ncgt=(double) (cgt2.tv_sec-cgt1.tv_sec)+
       (double) ((cgt2.tv_nsec-cgt1.tv_nsec)/(1.e+9));

  //Imprimir resultado de la suma y el tiempo de ejecución
	if (N <= 11){
		printf("Tiempo:%11.9f\n",ncgt);
		for (int i = 0; i < N; i++)
	    {
	    	printf("\n| ");
	      for (int j = 0; j< N; j ++){
	          printf(" %f " ,M1[i][j]);
	  }
	    printf(" | ");
	  }
	  printf("\n\n ");


		for (int i = 0; i < N; i++)
	    {
	    	printf("\n| ");
	      for (int j = 0; j< N; j ++){
	          printf(" %f " ,M2[i][j]);
	  }
	    printf(" | ");
	  }
	  printf("\n\n ");


    printf ("\nResultado: ");
		for (int i = 0; i < N; i++)
	    {
	    	printf("\n| ");
	      for (int j = 0; j< N; j ++){
	          printf(" %f " ,M3[i][j]);
	  }
	    printf(" | ");
	  }
	  printf("\n\n ");



	}
	else {
		printf("%11.9f", ncgt);}
  return 0;
}
