#!/bin/bash
##Script de ejecución múltiple
##Formato de ejecución : ./script.sh nombredelprograma
##La variable i representa el número de entradas para el programa y la variable j representa el número de veces que se va a ejecutar con el mismo i
##Para obtener salida.txt con valores adecuados para gnuplot asegúrense de que el programa solo saque como salida los segundos utilizados para la ejecucion
##Se recomienda usar i<40 para el programa de Hanoi e i<3000 para el programa de Floys


programa=$1
echo "" >> $1.txt
for (( i=1000; i<25000; i+=1000 ))
	do
	n=0
	for (( j=0; j<25; j++ ))
		do
		
		a=$(./$1 $i | awk '{printf("%.15f\n", $1)}')

		n=`echo $n + $a | bc -l`

		done;
	n=`echo $n / 25 | bc -l`

	printf "$i	$n\n" >> $1.txt
	done;
echo "done"
