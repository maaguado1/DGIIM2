/**
 * @file receta.h 
 * @author María Aguado y Amparo Almohalla
 * @brief Archivo cabecera de la clase @c receta
 */


#include <iostream>
#include <string>
#include "instrucciones.h"
#include <list>


using namespace std;

#ifndef RECETA_H
#define RECETA_H

/**
 * @brief T.D.A. Receta
 * Una instancia @c Receta es un conjunto de ingredientes (datos del tipo Ingrediente) que se organizan de la siguiente manera:
 * -string code: identifica a cada receta mediante un código de tipo string único
 * -unsigned int: indica el orden en el que se sirviría en un menú (primer plato, segundo plato...)
 * -string nombre: nombre del plato que se obtiene al seguir los pasos de la receta
 * -list<pair<string, unsigned int> > ings: lista de ingredientes que se necesitan para la recetea
 *      Cada elemento de la lista es una pareja que índica el ingrediente y la cantidad que se necesita de él
 * -float calorias: total de calorías que tiene la receta (en base a las calorías de cada ingrediente)
 * -float hc: total de hidratos de carbono que tiene la receta
 * -float grasas: total de grasa que tiene la receta
 * -float proteinas: total de proteínas que aporta la receta
 * -float fibra: total de fibra que aporta la receta
*/
class receta{

    /**
     * Un objeto válido @e de receta debe cumplir:
     * -string code != ""
     * -número de ingredientes>0
    */
    private:

        /**
         * @brief Código único que identifica y representa al objeto de tipo receta que tiene asociado
        */
        string code;
        /**
         * @brief Orden que ocuparía la receta en un menú: primer plato, segundo plato...
        */
        unsigned int plato;
        /**
         * @brief Nombre del plato que se obtiene tras llevar a cabo la receta
        */
        string nombre;
        /**
         * @brief lista de ingredientes necesarios para la receta
         * Cada elemento de la lista es una pareja formada por un objeto del tipo Ingrediente y una cantidad del ingrediente
        */
        list<pair<string, unsigned int> > ings;
        /**
         * @brief Valores nutricionales de la receta
         * Se basa en los valores de cada uno de los ingredientes y en la cantidad de cada ingrediente
         * que se necesita para la receta
        */
        float calorias, hc, grasas, proteinas, fibra;
        instrucciones inst;

    public:

        /**
         * @brief Constructor por defecto
         * Inicializa todo al vacío
        */
        receta();
        
         /**
         * @brief Constructor de copia
         * @param const receta& original, receta original a copiar
         * @note
         *   Crea otra receta que es igual que la pasada por parámetro
         * 
         */
        receta (const receta& orig);

        
        inline void insertar(pair<string, unsigned int> par) {ings.push_back(par);};
        
        /**
         * @brief Número de ingredientes de la receta
         * @return el número de ingredientes que se necesitan para la receta
        */
        inline int ningredientes() const{return ings.size();}


        /**
         * @brief Código de la receta
         * @return el código que identifica a la receta apuntada por this
        */
        inline string getCode()const {return code;}


        /**
         * @brief Plato de la receta
         * @return Lugar que ocuparía en un menú la receta: primer plato, segundo plato...
        */
        inline unsigned int getPlato()const{return plato;}


        /**
         * @brief Nombre de la receta
         * @return Nombre del plato que se obtiene tras llevar a cabo la receta
        */
        inline string getNombre()const {return nombre;}


        /**
         * @brief Ingredientes y cantidad de cada ingrediente necesarios para la receta
         * @return Lista de parejas ingrediente-cantidad necesarios para la receta
        */
        inline list<pair<string, unsigned int> > getIngredientes()const{return ings;}


        /**
         * @brief Calorías en total de la receta
         * @return Número de calorías que aporta la receta
        */
        inline float getCalorias()const {return calorias;}


        /**
         * @brief Hidratos de carbono en total de la receta
         * @return Número de hidratos de carbono que aporta la receta
        */
        inline float getHc()const {return hc;}


        /**
         * @brief Grasas en total de la receta
         * @return Número de grasas que aporta la receta
        */
        inline float getGrasas()const{return grasas;}


        /**
         * @brief Proteínas en total de la receta
         * @return Número de proteínas que aporta la receta
        */
        inline float getProteinas()const{return proteinas;}


        /**
         * @brief Fibra
         * 
         * 
         * 
         *  en total de la receta
         * @return Cantidad de fibra que aporta la receta
        */
        inline float getFibra()const {return fibra;}


         /**
         * @brief Consulta de los pasos a seguir para esta receta 
         * @return Pasos a seguir
        */
        inline instrucciones getInstrucciones()const {return inst;}

        /**
         * @brief Establece el código de la receta apuntada por this
         * @param codigo código a adjudicarle a la receta
        */
        inline void setCode(string codigo){code=codigo;};


         /**
         * @brief Establece el plato de la receta apuntada por this
         * @param num plato a adjudicarle a la receta
        */
        inline void setPlato(int num){plato=num;};


         /**
         * @brief Establece el nombre de la receta apuntada por this
         * @param nom nombre a adjudicarle a la receta
        */
        inline void setNombre(string nom){nombre=nom;};


         /**
         * @brief Establece las calorías de la receta apuntada por this
         * @param cal calorías a adjudicarle a la receta
        */
        inline void setCalorias(float cal){calorias=cal;};


         /**
         * @brief Establece los hidratos de carbono de la receta apuntada por this
         * @param hc hidratos de carbono a adjudicarle a la receta
        */
        inline void setHc(float hidratos){hc=hidratos;};


         /**
         * @brief Establece las grasas de la receta apuntada por this
         * @param grasa grasa a adjudicarle a la receta
        */
        inline void setGrasas(float grasa){grasas=grasa;};

        
         /**
         * @brief Establece las proteínas de la receta apuntada por this
         * @param proteina proteínas a adjudicarle a la receta
        */
        inline void setProteinas(float proteina){proteinas=proteina;};


         /**
         * @brief Establece la fibra de la receta apuntada por this
         * @param fib fibra a adjudicarle a la receta
        */
        inline void setFibra(int fib){fibra=fib;};


         /**
         * @brief Establece la lista de ingredientes-cantidad de la receta apuntada por this
         * @param ingredientes ingredientes a adjudicarle a la receta
        */
        inline void setIngredientes(list<pair<string,unsigned int> > ingredientes){ings=ingredientes;};
        
        
        /**
         * @brief Añade el conjunto de instrucciones de la receta.
         * @param const instrucciones& ins: instrucciones a añadir a la receta
         *
         * */
        inline void setInstrucciones(const instrucciones& ins){ inst = ins;};


        /**
         * @brief Actualiza el valor nutricional de la
         * @param nueva: receta a insertar en el conjunto
        */
        void setValores(const ingredientes& valores);

        /**
         * @brief Operador de asignación de receta
         * @param nueva: receta a copiar
        */
        receta& operator=(const receta& rec);

        /**
         * @brief Compara cada uno de los valores de la receta apuntada por this y de la pasada como parámetro
         * @param const receta& orig: receta a comparar con la apuntada por this
         * @return true si son iguales, false si no lo son
        */
        bool operator==(const receta& orig);



        /**
         * @brief Operador de salida para el TDA receta
         * @param ostream & os: flujo de salida
         * @param const receta& i: receta a imprimir
        */
        friend std::ostream & operator<<(std::ostream & os, const receta & i);



        /**
         * @brief Operador de entrada para el TDA receta
         * @param istream & is: flujo de entrada
         * @param receta & i: receta a introducir
        */
        friend std::istream & operator>>(std::istream & is, receta & i);

    
    
        /**
         * @brief Calcula la razón de cada uno de los valores de la receta
         * Es decir, calcula la razón de proteínas de la receta, calorías... y así con cada uno de los valores nutricionales
        */
        float razon();


        /**
         * @brief Fusiona la receta pasada como parámetro con el objeto apuntado por this
         * @param rec: receta a fusionar con la apuntada por this
         * @return una nueva receta que es la fusión de ambas
         */
        receta fusionar(const receta& rec);








        /**
         * @brief Especificación e implementación del tipo iterador de la clase receta
         * Define un iterador no constante que recorre los ingredientes del TDA receta
        */
        class iterator{
            private:
                list<pair<string, unsigned int> >::iterator it;

            public:
                iterator():it(0){};
                iterator(const receta::iterator &i):it(i.it){};
                ~iterator(){};

                inline receta::iterator & operator=(const iterator& orig){this->it=orig.it; return *this;};
                inline pair<string, unsigned int>& operator*()const {return *(it);};
                
                inline receta::iterator& operator++(){ it++; return *this;};
                inline receta::iterator& operator--(){it--; return *this;};
                inline  bool operator==(receta::iterator &i)const{return this->it==i.it;};
                inline bool operator!=(const receta::iterator &i) const {return (it != i.it);};
                friend class const_iterator;
                friend class receta;

        };
        /**
         * @brief Implementación y especificación de la clase const_iterator a la clase receta
         * Define un iterador de tipo constante que recorre los ingredientes de una receta
        */
        class const_iterator{
            private:
                list<pair<string, unsigned int> >::const_iterator it;
            public:

                const_iterator():it(0){};
                const_iterator(const receta::const_iterator &i):it(i.it){};
                
                inline receta::const_iterator& operator=(const receta::const_iterator& orig){it=orig.it; return *this;};
                inline receta::const_iterator & operator++(){it++; return *this;};
                inline receta::const_iterator & operator--(){ it--; return *this;};
                inline const pair<string, unsigned int>& operator*()const {return *(this->it);};
                inline bool operator==(const receta::const_iterator &i)const {return this->it==i.it;}
                inline bool operator!=(const receta::const_iterator &i)const {return (it!=i.it);};
                friend class receta;    
        };
        /**
         * @brief Iterador begin() del TDA receta
         * @return El primer elemento de la lista de ingredientes de la receta
        */
        inline iterator begin(){receta::iterator i; i.it=ings.begin(); return i;};
        /**
         * @brief Iterador end() del TDA receta
         * @return El último elemento de la lista de ingredientes de la receta
        */
        inline iterator end(){receta::iterator i; i.it=ings.end(); return i;};
        /**
         * @brief Iterador constante begin() del TDA receta
         * @return Un iterador constante que apunta al primer elemento de la lista de ingredientes de la receta
        */
        inline const_iterator begin() const{receta::const_iterator i; i.it=ings.begin(); return i;};
        /**
         * @brief Iterador constante end() del TDA receta
         * @return Un iterador constante que apunta al último elemento de la lista de ingredientes de la receta
        */
        inline const_iterator end() const{receta::const_iterator i; i.it=ings.end(); return i;};

        /**
         * @brief Busca la receta cuyo código coincide con el string pasado como parámetro, para determinar si está o no en nuestro conjunto
         * @param nuevo: código de la receta que queremos comprobar si está en nuestro conjunto
         * @return un interador apuntando a la receta si la hemos encontrado, un iterador a end() si la receta no se encuentra en el conjunto
        */
        iterator find(const string nuevo);

        /**
         * @brief Busca la receta cuyo código coincide con el string pasado como parámetro, para determinar si está o no en nuestro conjunto
         * @param nuevo: código de la receta que queremos comprobar si está en nuestro conjunto
         * @return un iterador constante apuntando a la receta si la hemos encontrado, un iterador a end() si la receta no se encuentra en el conjunto
        */
        const_iterator find(const string nuevo)const;
};

#endif /*RECETA.H*/