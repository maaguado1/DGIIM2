
#encoding:utf-8

require_relative 'SuppliesPackage'
require_relative 'Damage'
require_relative 'Weapon'
require_relative 'WeaponType'
require_relative 'Loot'
require_relative 'ShieldBooster'
require_relative 'Hangar'
require_relative 'CardDealer'
require_relative 'ShotResult'
require_relative 'SpaceStationToUI'
require_relative 'Transformation'



module Deepspace
 
# Class to represent a space station a player can have
#
# @author María Aguado Martínez

class SpaceStation
  
  public_class_method :new
  # Class atributes
	# ==========================================================================

  
  # @!attribute [Integer] Maximum fuel quantity that a space station can have
  @@MAXFUEL=100.0
  
  
  # @!attribute [Float] Shield units lost per each shot unit taken
  @@SHIELDLOSSPERUNITSHOT = 0.1
  
  
  # Constructors
	# ==========================================================================
	
  # Class initializer
	# @param _name [String] name of the space station
	# @param _supplies [SuppliesPackage] starting fuel units, weapons and shields
  def initialize(n,s)
    @name = n
    @nMedals = 0
    
    @ammoPower = s.ammoPower
    @fuelUnits = s.fuelUnits
    @shieldPower = s.shieldPower
    
    @weapons = []
    @shieldBoosters = []
    @hangar = nil
    @pendingDamage = nil
    
  end
  
  
  # Class copy constructor
	# @param other [SpaceSpation] instance of which the copy is made
  def newCopy(other)
    @name = other.name
		@ammoPower = other.ammoPower
		@shieldPower = other.shieldPower
		@fuelUnits = other.fuelUnits
		@nMedals = other.nMedals

		if !other.hangar.nil?
			@hangar = Hangar.newCopy(other.hangar)
		else
			@hangar = nil
		end

		if !other.pendingDamage.nil?
			@pendingDamage = Damage.newCopy(other.pendingDamage)
		else
			@pendingDamage = nil 
		end

		@shieldBoosters = []
		for s in other.shieldBoosters
			@shieldBoosters << ShieldBooster.newCopy(s)
		end

		@weapons = []
		for w in other.weapons
			@weapons << Weapon.newCopy(w)
		end
    
  end
  
  
  
  # Getters
	# ==========================================================================
  
  
 attr_reader :ammoPower, :fuelUnits, :hangar, :name, :nMedals,
			:pendingDamage, :shieldBoosters, :shieldPower, :weapons
 
  
  
  # Gets the speed of the space station.
	# Speed is calculated as fraction of fuel units and max fuel possible
	# @param [Float] percentage of speed, that's to say, a number in [0, 1]
  def speed
    return (@fuelUnits/@@MAXFUEL)
  end
  
  
  
  # Checks the state of the space ship.
	# Valid state means no pending damage or pending damage with no effect
	# @return [Boolean] true, if the space ship is on a valid state;
	#                   false, otherwise
  def validState
    if @pendingDamage== nil  or @pendingDamage.hasNoEffect
      return true
    else
      return false
    end
  end
  
  
  
  
  # Setters
	# ==========================================================================

  
  # If pending damage has no effect, fixes the atribute to nil
  def cleanPendingDamage
		if !@pendingDamage.nil?
			if @pendingDamage.hasNoEffect
				@pendingDamage = nil
			end
		end
	end
  
  
  # Adjusts certain damage to some weapon and shieldBoosters lists, and its
	# value is stored in the object
	# @param d [Damage] the damage to be set
  def setPendingDamage(d)
    danio= d.adjust(@weapons, @shieldBoosters)
    @pendingDamage = danio
  end
  
  def assignFuelValue(f)
      if f<@@MAXFUEL
        @fuelUnits=f
      else
        @fuelUnits=@@MAXFUEL
      end
    end
  
  
 # Deletes all mounted weapons and mounted shields with no uses left
  def cleanUpMountedItems
    
    @shieldBoosters.delete_if{|x| x.uses == 0}
    @weapons.delete_if{|x| x.uses == 0}
  end
  
  
  
  # Discards current hangar (nil reference)
  def discardHangar
    @hangar=nil
  end
  
  
  # Discards shield in certain position from the collection of weapons in use
	# @param i [Integer] index where the shield that wants to be discarded is located
  def discardShieldBooster(i)
    size = @shieldBoosters.length
    if (i>=0 and i<size)
      s = shieldBoosters.delete_at(i)
      if (pendingDamage != nil)
        pendingDamage.discardShieldBooster()
        cleanPendingDamage
      end
    end
  end
  
  
  
  # If a hangar is available, discards a shield booster from it, in a certain
	# position
	# @param i [Integer] index where the booster that wants to be discarded is
	#                    located in the hangars
  def discardShieldBoosterInHangar(i)
    if (@hangar != nil)
      @hangar.removeShieldBooster(i)
    end
  end
  
  
  # Discards weapon in certain position from the collection of weapons in use
	# @param i [Integer] index where the weapon that wants to be discarded is located
  def discardWeapon(i)
    size = @weapons.length
    if (i>=0 and i<size)
      w = weapons.delete_at(i)
      if (pendingDamage != nil)
        pendingDamage.discardWeapon(w)
        cleanPendingDamage
      end
    end
  end
 
  
  
	# If a hangar is available, discards a weapon from it, in a certain
	# position
	# @param i [Integer] index where the weapon that wants to be discarded is
	#                    located in the hangar 
  def discardWeaponInHangar(i)
    if (@hangar != nil)
      @hangar.removeWeapon(i)
    end
  end
  
  
  # Tries to add a weapon to the hangar
	# @param [Weapon] the weapon to add
	# @return [Boolean] true, if weapon is successfully added;
	#                   false, if the operation fails
  def receiveWeapon(w)
    if @hangar != nil
      if @hangar.addWeapon(w)
        return true
      else
        return false
      end
    else
      return false
    end
  end
  
  
  # Tries to add a hangar.
	# If there's already a hangar, this method has no effect
	# @param h [Hangar] the hangar to add
  def receiveHangar(h)
    if (@hangar==nil)
      @hangar=h

    end
  end
  
  
  # Tries to add a shield booster to the hangar
	# @param [ShieldBooster] the shield booster to add
	# @return [Boolean] true, if booster is successfully added;
	#                   false, if the operation fails
  def receiveShieldBooster(s)
    if (@hangar != nil)
      return hangar.addShieldBooster(s)
    else
      return false
    end
    
    
  end
  
  
  
  
	# Shot, shield and fuel power increase by a certain supplies package
	# @param s [SuppliesPackage] the supplies to add
  def receiveSupplies(s)
    assignFuelValue(s.fuelUnits)
    @ammoPower += s.ammoPower
    @shieldPower += s.shieldPower
  end
  
  
  
  # Make the operations related to the reception of an enemy's impact
	# @param shot [Float] enemy's impact shot power
	# @return [Boolean] true, if the shield resisted the impact; else, otherwise
  def receiveShot(s)
    myProtection = protection
    if (myProtection >= s)
      @shieldPower -= @@SHIELDLOSSPERUNITSHOT*s
      @shieldPower = (0.0 > @shieldPower)?0.0:@shieldPower
      return ShotResult::RESIST
    else
      @shieldPower =0.0
      return ShotResult::DONOTRESIST
    end
    
    
  end
 
  
  
  # Makes a shot
	# @return [Float] the shot power
  def fire
    factor=1
    
    for w in @weapons == nil ? [] : @weapons
      factor *= w.useIt()
    end
    return ammoPower*factor;
  end
  
  
  # The spaceships moves. Therefore, fuel units decrease
  def move
    if (@fuelUnits-speed >=0)
      @fuelUnits=@fuelUnits-speed
    end
  end
  
  # Use protection shield
	# @return [Float] the shield's energy
  def protection
    factor=1
    for h in @shieldBoosters
      factor *= h.useIt
    end
    return shieldPower*factor
    
  end
 
  
  # A shield booster from the hangar is mounted to be used.
	# If method runs successfully, booster is erased from Hangar, and the weapon
	# is added to the collection of boosters in use
	# @param i [Integer] index of the booster to mount
  def mountShieldBooster(i)
    if (@hangar!=nil)
      shield = @hangar.removeShieldBooster(i)
      if ( shield != nil)
        @shieldBoosters.push(shield)
      end
    end
  end
  
  
  # A weapon from the hangar is mounted to be used.
	# If method runs successfully, weapon is erased from Hangar, and the weapon
	# is added to the collection of weapons in use
	# @param i [Integer] index of the weapon to mount
  def mountWeapon(i)
    if (@hangar!=nil)
      weapon = @hangar.removeWeapon(i)
      if weapon != nil
        @weapons << weapon
        
      end
    end
  end
  
  
 
  
  
  # Receives a loot
	# @param [Loot] loot to be received
  def setLoot(l)
    dealer = CardDealer.instance
    h = l.nHangars
    if h>0
      hangar = dealer.nextHangar
      receiveHangar(hangar)
    end
    
    elements = l.nSupplies

    for i in 1..elements
      sup = dealer.nextSuppliesPackage
      receiveSupplies(sup)
    end
    
    elements = l.nWeapons
    for i in 1..elements
      weap = dealer.nextWeapon
      receiveWeapon(weap)
    end
    
    elements = l.nShields
    for i in 1..elements
      sh = dealer.nextShieldBooster
      receiveShieldBooster(sh)
    end
    
    medals = l.nMedals
    @nMedals += medals
    
    if (l.spaceCity)

      return Deepspace::Transformation::SPACECITY
   
    elsif (l.efficient)

        return Deepspace::Transformation::GETEFFICIENT
    else

        return Deepspace::Transformation::NOTRANSFORM
  end
    
  end
  
  
  
  
  # String representation, UI version
	# ==========================================================================

  
  def to_s
    out="Space Station - Name: #{name}\n"
    out+="\tnMedals: #{@nMedals}, Fuel units: #{@fuelUnits.round(2)}, Power: #{@ammoPower}, Shields: #{@shieldPower}\n"
    out+="\tWeapons: [#{@weapons.join(', ')}]\n"
    out+="\tShieldBooster: [#{@shieldBoosters.join(', ')}]\n"
    out+="\tHangars: #{@hangar}\n"
    out+="\tPendingDamage: #{@pendingDamage}\n" 
    out+="------- end of Space Station >> #{@name} << -------\n\n\n"
    return out
  end
  
  def getUIversion
    return SpaceStationToUI.new(self)
  end
  
end


end

