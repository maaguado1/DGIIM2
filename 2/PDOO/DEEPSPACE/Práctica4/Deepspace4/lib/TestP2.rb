#!/usr/bin/env ruby
#enconding:utf-8


require_relative "Damage"
require_relative "Hangar"
require_relative "Weapon"
require_relative "GameUniverse"
require_relative "EnemyStarShip"
require_relative "SpaceStation"
require_relative "ShotResult"


class TestP2
  def self.main
    
    puts "\nPROBANDO LAS CLASES NUEVAS IMPLEMENTADAS"
    
    puts "\n1.PRUEBA DE: HANGAR"
    hangar1 = Deepspace::Hangar.new(5)
    arma1 = Deepspace::Weapon.new("Pistola", Deepspace::WeaponType::PLASMA, 2)
    arma2 = Deepspace::Weapon.new("Cannon", Deepspace::WeaponType::LASER, 0)
    arma3 = Deepspace::Weapon.new("Misil", Deepspace::WeaponType::PLASMA, 5)
    
 
    potenciador1=Deepspace::ShieldBooster.new("Pinchos",4,1)
    potenciador2=Deepspace::ShieldBooster.new("Recubrimiento",5,5)
    potenciador3=Deepspace::ShieldBooster.new("Hierro",2,4)
    
    puts "\nSALIDA ARMA: #{arma1.to_s}"
    
    
    hangar1.addWeapon(arma1)
    hangar1.addShieldBooster(potenciador1)
    hangar1.addWeapon(arma2)
    hangar1.addShieldBooster(potenciador2)
    hangar1.addWeapon(arma3)
    hangar1.addShieldBooster(potenciador3)
    
    puts "\nSALIDA HANGAR  #{hangar1.to_s}, \nVoy a crear hangar2"
    
    hangar2=Deepspace::Hangar.newCopy(hangar1)
    hangar2.removeShieldBooster(1)
    hangar2.removeWeapon(2)
    
    puts "\nSALIDA HANGAR2 #{hangar2.to_s}"
   
    puts "\n\nPrueba de Damage\n"
    damage1=Deepspace::Damage.newNumericWeapons(5,4)#0 armas y 4 escudos
    wl=[Deepspace::WeaponType::PLASMA,Deepspace::WeaponType::MISSILE,Deepspace::WeaponType::PLASMA]
    damage2=Deepspace::Damage.newSpecificWeapons(wl,2)#Vector de tipos y 2 escudos

    puts damage1.to_s
    puts damage2.to_s

    damage3=damage1.adjust(hangar1.weapons, hangar1.shieldBoosters)
    puts "\ndamage1 ajustado:\n"
    puts damage3.to_s
    damage3=damage2.adjust(hangar1.weapons, hangar1.shieldBoosters)
    puts "\ndamage2 ajustado:\n"
    puts damage3.to_s
      
    damage1.discardWeapon(arma1)
   
    damage1.discardShieldBooster
    
    
    puts damage1.to_s
    puts damage2.to_s
      
    puts"\nPrueba de hasNoEffect:"
    damagenull=Deepspace::Damage.newSpecificWeapons([],0)
    if(damagenull.hasNoEffect)
      puts"Funciona"
    end
    damagenull=Deepspace::Damage.newNumericWeapons(0,0)
    if(damagenull.hasNoEffect)
       puts"Tambien Funciona"
    end
    if(!damage1.hasNoEffect)
      puts"Sigue funcionando"
    end
    if(!damage2.hasNoEffect)
      puts"Definitivamente funciona"
    end

    puts "\n\nPrueba de EnemyStarShip"

    loot=Deepspace::Loot.new(3,2,4,2,7) #3 suplementos, 2 armas, 4 escudos, 2 hangares, 7 medallas
    enemyStarShip=Deepspace::EnemyStarShip.new("HalconMilenario",30,25,loot,damage2)
    
    puts enemyStarShip.to_s

    puts"Prueba de fire: #{enemyStarShip.fire}, Prueba de protection: #{enemyStarShip.protection}"
    

    puts"\n\nPrueba de SpaceStation"
    suppliesPackage=Deepspace::SuppliesPackage.new(30,150,25)
    suppliesPackage1=Deepspace::SuppliesPackage.new(2,20,17)
    suppliesPackage2=Deepspace::SuppliesPackage.new(3,3,3)

    spaceStation=Deepspace::SpaceStation.new("Skylab", suppliesPackage)
    puts spaceStation.to_s
    spaceStation.setPendingDamage(damage1)
    puts hangar1.to_s
    spaceStation.receiveHangar(hangar1)
    
    puts spaceStation.to_s
    spaceStation.move
    spaceStation.receiveSupplies(suppliesPackage1)
    spaceStation.move
    
    puts spaceStation.to_s
    if(spaceStation.validState)
      puts"\nLa estacion esta bien se ha acabado los movimientos \n\n\n"
    end

    arma4=Deepspace::Weapon.new("Bazooka", Deepspace::WeaponType::MISSILE, 5)
    potenciador3=Deepspace::ShieldBooster.new("CapaExtra",5,7)
    if spaceStation.receiveWeapon(arma4)
      puts"Algo falla"
    end
    

    
    spaceStation.mountWeapon(0)
    spaceStation.mountWeapon(0)
    spaceStation.mountWeapon(0)
    spaceStation.mountShieldBooster(0)
    spaceStation.mountShieldBooster(0)
    
    
    puts"\n\nDespues de montarlo todo:\n"
    puts spaceStation.to_s
    if(spaceStation.receiveShieldBooster(potenciador3) && spaceStation.receiveWeapon(arma4))
      puts"\n\nDespues de anadir un potenciador y un arma\n"
      puts spaceStation.to_s
      spaceStation.discardWeaponInHangar(0)
      spaceStation.discardShieldBoosterInHangar(0)
      puts"\n\nDespues de unos descartes en el hangar:\n"
      puts spaceStation.to_s
    end

    spaceStation.weapons.at(0).useIt
    spaceStation.weapons.at(0).useIt
    spaceStation.shieldBoosters.at(0).useIt
    
    puts spaceStation.to_s
    spaceStation.cleanUpMountedItems
    
    
    puts"\n\nDespues de unos usos y una limpieza:"

    spaceStation.discardHangar

    puts spaceStation.to_s

   
    puts "AmmoPower: #{spaceStation.ammoPower}"
   
    
    puts spaceStation.to_s
    spaceStation.mountShieldBooster(0)
    spaceStation.mountWeapon(0)
    spaceStation.setPendingDamage(Deepspace::Damage.newSpecificWeapons([Deepspace::WeaponType::PLASMA, Deepspace::WeaponType::LASER, Deepspace::WeaponType::MISSILE], 3))
    puts spaceStation.to_s
    spaceStation.discardShieldBooster(0)
    spaceStation.discardWeapon(0)
    puts spaceStation.to_s
    puts spaceStation.receiveShot(10)

    puts "===================================================================="
      puts "\nPrueba de GameUniverse"
      gameUniverse=Deepspace::GameUniverse.new
      nombres=["Carlos","Juan"]
      gameUniverse.init(nombres)
      puts gameUniverse.to_s
      
    
    puts "VAMOS A MODIFICAR UN POCO LAS COSAS, estacion actual #{gameUniverse.currentStation.to_s}"
      puts "Montamos: la 1"
      gameUniverse.mountShieldBooster(0)
      gameUniverse.mountWeapon(0)
      puts "\nEESTACION AHORA: #{gameUniverse.currentStation.to_s}"

   
      enemyStarShip2=Deepspace::EnemyStarShip.new("DeadStar",999,998,loot,damage2)
      enemyStarShip3=Deepspace::EnemyStarShip.new("Patata",0.1,1,loot,damage2)
      spaceStation2=Deepspace::SpaceStation.new("Chetada",suppliesPackage2)
      result=gameUniverse.combatGo(spaceStation2,enemyStarShip2)
      puts result
      puts damage2.getUIversion.to_s
      puts "===================================================================="
      #puts gameUniverse.to_s
      if gameUniverse.nextTurn
        puts "TURNO SIGUIENTE"
      else
        puts "MISMO TURNO"
      end

      

    end
  end
  
  TestP2.main

