## MODELO VISTA CONTROLADOR

### Patrón de diseño

Consiste en no reinventar la rueda, es decir no repetir cossas que sean iguales. Se repiten las soluciones para un tipo de problema.

### Modelo vista- Controlador

Un programa que se comunique con el usuario evitando caer en los problemas clásicos.

#### ¿Qué hace el controlador?

- Recibe órdenes del usuario.

- Decide qué hace el modelo.

- Intermediatrio entre cambios controlador-vista

  <img src="/home/maguado/Imágenes/Captura de pantalla de 2020-04-30 11-44-09.png" alt="Captura de pantalla de 2020-04-30 11-44-09"  align="left" style="zoom:50%;" />



Se mantiene la alta cohesión (todo lo que esté junto o en una clase tiene una razón para estar ahi) y el bajo acoplamiento(dependencias).

El controlador está de gestor intermedio entre la vista y el modelo.

### En nuestro código:



#### CREAMOS UN PROYECTO GRÁFICO Y UNA CLASE GRÁFICO



#### Creamos una VENTANA GENERAL

Clase del tipo JFrame, es la ventana que lo contiene todo. Podemos estar en design o en source.

podemos crear una instancia del JFrame y fijamos la visibilidad a true `setVisible(true)`.

#### CREAMOS PANEL

Igual que lo anterior, create Jpanel

Podemos añadir elementos al panel como por ejemplo botones o elementos de texto.





ERRORES

- Flow, layout, repaint, revalidate.
- 