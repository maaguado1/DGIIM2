
package deepspace;

import java.util.ArrayList;

/**
 *
 * @author maguado
 */
public class SpaceCity extends SpaceStation {
    
    
    private SpaceStation base;
    private ArrayList<SpaceStation> collaborators;
    
    
    SpaceCity(SpaceStation bas, ArrayList<SpaceStation> rest){
        super(bas);
        base= bas;
        collaborators= new ArrayList<SpaceStation>(rest);

    }
    
    public ArrayList<SpaceStation> getCollaborators(){
        return collaborators;
    }
    
    
    @Override
    public Transformation setLoot(Loot l){
        super.setLoot(l);
        return Transformation.NOTRANSFORM;
    }
    
    
    @Override
    public float fire(){
        float potencia=super.fire();
        for (SpaceStation w :collaborators){
            potencia += w.fire();
        }
        return potencia;      
    }
    
    
    @Override
    public float protection(){
        float prot=super.protection();
        for (SpaceStation w :collaborators){
            prot += w.protection();
        }
        return prot;      
    }

    @Override
    SpaceCityToUI getUIversion(){
        return new SpaceCityToUI(this);
    }

}
