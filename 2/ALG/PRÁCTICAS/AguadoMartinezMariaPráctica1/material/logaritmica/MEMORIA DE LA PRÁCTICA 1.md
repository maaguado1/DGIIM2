### MEMORIA DE LA PRÁCTICA 1

##### María Aguado

#### A) Algoritmos O(n)

##### A.1.- Algoritmo de la Burbuja

- ###### **Eficiencia empírica** 

  Sabemos que el algoritmo de la burbuja tiene una eficiencia teórica O(n), los puntos obtenidos después de ejecutarlo 30 veces son ^3  n:

![1](/home/maguado/Escritorio/p1/lineal/burbuja/1.png)



- ###### **Eficiencia empírica** 

  Si realizamos el ajuste lineal podemos ver lo siguiente:



![burbu](/home/maguado/Escritorio/p1/lineal/burbuja/burbu.png)

Claramente podemos observar que es un ajuste muy bueno.

##### A.2.- Algoritmo por inserción

- **Eficiencia empírica** 

  La gráfica obtenida es:

![2](/home/maguado/Escritorio/p1/lineal/2.png)



- ###### Eficiencia híbrida

Y con los datos ajustados de manera lineal tendríamos:

![inserc](/home/maguado/Escritorio/p1/lineal/inserc.png)



##### A.3.- Algoritmo por selección

- **Eficiencia empírica** 

La gráfica obtenida es:

![3](/home/maguado/Escritorio/p1/lineal/3.png)



- **Eficiencia híbrida** 

La gráfica ajustada será:

![selec](/home/maguado/Escritorio/p1/lineal/selec.png)

##### A.4.- Conclusiones

Para empezar, la tabla con los resultados obtenidos tras realizar las ejecuciones descritas es la siguiente:



Y la gráfica obtenida es:



![SELECCIÓN, INSERCIÓN y BURBUJA](/home/maguado/Descargas/SELECCIÓN, INSERCIÓN y BURBUJA.png)

Además, el coeficiente de correlación en el algoritmo por selección es $$0.97068475188942$$, en el segundo(inserción) es de $$0.97036398742228 $$, y por último en el de la burbuja será de $$ 0.96657965809216$$. Después de analizar estos resultados podemos afirmar que el algoritmo de selección es el que mejor se ajusta.



#### B. Algoritmos logarítmicos

Para la obtención de datos he vuelto a ejecutar los programas con tamaños desde 1000 hasta 30000, con un aumento de 1000 entre ejecuciones.

##### B.1.- Algoritmo quicksort

- **Eficiencia empírica**

  

![quick](/home/maguado/Escritorio/p1/quick.png)	

- **Eficiencia híbrida**  

(FALTA)



##### B.2.- Algoritmo heapsort

- **Eficiencia empírica**



![heap](/home/maguado/Escritorio/p1/heap.png)

- **Eficiencia híbrida**

Primero realizamos el ajuste logarítmico, 



##### B.3.- Algoritmo mergesort

- **Eficiencia empírica**

![merg2](/home/maguado/Escritorio/p1/merg2.png)

- **Eficiencia híbrida**

  Primero realizo el ajuste logarítmico:

  ![meg2](/home/maguado/Escritorio/p1/meg2.png)

  

  - OTROS AJUSTES: Compruebo un ajuste cuadrático





##### B.4.- Conclusiones

De manera análoga al apartado anterior, observamos la tabla producida por los resultados de las ejecuciones:





![MERGESORT, QUICKSORT y HEAPSORT](/home/maguado/Descargas/MERGESORT, QUICKSORT y HEAPSORT.png)





#### C. Algoritmos $$O(n^3)$$ 

##### C.1.-Floyd 

- **Eficiencia empírica**







- **Eficiencia híbrida**









#### D. Algoritmos $$ O(2^n)$$

##### D.1.- Hanoi



- **Eficiencia empírica**







- **Eficiencia híbrida**



