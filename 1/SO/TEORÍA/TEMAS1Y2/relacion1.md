## TEMA 1: Estructura del SO

### Relación de problemas

#### 1. Cuestiones generales relacionadas con un SO:

##### a) ¿Qué es el núcleo (kernel) de un SO?

El núcleo es la parte del sistema operativo que interactúa de forma directa con el hardware de una máquina.
Contiene instrucciones privilegiadas y utilidades de gestión como por ejemplo de memoria, archivos y ficheros, dispositivos de E/S, procesos y recursos, etc.

Hay distintos enfoques sobre la implementación del núcleo en los sistemas operativos. Por ejemplo, puede ser que el núcleo contenga sólo las utilidades fundamentales
 mientras que todas las demás se encuentran  en módulos cargables (microkernel).

##### b) ¿Qué es un modelo de memoria para un programa? Explique los diferentes modelos de memoria para la arquitectura IA-32.

Un modelo de memoria de un programa describe las interacciones de los hilos a través de la memoria y su uso compartido de datos.
Los modelos son: flat, segmented y real-address.

##### c) ¿Cómo funciona el mecanismo de tratamiento de interrupciones mediante interrupciones vectorizadas? Explique que parte es realizada por el hardware y que parte por el software.

HARDWARE:

- El controlador de dispositivo u otro hardware genera una interrupción.
- El proc. termina la ejecución de la instrucción actual y revisa la recepción de interrupción.
- El proc. reconoce la interrupción.
- El proc. apila PSW y el PC.
- El proc. carga el nuevo valor en el PC basado en el reconocimiento de la interrupción.


SOFTWARE:
- Salva el resto de la información de estado del proceso.
- Procesa la interrupción.
- Restaura la información de estado del proceso.
- Restaura PC y PSW.


##### d) Describa detalladamente los pasos que lleva a cabo el SO cuando un programa solicita una llamada al sistemas

Cuando un programa realiza una llamada al sistema, el sistema operativo mediante la interfaz de llamadas realiza un context_switch(), para poder salvar los registros de contexto, cambia el bit a modo kernel
 y ejecuta las instrucciones de la llamada al sistema, que devuelven un estado de finalización o un valor de retorno.
 Los parámetros se pueden pasar: en registros de la CPU, en memoria principal o en la pila(mejor implementación).


#### 2. Explique tres responsabilidades asignadas al gestor de memoria de un SO y tres asignadas al gestor de procesos.

 GESTOR DE MEMORIA:
 - Protección de la región de memoria ocupada por el kernel y los programas.
 - Compartición de parte de las regiones ocupaas por los programas para permitir la comunicación entre ellos.
 - Gestión automática de la asignación/liberación de la memoria disponible para un programa.


 GESTOR DE PROCESOS:

 - Equitatividad, es decir, cada proceso tiene las mismas oportunidades de acceder a recursos que solicite.

 - Respuesta diferencial, se pueden dividir los procesos según necesidades de recursos y se toman decisiones dinámicas teniendo en cuenta las características del proceso.

 - Maximizar la eficiencia.


#### 3. Contraste las ventajas e inconvenientes de una arquitectura de SO monolítica frente a una arquitectura microkernel.

Una arquitectura monolítica tiene todas las funcionalidades del so en el núcleo, en memoria principal. Sus ventajas son:
- Eficiencia, pues la llamada al sistema resulta muy rápida.

Sus desventajas son:
- El núcleo no es flexible, ni portable, es muy sensible a cambios y no puede actualizarse, pues cuando se realiza un cambio, el núcleo completo tiene que volver a compilarse, enlazarse, etc.
- Se tiene que cambiar de modo siempre que se quiera acceder a una funcionalidad del so.

Sin embargo, en una arquitectura microkernel, las funcionalidades fundamentales del SO están en el núcleo, y las demás se implementan como programas de usuario.
Las ventajas que presenta este enfoque son:

- Flexibilidad
- Portabilidad
- Interfaz uniforme
- Extensibilidad
- Fiabilidad

La principal desventaja es la eficiencia, pues cuando se valora la eficiencia microkernel frente a una monolítica, se ve claramente que el tiempo de respuesta es peor, es decir que se tarda más en enviar un mensaje al micronúcleo para que llame al servicio correspondiente que en cambiar de modoy ejecutar la utilidad kernel.
Para solucionar este problema se han intentado cambios en la implementación como por ejemplo ampliar el micronúclo, pero aquí estaríamos perdiendo alguna de las ventajas que aportaba el pequeño tamaño, o reducirlo todavía más, siendo este un área importante de investigación.

#### 4. Cuestiones relacionadas con virtualización:

##### a) ¿Qué se entiende por virtualización mediante hipervisor?

La virtualización mediante hipervisor se realiza mediante una capa de abstracción software (el hipervisor), que se sitúa entre el hardware y el software a ejecutar. El hipervisor crea un entorno de trabajo que se llama Virtual Machine, para el software a ejecutar.
La máquina virtual dispone de unos recursos virtuales proporcionados por el hipevisor y sobre ellos se ejecuta el guest OS, las bibliotecas y aplicaciones.

##### b) ¿Qué clases de hipervisores existen de manera general y que ventajas e inconvenientes plantea una clase con respecto a la otra?

Hipervisor tipo 1 e hipervisor tipo 2.
**Tipo 1:** Se ejecutandirectamente en el host HW para proporcionarVM a los SO invitados. Ej. Xen y Vmware ESX/ESXi.
**Tipo 2** Se ejecutan sobre un SO
convencional como el resto de programas y el
SO invitado se ejecuta sobre la abstracción
proporcionada por el hipervisor. Ej. VMware
Workstation y VirtualBox .

Los hipervisores tipo 1 obtienen mejor rendimiento que los de tipo 2 ya que: disponen de todos los recursos para las VM y los múltiples niveles de abstracción entre el SO invitado y el HW real no permiten alto rendimiento de la máquina virtual.

Los hipervisores tipo 2 permiten realizar virtualización sin tener que dedicar toda la máquina a dicho fin. Ejemplo, prueba de kernels o puesta a punto de servidores.


#### 5. Cuestiones relacionadas con RTOS:

##### a) ¿Qué característica distingue esencialmente a un proceso de tiempo real de otro que no lo es?

Un proceso de tiempo real tiene asignado un tiempo de ejecución, es decir, tiene cierto grado de urgencia. Intentan controlar eventos que ocurren en el mundo exterior, una tarea de tiempo real debe ser capaz de mantener el ritmo de aquellos eventos que le conciernen. Así, normalmente es posible asociar un plazo de tiempo límite con una tarea concreta.


##### b) ¿Cuáles son los factores determinantes del tiempo de respuesta en un RTOS, e.d. define determinismo y reactividad?

**Determinismo:** un sistema operativo se dice determinista en el sentido de que realiza operaciones en instantes de tiempos fijos predeterminados. El determinismo se ocupa de cuánto tarda el sistema operativo antes del reconocimiento de una interrupción.

**Reactividad:** se ocupa de cuánto tiempo tarda el sistema operativo en servir la interrupción, incluye los siguientes aspectos

- Cantidad de tiempo necesario para manejar inicialmente la interrupción y comenzar a ejecutar la rutina de servicio de la interrupción.
- La cantidad de tiempo necesario para realizar RSI.
- El efecto de anidamiento de interrupciones.
