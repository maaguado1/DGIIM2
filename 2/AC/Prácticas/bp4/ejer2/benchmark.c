/*
 * Nucleo original
 */

#include <stdio.h>  // biblioteca donde se encuentra la función printf()
#include <stdlib.h> // biblioteca con funciones atoi(), malloc() y free()
#include <time.h>   // biblioteca donde se encuentra la función clock_gettime()
#include "daxyp.c"

unsigned int N;
double a;
double* x;
double *y;

void daxpy();

int main(int argc, char **argv)
{

    int i, j;
    int R[40000];
    struct timespec start, end;
    double time;

    if (argc < 3)
    {
        printf ("\nERROR, número de argumentos");
        exit (-1);
    }
    N = atoi(argv[1]);
    x = (double*)malloc(N*sizeof(double));
    y = (double*)malloc(N*sizeof(double));
    a= atoi(argv[2]);

    for (i =0;i <N; i ++){
        x[i]= i;
        y[i]=i+3;
    }

    clock_gettime(CLOCK_REALTIME, &start);
    daxpy();
    clock_gettime(CLOCK_REALTIME, &end);

    time = (double)(end.tv_sec - start.tv_sec) + ((double)(end.tv_nsec - start.tv_nsec) / (1.e+9));

    printf("\n %i %11.9f ", N, time);
    return 0;
}
