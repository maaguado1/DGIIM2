


package deepspace;

import java.util.ArrayList;

/**
 *
 * @author maguado
 */
public class GameUniverse {
    private static final int WIN = 10;
    
    /**
     * Index of the station that is currently playing.
     */
    private int currentStationIndex;
    
    /**
     * Number of turns.
     */
    private int turns;
    
    /**
     * Game dice.
     */
    private Dice dice;
    
    /**
     * Current space station.
     */
    private SpaceStation currentStation;
    
    /**
     * Set of space stations that are playing.
     */
    private ArrayList<SpaceStation> spaceStations;
    
    /**
     * Current enemy star ship.
     */
    private EnemyStarShip currentEnemy;
    
    /**
     * Game state.
     */
    private GameStateController gameState;
    
    /**
     * Whether game has a space city or not.
     */
    private boolean haveSpaceCity;
    
    
    public GameUniverse(){
        gameState= new GameStateController();
        turns=0;
        dice = new Dice();
    }
    
    CombatResult combat (SpaceStation station, EnemyStarShip enemy){
        GameCharacter ch = dice.firstShot();
        boolean enemyWins;
        CombatResult combatResult;
        if (ch == GameCharacter.ENEMYSTARSHIP){
            
            float fire = enemy.fire();
            ShotResult result = station.receiveShot(fire);
            
            if (result == ShotResult.RESIST){
                fire = station.fire();
                result = enemy.receiveShot(fire);
                enemyWins = (result==ShotResult.RESIST);
            }
            else{
                enemyWins = true;
            }
        }
        else{
            float fire = station.fire();
            ShotResult result = enemy.receiveShot(fire);
            enemyWins=(result==ShotResult.RESIST);
        }
        
        if (enemyWins){
            float s = station.getSpeed();
            boolean moves = dice.spaceStationMoves(s);
            
            if (!moves){
                Damage damage = enemy.getDamage();
                station.setPendingDamage(damage);
                combatResult = CombatResult.ENEMYWINS;
            }
            else{
                station.move();
                combatResult = CombatResult.STATIONESCAPES;
            }
        }
        else{
            Loot aLoot = enemy.getLoot();
            station.setLoot(aLoot);
            
            combatResult = CombatResult.STATIONWINS;
            
        }
        gameState.next(turns, spaceStations.size());
        return combatResult;
    }
    
    
    public CombatResult combat(){
        GameState state = gameState.getState();
        if (state == GameState.BEFORECOMBAT || state == GameState.INIT){
            return combat(currentStation, currentEnemy);
        }
        else
            return CombatResult.NOCOMBAT;
    }
    
    
    public void discardHangar(){
        if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.discardHangar();
        }
        
        
    }
    
    
    public void discardShieldBooster(int i){
        if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.discardShieldBooster(i);
        }
    }
    
    
    public void discardWeapon(int i){
        if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.discardWeapon(i);
        }
    }
    
    public void discardWeaponInHangar(int i){
         if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.discardWeaponInHangar(i);
        }
    }
    
     public void discardShieldBoosterInHangar(int i){
        if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.discardShieldBoosterInHangar(i);
        }
    }
    
    public GameState getState(){
        return gameState.getState();
        
    }
    
    
    public GameUniverseToUI getUIversion(){
        return new GameUniverseToUI(currentStation, currentEnemy);
        
    }
    
    
    public boolean haveAWinner(){
        return currentStation.getNMedals()==WIN;
        
    }
    
    
    public void init(ArrayList<String> names){
        GameState state = gameState.getState();
        if (state == GameState.CANNOTPLAY){
            spaceStations = new ArrayList<SpaceStation>();
            CardDealer dealer= CardDealer.getInstance();
            SuppliesPackage supplies;
            for (String name: names){
                supplies = dealer.nextSuppliesPackage();
                SpaceStation station = new SpaceStation(name, supplies);
                spaceStations.add(station);
                
                int nh = dice.initWithNHangars();
                int nw =dice.initWithNWeapons();
                int ns = dice.initWithNShields();
                
                Loot lo = new Loot(0, nw, ns, nh, 0);
                station.setLoot(lo);
            }
            
            currentStationIndex = dice.whoStarts(names.size());
            currentStation = spaceStations.get(currentStationIndex);
            currentEnemy = dealer.nextEnemy();
            gameState.next(turns, spaceStations.size());
        }
    }
    
    
    public void mountShieldBooster(int i){
        if (gameState.getState()== GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.mountShieldBooster(i);
        }
        
    }
    
    public void mountWeapon(int i){
        if (gameState.getState() == GameState.INIT || gameState.getState()==GameState.AFTERCOMBAT){
            currentStation.mountWeapon(i);
        }
        
    }
    
    
    public boolean nextTurn(){
        GameState state = gameState.getState();
        if (state == GameState.AFTERCOMBAT){
            if (currentStation.validState()){
                currentStationIndex = (currentStationIndex+1)%spaceStations.size();
                currentStation = spaceStations.get(currentStationIndex);
                currentStation.cleanUpMountedItems();
                CardDealer dealer = CardDealer.getInstance();
                currentEnemy = dealer.nextEnemy();
                gameState.next(turns, spaceStations.size());
                return true;
            }
            else
                return false;
        }
        return false;
        
    }
    
    public String toString() {
        return  "GameUniverse(\n" +
                "\tcurrentStationIndex = " + currentStationIndex + "\n" +
                "\tcurrentStation = " + currentStation + "\n" +
                "\tcurrentEnemy = " + currentEnemy + "\n" +
                "\tturns = " + turns + "\n" +
                "\tdice = " + dice + "\n" +
                "\tgameState = " + gameState + "\n" +
                "\tspaceStations = " + spaceStations + "\n" +
                "\tWIN = " + WIN + "\n" +
                ")";
    }
    
    
    
    
}
