/* SumaVectoresC.c
 Suma de dos vectores: v3 = v1 + v2

 Para compilar usar (-lrt: real time library, es posible que no sea necesario usar -lrt):
	gcc  -O2  SumaVectores.c -o SumaVectores -lrt
	gcc  -O2 -S SumaVectores.c -lrt

 Para ejecutar use: SumaVectoresC longitud

*/

#include <stdlib.h>	// biblioteca con funciones atoi(), malloc() y free()
#include <stdio.h>	// biblioteca donde se encuentra la función printf()
#include <time.h>	// biblioteca donde se encuentra la función clock_gettime()
#include "nucleo3mod.c"




void core();

int R[40000];
int N;
int M;
int main(int argc, char** argv){

  int ii, i, X1, X2;
  N= 5000;
	M = 40000;
  struct timespec start, end;
  double time;

  clock_gettime(CLOCK_REALTIME, &start);

	core();
  clock_gettime(CLOCK_REALTIME, &end);

  time = (double) (end.tv_sec-start.tv_sec)+((double) (end.tv_nsec-start.tv_nsec)/(1.e+9));

  printf("\nTiempo: %11.9f\t R[0]= %i\t R[39999]= %i\n", time, R[0], R[39999]);
  return 0;
}
