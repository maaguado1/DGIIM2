	.file	"sumavec.c"
	.text
	.p2align 4,,15
	.globl	core
	.type	core, @function
core:
.LFB41:
	.cfi_startproc
	movl	M(%rip), %eax
	testl	%eax, %eax
	jle	.L13
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	leaq	s(%rip), %r12
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	N(%rip), %ebx
	leal	-1(%rax), %r11d
	leaq	8(%r12), %r9
	leaq	R(%rip), %rbp
	xorl	%r10d, %r10d
	addq	$1, %r11
	leal	-1(%rbx), %r8d
	leaq	0(,%r8,8), %rax
	leaq	12(%r12), %r8
	addq	%rax, %r9
	addq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	testl	%ebx, %ebx
	movl	%r10d, %edi
	movq	%r12, %rax
	jle	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%rax), %edx
	addq	$8, %rax
	leal	(%rdi,%rdx,2), %edx
	addl	%edx, %ecx
	cmpq	%r9, %rax
	jne	.L4
	leaq	4+s(%rip), %rax
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%rax), %edx
	addq	$8, %rax
	leal	(%rdx,%rdx,2), %edx
	subl	%edi, %edx
	addl	%edx, %esi
	cmpq	%rax, %r8
	jne	.L5
	cmpl	%ecx, %esi
	jle	.L6
	movl	%ecx, 0(%rbp,%r10,4)
.L7:
	addq	$1, %r10
	cmpq	%r11, %r10
	jne	.L3
	popq	%rbx
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movl	%esi, 0(%rbp,%r10,4)
	jmp	.L7
.L13:
	.cfi_def_cfa_offset 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	rep ret
	.cfi_endproc
.LFE41:
	.size	core, .-core
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"\nTiempo: %11.9f\t R[0]= %i\t R[39999]= %i\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB42:
	.cfi_startproc
	subq	$56, %rsp
	.cfi_def_cfa_offset 64
	xorl	%edi, %edi
	movl	$5000, N(%rip)
	movq	%rsp, %rsi
	movl	$40000, M(%rip)
	movq	%fs:40, %rax
	movq	%rax, 40(%rsp)
	xorl	%eax, %eax
	call	clock_gettime@PLT
	xorl	%eax, %eax
	call	core
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	call	clock_gettime@PLT
	movq	24(%rsp), %rax
	subq	8(%rsp), %rax
	leaq	.LC1(%rip), %rsi
	pxor	%xmm0, %xmm0
	movl	R(%rip), %edx
	pxor	%xmm1, %xmm1
	movl	159996+R(%rip), %ecx
	movl	$1, %edi
	cvtsi2sdq	%rax, %xmm0
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	cvtsi2sdq	%rax, %xmm1
	movl	$1, %eax
	divsd	.LC0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	call	__printf_chk@PLT
	movq	40(%rsp), %rdx
	xorq	%fs:40, %rdx
	jne	.L20
	xorl	%eax, %eax
	addq	$56, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L20:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE42:
	.size	main, .-main
	.comm	M,4,4
	.comm	N,4,4
	.comm	R,160000,32
	.comm	s,40000,32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1104006501
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
