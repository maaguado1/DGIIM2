#include <iostream>
#include <chrono>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>
#include <climits>
#include <stdlib.h>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto{
  float x;
  float y;
};

struct Arista{
  int inicio;
  int final;
}
/**
 * @brief Función para leer un fichero que contendrá un conjunto de nodos.
 * @param Un string, que hace referencia al nombre del fichero de entrada.
 * @param Un vector de Punto, donde iremos introduciendo los nodos del fichero(por ello, se pasa por referencia).
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_entrada (const string origen, vector<Punto> & nodos) {

  ifstream f(origen);
  bool return_value;

  if (!f) {
      cerr << "Fallo al abrir el fichero\n";
      return_value = false;
  }
  else {
      float n;
      string dimension;

      do {
          f >> dimension;
      } while (dimension != "DIMENSION:");

      f >> n;

      string coordenadas;

      do {
          f >> coordenadas;
      } while (coordenadas != "NODE_COORD_SECTION");

      nodos.clear();

      int i, j;
	  Punto P;
      float temporal;
      vector<float> vector_coordenadas;
      for (j = 0; j<n; j++) {
          f >> i;
          f >> temporal;
          P.x=temporal;

          f >> temporal;
          P.y=temporal;
		  nodos.push_back(P);
      }
   }

      return_value = true;

      f.close();
      return return_value;
}

/**
 * @brief Función para sacar un fichero que contendrá la solución del problema
 * @param Un string, que hace referencia al nombre del fichero de salida.
 * @param Un vector de enteros, que es el resultado obtenido por el algoritmo escogido.
 * @param Un vector de Punto, que contiene los nodos.
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_salida(const string fichero, const vector<int> & resultados, vector<Punto> nodos) {
    ofstream f(fichero);

    if (!f) {
        cerr << "Fallo al abrir el archivo de salida";
        return false;
    }
    else {
        f << "DIMENSION: " << resultados.size() << endl;
        f << "TOUR_SECTION" << endl;

        for (int i=0; i<resultados.size(); i++)
            f << resultados[i] + 1 << " " << nodos[i].x<< " " <<nodos[i].y<< endl;

        f << resultados[0] + 1 << " " << nodos[0].x << " " << nodos[0].y << endl;

        f.close();

        return true;
    }
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */
int distancia(Punto uno, Punto dos){
  return rint(sqrt( pow((dos.x- uno.x), 2) + pow((dos.y-uno.y), 2)));
}

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T){
  int n = T.size();
  for (int i = 0; i < n; i++)
    {
      cout << "\n| ";
      for (int j = 0; j< n; j ++){
    cout << T[i][j] <<" " ;
  }

  cout  << "  |";

  }
  cout << "\n\n";
}

/**
 * @brief Genera una matriz con las distancias entre los puntos
 * @param nodos, los puntos sobre los que calculamos las distancias
 * @return una matriz con las distancias entre los puntos
 *
 * Matriz[i][j] := distance(pi, pj)
 *
 * */
vector<vector<int>> genera_matriz(vector<Punto> nodos){
  vector<vector<int>> T;
  int n =nodos.size();
  for (int i =0; i < n; i ++){
    vector<int> fila;
    for (int j = 0; j < n; j ++){
      if (j < i )
        fila.push_back(T[j][i]);
      else if (j == i)
        fila.push_back(0.0);
      else
      fila.push_back(distancia( nodos[i], nodos[j]));

    }
    T.push_back(fila);
  }
  return T;
}


/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

 int calcular_valor (vector<int> solucion, vector<vector<int>> T){
   int valor=0;
   for (int i =0; i < solucion.size()-1; i++){
     valor += T[solucion[i]][solucion[i+1]];
   }
   valor += T[solucion[0]][solucion[T.size()-1]];
   return valor;
 }

// Algoritmo basado en cercanía.
/**
 * @brief Calcula la mejor solucion para un conjunto de puntos, por Shortest Neighbor First
 * @param T, la matriz de distancias de los puntos con los que trabajamos
 * @pre T.size() > 0
 * @return el camino cuya distancia es la mínima, si todo sale bien o un vector vacío, si ocurre algún error.
 * */
vector<int> solucion_cercania(vector<vector<int>> distancias){
	// Creamos el vector que contendrá el recorrido.
	vector<int> solucion;			 //O(1)
	int n = distancias.size();		 //O(1)
	int distancia_minima = 0;		 //O(1)
	int distancia = 0;				 //O(1)
	// Seleccionamos la primera ciudad de forma aleatoria
	int posicion= rand() % n;		 //O(1)
	int i;						     //O(1)
	// Introducimos la primera ciudad en el vector solución
	solucion.push_back(posicion);	 //O(1)

	//Mientramos no recorramos todas las ciudades, continuamos escogiendo.
	while(solucion.size() < distancias.size()){ 	    //O(n-1)
		i=posicion;							           //O(1)
		distancia_minima=INT_MAX;			            //O(1)
		//Veamos quién es la ciudad más cercana a la actual(i)
		for(int j=0; j< distancias.size(); j++){ 		//O(n)
			//Comprobamos si dicha ciudad se encuentra ya en el vector solución. Si no es así, se mira si la distancia es menor que la distancia mínima actual. Esta parte se corresponde con la eliminación de la ciudad del conjunto de candidatos una vez ha sido escogida.
			if(find(solucion.begin(),solucion.end(),j) == solucion.end()){ //O(1)
				distancia=distancias[i][j];		 //O(1)
				// Si la distancia es menor, dicha distancia pasa a ser la mínima y dicha ciudad pasa a ser la escogida.
				if(distancia<distancia_minima){	 //O(1)
					posicion=j;				     //O(1)
					distancia_minima=distancia;  //O(1)
				}
			}
		}
		// Una vez encontrada dicha ciudad, la añadimos al vector solución y esta pasa a ser la ciudad actual.
		solucion.push_back(posicion);         	  //O(1)
	}
	return solucion;							  //O(1)
}



// Algoritmo basado en optimización(nuestra idea).
vector<int> optimizacion (vector<vector<int>> T, vector<int> solucion){
  float valor1= calcular_valor(solucion, T), valor2;
  cout << "\nVALOR INICIAL: " << valor1;
  int n = T.size();
  int i =0, cambio;
  int j =i+1;
  vector<int> sol2 = solucion;
  while (i < n){

    j = i+1;


    while (j < n){
      valor2= valor1;
      solucion = sol2;
      cambio = solucion[i];
      solucion[i]= solucion[j];
      solucion[j]= cambio;
      valor2=calcular_valor(solucion, T);

      if (valor2 >= valor1){
        j ++;
      }
      else{ //Mejora el camino
        sol2 = solucion;
        valor1= calcular_valor(solucion, T);
        i =0;
        j = i+1;
      }
    }
    i ++;
    j ++;

  }
  return sol2;
}




vector<int> mst(vector<vector<int>> T, vector<Punto> nodes){


}


void erase(vector<int> &v, int valor){

  for (auto it = v.begin(); it!= v.end();){
    if (*it == valor){
      v.erase(it);
    }
    else{
      ++it;
    }
  }
}







vector<Arista> Prim (vector<vector<int>>& T, int n){
    vector <bool> vis(n, false); //nodos visitados, inicialmente ninguno
    priority_queue <pair<int, Arista>> Q; //cola de prioridad de parejas de enteros (-distancia del nodo a F, nodo)
    Q.push({-0,0}); //introducimos un nodo cualquiera (a distancia 0 de él mismo)
    vector<Arista> arbol;
    Arista actual, nuevas;
    actual.inicio = 0;
    while (not Q.empty()){
        ii arc = Q.top() //arco con menor peso desde F hasta G\F
        Q.pop(); //lo quitamos de la cola
        actual.inicio = arc.second.inicio;
        actual.final = arc.second.final; //vértice de Q a menor distancia de F
        arbol.push_back(actual);
        int v = arc.second;

        if (not vis[v]){ //si no lo hemos visitado

            vis[v] = true;
            nuevas.inicio=v;

            for (int i = 0; i < T.size(); ++i){ //miramos sus vecinos
                nuevas.final=i;

                int w = T[v][i];

                Q.push({-w, nuevas}); // añadimos los vecinos conectados con u
            }
        }
    }
  }






}


    //Tenemos ya el MST creado, ahora
//
//
// ──────────────────────────────────────────────── I ──────────
//   :::::: M A I N : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//

int main(int argc, char *argv[]){

	if(argc!=3){
		printf("Error, modo de ejecución; ./cercania <caracter> <nombre_fichero>");
		exit(-1);
	}

	clock_t t_antes;    // Valor del reloj antes de la ejecución
 	clock_t t_despues;  // Valor del reloj después de la ejecución
	vector<Punto> ciudades;
	string f_entrada,algoritmo;
	string f_salida;
	vector<vector<int>> matriz;
	vector<int> solucion;

	f_entrada = argv[2];
	algoritmo = argv[1];
	//Abrimos el fichero de entrada
	if(!fichero_entrada(f_entrada,ciudades)){
		printf("Error en el fichero de entrada\n");
		return 1;
	}

	//Calculamos la matriz de distancias y aplicamos el algoritmo indicado.
	matriz=genera_matriz(ciudades);


  //Mostramos las coordenadas de las ciudades introducidas
  cout<<"Las coordenadas de las ciudades introducidas son " << endl;
  for(int i=0; i<ciudades.size();i++){
    cout <<"("<< ciudades[i].x << "," << ciudades[i].y << ")" << endl;
  }

	if(algoritmo == "-c"){
		t_antes = clock();
		solucion=solucion_cercania(matriz);
		t_despues = clock();
	}
	else if(algoritmo == "-i"){
		t_antes = clock();

		t_despues = clock();
	}
	else if(algoritmo == "-o"){
		t_antes = clock();
		solucion=solucion_cercania(matriz);
		solucion=optimizacion(matriz,solucion);
		t_despues = clock();
	}
	else{
		printf("Selecciona -c(cercanía),-i(inserción),-o(optimización)");
		exit(-1);
	}



	//Mostramos el recorrido
	cout<< "\nEl orden de ciudades recorrido es el siguiente: " << endl;
	for(int i=0; i<solucion.size(); i++){
		cout << solucion[i] + 1 << endl;
	}

	//Mostramos dicho tiempo
	cout << ((double)(t_despues-t_antes))/CLOCKS_PER_SEC << endl;

	f_salida = f_entrada.append("_salida");

	//Introducimos en el fichero de salida la solución(el recorrido).
	if(fichero_salida(f_salida, solucion, ciudades))
        cout << "\nGuardado. Salida en: " << fichero_salida << endl;
    else{
        cerr << "\nError al guardar el fichero\n";
		return 1;
	}

}
