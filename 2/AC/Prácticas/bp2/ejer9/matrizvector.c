#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv){


	if (argc <2){

		printf ("Falta argumento\n");
		exit (-1);
	}
	int i, j;

	unsigned int N = atoi(argv[1]);
	double **M1, *v1, *v2, t1, t2, ncgt;

	M1= (double**) malloc(N*sizeof(double*));
	v1= (double*)malloc(N*sizeof(double));
	v2 = (double*) malloc (N*sizeof(double));

	if (M1 == NULL || v1== NULL || v2== NULL){
		printf ("\nError en la reserva de memoria");
		exit(-1);

	}

	for (int i=0; i < N; i++){
		M1[i]=(double*) malloc(N*sizeof(double));


		if (M1[i] == NULL){
			printf ("Error en la reserva de memoria 2");
			exit(-1);

		}
	}

	//Inicialización de la matriz

	#pragma omp parallel for
	for (int i=0; i < N; i ++){
		v1[i]= i;
		v2[i]=0.0;
		for (int j=0; j < N; j++){
			M1[i][j] = i-j;

		}

	}


	t1 = omp_get_wtime();


	//Cálculo
	#pragma omp parallel for
	for (int i=0; i < N; i ++){

		for (int j=0; j < N; j++){
			v2[i] += M1[i][j]*v1[j];

		}

	}


	t2 = omp_get_wtime();
	ncgt = t2-t1;

	//Salida

	if (N <= 11){
		printf("Tiempo:%11.9f\n",ncgt);
		printf ("\nMatriz: ");
		for (int i=0; i < N; i ++){
			printf ("\n|");
			for (j=0; j<N;j++){

				printf (" %f", M1[i][j]);


			}
			printf (" |");
		}

		printf ("\nVector: [ ");

		for (int i=0; i<N;i++){

				printf (" %f ", v1[i]);


			}
		printf (" ]\n");



		printf ("\nResultado: [ ");

		for (int i=0; i<N;i++){

				printf (" %f ", v2[i]);


			}
		printf (" ]\n");

	}
	else {

		printf ("  ");
		printf("Tiempo:%11.9f\t / Tamaño Vectores:%u\t/ V2[0]=%8.6f / / V3[%d]=(%8.6f) /\n", ncgt,N,v2[0],N-1,v2[N-1]);




	}

	free(v1); // libera el espacio reservado para v1
	free(v2); // libera el espacio reservado para v2

	for (int i=0; i < N; i++){
		free(M1[i]);
}
	 // libera el espacio reservado para v3
	 free (M1);
  return 0;


}
