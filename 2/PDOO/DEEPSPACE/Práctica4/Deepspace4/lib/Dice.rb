#encoding:utf-8

require_relative 'GameCharacter'

module Deepspace
  
# Class that makes all the random decisions of the game 
# @author María Aguado

class Dice
  
  # Constructors
	# ==========================================================================
  
  # Class initializer
  # @!attributes [Float] Constant probabilities for hangars, shields, weapons and first shots.
  # @!attribute [Random] Random number generator
  def initialize
    
     
    @NHANGARSPROB = 0.25
    
    @NSHIELDSPROB = 0.25
    
    @NWEAPONSPROB = 0.33
    
    @FIRSTSHOTPROB= 0.5
    
    @EXTRAEFFICIENCYPROB = 0.8
    
    @generator = Random.new()
  end
  
  
  
  
  
	# Getters
	#=======================================================================
  
  # Determines the number of hangars that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NHANGARSPROB's probability.
  
  def initWithNHangars
    valor =1
    if @generator.rand(1.0) < @NHANGARSPROB
        valor= 0
    end
    valor
    
  end
  
  
  
  # Determines the number of weapons that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NWEAPONSPROB's probability.
 
  def initWithNWeapons
    rand = @generator.rand(1.0)
    if ( rand < @NHANGARSPROB)
      return 1
    
    elsif (rand < (2*@NHANGARSPROB))
      return 2
    else 
      return 3
    end
  
  end
  
  
  
  # Determines the amount of shield enhancers that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NSHIELDSPROB's probability.
 
  def initWithNShields
    if @generator.rand(1.0) < @NSHIELDSPROB
        return 0
    else
      return 1
      
    end
  end
  
  
  # Determines the player index which will start the game
  # @param nPlayers number of players
  # @return player
 
  def whoStarts(nPlayers)
    @generator.rand(nPlayers)
  end
  
  # Determines who shoots first
  # @return [GameCharacter] SPACESTATION if the player shoots first,
	#                         ENEMYSTARSHIP if the enemy shoots first

  def firstShot
    if @generator.rand(1.0) < @FIRSTSHOTPROB
      return GameCharacter::SPACESTATION
    else
      return GameCharacter::ENEMYSTARSHIP
    end
  end
  
  
  # Determines if a space station moves in order to avoid a shot
  def spaceStationMoves(speed)
    return ( @generator.rand(1.0) < speed)
  end
  
  
  def extraEfficiency()
    if @generator.rand(1.0) < @EXTRAEFFICIENCYPROB
      return true
    else 
      return false
    end
  end
  
 
end
    
end
