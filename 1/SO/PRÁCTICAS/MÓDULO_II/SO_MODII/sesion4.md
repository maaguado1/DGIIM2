## MÓDULO II: Sesión 4

### Comunicación entre procesos utilizando cauces

## 1. Concepto y tipos de cauces

Se sigue la interacción productor/consumidor. Hay cauces con y sin nombre:
El pipeline, |, es un ejemplo de cauce sin nombre, cumple:
- No tiene archivo asociado en disco.
- Al crearlo se crean dos descriptores.
- Sólo pueden ser usados como comunicación entre el que crea y los descendientes que se han creado después del cauce.
- Se cierra automáticamente cuando los contadores=0.

Un archivo FIFO, es un cauce con nombre:
- Tienen un nombre y un sitio en disco, aparecen asociados a directorios.
- Los procesos abren con close y open el archivo.
- Cualquier pproceso puede compartir datos (read/write).
- Permanece en el SA hasta que se borra explícitamente.


## 2. Cauces con nombre

### 2.1. Creación de archivos FIFO

SINTAXIS:

~~~
int mknod (const char *FILENAME, mode_t MODE, dev_t DEV)
~~~

-**FILENAME** nombre del archivo (obv)
- **MODE** valores en el st_mode del archivo
| Valor | Representa el valor del código de tipo de archivo para.. |
|--- |--- |
| S_IFCHR | Archivo de disp. caracteres |
| S_IFBLK | Archivo de disp. bloques |
| S_IFSOCK | Socket |
| S_IFIFO| FIFO |

- **DEV** a qué dispositivo se refiere (si es FIFO =0)

SINTAXIS PARA FIFO:
~~~
int mknod (("/tmp/FIFO", S_IFIFO|0666,0);
int mkfifo (const char *FILENAME, mode_t MODE);


#include <unistd.h>
  int unlink(const char *pathname); //ELIMINACIÓN
~~~

### 2.2 Utilización del FIFO
Las operaciones son iguales salvo por el `lseek()`, pues sigue una política FIFO.
NOTA: La llamada `read()`, es bloqueante para consumidores si no hay datos y desbloquea devolviendo 0 cuando todos los procesos productores han cerrado.


## 3. Cauces sin nombre

### 3.1 Creación

#### SINTAXIS:
~~~
#include <unistd.h>
int pipefd[2];
int pipe(int pipefd[2]); //si tiene éxito, el vector tiene los nuevos descriptores
~~~
 Creamos un hijo y establecemos el sentido del flujo.

#### COSITAS:

- Siempre hay que cerrar el descriptor que no vayamos a usar.
- Para redireccionar entrada/salida al descriptor de lectura/escritura usamos `close, dup y dup2` (close y dup para dejar la entrada deseada libre).
- pipe() siempre va antes que fork().

 **dup** duplica el descriptor-parámetro en la primera entrada libre de la tabla de descriptores de archivo.
 **dup2(nuevo, antiguo)** cierra el antiguo y duplica.

- Se pueden crear dos cauces entre dos procesos.
- Todos los cauces tienen  que estar abiertos por los dos extremos:
  → Si el primero es el lector, se bloquea hasta que se abre otro.
  → El primero es el escritor, open no bloquea, cuando se escribe envía `SIGPIPE`
