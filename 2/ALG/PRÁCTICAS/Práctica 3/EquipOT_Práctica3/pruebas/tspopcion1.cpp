#include<cmath>
#include<iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <climits>
#include <cassert>


using namespace std;


struct coordenadas{
  float x;
  float y;
};

float distancia(coordenadas uno, coordenadas dos){
  return sqrt( pow((dos.x- uno.x), 2) + pow((dos.y-uno.y), 2));
}

void SalidaMatriz(float **T, int n){
  for (int i = 0; i < n; i++)
    {
      cout << "\n| ";
      for (int j = 0; j< n; j ++){
    cout << T[i][j] <<" " ;
  }

  cout  << "  |";

  }
  cout << "\n\n";
  }

float calcular_valor (vector<int> solucion, float **T, int n){
  float valor;
  for (int i =0; i < solucion.size()-1; i++){
    valor += T[i][i+1];
  }
  valor += T[0][n-1];
}


int main (int argc, char *argv[]){

  int n;
  float x, y, num;
  cout << "\nINTRODUCE EL NÚMERO DE NODOS" << endl;
  cin >> n;
  float **T = new float*[n];
  coordenadas nodos[n];
  float suma =0;
  for (int i =0; i < n; i ++){
    T[i]= new float[n];
    cout << "\nINTRODUCE LAS COORDENADAS DEL NODO " <<  i+1<< " :" << endl;
    cin >> num;
    cin  >> x;
    cin >> y;
    nodos[i].x=x;
    nodos[i].y=y;

  }

  //Rellenamos la matriz de aristas

  for (int i =0; i < n; i ++){
    T[i][i]=0.0;
    for (int j = i+1; j < n; j ++){

      T[i][j] = distancia( nodos[i], nodos[j]);
      T[j][i] = T[i][j];
    }
  }


    //Buscamos el mínimo de todas las aristas y creamos la situación inicial
    int n1, n2;
    float min= T[0][1];
    for (int i =0; i <n ;i ++){
      for (int j =i+1; j <n ; j ++){
        if (T[i][j] < min){
            min = T[i][j];
            n1=i;
            n2=j;
          }
      }
    }

    int contador=2;
    vector<int> solucion;
    solucion.push_back(n1);
    solucion.push_back(n2);
    int uno;
    bool derecha= false;
    suma += T[n1][n2];
    T[n1][n2]= -1;
    T[n2][n1]=-1;
    while (solucion.size() < n){

        cout << "\nAhora conecto los nodos " << n1 << " y " << n2 << "\nBuscando la arista menor"<< endl;

        //FUNCIÓN SELECCIÓN
        derecha = false;
        uno =0;
        while (uno < n && T[uno][n1] <=0)
          uno ++;
        min = T[uno][n1];


        for (int i =0 ;i < n; i ++){
            if (T[i][n1] < min && T[i][n1]>0){

              min = T[i][n1];
              uno = i;
            }
          }

          for (int i =0;i < n ; i ++){
              if (T[i][n2] < min && T[i][n2]>0){
                min = T[i][n2];
                uno = i;
                derecha =true;
              }
            }

        if (! derecha){

          suma += T[uno][n1];
          for (int i =0;i < n ; i ++){
              T[i][n1]=-1;
              T[n1][i]= -1;
              T[uno][n2]=-1;
              T[n2][uno]=-1;
            }
          n1=uno;


          //Lo añadimos al principio
          solucion.insert(solucion.begin(), uno);


          }
      else{
        suma += T[uno][n2];
          for (int i =0;i < n; i ++){
            T[i][n2]=-1;
            T[n2][i]= -1;
            T[uno][n1]=-1;
            T[n1][uno]=-1;
          }
        n2=uno;
        solucion.push_back(uno);
      }


    }

    suma += distancia(nodos[solucion[0]], nodos[solucion[n-1]]);
    cout << "\nLA SOLUCIÓN SERÁ LA SECUENCIA: " << endl;
    for (int i =0;i < n; i ++){
        cout << " "<< solucion[i] << " ";
      }
    cout << "\nLA SUMA ES: " << suma;

      for (int i =0;i < n; i ++){
          delete[] T[i];
        }

      delete[] T;


}
