

### Alternativa de resolución

Usaremos un algoritmo de tipo greedy-opt2, es decir, realizamos optimizaciones de la solución inicial hasta llegar a la solución ótpima.

Describimos las especificaciones de nuestra solución a continuación:

- Lista de candidatos: Soluciones que resulta de permutar dos nodos.
- Lista de candidatos utilizados: Soluciones que tienen un valor mayor que la actual.
- Función solución: Se han recorrido todos los nodos.
- Función de selección: La permutación mejora el valor de selección.
- Criterio de fiabilidad: No se pueden establecer ciclos.
- F. objetivo: minimizar el recorrido original.

La optimización descrita resulta de permutar dos nodos, en el caso de que dicha permutación sea favorable, es decir que disminuya el valor de la solución(distancia total), se repite el algoritmo partiendo de la nueva versión.

##### Eficiencia teórica

La eficiencia teórica de nuestra función, dispuesta a continuación:

```c++
// Algoritmo basado en optimización(nuestra idea).
vector<int> optimizacion (vector<vector<int>> T, vector<int> solucion){
  float valor1= calcular_valor(solucion, T), valor2;
  cout << "\nVALOR INICIAL: " << valor1;
  int n = T.size();
  int i =0, cambio;
  int j =i+1;
  vector<int> sol2 = solucion;
  while (i < n){

    j = i+1;


    while (j < n){
      valor2= valor1;
      solucion = sol2;
      cambio = solucion[i];
      solucion[i]= solucion[j];
      solucion[j]= cambio;
      valor2=calcular_valor(solucion, T);

      if (valor2 >= valor1){
        j ++;
      }
      else{ //Mejora el camino
        sol2 = solucion;
        valor1= calcular_valor(solucion, T);
        i =0;
        j = i+1;
      }
    }
    i ++;
    j ++;

  }
  return sol2;
}

```

Para analizar la eficiencia teórica de dicho algoritmo, nos ponemos en el peor de los casos, es decir, en cada permutación se mejora la eficiencia, es decir, se repite n veces el algoritmo.

Tendríamos que tener en cuenta, además, que la función para calcular el valor tiene una eficiencia de $O(n)$:

```c++
int calcular_valor (vector<int> solucion, vector<vector<int>> T){
   int valor=0;
   for (int i =0; i < solucion.size()-1; i++){
     valor += T[solucion[i]][solucion[i+1]];
   }
   valor += T[solucion[0]][solucion[T.size()-1]];
   return valor;
 }
```

De esta manera, la eficiencia de la función de optimización sería

$n*\sum_{i=0}^n \sum_{j=i+1}^{n} O(n)  = n*\sum_{i=0}^n (n* (n-i-1)) = n (\sum_{i=0}^n n^2 - n \sum_{i=0}^n i - n \sum_{i=0}^n 1) $

Así,  tendríamos

$Eficiencia = n (n * n^2 - n * \frac{n(n+1)}{2} - n^2 )= O(n^4)$

#### Eficiencia empírica

Para poder comprobar la eficiencia empírica, tenemos que tener en cuenta que el problema a generar no va a ser siempre el mismo, pues aunque las ciudades se mantengan constantes, al añadir una ciudad nueva el problema cambia drásticamente.

Aún así, hemos realizado un estudio de eficiencia con una generación aleatoria de problema, obteniendo los resultados que se indican a continuación.

![ef](../eficienci/ef.png)



#### Eficiencia híbrida

Como ya hemos comentado, el análisis de eficiencia de un problema como este no resulta adecuado de forma aislada, pero de todas formas hemos realizado el ajuste de los datos.

<img src="../eficienci/efh.png" alt="efh" style="zoom:50%;" />

Se ve que sigue la tendencia descrita, aunque no es un buen ajuste por la aleatoriedad de los datos.



