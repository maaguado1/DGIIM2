//María Aguado Martínez

#include "../Comportamientos_Jugador/jugador.hpp"
#include "motorlib/util.h"

#include <iostream>
#include <cmath>
#include <set>
#include <queue>
#include <stack>
#include <unordered_map>
#include <vector>
#include <list>
#include <algorithm>

/*
 ___            __     __        ___  __                                  __   ___  __  
|__  |  | |\ | /  ` | /  \ |\ | |__  /__`     /\  |  | \_/ | |    |  /\  |__) |__  /__` 
|    \__/ | \| \__, | \__/ | \| |___ .__/    /~~\ \__/ / \ | |___ | /~~\ |  \ |___ .__/ 
                                                                                       */

bool Iguales(const list<Action> una, const list<Action> dos)
{
	list<Action>::const_iterator it = una.begin();
	list<Action>::const_iterator it2 = dos.begin();
	bool condicion = true;
	while (condicion and it != una.end() and it2 != dos.end())
	{
		if ((*it) != (*it2))
			condicion = false;
		it++;
		it2++;
	}
	return condicion;
}
bool EsDestino(const estado &state, const estado &destino)
{
	return state.columna == destino.columna and state.fila == destino.fila;
}

int EsDestino(const estado &state, const list<estado> &destino)
{
	list<estado> otros = destino;
	bool dest = false;
	int index = -1;
	for (int i = 0; i < destino.size() && !dest; i++)
	{
		dest = EsDestino(state, otros.front());
		otros.pop_front();
		if (dest)
			index = i;
	}
	return index;
}

list<estado>::const_iterator BuscarDestino(const estado &state, const list<estado> &destino)
{
	list<estado>::const_iterator it = destino.begin();
	bool dest = false;
	int index = -1;
	while (!dest && it != destino.end())
	{
		if (state.fila == (*it).fila and state.columna == (*it).columna)
		{
			dest = true;
		}
		else
		{
			it++;
		}
	}
	return it;
}

/*

___                  
 |  |__| | |\ | |__/ 
 |  |  | | | \| |  \ 
                     
					 */
// Este es el método principal que se piden en la practica.
// Tiene como entrada la información de los sensores y devuelve la acción a realizar.
// Para ver los distintos sensores mirar fichero "comportamiento.hpp"
Action ComportamientoJugador::think(Sensores sensores)
{

	instantesSimulacion++;
	actual.fila = sensores.posF;
	actual.columna = sensores.posC;
	actual.orientacion = sensores.sentido;

	ProcesarCasilla(sensores.terreno[0], sensores.bateria);

	int porcentaje = casillasDescubiertas * 100 / (mapaResultado.size() * mapaResultado.size());
	if (porcentaje < 15)
		modificacionDesconocida = 1;
	else if (porcentaje < 30)
		modificacionDesconocida = 2;
	else if (porcentaje < 45)
		modificacionDesconocida = 3;
	else if (porcentaje < 60)
		modificacionDesconocida = 4;
	else if (porcentaje >= 60)
		modificacionDesconocida = 5;

	//Comprobamos si estamos al final de la misión
	FinalMision();

	//Si estamos al final de la misión reiniciamos objetivos

	if ((!destinos) or (misionCompletada && sensores.nivel == 4))
	{
		cout << "Reiniciando objetivos, llevas " << numdestinosConseguidos << " objetivos" << endl;
		ReiniciarObjetivos(sensores.num_destinos, sensores.destino, sensores.nivel);
	}

	//Actualizamos el index
	list<estado>::const_iterator i = BuscarDestino(actual, objetivos);
	bool esDestino = i != objetivos.end();

	Action sigAccion = actIDLE;

	if (sensores.nivel == 4)
	{
		int fila = sensores.posF;
		int columna = sensores.posC;
		ActualizarMapa(sensores.terreno, fila, columna, sensores.sentido);

		if (!recarga && sensores.bateria <= umbralDinamico && !justCharged)
		{
			hayPlan = false;
			recarga = true;
			if (3000 - instantesSimulacion <= 500)
			{
				cantidadRecarga = 10 * umbralDinamico;
			}
			else
				cantidadRecarga = 3000 - sensores.bateria;
		}

		//Si estamos en un objetivo, ya sabemos que el ultimo no es, si no, hubiera reiniciado objetivos
		if (esDestino)
		{
			justCharged = false;
			numdestinosConseguidos++;
			objetivos.erase(i);

			destino = ObjetivoCercano();
			cout << "El destino siguiente es f" << destino.fila << " c" << destino.columna << endl;
			sigAccion = plan.front();
		}

		//Calculamos plan
		if (!esperando_aldeano)
		{
			//Si tenemos que recargar, establecemos el plan a la recarga más cercana
			if (recarga && !recargas.empty() && !justCharged)
			{
				cout << "Yendo a una recarga" << endl;
				siguiente = RecargaCercana();
				if (!hayPlan)
					hayPlan = pathFinding(sensores.nivel, actual, objetivos, plan);
				sigAccion = plan.front();
			}
			else
			{
				siguiente = destino;

				//Si tenemos un plan, cogemos la primera acción del plan:
				if (hayPlan and plan.size() > 0)
				{

					VisualizaPlan(actual, plan);
					sigAccion = plan.front();
				}

				else
				{
					cout << "No hay plan, yendo a f" << siguiente.fila << " c" << siguiente.columna << endl;
					hayPlan = pathFinding(sensores.nivel, actual, objetivos, plan);

					sigAccion = plan.front();
				}
			}

			//Si la acción que hemos elegido es ir hacia delante y hay un aldeano, ponemos a true el esperando aldeano y esperamos
			list<Action> plan2;
			bool condBucle = true;
			while (sigAccion == actFORWARD && (sensores.terreno[2] == 'M' or sensores.terreno[2] == 'P'))
			{
				cout << "me muero, recalculando plan" << endl;
				hayPlan = pathFinding(sensores.nivel, actual, objetivos, plan);
				sigAccion = plan.front();
			}
			while (condBucle && ((sigAccion == actFORWARD && sensores.terreno[2] == 'B' and actual.objeto != 2) or (sigAccion == actFORWARD && sensores.terreno[2] == 'A' and actual.objeto != 1)))
			{
				cout << "Prefiero no pasar por ahi, voy a ver si recalculando...." << endl;
				hayPlan = pathFinding(sensores.nivel, actual, objetivos, plan2);
				sigAccion = plan2.front();
				if (Iguales(plan, plan2))
				{
					cout << "Es el mismo plan, tiramos palante" << endl;
					condBucle = false;
				}
				plan = plan2;
			}

			if (sigAccion == actFORWARD && sensores.superficie[2] == 'a')
			{
				esperando_aldeano = true;
				sigAccion = actIDLE;
			}
			else
			{
				plan.erase(plan.begin());
			}
		}
		else
		{
			//Si estamos esperando un aldeano, la acción es IDLE
			sigAccion = plan.front();
			if (sigAccion == actFORWARD && sensores.superficie[2] == 'a')
				sigAccion = actIDLE;
			else
				plan.erase(plan.begin());
			esperando_aldeano = false;
		}
	}
	else
	{

		if (!hayPlan)
		{

			hayPlan = pathFinding(sensores.nivel, actual, objetivos, plan);
		}

		if (hayPlan and plan.size() > 0)
		{
			sigAccion = plan.front();
			plan.erase(plan.begin());
		}
		else
		{
			cout << "no se puede encontrar un plan\n"
				 << endl;
		}
	}
	return sigAccion;
}

// Llama al algoritmo de busqueda que se usara en cada comportamiento del agente
// Level representa el comportamiento en el que fue iniciado el agente.
bool ComportamientoJugador::pathFinding(int level, const estado &origen, const list<estado> &objetivos, list<Action> &plan)
{
	estado un_objetivo;
	int costo;
	switch (level)
	{
	case 0:
		cout << "Demo\n";
		un_objetivo = objetivos.front();
		cout << "fila: " << un_objetivo.fila << " col:" << un_objetivo.columna << endl;
		return pathFinding_Profundidad(origen, un_objetivo, plan);
		break;

	case 1:
		cout << "Optimo numero de acciones\n";

		un_objetivo = objetivos.front();
		cout << "fila: " << un_objetivo.fila << " col:" << un_objetivo.columna << endl;
		return pathFinding_Anchura(origen, un_objetivo, plan);
		break;
	case 2:
		cout << "Optimo costo bateria\n";

		un_objetivo = objetivos.front();
		destino = un_objetivo;
		cout << "fila: " << un_objetivo.fila << " col:" << un_objetivo.columna << endl;
		return pathFinding_A(origen, un_objetivo, plan, costo);
		break;
	case 3:
		cout << "Optimo en coste 3 Objetivos\n";

		return pathFinding_CostoUniformeVarios(origen, objetivos, plan);
		break;
	case 4:
		cout << "fila: " << un_objetivo.fila << " col:" << un_objetivo.columna << endl;
		return pathFinding_Amodificado(origen, siguiente, plan, costo);
		break;
	}
	return false;
}

bool EstaDentro(const estado &state, const set<estado, Compara<estado>> &destino)
{
	return destino.find(state) == destino.end();
}

//---------------------- Implementación de la busqueda en profundidad ---------------------------

// Dado el codigo en caracter de una casilla del mapa dice si se puede
// pasar por ella sin riegos de morir o chocar.
bool EsObstaculo(unsigned char casilla)
{
	if (casilla == 'P' or casilla == 'M')
		return true;
	else
		return false;
}

// Comprueba si la casilla que hay delante es un obstaculo. Si es un
// obstaculo devuelve true. Si no es un obstaculo, devuelve false y
// modifica st con la posición de la casilla del avance.
bool ComportamientoJugador::HayObstaculoDelante(estado &st)
{
	int fil = st.fila, col = st.columna;

	// calculo cual es la casilla de delante del agente
	switch (st.orientacion)
	{
	case 0:
		fil--;
		break;
	case 1:
		col++;
		break;
	case 2:
		fil++;
		break;
	case 3:
		col--;
		break;
	}

	// Compruebo que no me salgo fuera del rango del mapa
	if (fil < 0 or fil >= mapaResultado.size())
		return true;
	if (col < 0 or col >= mapaResultado[0].size())
		return true;

	// Miro si en esa casilla hay un obstaculo infranqueable
	if (!EsObstaculo(mapaResultado[fil][col]))
	{
		// No hay obstaculo, actualizo el parametro st poniendo la casilla de delante.
		st.fila = fil;
		st.columna = col;
		return false;
	}
	else
	{
		return true;
	}
}

struct nodo
{
	estado st;
	list<Action> secuencia;
};

/* 
 ____    ____ ___  ___   ___  
 || \\  ||    ||\\//||  // \\ 
 ||  )) ||==  || \/ || ((   ))
 ||_//  ||___ ||    ||  \\_// 
                              

*/

/*
Estructura que sirve para darle un orden completo al conjunto de estados (cerrados) en los algoritmos de este nivel. Compara 
fila, columna y orientación.
*/
struct ComparaEstadosNivel1
{
	bool operator()(const estado &a, const estado &n) const
	{
		return ((a.fila > n.fila) or (a.fila == n.fila and a.columna > n.columna) or
				(a.fila == n.fila and a.columna == n.columna and a.orientacion > n.orientacion));
	}
};

// Implementación de la busqueda en profundidad.
// Entran los puntos origen y destino y devuelve la
// secuencia de acciones en plan, una lista de acciones.
bool ComportamientoJugador::pathFinding_Profundidad(const estado &origen, const estado &destino, list<Action> &plan)
{
	//Borro la lista
	cout << "Calculando plan\n";
	plan.clear();
	set<estado, ComparaEstadosNivel1> Cerrados; // Lista de Cerrados
	stack<nodo> Abiertos;						// Lista de Abiertos

	nodo current;
	current.st = origen;
	current.secuencia.empty();

	Abiertos.push(current);

	while (!Abiertos.empty() and (current.st.fila != destino.fila or current.st.columna != destino.columna))
	{

		Abiertos.pop();
		Cerrados.insert(current.st);

		// Generar descendiente de girar a la derecha
		nodo hijoTurnR = current;
		hijoTurnR.st.orientacion = (hijoTurnR.st.orientacion + 1) % 4;
		if (Cerrados.find(hijoTurnR.st) == Cerrados.end())
		{
			hijoTurnR.secuencia.push_back(actTURN_R);
			Abiertos.push(hijoTurnR);
		}

		// Generar descendiente de girar a la izquierda
		nodo hijoTurnL = current;
		hijoTurnL.st.orientacion = (hijoTurnL.st.orientacion + 3) % 4;
		if (Cerrados.find(hijoTurnL.st) == Cerrados.end())
		{
			hijoTurnL.secuencia.push_back(actTURN_L);
			Abiertos.push(hijoTurnL);
		}

		// Generar descendiente de avanzar
		nodo hijoForward = current;
		if (!HayObstaculoDelante(hijoForward.st))
		{
			if (Cerrados.find(hijoForward.st) == Cerrados.end())
			{
				hijoForward.secuencia.push_back(actFORWARD);
				Abiertos.push(hijoForward);
			}
		}

		// Tomo el siguiente valor de la Abiertos
		if (!Abiertos.empty())
		{
			current = Abiertos.top();
		}
	}

	cout << "Terminada la busqueda\n";

	if (current.st.fila == destino.fila and current.st.columna == destino.columna)
	{
		cout << "Cargando el plan\n";
		plan = current.secuencia;
		cout << "Longitud del plan: " << plan.size() << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
	}

	return false;
}

// Sacar por la consola la secuencia del plan obtenido
void ComportamientoJugador::PintaPlan(list<Action> plan)
{
	auto it = plan.begin();
	while (it != plan.end())
	{
		if (*it == actFORWARD)
		{
			cout << "A ";
		}
		else if (*it == actTURN_R)
		{
			cout << "D ";
		}
		else if (*it == actTURN_L)
		{
			cout << "I ";
		}
		else
		{
			cout << "- ";
		}
		it++;
	}
	cout << endl;
}

// Funcion auxiliar para poner a 0 todas las casillas de una matriz
void AnularMatriz(vector<vector<unsigned char>> &m)
{
	for (int i = 0; i < m[0].size(); i++)
	{
		for (int j = 0; j < m.size(); j++)
		{
			m[i][j] = 0;
		}
	}
}

// Pinta sobre el mapa del juego el plan obtenido
void ComportamientoJugador::VisualizaPlan(const estado &st, const list<Action> &plan)
{
	AnularMatriz(mapaConPlan);
	estado cst = st;

	auto it = plan.begin();
	while (it != plan.end())
	{
		if (*it == actFORWARD)
		{
			switch (cst.orientacion)
			{
			case 0:
				cst.fila--;
				break;
			case 1:
				cst.columna++;
				break;
			case 2:
				cst.fila++;
				break;
			case 3:
				cst.columna--;
				break;
			}
			mapaConPlan[cst.fila][cst.columna] = 1;
		}
		else if (*it == actTURN_R)
		{
			cst.orientacion = (cst.orientacion + 1) % 4;
		}
		else
		{
			cst.orientacion = (cst.orientacion + 3) % 4;
		}
		it++;
	}
}

int ComportamientoJugador::interact(Action accion, int valor)
{
	return false;
}

void Salida(nodo nod)
{
	cout << "Info del nodo" << endl;
	cout << "Fila" << nod.st.fila << endl;
	cout << "Col" << nod.st.columna << endl;
	cout << "Orientacion" << nod.st.orientacion << endl;
}

string SalidaAccion(Action accion)
{
	if (accion == actFORWARD)
	{
		return "Frente";
	}
	else if (accion == actTURN_R)
	{
		return "Derecha";
	}
	else if (accion == actTURN_L)
	{
		return "Izquierda";
	}
	else
	{
		return "Nada";
	}
}

/*  __  __ __ _
_ __  ____ __       __
 ||\ || || || || ||    ||       ||
 ||\\|| || \\ // ||==  ||       ||
 || \|| ||  \V/  ||___ ||__|    ||
                                  */


/**
 * @brief Implementación de la búsqueda en anchura
 * @param origen: estado origen desde el que crear el plan
 * @param destino: el estado destino.
 * @param plan: secuencia de acciones hasta llegar al destino
 * @return true si encuentra camino, false si no
 * 
 * Este algoritmo prinero expande un nivel completo (se mete en una cola FIFO) antes de pasar 
 * a expandir los nodos del nivel siguiente.
 * 
 * */

bool ComportamientoJugador::pathFinding_Anchura(const estado &origen, const estado &destino, list<Action> &plan)
{
	//Borramos la lista
	cout << "Creando plan \n";
	plan.clear();

	set<estado, ComparaEstadosNivel1> generados; // Lista de Cerrados
	queue<nodo> cola;							 // Lista de Abiertos

	nodo current;
	current.st = origen;
	current.secuencia.empty();

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};
	while (!cola.empty() and !EsDestino(current.st, destino))
	{
		cola.pop();
		generados.insert(current.st);

		for (int i = 0; i < 3; i++)
		{
			nodo nuevo_hijo = current;
			int k = (i == 2) ? 3 : i;
			nuevo_hijo.st.orientacion = (nuevo_hijo.st.orientacion + k) % 4;

			bool push = true;

			if (i != 0 or !HayObstaculoDelante(nuevo_hijo.st))
			{

				if (generados.find(nuevo_hijo.st) == generados.end())
				{
					nuevo_hijo.secuencia.push_back(actions[i]);
					cola.push(nuevo_hijo);
				}
			}
		}
		//SalidaCola(cola);
		if (!cola.empty())
		{
			current = cola.front();
		}
	}

	cout << "\nTerminada la búsqueda";
	if (EsDestino(current.st, destino))
	{
		cout << "\nMostrando plan..";
		plan = current.secuencia;
		cout << "Longitud del plan: " << plan.size() << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
		return false;
	}
}

/*FUNCIONES DE DEPURACIÓN

	void SalidaRecorrido(estado actual)
	{
		cout << "[ " ;
		for (int i =0; i < actual.objetivosvisitados.size(); i ++ )
		{
			string salida=actual.objetivosvisitados[i]?"si":"no";
			cout << i+1 << ": "<< salida << " ";

		}
		cout << "]" << endl;
	}

	void SalidaCola(priority_queue<nodoA> cola)
	{
		priority_queue<nodoA> otra = cola;
		cout << "COLA ACTUAL: [";
		while (!otra.empty())
		{
			nodoA uno = otra.top();

			cout << "(" << uno.st.fila << ", " << uno.st.columna << ")-(" << uno.costo + uno.h << ") ";
			otra.pop();
		}
		cout << "]" << endl;
	}
*/

/*

 __  __ __ __ __  ____ __       __ __
 ||\ || || || || ||    ||       || ||
 ||\\|| || \\ // ||==  ||       || ||
 || \|| ||  \V/  ||___ ||__|    || ||
                                     


(Los titulitos así quedaban genial jeje)

 __   __   __  ___  __                  ___  __   __         ___ 
/  ` /  \ /__`  |  /  \    |  | |\ | | |__  /  \ |__)  |\/| |__  
\__, \__/ .__/  |  \__/    \__/ | \| | |    \__/ |  \  |  | |___ 
                                                                 

																 */

/*
Estructura que sirve para darle un orden completo al conjunto de estados (cerrados) en los algoritmos de este nivel. Además de comparar 
fila, columna y orientación, compara los objetos, considerando que el Bikini es superior a las Zapatillas.
*/
struct ComparaEstadosNivel2
{
	bool operator()(const estado &a, const estado &n) const
	{
		if ((a.fila > n.fila) or (a.fila == n.fila and a.columna > n.columna) or
			(a.fila == n.fila and a.columna == n.columna and a.orientacion > n.orientacion))
			return true;
		else if ((a.fila == n.fila and a.columna == n.columna and a.orientacion == n.orientacion) and ((a.objeto != 0) and (n.objeto == 0)))
			return true;
		else if (a.fila == n.fila and a.columna == n.columna and a.orientacion == n.orientacion and ((a.objeto != 0) and (n.objeto != 0) and ((a.objeto == 1) and n.objeto == 2)))
			return true;
		else
			return false;
	}
};

/*Estructura de datos que incorpora un Costo y un valor correspondiente a la heurística a un nodo como los que teníamos antes. 
Es necesario utilizar la sobrecarga del operador porque se utilizará en una cola de prioridad.*/
struct nodoCosto
{

	estado st;
	list<Action> secuencia;
	unsigned int costo;
	unsigned int h;
	bool operator<(const nodoCosto &otro) const
	{
		return costo + h > otro.costo + otro.h;
	}
};

/**
 * @brief Función correspondiente al cálculo del costo según las normas del mapa
 * @param i: acción que se acaba de realizar
 * @param celda: tipo de casilla anterior
 * @param objeto: objeto que tiene el personaje en el estado actual
 * @return int: costo correspondiente a la acción en la casilla
 * 
 *
 * */
int calcularCosto(int i, const char &celda, const nodoCosto &hijo)
{
	unsigned int costo;
	if (celda == 'A')
		costo = (i == 0) ? ((hijo.st.objeto == 1) ? 10 : 200) : ((hijo.st.objeto == 1) ? 5 : 500);
	else if (celda == 'B')
		costo = (i == 0) ? ((hijo.st.objeto == 2) ? 15 : 100) : ((hijo.st.objeto == 2) ? 1 : 3);
	else if (celda == 'T')
		costo = 2;
	else
		costo = 1;
	return costo;
}

/**
 * @brief Función que produce como salida un plan según el algoritmo de Costo Uniforme aplicado un objetivo
 * @param origen: estado del que se comprueba
 * @param destino: estado objetivo al que hay que llegar
 * @param plan: plan que seguirá el personaja
 * @return bool: si se ha conseguido o no
 * */

bool ComportamientoJugador::pathFinding_CostoUniforme(const estado &origen, const estado &destino, list<Action> &plan)
{
	//Borramos la lista
	cout << "Creando plan \n";
	plan.clear();

	set<estado, ComparaEstadosNivel2> generados; // Lista de Cerrados
	priority_queue<nodoCosto> cola;				 // Lista de Abiertos

	nodoCosto current;
	current.st = origen;
	current.secuencia.empty();
	current.costo = 0;
	current.st.objeto = actual.objeto;
	current.st.objeto = actual.objeto;

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};
	while (!cola.empty() and !EsDestino(current.st, destino))
	{

		cola.pop();
		if (generados.find(current.st) == generados.end())
		{
			generados.insert(current.st);
			char tipocelda = mapaResultado[current.st.fila][current.st.columna];
			for (int i = 0; i < 3; i++)
			{
				nodoCosto hijo = current;
				int k = (i == 2) ? 3 : i;
				hijo.st.orientacion = (hijo.st.orientacion + k) % 4;

				if (tipocelda == 'D')
				{
					hijo.st.objeto = 2;
				}
				else if (tipocelda == 'K')
				{
					hijo.st.objeto = 1;
				}

				hijo.costo += calcularCosto(i, tipocelda, hijo);
				hijo.h = 0;
				if (i != 0 or !HayObstaculoDelante(hijo.st))
				{

					if (generados.find(hijo.st) == generados.end())
					{
						hijo.secuencia.push_back(actions[i]);
						cola.push(hijo);
					}
				}
			}
		}
		if (!cola.empty())
		{
			current = cola.top();
		}
	}

	cout << "\nTerminada la búsqueda";
	if (EsDestino(current.st, destino))
	{
		cout << "\nMostrando plan..";
		plan = current.secuencia;
		cout << "Longitud del plan: " << plan.size() << endl;
		cout << "Costo del plan: " << current.costo << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
		return false;
	}
}

/*
           __   __   __    ___        __          
 /\  |    / _` /  \ |__) |  |   |\/| /  \     /\  
/~~\ |___ \__> \__/ |  \ |  |   |  | \__/    /~~\ 
                                                  

*/

/**
 * @brief Función que representa la heurística aplicada al problema: distancia Manhattan entre dos estados.
 * @param st1: estado inicio
 * @param st2: estado final
 * @return int: el valor de la heurística para el estado st1.
 * */
int ComportamientoJugador::h(const estado &st1, const estado &st2)
{
	return abs(st1.fila - st2.fila) + abs(st1.columna - st2.columna);
}

/**
 * @brief Función que produce como salida un plan según el algoritmo de A* para un solo objetivo
 * @param origen: estado del que se comprueba
 * @param destinos: destino que se tiene que visitar
 * @param plan: plan que seguirá el personaje
 * @return bool: si se ha conseguido o no
 * 
 * 
 *
 * */
bool ComportamientoJugador::pathFinding_A(const estado &origen, const estado &destino, list<Action> &plan, int &costo)
{
	//Borramos la lista
	cout << "Creando plan \n";
	if (actual.objeto == 1)
		cout << "Tengo bikini" << endl;
	else if (actual.objeto == 2)
		cout << "Zapas" << endl;
	else
		cout << " No tengo objeto" << endl;
	plan.clear();

	set<estado, ComparaEstadosNivel2> generados; // Lista de Cerrados
	priority_queue<nodoCosto> cola;				 // Lista de Abiertos

	nodoCosto current;
	current.st = origen;
	current.secuencia.empty();
	current.costo = 0;
	current.st.objeto = actual.objeto;

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};
	while (!cola.empty() and !EsDestino(current.st, destino))
	{
		cola.pop();
		if (generados.find(current.st) == generados.end())
		{
			generados.insert(current.st);

			for (int i = 0; i < 3; i++)
			{
				nodoCosto hijo = current;
				int k = (i == 2) ? 3 : i;
				hijo.st.orientacion = (hijo.st.orientacion + k) % 4;

				char tipocelda = mapaResultado[hijo.st.fila][hijo.st.columna];
				if (tipocelda == 'D')
				{
					hijo.st.objeto = 2;
				}

				else if (tipocelda == 'K')
				{
					hijo.st.objeto = 1;
				}

				hijo.costo += calcularCosto(i, tipocelda, hijo);
				hijo.h = h(hijo.st, destino);
				if (i != 0 or !HayObstaculoDelante(hijo.st))
				{

					if (generados.find(hijo.st) == generados.end())
					{
						hijo.secuencia.push_back(actions[i]);
						cola.push(hijo);
					}
				}
			}
		}
		if (!cola.empty())
		{
			current = cola.top();
		}
	}

	cout << "\nTerminada la búsqueda";
	if (EsDestino(current.st, destino))
	{
		cout << "\nMostrando plan..";
		plan = current.secuencia;
		costo = current.costo;
		cout << "Longitud del plan: " << plan.size() << endl;
		cout << "Costo del plan: " << current.costo << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
		return false;
	}
}

/*


||\ || || || || ||    ||       || || ||
||\\|| || \\ // ||==  ||       || || ||
|| \|| ||  \V/  ||___ ||__|    || || ||



*/

/*
Estructura que sirve para darle un orden completo al conjunto de estados (cerrados) en los algoritmos de este nivel. Además de comparar 
fila, columna y orientación, compara los estados visitados de uno y de otro. De ser iguales, pasa a comparar los objetos, considerando que
el Bikini es superior a las Zapatillas.
*/
struct ComparaEstadosNivel3
{
	bool operator()(const estado &a, const estado &n) const
	{
		if (!((a.fila > n.fila) or (a.fila == n.fila and a.columna > n.columna) or
			  (a.fila == n.fila and a.columna == n.columna and a.orientacion > n.orientacion)))
		{
			if (a.fila == n.fila and a.columna == n.columna and a.orientacion == n.orientacion)
			{
				//Itero en los set de cada uno
				bool condicion2 = true;
				bool mayor = true;
				for (int i = 0; i < a.objetivosvisitados.size() && condicion2; i++)
				{
					if (!n.objetivosvisitados[i] && a.objetivosvisitados[i])
					{
						return true;
					}
					else if (n.objetivosvisitados[i] && !a.objetivosvisitados[i])
					{
						return false;
					}
				}
				if (condicion2)
				{
					if ((a.fila == n.fila and a.columna == n.columna and a.orientacion == n.orientacion) and ((a.objeto != 0) and (n.objeto == 0)))
						return true;
					else if (a.fila == n.fila and a.columna == n.columna and a.orientacion == n.orientacion and ((a.objeto != 0) and (n.objeto != 0)) and ((a.objeto == 1) and n.objeto == 2))
						return true;
					else
						return false;
				}
			}

			else
				return false;
		}
		else
			return true;
	}
};

/**
 * @brief Función que indica si un estado ya ha recorrido los destinos deseados
 * @param origen: estado del que se comprueba
 * @param destinos: lista de destinos que se tienen que visitar
 * @return true si ya ha visitado todos, false si no
 * 
 * NOTA: La función EsDestino comprueba si un estado pasado como parámetro está en la lista de destinos
 * pasada como parámetro también.
 * */

bool ComportamientoJugador::Check(estado &origen, const list<estado> &destinos)
{
	bool resultado;
	int visitados = count(origen.objetivosvisitados.begin(), origen.objetivosvisitados.end(), true);
	int i = EsDestino(origen, destinos);

	//Si estamos en un destino que no está incluido y es el último
	if ((visitados == destinos.size() - 1) && (i != -1 && !origen.objetivosvisitados[i]))
	{
		//Si están todos visitados menos uno, y justo estamos en ese, el recorrido se termina

		return true;
	}

	//Si no es el último
	else if (i != -1 && !origen.objetivosvisitados[i])
	{
		origen.objetivosvisitados[i] = true;
	}
	return false;
}

/**
 * @brief Función que produce como salida un plan según el algoritmo de Costo Uniforme aplicado a varios objetivos
 * @param origen: estado del que se comprueba
 * @param destinos: lista de destinos que se tienen que visitar
 * @param plan: plan que seguirá el personaja
 * @return bool: si se ha conseguido o no
 * */

bool ComportamientoJugador::pathFinding_CostoUniformeVarios(const estado &origen, const list<estado> &destinos, list<Action> &plan)
{
	cout << "Creando plan \n";
	plan.clear();

	set<estado, ComparaEstadosNivel3> generados; // Lista de Cerrados
	priority_queue<nodoCosto> cola;				 // Lista de Abiertos

	nodoCosto current;
	current.st = origen;
	current.secuencia.empty();
	current.costo = 0;
	current.st.objeto = actual.objeto;

	for (int i = 0; i < destinos.size(); i++)
	{
		current.st.objetivosvisitados.push_back(false);
	}

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};

	while (!cola.empty() && (!Check(current.st, destinos)))
	{
		cola.pop();
		generados.insert(current.st);

		for (int i = 0; i < 3; i++)
		{

			//Expando el nodo
			nodoCosto hijo = current;
			int k = (i == 2) ? 3 : i;
			hijo.st.orientacion = (hijo.st.orientacion + k) % 4;

			//Calculo el costo del nuevo hijo
			char tipocelda = mapaResultado[hijo.st.fila][hijo.st.columna];
			if (tipocelda == 'D')
			{
				hijo.st.objeto = 2;
			}
			else if (tipocelda == 'K')
			{
				hijo.st.objeto = 1;
			}

			hijo.costo += calcularCosto(i, tipocelda, hijo);
			//Heurística para A* (ahora no)
			hijo.h = 0;

			//Inserto si no se ha insertado ya
			if (i != 0 or !HayObstaculoDelante(hijo.st))
			{

				if (generados.find(hijo.st) == generados.end())
				{
					hijo.secuencia.push_back(actions[i]);
					cola.push(hijo);
				}
			}
		}
		if (!cola.empty())
		{
			current = cola.top();
		}
	}

	cout << "\nTerminada la búsqueda";
	if (EsDestino(current.st, destinos) != -1)
	{
		cout << "\nMostrando plan..";
		plan = current.secuencia;
		cout << "Longitud del plan: " << plan.size() << endl;
		cout << "Costo del plan: " << current.costo << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
		return false;
	}
}

/**
 * @brief Función que representa la heurística aplicada al problema: distancia Manhattan pero aplicada a varios objetivos.
 * @param st1: estado inicio
 * @param destinos: lista de destinos que se tienen que comprobar
 * @return int: el valor de la heurística para el estado st1.
 * */
int ComportamientoJugador::hvarios(const estado &st1, const list<estado> &destinos)
{
	int min = -1;
	for (list<estado>::const_iterator it = destinos.begin(); it != destinos.end(); it++)
	{
		if (unsigned int medida = h(st1, (*it)); medida < min)
		{
			min = medida;
		}
	}
	return min;
}

/**
 * @brief Función que produce como salida un plan según el algoritmo de A* aplicado a varios objetivos
 * @param origen: estado del que se comprueba
 * @param destinos: lista de destinos que se tienen que visitar
 * @param plan: plan que seguirá el personaje
 * @return bool: si se ha conseguido o no
 * */
bool ComportamientoJugador::pathFinding_AVarios(const estado &origen, const list<estado> &destinos, list<Action> &plan)
{
	cout << "Creando plan \n";
	plan.clear();

	set<estado, ComparaEstadosNivel3> generados; // Lista de Cerrados
	priority_queue<nodoCosto> cola;				 // Lista de Abiertos

	nodoCosto current;
	current.st = origen;
	current.secuencia.empty();
	current.costo = 0;
	current.st.objeto = actual.objeto;

	for (int i = 0; i < destinos.size(); i++)
	{
		current.st.objetivosvisitados.push_back(false);
	}

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};

	while (!cola.empty() && (!Check(current.st, destinos)))
	{
		cola.pop();
		generados.insert(current.st);

		for (int i = 0; i < 3; i++)
		{

			//Expando el nodo
			nodoCosto hijo = current;
			int k = (i == 2) ? 3 : i;
			hijo.st.orientacion = (hijo.st.orientacion + k) % 4;

			//Calculo el costo del nuevo hijo
			char tipocelda = mapaResultado[hijo.st.fila][hijo.st.columna];
			if (tipocelda == 'D')
			{
				hijo.st.objeto = 2;
			}
			else if (tipocelda == 'K')
			{
				hijo.st.objeto = 1;
			}

			hijo.costo += calcularCosto(i, tipocelda, hijo);
			//Heurística para A* (ahora no)
			hijo.h = hvarios(hijo.st, destinos);

			//Inserto si no se ha insertado ya
			if (i != 0 or !HayObstaculoDelante(hijo.st))
			{

				if (generados.find(hijo.st) == generados.end())
				{
					hijo.secuencia.push_back(actions[i]);
					cola.push(hijo);
				}
			}
		}
		if (!cola.empty())
		{
			current = cola.top();
		}
	}

	cout << "\nTerminada la búsqueda";
	if (EsDestino(current.st, destinos) != -1)
	{
		cout << "\nMostrando plan..";
		plan = current.secuencia;
		cout << "Longitud del plan: " << plan.size() << endl;
		cout << "Costo del plan: " << current.costo << endl;
		PintaPlan(plan);
		// ver el plan en el mapa
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		cout << "No encontrado plan\n";
		return false;
	}
}

/*

 __  __ __ __ __  ____ __       __ __ __
 ||\ || || || || ||    ||       || || ||
 ||\\|| || \\ // ||==  ||       || \\ //
 || \|| ||  \V/  ||___ ||__|    ||  \V/ 
                                        

*/

/**
 * @brief Función correspondiente a la modificación del costo para el A* modificado
 * @param i: acción que se acaba de realizar
 * @param celda: tipo de casilla anterior
 * @param objeto: objeto que tiene el personaje en el estado actual
 * @return int: costo correspondiente a la acción en la casilla
 * 
 * 
 * NOTA: La modificación se basa en cambiar el peso de las casillas marcadas como desconocidas ("?") según el porcentaje 
 * de casillas descubiertas que tenemos.
 *
 * */
int ComportamientoJugador::calcularCostoModificado(int i, const char &celda, int objeto)
{
	unsigned int costo;
	if (celda == 'A')
		costo = (i == 0) ? ((objeto == 1) ? 10 : 200) : ((objeto == 1) ? 5 : 500);
	else if (celda == 'B')
		costo = (i == 0) ? ((objeto == 2) ? 15 : 100) : ((objeto == 2) ? 1 : 3);
	else if (celda == 'T')
		costo = 2;
	else if (celda == '?')
		costo = modificacionDesconocida;
	else
		costo = 1;
	return costo;
}

/**
 * @brief Función que produce como salida un plan según el algoritmo de A* modificado
 * @param origen: estado del que se comprueba
 * @param destinos: lista de destinos que se tienen que visitar
 * @param plan: plan que seguirá el personaje
 * @return bool: si se ha conseguido o no
 * 
 * 
 * NOTA: La modificación se basa en cambiar el peso de las casillas marcadas como desconocidas ("?") según el porcentaje 
 * de casillas descubiertas que tenemos.
 *
 * */
bool ComportamientoJugador::pathFinding_Amodificado(const estado &origen, const estado &destino, list<Action> &plan, int &costo)
{
	plan.clear();

	set<estado, ComparaEstadosNivel2> generados; // Lista de Cerrados
	priority_queue<nodoCosto> cola;				 // Lista de Abiertos

	nodoCosto current;
	current.st = origen;
	current.secuencia.empty();
	current.costo = 0;
	current.st.objeto = actual.objeto;

	cola.push(current);
	Action actions[] = {actFORWARD, actTURN_R, actTURN_L};
	while (!cola.empty() and !EsDestino(current.st, destino))
	{

		cola.pop();
		if (generados.find(current.st) == generados.end())
		{
			generados.insert(current.st);

			for (int i = 0; i < 3; i++)
			{
				nodoCosto hijo = current;
				int k = (i == 2) ? 3 : i;
				hijo.st.orientacion = (hijo.st.orientacion + k) % 4;

				bool push = true;
				char tipocelda = mapaResultado[hijo.st.fila][hijo.st.columna];
				if (tipocelda == 'D')
				{
					hijo.st.objeto = 2;
				}

				else if (tipocelda == 'K')
				{
					hijo.st.objeto = 1;
				}

				hijo.costo += calcularCostoModificado(i, tipocelda, hijo.st.objeto);
				hijo.h = h(hijo.st, destino);
				if (i != 0 or !HayObstaculoDelante(hijo.st))
				{

					if (generados.find(hijo.st) == generados.end())
					{
						hijo.secuencia.push_back(actions[i]);
						cola.push(hijo);
					}
				}
			}
		}

		if (!cola.empty())
		{
			current = cola.top();
		}
	}
	if (EsDestino(current.st, destino))
	{
		plan = current.secuencia;
		costo = current.costo;
		PintaPlan(plan);
		VisualizaPlan(origen, plan);
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @brief Función que actualiza el valor de las casillas a partir la información obtenida con los sensores
 * @param terreno: información de sensores.terreno
 * @param fila: fila donde nos encontramos
 * @param columna: columna donde nos encontramos
 * @param sentido: la orientación actual 
 * 
 * NOTA: Esta función también actualiza la cantidad de casillas descubiertas, que nos ayudará con la modificación de los pesos.
 * */
void ComportamientoJugador::ActualizarMapa(vector<unsigned char> terreno, int fila, int columna, Orientacion sentido)
{
	mapaResultado[fila][columna] = terreno[0];
	int sensor = 1;

	switch (sentido)
	{
	case norte:

		for (int i = 1; i < 4; i++)
		{

			for (int j = -i; j <= i; j++)
			{
				if (mapaResultado[fila - i][columna + j] == '?')
				{
					mapaResultado[fila - i][columna + j] = terreno[sensor];
					casillasDescubiertas++;
				}
				if (terreno[sensor] == 'X')
				{

					estado recarg;
					recarg.fila = fila - i;
					recarg.columna = columna + j;
					recarg.orientacion = 0;
					recargas.insert(recarg);
				}

				sensor++;
			}
		}
		break;
	case este:
		for (int i = 1; i < 4; i++)
		{

			for (int j = -i; j <= i; j++)
			{
				if (mapaResultado[fila + j][columna + i] == '?')
				{
					mapaResultado[fila + j][columna + i] = terreno[sensor];
					casillasDescubiertas++;
				}
				if (terreno[sensor] == 'X')
				{

					estado recarg;
					recarg.fila = fila + j;
					recarg.columna = columna + i;
					recarg.orientacion = 0;
					recargas.insert(recarg);
				}
				sensor++;
			}
		}
		break;
	case sur:
		for (int i = 1; i < 4; i++)
		{

			for (int j = -i; j <= i; j++)
			{
				if (mapaResultado[fila + i][columna - j] == '?')
				{
					mapaResultado[fila + i][columna - j] = terreno[sensor];
					casillasDescubiertas++;
				}
				if (terreno[sensor] == 'X')
				{

					estado recarg;
					recarg.fila = fila + i;
					recarg.columna = columna - j;
					recarg.orientacion = 0;
					recargas.insert(recarg);
				}
				sensor++;
			}
		}
		break;
	case oeste:
		for (int i = 1; i < 4; i++)
		{

			for (int j = -i; j <= i; j++)
			{
				if (mapaResultado[fila - j][columna - i] == '?')
				{
					mapaResultado[fila - j][columna - i] = terreno[sensor];
					casillasDescubiertas++;
				}
				if (terreno[sensor] == 'X')
				{

					estado recarg;
					recarg.fila = fila - j;
					recarg.columna = columna - i;
					recarg.orientacion = 0;
					recargas.insert(recarg);
				}
				sensor++;
			}
		}

		break;
	}
}

/**
 * @brief Función que se encarga de actualizar el objeto y de realizar el plan de recarga, en caso de ser necesario.
 * @param tipocelda: tipo de celda donde nos encontramos
 * @param bateria: cantidad de batería restante 
 * 
 * NOTA: Si pasamos por una casilla de recarga y el valor de la batería no había pasado el umbraldinamico, recargamos hasta que llegue
 * a 2000.
 * */
void ComportamientoJugador::ProcesarCasilla(char tipocelda, int bateria)
{
	switch (tipocelda)
	{
	case 'D':
		actual.objeto = 2;
		break;
	case 'K':
		actual.objeto = 1;
		break;
	case 'X':
		cout << "Estoy en una casilla de recarga" << endl;
		//Si estamos en una casilla de recarga, nos quedamos hasta que recargamos una cantidad prefijada
		if (recarga)
		{
			cout << "recargo " << cantidadRecarga << endl;
			recarga = false;
			justCharged = true;
			hayPlan = true;
			plan.clear();
			for (int i = 0; i < cantidadRecarga; i++)
				plan.push_back(actIDLE);
			PintaPlan(plan);

			break;
		}
		else if (bateria < 2000)
		{
			recarga = false;
			justCharged = true;
			hayPlan = true;
			plan.clear();
			for (int i = 0; i < 2000 - bateria; i++)
				plan.push_back(actIDLE);
			break;
		}
	}
}

/**
 * @brief Función que se encarga de comprobar is hemos llegado al final de una misión
 * NOTA: Esta función actualizará los valores de numdestinosconseguidos y de misioncompletada, que marcan el progreso del personaje.
 * */
void ComportamientoJugador::FinalMision()
{
	list<estado>::const_iterator i = BuscarDestino(actual, objetivos);
	if (i != objetivos.end() && objetivos.size() == 1)
	{
		numdestinosConseguidos++;
		misionCompletada = true;
	}
}

/**
 * @brief Función que reinicia los objetivos, a partir de los obtenidos con los sensores de objetivos.
 * @param num_destinos: numero de destinos de la siguiente misión
 * @param sensdestinos: sensores de destinos de la
 * @param nivel: nivel en el que nos encontramos
 * 
 * NOTA: Si estamos en los niveles 1,2, y 3, esta función se realiza una vez al inicio de la ejecución, se actualizará la variable
 * destinos para asegurarlo, y se coge como destino el primero. Si estamos en el nivel 4, esta función se realizará cada vez que se 
 * complete una misión, y se cogerá como destino inmediato el destino más cercano.
 * */
void ComportamientoJugador::ReiniciarObjetivos(int num_destinos, vector<unsigned int> sensdestinos, int nivel)
{

	objetivos.clear();
	for (int i = 0; i < num_destinos; i++)
	{
		estado aux;
		aux.fila = sensdestinos[2 * i];
		aux.columna = sensdestinos[2 * i + 1];
		if (aux.fila != actual.fila or aux.columna != actual.columna)
		{
			objetivos.push_back(aux);
		}
	}
	destinos = true;
	misionCompletada = false;
	if (nivel == 4)
	{
		destino = ObjetivoCercano();
		cout << "El destino siguiente es f" << destino.fila << " c" << destino.columna << endl;
	}
	else
		destino = objetivos.front();
}

/**
 * @brief Función que se encarga de devolver la casilla de recarga más cercanna al estado actual, basándose en las casillas de recarga
 * ya conocidas. Para no utilizar demasiado las llamadas al algoritmo A*, usamos como medida de cercanía la heurística de la distancia Manhattan.
 * 
 * */
estado ComportamientoJugador::RecargaCercana()
{
	if (!recargas.empty())
	{
		estado recargaCercana = (*recargas.begin());
		int minh = h(actual, recargaCercana);
		for (estado e : recargas)
		{
			if (h(actual, e) < minh)
			{
				recargaCercana = e;
				minh = h(actual, e);
			}
		}
		return recargaCercana;
	}
}

/**
 * @brief Función que se encarga de devolver la casilla de objetivo más cercanna al estado actual. En este caso, usamos como medida de cercanía
 * el costo hasta el objetivo. Para optimizar en llamadas, almacenamos el plan de dicho objetivo.
 * 
 * */
estado ComportamientoJugador::ObjetivoCercano()
{
	int costo = 3000;
	int nuevo;
	estado cercano;
	int media = 0;
	list<Action> plan1;
	int numobjetivos = objetivos.size();
	for (list<estado>::const_iterator it = objetivos.begin(); it != objetivos.end(); it++)
	{
		if (pathFinding_Amodificado(actual, (*it), plan1, nuevo) && (nuevo < costo))
		{

			cercano = (*it);
			costo = nuevo;
			plan = plan1;
			hayPlan = true;
		}
		media += nuevo;
	}
	umbralDinamico = 1.3 * media;
	return cercano;
}
