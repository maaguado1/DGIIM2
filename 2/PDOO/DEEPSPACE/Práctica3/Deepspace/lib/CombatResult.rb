#encoding:utf-8

module Deepspace
  
# Enum that controls the possible results of a combat between 
# a spacial station and an enemy ship.

module CombatResult
    ENEMYWINS=:enemywins;
    NOCOMBAT=:nocombat;
    STATIONESCAPES=:stationescapes;
    STATIONWINS=:stationwins;
end #enum CombatResult


end # module Deepspace