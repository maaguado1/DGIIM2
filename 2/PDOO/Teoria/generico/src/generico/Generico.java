/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generico;

/**
 *
 * @author Profesor
 */
interface Mandador {
    public void mandar();
}

class Lider implements Mandador {
    @Override
    public void mandar() {
        System.out.println("Soy un líder mandando");
    }
}

class Cantante implements Mandador {
    @Override
    public void mandar() {
        System.out.println("Soy un cantante mandando");
    }
}

class Jefe implements Mandador {
    @Override  //Si no ponemos el override no da un error, obtenemos un warning pero no es un error(redefine). Si ahora cambiamos el número de parámetros, no se podría decir que está redefiniendo, pues estaría sobrecargando.
    public void mandar() {
        System.out.println("Soy un Jefe mandando");
    }
}

class GrupoOrganizado<T extends Mandador> { //prueba a quitar "extends Mandador", si se pone implements da error. Hace que el T cumpla la condicion.
    private T mandador;
    public GrupoOrganizado(T m) {
        mandador=m;
    }
    
    public void posturaOficial() { //si quitamos el extens no nos aseguramos de que tenga el método mandar.
        mandador.mandar();
    }
}

class GrupoMusica extends GrupoOrganizado<Cantante> {
    public GrupoMusica(Cantante c) {
        super(c);
    }    
    public void tocarMusica() {
        System.out.println("Tocando música....");        
    }
}

class GrupoPersonas extends GrupoOrganizado<Lider> {
    public GrupoPersonas(Lider l) {
        super(l);
    }    
    public void andar() {
        System.out.println("Andando....");        
    }
}

class Empresa extends GrupoOrganizado<Jefe> {
    public Empresa(Jefe j) {
        super(j);
    }    
    public void hacerNegocios() {
        System.out.println("Haciendo negocios....");        
    }
}

class Auxiliar {
    public static void metodoAuxiliar(GrupoOrganizado<? extends Mandador> go) {
        go.posturaOficial();
    }
    
    public static void metodoAuxiliar2(GrupoOrganizado<Mandador> go) {
        go.posturaOficial();
    }
}

public class Generico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cantante c=new Cantante();
        GrupoMusica gm=new GrupoMusica(c);
        gm.posturaOficial();
        gm.tocarMusica();
        
        Jefe j=new Jefe();
        Empresa e=new Empresa(j);
        e.posturaOficial();
        e.hacerNegocios();
        
        Lider l=new Lider();
        GrupoPersonas gp=new GrupoPersonas(l);
        gp.posturaOficial();
        gp.andar();
        
        /** ERROR ejecucion
        Mandador m=new Cantante();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                //new Lider();
        GrupoPersonas gp2=new GrupoPersonas((Lider) m);
        gp2.posturaOficial();
        gp2.andar();*/   
        
        Auxiliar.metodoAuxiliar(gm);
        Auxiliar.metodoAuxiliar(e);
        Auxiliar.metodoAuxiliar(gp);
                
        //Auxiliar.metodoAuxiliar2(gm);
        //Auxiliar.metodoAuxiliar2(e);
        //Auxiliar.metodoAuxiliar2(gp);                
    }
}
