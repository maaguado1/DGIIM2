

## TEMA 2: PROGRAMACIÓN PARALELA

### Lección 4: Herramientas, estilos y estructuras

#### A) Problemas que plantea la programación paralela al programador

Surgen nuevos problemas como por ejemplo:

- La división en unidades de cómputo independientes (=tareas).
- Agrupación y asignación de las tareas(=carga de trabajo) en procesos o threads, es decir la agrupación en flujos de instrucción.
- Asignación a procesadores o núcleos (hardware).
- Sincronización y comunicación (variables compartidas en el código).

Estos problemas los aborda la herramienta de programación o el SO.

¿Cómo se puede paralelizar?

El **punto de partida** es:

- Código secuencial, intentamos analizar qué parte tarda más para poder mejorarla.
- Descripción o definición de la aplicación.
- Definir un programa paralelo parecido.
- Realizamos versiones paralelas u optimizadas de bibliotecas de funciones (_Blas_ = Basic linear algebra subrutine o _LA PACK_ = Linear Algebra Package).

Además, hay distintos **modos de programación MIMD**:

- SPMD (Single-Program Multiple Data): el paralelismo reside en que cada copia de mi programa ejecuta con datos distintos, que son los flujos que se asignan  cada procesador. Imita al SIMD, es decir utiliza paralelismo de datos o de bucle.
- MPMD (Multiple program multiple data): Cada procesador ejecuta programas distintos, utiliza paralelismo de tareas o funciones.
- Modos combinados, mezcla entre ambas opciones.

#### B) Herramientas para la programación paralela

Para empezar se dispone, a medida que se va disminuyendo el nivel de abstracción:

1. _Compiladores paralelos_: extraen la paralelización de forma automática.
2. _Lenguajes paralelos_: tienen construcciones particulares y bibliotecas de funciones que requieren un compilador exclusivo. Las API tienen un conjunto de directivas y funciones que se añaden a un compilador de un lenguaje secuencial. El paralelismo implícito es localizado por el programador.
   - _API funciones + directivas (OPENMP)_, se puede paralelizar de dos maneras:
     - Construcciones del lenguaje específicas+ funciones.
     - Lenguaje secuencial + directivas+ funciones
3. _API y funciones_ (MPI), que utilizan lenguaje secuencial y funciones. Las API de funciones consisten en una biblioteca de funcones que se añaden a un compilador de un lenguaje secuencial habitual. La asignación de tareas la realiza el programador.

Entonces, en resumen, las herramientas permiten de forma implícita o explícita:

- Localizar paralelismo y descomponer en tareas independientes (**decomposition**).
- Asignar las tareas (= carga de trabajo, incluye código y datos) a procesos y threads(**scheduling**).
- Crear y terminar procesos y threads (**openmp**).
- Comunicar y sincronizar procesos y threads (el programador indica dónde).
- Asignar procesos a Unidad de Procesamiento (**mapping**), por parte del programador, la herramienta de programación o el SO, ver la lista de arriba.

En OpenMP, junto con otras herramientas, también disponemos de directivas o funciones colectivas, que facilitan el trabajo del programador.

**Bibliotecas de funciones**

Cuando se trabaja con bibliotecas de funciones el cuerpo de los procesos se escribe en lenguaje secuencial y para la creación y gestión de los procesos, comunicación y sincronización se usan funciones de la biblioteca.

En este tema consideramos que la carga de trabajo, que incluye código y datos, es equivalente a las tareas.

##### Comunicaciones colectivas

Las herraminetas de programación paralela, además de comunicación entre dos procesos, pueden ofrecer comunicaciones en las que intervienene varios procesos. Estos esquemas pueden incrementar la eficiencia del programa paralelo.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-36-18.png" alt="Captura de pantalla de 2020-04-18 13-36-18" style="zoom: 33%;" />



###### 	1. Comunicación uno-a-todos

- Difusión (broadcast): <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-38-50.png" alt="Captura de pantalla de 2020-04-18 13-38-50" style="zoom:33%;" />				

- Dispersión(scatter):<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-38-54.png" alt="Captura de pantalla de 2020-04-18 13-38-54" style="zoom:33%;" />



En el primero, a cada P le llega la misma información, y en el segundo, se divide la información y a cada P le llega una porción distinta.

###### 2. Comunicación todos-a-uno

- Reducción:<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-41-16.png" alt="Captura de pantalla de 2020-04-18 13-41-16" style="zoom:33%;" />

En reducción, se junta la información obtenida de cada procesador según una función predefinida f, obteniendo un único valor, la función f es conmutativa y asociativa.

- Acumulación(gather):	 <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-41-58.png" alt="Captura de pantalla de 2020-04-18 13-41-58" style="zoom:33%;" />

Los mensajes se reciben de forma concatenada en el receptor de una estructura vectorial.

###### 3. Comunicación múltiple uno-a-uno

Si todos los componentes del grupo envían y reciben se llama permutación, si no se llama **desplazamiento**.

- Permutacion rotación:	<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-44-19.png" alt="Captura de pantalla de 2020-04-18 13-44-19" style="zoom:33%;" />

- Permutación **baraje-2**:	 <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-44-49.png" alt="Captura de pantalla de 2020-04-18 13-44-49" style="zoom:33%;" />

###### 4. Comunicación todos-a-todos

- Todos difunden( **all-broadcast**):	<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-46-37.png" alt="Captura de pantalla de 2020-04-18 13-46-37" style="zoom:33%;" />

En all-broadcast cada procesador difunde a todos la misma información (difusión). Las n transferencias recibidas por un proceso se concatenan en función del identificador del proceso que las envía, de forma que todos los procesos reciben el mismo.

- Todos dispersan (**all-scatter**): 	<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-47-54.png" alt="Captura de pantalla de 2020-04-18 13-47-54" style="zoom:33%;" /> 

  Los procesos concatenen diferentes transferencias.

###### 5. Servicios compuestos

- Todos combinan:		 <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-48-35.png" alt="Captura de pantalla de 2020-04-18 13-48-35" style="zoom:33%;" />

Cada procesador tiene una función predefinida que combina la información obtenida de los demás.

- Recorrido prefijo paralelo: 	<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-50-08.png" alt="Captura de pantalla de 2020-04-18 13-50-08" style="zoom:33%;" />
- Recorrido sufijo paralelo:      <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-50-34.png" alt="Captura de pantalla de 2020-04-18 13-50-34" style="zoom:33%;" />

Las funciones colectivas son implementadas por la Interfaz de Paso de Mensajes (MPI).



#### C) Estilos de programación paralela

1. **Paso de mensajes** (message passing): en multicomputadores, donde la memoria no se comparte. Ofrece herramientas para copiar datos de un espacio de memoria a otros.

   <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-55-17.png" alt="Captura de pantalla de 2020-04-18 13-55-17" style="zoom: 50%;" />

   Utiliza:

   - Lenguajes de programación:  Ada, Occam

   - API (bibliotecas de funciones): MPI, PVM

     

2. **Variables compartidas** (shared memory)  en multiprocesadores, pues todos los procesadores tienen acceso a las zonas de memoria compartidas.

   <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-55-26.png" alt="Captura de pantalla de 2020-04-18 13-55-26" style="zoom:50%;" />

   Utiliza:

   - Lenguajes de programación: Java, Ada
   - API(directivas+funciones): OPENMP
   - API (bibliotecas de funciones): PosIx threads
   
   Hay que tener en cuenta que la herramienta tiene que ofrecer herramientas de sincronización, pues las funciones comparten el mismo espacio de direcciones.



3. **Paralelismo de datos** (data parallelism):  en procesadores matriciales o en GPU, donde se pasa primero por la unidad de control.

   <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 13-55-29.png" alt="Captura de pantalla de 2020-04-18 13-55-29" style="zoom:50%;" />

   Utiliza: 

   - Lenguajes de programación + funciones stream processing: Fortran, HPF, 
   - API(directivas+funciones): OpenACC (incluida en OpenMP), Nvidia CUDA



#### D) Estructuras típicas de códigos paralelos

Hay 5 estructuras típicas a la hora de paralelizar un código:

- ###### **Descomposición de dominio o descomposición de datos:**

  El trabajo a realizar por cada proceso se determina dividiendo las estructuras de datos de entrada y/o salida en tantos trozos como flujos.

  Las tareas realizan tareas similares (útil en algoritmos con imágenes)

  

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 14-05-08.png" alt="Captura de pantalla de 2020-04-18 14-05-08" style="zoom:50%;" />

  Por ejemplo, si tenemos 4 procesadores y 4 flujos de instrucciones, existen varias alternativas:

  - Si dividimos la estructura de entrada, se asigna un $FI_i$ a todas las instrucciones que utilicen los datos del bloque i.
  - Si se divide la estructura de salida, se asigna un $FI_i$ a todas las instrucciones que hay que realizar para llegar a los datos del bloque i.

- **Cliente/servidor:** El servidor se ejecuta en un procesador y los clientes en los demás. Cada cliente lanza una petición al servidor y al recibir la respuesta ejecuta el programa.

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 14-07-06.png" alt="Captura de pantalla de 2020-04-18 14-07-06" style="zoom:50%;" />

  

  

- **Divide y vencerás:** cada iteración de la recursividad se realiza en un procesador.

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 14-08-00.png" alt="Captura de pantalla de 2020-04-18 14-08-00" style="zoom:50%;" />

- **Segmentación o flujo de datos:** Se divide el cauce en etapas o segmentos, donde cada etapa se ejecuta en un procesador (en OMP, sections). Se utiliza cuando se aplica a un flujo de datos de entrada las mismas funciones en secuencia, haciendo que cada proceso ejecute distinto código.

  

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 14-09-22.png" alt="Captura de pantalla de 2020-04-18 14-09-22" style="zoom:50%;" />

  La velocidad del trabajo depende de la función más lenta.

- **Master-slave, granja de tareas:** Se distingue un proceso(master) que se encarga de mandar tareas a otros procesos(esclavos), que luego devuelven resultados de vuelta al master. 

  El master se ejecuta en un procesador, y es quien reparte trabajos entre los esclavos, que se ejecutan en los demás procesadores (1 procesador=1 esclavo). No hay comunicación entre esclavos.
  
  A diferencia de en el de cliente, no se realizan peticiones, sino que es el master quien tiene todo el "poder".
  
  - Se escribe un único trabajo, y cada esclavo ejecuta con datos distintos.
  - El master controla la ociosidad de todos los procesadores, puede llegar a auto asignarse un trabajo.
  - La asignación se puede hacer de forma estática o dinámica. Si es estática, se conoce la tarea de cada esclavo.
  
  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-18 14-10-45.png" alt="Captura de pantalla de 2020-04-18 14-10-45" style="zoom:50%;" />

#### E) Buena asignación de tareas

1. Decidimos cuántos flujos de instrucciones diferentes queremos (**mapeo**). Siendo el número de FI (flujo de instrucciones) el número máximo de tareas que en un momento dado se podrían ejecutar en paralelo, es decir el **grado de paralelismo de la aplicación**.
2. Decidimos qué operaciones asignamos a cada FI (**planificación**). Teniendo en cuenta las herramientas de sincronización y comunicación, por ejemplo, si queremos una comunicación por red, los procesadores cuyos FI necesiten datos entre ellos estarán cercanos.

### Lección 5: Proceso de paralelización

El proceso de paralelización tiene 4 pasos:

1. **Descomposición** en tareas independientes.
2. **Asignación** de tareas a procesos y threads.
3. **Redacción** de código paralelo.
4. **Evaluación** de prestaciones.

NOTA: En este tema usamos indistintamente flujo de instrucciones y proceso o núcleo. Un flujo de instrucciones realmente es un proceso o thread del sistema operativo. 

Además, la carga de trabajo es el conjunto de tareas asignadas, incluyen código y datos.

#### 5.1. Descomposición en tareas independientes.

Es importante analizar la dependencia entre funciones y entre iteraciones de bucles.

A partir de un código secuencial nos podemos situar en dos niveles de abstracción para extraer paralelismo:

- Nivel de función: Encontramos las funciones independientes y ejecutables
  en paralelo (paralelismo de tareas).
- Nivel de bucle/bloque: Analizamos las iteraciones de los bucles de una
  función, encontrando las independientes (paralelismo de datos).

#### 5.2. Asignación de tareas a procesos y threads

Esta etapa incluye:

- **Scheduling** o agrupación de tareas en procesos.
- **Mapping** o el mapeo a los procesadores.

Definiremos la **granularidad** como el tamaño de los trozos de código que ejecutan los flujos de instrucciones entre comunicaciones, es decir, que se pueden ejecutar en paralelo. La granularidad depende de:

- El número de cores o procesadores (elementos de procesamiento).
- El tiempo de comunicación y sincronización frente al tiempo de cálculo.

Además, conviene seguir ciertas indicaciones:

- No asignar más de un proceso o hebra a cada procesador.
- Se asignan iteraciones de bucle a hebras y funciones a procesos.

------

##### 5.2.1. Equilibrado de la carga

Además, a la hora de asignar las tareas hay que controlar el **equilibrado de la carga**, donde lo conveniente es intentar disminuir al máximo el tiempo que los flujos de instrucciones (procesos o threads del SO), puedan permanecer ociosos. La situación ideal es que todos empiecen y acaben a la vez.

###### **¿De qué depende el equilibrado?**

- **La arquitectura**, que puede ser:
  - Heterogénea u homogénea: influye en el **tiempo de cálculo** de un programa paralelo.
    - Homogénea: los componentes de los nodos de cómputo son iguales, es decir que todos tardan lo mismo.
    - Heterogénea: cada componente puede tardar un tiempo, son las arquitecturas más comunes (procesadores de propósito general+GPU+FPGA).
  - Uniforme o no uniforme: influye en el **tiempo de comunicación** de un programa paralelo.
    - Uniforme: todos los nodos de cómputo tardan el mismo tiempo en comunicarse.
    - No uniforme: la mayoría de los procesadores (incluso un multiprocesador a nivel de placa).



- **La aplicación y descomposición en tareas independientes**:

  Hay casos en los que el equilibrado se complica, por ejemplo hay aplicaciones en las que las tareas que se pueden ejecutar en paralelo entre puntos de sincronización del código no tardan el mismo tiempo, o aplicaciones en las que no se sabe las tareas que se pueden ejecutar en paralelo  ni antes ni después de la ejecución.

  Alguna solución para facilitar el equilibrado es:

  - En la aplicación todas las tareas que se pueden ejecutar en paralelo entre puntos de sincronización tardan lo mismo.
  - La arquitectura es homogénea y uniforme.
  - Si lo que se complica es el equilibrado estático, se ouede usar una asignación en tiempo de ejecución

------

##### 5.2.2. Tipos de asignación

La asignación puede ser:

- **Estática**, donde la asignación se realiza en tiempo de compilación o al escribir el programa: 

  - La asignación no cambia entre ejecuciones.

  - La implementación es sencilla, no supone añadir mucho código extra.

    <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 17-13-01.png" alt="Captura de pantalla de 2020-04-19 17-13-01" style="zoom:50%;" />

- **Dinámica**, en este caso la asignación se va realizando durante la ejecución. Se va asignando trabajo a los flujos conforme terminan la tarea anterior.

  

  -  Facilita el equilibrado en arquitecturas heterogéneas y en las que las tareas suponen distinto tiempo.
    - Es la única implementación si no se conoce en ningún momento las tareas de la aplicación.
  - La asignación puede cambiar entre ejecuciones.
  - Su implementación requiere añadir código que penaliza el tiempo de ejecución (añade sobrecarga).

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 17-13-35.png" alt="Captura de pantalla de 2020-04-19 17-13-35" style="zoom:50%;" />

Ambas asignaciones pueden ser explícitas, donde la realiza el programador, o implícitas,  donde la realiza la herramienta de programación al generar el ejecutable.

------

##### 5.2.3. Mapeo de procesos a unidades de procesamiento

El mapeo normalmente se deja al SO, aunque lo puede realizar el entorno o sistema en tiempo de ejecución, e incluso puede ser influido por el programador.

------

------

#### 5.3. Redactar código paralelo

El código va a depender del estilo de programación que se utilice (Lección 4.3), del modo de programación (Lección 4.1.2), del punto de partida (Lección 4.1.1), de la herramienta que usemos (Lección 4.2) y de la estructura (Lección 4.4).



#### 5.4. Evaluación de prestaciones

Se encarga de comprobar si se han obtenido las prestaciones mínimas.



### Lección 6: Evaluación de prestaciones

En programación paralela se usa, para evaluar las prestaciones de un código paralelo:

- **Tiempo de respuesta:**
  - Real (wall-clock y elapsed time).
  - Usuario, sistema y CPU 
- Productividad
- **Escalabilidad**
- **Eficiencia**, donde se tiene en cuenta:
  - La relación en prestaciones/prestaciones máximas.
  - El **rendimiento**=prestaciones/nº de recursos
  - Otras medidas, como prestaciones/consumo_potencia o prestaciones/área_ocupada.



##### 6.1 Escalabilidad

La escalabilidad define en qué medida aumentan las prestaciones de un código paralelo al añadir procesadores.

El incremento en prestaciones que se obtiene al usar p procesadores se llama **speed-up**, y es:

$S(p)= \frac{T_{secuencial}}{T_{paralelo}}$

El tiempo de ejecución en paralelo  ($T_{paralelo} = TC + TO $) depende de:

- **Tiempo de cálculo paralelo** (TC), que es el tiempo que supone la ejecución en paralelo de las tareas del código secuencial.
- **Tiempo de sobrecarga **(TO) introducido por la paralelización. A su vez, este depende de:
  - *Tiempo de creación/destrucción* de los flujos de instrucciones (depende de p).
  - *Tiempo de comunicación y sincronización* , puede depender de p.
  - *Tiempo de cálculos y funciones no presentes en el código secuencial*.
  - *Falta de equilibrado*.

Además, tanto TC como TO pueden depender del tamaño del problema que se está resolviendo.

------

**GANANCIA MÁXIMA**

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 17-51-55.png" align= "left" alt="Captura de pantalla de 2020-04-19 17-51-55" style="zoom:50%;" />



Compararemos en qué medida aumentan las prestaciones a medida que aumentamos el número de procesadores. 



<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 17-51-47.png" alt="Captura de pantalla de 2020-04-19 17-51-47" style="zoom: 67%;" />



- **Caso ideal (a)**: esperamos obtener un tiempo de ejecución $\frac {TS}{p}$ con p procesadores para un tiempo de ejecución secuencial TS.

  Es decir, se reparte el código entre los procesadores disponibles, independientemente de p (paralelismo ilimitado). En este reparto, se asigna a cada procesador la misma cantidad de trabajo.

  - La escalabilidad es una línea recta.

  Además, hay otros casos a considerar, en primer lugar, la ganancia puede dejar de crecer a partir de un número de procesadores, debido a que puede llegar un momento en el que no se podrá repartir el trabajo entre más procesadores. (**3**)

  O incluso la ganancia puede decrecer debido a que la sobrecarga se puede incrementar con el número de proccesadores (**4**).

  

- **Caso b**: no todo el código se puede paralelizar (fracción f), y por tanto, la escalabilidad será:

  $S(p) = \frac{TS}{	TS*f +  \frac{TS (1-f)}{p}}=\frac{1}{f}$

  Entonces la ganancia en prestación máxima va a estar limitada por la fracción del código no paralelizable. En esta porción de código, el tiempo (f*TS), permanecerá constante.

  Por tanto, el tiempo de ejecución paralelo nunca podrá ser inferior al tiempo que supone la ejecución de la parte no paralizable y, por tanto, la ganancia en prestaciones nunca podrá ser superior a 1/f (=TS/f×TS).

------

##### 6.2 Ley de Amdahl

La **ley de Amdahl** dice que la ganancia en prestaciones que se puede conseguir aplicando paralelismo a un código secuencial está limitada por la fracción no paralelizable del mismo. 

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 18-06-44.png" alt="Captura de pantalla de 2020-04-19 18-06-44" style="zoom:50%;" />

Algo importante a destacar es que en la práctica, la fracción de código que limita la escalabilidad se puede disminuir aumentando el tamaño del problema.

Además, la ganancia disminuye conforme aumenta la fracción de código no paralelizable.

------

#### 6.3. Ganancia escalable

Si el objetivo es mejorar la calidad de los resultados aumentando el tamaño del problema, se puede obtener una ganancia que no está limitada y que depende de p.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-19 18-13-47.png" align="left" alt="Captura de pantalla de 2020-04-19 18-13-47" style="zoom:50%;" />

En la gráfica se representa el tiempo paralelo TP en el eje x y el número de procesadores que se usa para diferentes trozos del código paralelo en el eje y. En este caso, en f×TP se usa 1 procesador (código no paralelizable) y, en (1-f)×TS , p procesadores. 

Cada procesador ejecuta k=n/p tareas independientemente del valor de p; es decir, conforme se incrementa p se incrementa también el tamaño del problema para mantener constante la carga de trabajo asignada a cada uno de los procesadores. 



#### **Escalabilidad débil**  o **Ley de Gustafon**:

Estudiar la **escalabilidad débil** (ganancia escalable) de un código paralelo supone estudiar cómo evoluciona la ganancia o el tiempo conforme se incrementa p *manteniendo constante el tamaño del problema* que resuelve cada procesador, es decir, se mantiene el tamaño n. 

Se pretende ver si el tiempo de ejecución paralelo se mantiene constante: 

- Si aumenta, será menos escalable (escalabilidad déssbil) el código paralelo cuanto mayor sea la pendiente con la que aumenta el tiempo.

  $S(p)= \frac{T_s(n=kp)}{T_p}=\frac{f*T_p+p(1-f)T_p}{T_p}$

En el caso de un estudio de **escalabilidad fuerte** (Ley de Amdahl) se representa cómo evoluciona la ganancia (a veces se usa el tiempo paralelo) conforme se incrementa p manteniendo constante el tamaño del problema que se resuelve (TS). Cuanto más cercana se mantenga la ganancia a la ganancia lineal mejor código paralelo será.

En resumen, en la escalabilidad débil se mantiene constante Tp, obteniendo Ts a partir de Tp, mientras que en la escalabilidad fuerte mantenemos constante Ts, y obtenemos Tp a a partir de Ts.



NOTA: El significado y el valor de f en la ley de Amdalh es distinto del f que se utiliza en la ley de Gustafson. 

**EFICIENCIA**: Para evaluar en qué medida las prestaciones que ofrece un sistema para un código paralelo se acercan a las prestaciones máximas que idealmente debería ofrecer dado los procesadores disponibles en el mismo, se usa la siguiente expresión de eficiencia: 

$E(p)= \frac{Prestaciones(p)}{p*Prestaciones(1)}= \frac{S(p)}{p}$

