#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <dirent.h>

// USAMOS STRUCT SIEMPRE QUE PODAMOS PARA ENVIAR COSAS ATOMICAMENTE (DE UNA SOLA VEZ)
struct mensaje_t{
	int num; 
	char metadato; 
	int _pid;
};

void error(char* mensaje){
	perror(mensaje);
	exit(EXIT_FAILURE);
}

static void sigchld_handler(int n){
	int pid = wait(NULL);
	//printf("se ma muerto %d\n", pid);
}

int main(int argc, char* argv[]){

	// MANEJO DE SEÑALES
	struct sigaction sa;
	sa.sa_handler = sigchld_handler;
    sa.sa_flags = SA_RESTART;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
		error("Sigaction sigchld");

	char* pathname = argv[1]; 
	DIR* directorio = opendir(pathname);
	struct dirent* ed; 	// elemento del directorio		
	struct stat atributos; 
	char cadena[500];
	int metadato = -1;	  
	
	struct mensaje_t msg;
	int leidos, pid; 
	
	// Bloquea esperando la peticion de un cliente	
		while(leidos = read(STDIN_FILENO, &msg, sizeof(msg)) != 0){
			pid = fork();
			if(pid == 0){
				if (leidos == -1)
					error("\nError en la lectura de los datos.\n");	
				metadato = -1;
																										
				//recorremos todo el directorio, y vemos si la entrada n-esima proporcionada es valida
				rewinddir(directorio);
				for( int i = 0; i<msg.num; i++)
					ed = readdir(directorio);
				if(ed != NULL ){	
					printf("SERVER: Peticion recibida %s\n", ed->d_name);				
					if(pathname[strlen(pathname)] != '/')
						sprintf(cadena,"%s/%s",pathname,ed->d_name);
					else
						sprintf(cadena,"%s%s",pathname,ed->d_name);
					if(stat(cadena, &atributos) < 0)
						error("Error en stat.\n");						
											
					if(S_ISREG(atributos.st_mode)){			
						if (msg.metadato == 'i')
							metadato = ed->d_ino; 
						else if (msg.metadato == 's')
							metadato = atributos.st_size;
					}															
				}
				else
					printf("Entrada no valida.\n");
				
				// Abrimos FIFO personalizado para escribir en él
				char nombre_personalizado[20];
				int fd_personalizado; 
				sprintf(nombre_personalizado, "FIFO.%d", msg._pid);
				fd_personalizado = open(nombre_personalizado, O_RDWR);
										
				if(write(fd_personalizado, &metadato, sizeof(int)) != sizeof(int))
					error("\nError en la escritura del fifo personalizado por parte del servidor.\n");		
				close(fd_personalizado);

				return 0; 			
			}			
		}								
	//cuando termina de leer
	close(STDIN_FILENO);
	int pid_padre = getppid();
	kill(pid_padre, SIGCHLD);
	printf("FIN SERVER.\n");
	return 0;	
}
