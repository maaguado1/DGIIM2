/*
 Reescribir el programa que implemente un encauzamiento de dos órdenes pero
utilizando fcntl. Este programa admitirá tres argumentos. El primer argumento y el tercero
serán dos órdenes de Linux. El segundo argumento será el carácter “|”. El programa deberá
ahora hacer la redirección de la salida de la orden indicada por el primer argumento hacia el
cauce, y redireccionar la entrada estándar de la segunda orden desde el cauce. Por ejemplo,
para simular el encauzamiento ls|sort, ejecutaríamos nuestro programa como:
*/

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char *argv[]){
    	

	if (argc <3 ){
		printf ("\nError en el número de argumentos");
	}
	else{
		int fd[2];
		pipe(fd);
		pid_t PID;
		char *str_command1=argv[1];
		char *str_command2=argv[3];
		char *str_cauce=argv[2];
		
		if ((PID=fork())<0){
			perror("Error en el fork");
			exit -1;
		}
		if (PID == 0){
			close(fd[0]);
			if (fcntl(fd[1], F_DUPFD, STDOUT_FILENO) == -1) 
				perror("Fallo en fcntl");
			execlp(str_command1, "", NULL);
		}


		else{
			close(fd[1]);
			if (fcntl(fd[0], F_DUPFD, STDIN_FILENO) == -1 ) 
				perror("Fallo en fcntl");
			execlp(str_command2, "", NULL);

		}


        }
	return 0;
}
	





	 
