
package deepspace;
import java.util.ArrayList;



class SpecificDamage extends Damage{


    private ArrayList<WeaponType> weapons;


    SpecificDamage ( ArrayList<WeaponType> a, int s){
    super(s);
    
    weapons = a==null?null:new ArrayList<WeaponType>(a);

    }
    
    SpecificDamage (SpecificDamage n){
        super(n.getNShields());
        weapons = n.getWeapons()==null?null:new ArrayList<WeaponType>(n.getWeapons());
    }
    public ArrayList<WeaponType> getWeapons(){
        return weapons;
    }


    public void discardWeapon(Weapon w){
    if (weapons != null)
        weapons.remove(w.getType());
    }


    @Override
    public boolean hasNoEffect(){
        if ((weapons==null || weapons.isEmpty()) && getNShields() == 0)
            return true;
        else
            return false;
    }


    


    @Override
    public SpecificDamage adjust(ArrayList<Weapon> w, ArrayList<ShieldBooster> s){

        int min1=(s.size() < getNShields())? s.size(): getNShields();
        ArrayList<Weapon> w1= new ArrayList<Weapon>(w);
        ArrayList<WeaponType> w2= new ArrayList<WeaponType>(weapons);



        ArrayList<WeaponType> w3=new ArrayList<WeaponType>();
        for (WeaponType t: w2){
            if (arrayContainsType(w1, t) != -1){
                w3.add(t);
                w1.remove(arrayContainsType(w1,t));
            }
        }

        SpecificDamage ajustado= new SpecificDamage(w3, min1);
        return ajustado;
    }

    @Override
    SpecificDamageToUI getUIversion(){
        return new SpecificDamageToUI(this);
    }


    @Override
    public String toString(){
    String mens2 = weapons==null?" ":weapons.toString();
    String message = "[Damage]: Number of Shields: " + getNShields()
            + ", Weapon Type: " + mens2;

    return message;
    }   
}














