#encoding:utf-8


require_relative 'Damage'
require_relative 'SpecificDamageToUI'

module Deepspace

  
  

class SpecificDamage < Damage
  
  public_class_method :new
  
   # Class specific initializer.
	# @param s [Integer] number of shields that will be lost
  # @param w WeaponTypes that will be lost 
  
  def initialize(w,s)
    super(s)
    @weapons = []
    for we in w
      @weapons << we
    end
  end
  
  
  
  # Class copy initializer.
	# @param d [Damage] makes a copy of the given damage.
  def newCopy(d)
    return new(d.weapons, d.nShields)
    
  end
  
  
   # Getters
	# ==========================================================================

  attr_reader :weapons
  
  
  # Checks whether the damage is affecting or not
	# @return [Boolean] true, if damage has no effect; false, otherwise
  def hasNoEffect
    if (@weapons.length==0) and @nShields==0
      return true
    else
      return false
    end
  end
  
  
  # Setters
	# =========================================================================

  
  
  def discardWeapon(w)
    if (@weapons.length > 0)
      @weapons.delete(w)
    
    end
  end
  
  
  # Creates a copy of current objet where shields which are not
	# included in arrays given as parameters are discarded. That's to say, we
	# shrink the Damage to the parameters
	# @param s [Array<ShieldBooster>] shields to fit
  # @param w [Array<Weapon>] weapons to fit
	# @return [Integer] a copy of the object adjusted as explained above
  
  def adjust(w,s)
      min1 = super(s)
      w1= w
      w2 = @weapons
      w3 = []
      for t in w2
        
        if (arrayContainsType(w1, t) != -1)
          
          w3 << t
          w1.delete_at(arrayContainsType(w1,t))
        end
      end
      
    otro= SpecificDamage.new(w3,min1)
    return otro
  
    
  end
  
  
  # String representation, UI version
	# ==========================================================================
  
  
  def getUIversion
    return  SpecificDamageToUI.new(self)
  end
  
  
  def to_s
    message = super
    message += "\tWeapon Types: {"
    for w in @weapons
      message += w.to_s + " "
    end
    message += "}"
    return message
                
  end
  
  
  
  
  
  
  
end


end