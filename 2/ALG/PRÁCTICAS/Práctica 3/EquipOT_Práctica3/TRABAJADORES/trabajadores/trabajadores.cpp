#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>

using namespace std;


// GENERACIÓN NÚMEROS ALEATORIOS

//Inicia la generación de números aleatorios
void startRandom(){	
	std::srand(time(NULL));
}


/**
 * Genera un double aleatorio en un intervalo dado
 * min = el minimo valor que se puede tomar
 * max = el maximo valor que se puede tomar
 * */
double random_double(double min, double max){
    return (std::rand() / (double) RAND_MAX ) * (max - min) + min;
}


/**
 * Genera un entero aleatorio en un intervalo dado
 * min = el minimo valor que se puede tomar
 * max = el maximo valor que se puede tomar
 * */
double random_int(int min, int max){
    return (int)(std::rand() / (double) RAND_MAX ) * (max - min) + min;
}





// GENERACIÓN DATOS DEL PROBLEMA
/**
 * Genera una matriz aleatoria con las asignaciones de trabajo
 * size = tamaño del problema (nº de trabajadores y trabajos)
 * */
std::vector<std::vector<double> > generar_matriz(int size){
    // Inicio una matriz llena de ceros
    std::vector<double> vacia(size, 0);
    std::vector<std::vector<double> > matriz(size, vacia);

    for(int row = 0; row < size; row++){
        for(int col = 0; col < size; col++){
            matriz[row][col] = random_double(0, size);
        }
    }

    return matriz;
}





// CÁLCULOS DEL PROBLEMA
/**
 * Calcula el coste de una asignación dada
 * matriz = la matriz con los costes
 * asignacion = la asignacion cuyo coste se calcula
 * */
double calcular_coste(std::vector<std::vector<double> > matriz, std::vector<int> asignacion){
    double cost = 0;

    for(int i = 0; i < asignacion.size(); i++){
        cost = cost + matriz[i][asignacion[i]];
    }

    return cost;
}
/**
*  Dado un vector de coste de tareas, encuentra la mejor tarea de las que quedan disponibles
*  coste_tareas = vector con el coste de las tareas
*  tareas_disponibles = indica true o false según si la tarea se ha usado ya o no, (true = no se ha *  usado, está disponible) 
**/
int mejor_tarea(vector<double> coste_tareas, vector<bool> tareas_disponibles){
	
	int mejor_tarea = 0;
	while(tareas_disponibles[mejor_tarea] == false){
		mejor_tarea++;
	}
	double mejor_coste = coste_tareas[mejor_tarea];

	
	// Comparo la encontrada con los costes de las que quedan disponibles
	for(int i=mejor_tarea; i < coste_tareas.size() ; i++){
		if ( tareas_disponibles[i] == true){
			if ( coste_tareas[i] < mejor_coste){
				mejor_coste = coste_tareas[i];
				mejor_tarea = i;
			}
		}
	}
	
	return mejor_tarea;
}



/**
* Calcula la asignación parcialmente óptima dada una matriz de costes mediante un algoritmo greedy
* datos = matriz de costes
**/
vector<int> optima(vector<vector<double> > datos){

	// Vector que almacena las tareas que están disponibles para elegir
	vector<bool> tareas_disponibles(datos.size(), true); 

	// Vector con la solución parcial
	vector<int> solucion;

	// A cada trabajador se le asigna la tarea con menor coste que quede disponible
	for(int i=0; i<datos.size(); i++){

		// Buscamos la mejor tarea de las que quedan
		int tarea_actual = mejor_tarea(datos[i], tareas_disponibles);

		// Añadimos la tarea a la solución
		solucion.push_back(tarea_actual);

		// Eliminamos la tarea recién añadida de la lista de disponibles
		tareas_disponibles[tarea_actual] = false;
	}

	return solucion;
}






// MOSTRAR DATOS
void mostrar_matriz(std::vector<std::vector<double> > matriz, char sep = '\t'){
    for(int row = 0; row < matriz.size(); row++){
        for(int col = 0; col < matriz[row].size(); col++){
            std::cout << matriz[row][col] << sep;
        }
        std::cout << std::endl;
    }
}






int main(){
	int size = 100;   		// Tamaño del problema
	
	// Generación datos
	startRandom();
	vector<vector<double> > datos = generar_matriz(size);
	
	// Cálculo
	vector<int> solucion = optima(datos);

	// Mostramos la solución encontrada
	cout << "El coste óptimo es: " << calcular_coste(datos, solucion) << endl;
	for(int i=0; i<size; i++){
		cout << "Al trabajador " << i << " se le asigna la tarea " << solucion[i] << endl;
	}

	return 0;
}










