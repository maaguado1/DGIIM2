---

---

## Tema 3: Arquitecturas con paralelismo a nivel de thread

### Lección 7: Arquitecturas TLP (Thread-Level-Paralelism):

#### A) Clasificación de arquitecturas con TLP explícito y una instancia de SO

Este tipo de arquitecturas se clasifican en:

- **Multiprocesador** , ejecutan varios thread en paralelo en un computador con varios cores/procesadores, asignando cada thread a un core distinto.
  - Incluyen distintos niveles de empaquetamiento (dado, encapsulado, placa, chasis, etc.)
  - Incluyen, por tanto los multicores en un chip (CMP-chip multiprocessor).
- **Multicore o multiprocesador en un chip(CMP)**, ejecutan varios threads en paralelo en un chip de procesamiento multicore.
- **Core multithread**, core que modifica su arquitectura ILP para ejecutar threads concurrentemente o en paralelo.

#### B) MULTIPROCESADORES

##### B.1 Clasificación

Como ya vimos en el Tema 1, se pueden clasificar en:

**Según el sistema de memoria**

- Multiprocesador con memoria centralizada (UMA), donde todos los procesadores comparten el espacio de direcciones de memoria. Y las transmisiones se realizan por interconexión.
  - Tiene mayor latencia y es poco escalable.

- Multiprocesador con memoria distribuida (NUMA), cada procesador tiene su propio espacio de memoria.
  - La latencia es menor y es más escalable, aunque se necesita distribución de datos y código.

**Según el nivel de empaquetamiento** (de pequeño a mayor)

- Chip
- Placa/board
- Armario/cabinet
- Sistema

##### B.2. Evolución UMA-NUMA en una placa

- UMA:

  - El controlador de memoria (quién gestiona todas las operaciones, el cerebro), se encuentra en un **chipset**.

  - Conexión y comunicación (red): mediante bus compartido.

  - En un ciclo sólo hay un acceso a memoria, por tanto sólo será posible la paralelización si se accede a módulos distintos.

    

- NUMA: 
  - El controlador de memoria se encuentra en un **chip del procesador**

  - La comunicación (Red) es por medio de 

    - Enlaces: conexiones punto a punto que permiten que un procesador pueda acceder a los módulos de memoria que no estánen su mismo nodo.
    - Conmutadores: en el chip del procesador

  - Cada procesador puede acceder tanto a su módulo de memoria como al de los demás

    

  <img src="/home/maguado/Imágenes/Captura de pantalla de 2020-04-23 11-15-57.png" align="left" alt="Captura de pantalla de 2020-04-23 11-15-57" style="zoom:50%;" /><img src="/home/maguado/Imágenes/Captura de pantalla de 2020-04-23 11-12-51.png" align="right" alt="Captura de pantalla de 2020-04-23 11-12-51" style="zoom:50%;" /> 











####  C) MULTICORES



Ejecutan threads en paralelo en un **chip de procesamiento multicore**. Vemos qué es (ejemplo de estructura):

<img src="/home/maguado/Imágenes/Captura de pantalla de 2020-04-23 11-18-24.png" alt="Captura de pantalla de 2020-04-23 11-18-24" style="zoom:50%;" />

En un multicore se combinan varios microprocesadores independientes. Para acceder a otros chips de memoria desde uno de los cores, se utiliza el conmutador, y de estar conectados, se utiliza el controlador.

Realmente son multiprocesadores implementados en un dado de silicio. Normalmente tienen una caché de último nivel que comparten todos los núcleos de procesamiento, y luego cada uno tiene su propia caché (niveles restantes). 

En la imagen, por ejemplo, tenemos 3 niveles de caché, dos de ellos independientes y el tercero compartido. En este caso se podría usar L3 para comunicación y sincronización. Es decir, el paso entre L2 y L3 se realizaría como en un multiprocesador UMA. Este esquema es, de todas las alternativas vistas en clase, la que supone una mejora en las prestaciones, pues un core no tiene que acceder a memoria para que otro core (que no comparta memoria) escriba un dato ya que todos los cores comparten L3.



#### D) CORES MULTITHREAD

En este tipo de cores se modifica la arquitectura original ILP para poder dar soporte a TLP. Estos cores buscan minimizar el tiempo donde se encuentran ociosos durante la etapa del cauce (por motivos de dependencia p.e.). Esta ociosidad se suele resolver añadiendo threads. Observamos que **coinciden**: nº de threads a añadir con el nº máximo de instrucciones `nop` +1.

Antes de nada, realizamos un repaso sobre las arquitecturas ILP.

###### D.1. Arquitecturas ILP

Hay dos tipos de arquitecturas ILP:

- **Procesadores segmentados**, ejecutan instrucciones concurrentemente haciendo uso de la segmentación de sus componentes, es decir, dividir el flujo de instrucciones en etapas.

  <img src="/home/maguado/Imágenes/Captura de pantalla de 2020-04-23 11-26-56.png" alt="Captura de pantalla de 2020-04-23 11-26-56" style="zoom:50%;" />

  

- **Procesadores VLIW y superescalares**, ejecutan instrucciones concurrentemente usando segmentación, y en paralelo. Es decir, tienen múltiples unidades funcionales y emiten múltiples instrucciones en paralelo a unidades funcionales. Hay dos tipos:

  - VLIW: las instrucciones que se ejecutan en paralelo se captan juntas de memoria, formando una palabra de instrucción más larga.
    - El hardware presupone que las instrucciones de una palabra son independientes (no filtra).
  - Superescalares: 
    - Tienen que encontrar instrucciones que se puedan emitir y ejecutar en paralelo.
      - El hardware extrae el paralelismo a nivel de instrucción.
  
  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-41-48.png" alt="Captura de pantalla de 2020-04-23 12-41-48" style="zoom:50%;" />

###### D.2. Modificación ILP a Core Multithread

Hay dos aspectos a modificar:

- Almacenamiento: se multiplexa, reparte o comparte entre threads, o se replica.
  - En SMT: repartir, compartir o replicar
- Hardware entre etapas: se multiplexa, reparte o comparte entre threads.
  - Crea un incremento en las prestaciones, aunque lo que se replica siempre es el fichero de registros de la arquitectura.
  - En SMT:
    - Las unidades funcionales (etapa EX) compartidas
    - Resto de etapas repartidas o compartidas
    - No se pueden multiplexar, debido a que en un SMT se pueden emitir a la vez instrucciones de distintos threads en paralelo. Si se multiplexara se ejecutarían concurrentemente.
    - Por ejemplo, se usa en predicción de saltos y decodificación.

###### D.3. Clasificación de Cores Multithread

**a) Temporal Multithreading (TMT)**:  Ejecutan varios threads concurrentemente en el mismo core.   								<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-47-16.png" align="right" alt="Captura de pantalla de 2020-04-23 12-47-16" style="zoom:50%;" />

- La conmutación entre threads la decide y controla el hardware.
- En un ciclo sólo emite instrucciones de un único thread.
- **Clasificación**:
  - **a.1) Fine-grain multithreading (FGMT),** la conmutación entre threads la decide el hardware en **cada ciclo**, mediante implementaciones sencillas y puede ser:<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-55-37.png" align="right" alt="Captura de pantalla de 2020-04-23 12-55-37" style="zoom:50%;" />
    - Por turno rotatorio.
    - Eventos de cierta latencia junto con alguna técnica de planificación.
      - Eventos: dependencia, operación con latencia,...
  - **a.2) Coarse-grain multithreading (CGMT**), la conmutación entre threads la decide el hardware, con un coste 0 a varios ciclos:<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-55-42.png" align= "right" alt="Captura de pantalla de 2020-04-23 12-55-42" style="zoom:50%;" />
    - Tras intervalos de tiempo t prefijados (_timeslice multithreading_).
    - Eventos de cierta latencia (_switch-on-event multithreading_).
    - **Clasificación:**
      - **a.2.1) Estática:**
        - Conmutación, puede ser **explícita** (instrucciones añadidas al repertorio explícitas para conmutación), o **implícita** (instrucciones de carga, almacenamiento, salto).
        - Ventaja: Coste de cambio de contexto mínimo (0-1 ciclo)
        - Inconveniente: Cambios de contexto innecesarios
      - **a.2.2) Dinámica:**
        - Conmutación según eventos como fallo en la última cache dentro del chip de procesamiento, interrupción (conmutación por señal), ...
        - Ventaja: Reduce cambios de contexto innecesarios.
        - Inconveniente: Mayor sobrecarga al cambiar de contexto, pues añade hardware para realizarlo.

**b) Simultaneous MultiThreading (SMT)**: Ejecutan en un core superescalar varios threads en paralelo.<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-55-32.png" align="right" alt="Captura de pantalla de 2020-04-23 12-55-32" style="zoom:50%;" />

- Pueden emitir instrucciones de varios threads en un ciclo.

​		

###### D.4. Alternativas

**En un core escalar segmentado**, solamente se emite una instrucción en cada ciclo de reloj, pero se puede implementar FGMT o CGMT:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 12-59-55.png" alt="Captura de pantalla de 2020-04-23 12-59-55" style="zoom:50%;" />

**En un core con emisión múltiple de instrucciones de un thread**, es decir superescalar o VLIW. En este tipo de core se emiten más de una instrucción por cada ciclo de reloj, pero siempre del mismo thread, vemos las alternativas:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 13-01-26.png" alt="Captura de pantalla de 2020-04-23 13-01-26" style="zoom:50%;" />

**En un core multithread**, es decir, un multicore o un core superescalar con SMT (Simultaneous MultiThread), se pueden emitir instrucciones de distintos threads cada ciclo de reloj:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-23 13-02-54.png" alt="Captura de pantalla de 2020-04-23 13-02-54" style="zoom:50%;" />

#### E) Hardware y arquitecturas TLP en un chip

Vemos como afecta la paralelización a nivel de thread al hardware del chip, según la alternativa que se escoja:

![Captura de pantalla de 2020-04-23 13-04-15](../../../../../../Imágenes/Captura de pantalla de 2020-04-23 13-04-15.png)

Las prestaciones en la tabla anterior concuerdan con la complejidad del hardware.

------

### Lección 8: Coherencia en el sistema de memoria

Los computadores que implementan mantenimiento de coherencia son:

- Multiprocesadores: 
  - NUMA: los CC-NUMA y COMA
  - UMA: los SMP

#### A) Sistema de memoria en multiprocesadores

El Sistema de memoria de los multiprocesadores incluye:

- Cachés de todos los nodos 
- Memoria principal
- Controladores de memoria
- Buffers
  - De escritura/almacenamiento
  - Combinados entre escrituras-almacenamientos, etc
- Red de interconexión, o el medio de comunicación entre los componentes anteriores.

Por tanto, la comunicación de los datos entre los procesadores la realiza el *sistema de memoria*. Podemos tener copias de una dirección de MP en distintos puntos de nuestro sistema de memoria (p.e. distintos niveles de caché). Si estas copias no tienen el mismo contenido se produce lo que llamamos **incoherencia**, que a su vez produciría que la devolución de esa dirección de memoria no sea lo último que se ha leído.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-24 12-25-56.png" alt="Captura de pantalla de 2020-04-24 12-25-56" align="left" style="zoom:50%;" /><img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-24 12-26-06.png" align="right" alt="Captura de pantalla de 2020-04-24 12-26-06" style="zoom:50%;" />











#### B) Concepto de coherencia en el sistema de memoria

La incoherencia surge cuando se realiza una modificación sobre un dato(modificable) en uno de los nodos, y no se actualiza el resto de copias de ese dato dentro de los nodos del multiprocesador.

| CLASES DE E.D                  | EVENTOS                              | Tipos de falta de coherencia                            |
| ------------------------------ | ------------------------------------ | ------------------------------------------------------- |
| Datos modificables             | E/S                                  | Cache-MP                                                |
| Datos modificables compartidos | Fallo de Cache                       | Cache-Mp                                                |
| Datos modificables privados    | Del thread/proceso al fallo de caché | Caché-MP, aunque también entre cachés de distinto nivel |
| Datos modificables compartidos | Lectura de caché no autorizada       | Caché-caché (entre el cachés del mismo nivel)           |

##### B.1.-**MÉTODOS DE ACTUALIZACIÓN DE MEMORIA PRINCIPAL**

También valen para actualización entre último nivel de caché-MP y entre niveles distintos de caché.

- Escritura inmediata **write-through**: cada vez que un procesador escribe en su caché escribe también en memoria principal.
  - Por los principios de localidad temporal y espacial sería más rentable si se escribiera el bloque entero en MP.
  - No se admiten faltas de coherencia
- Posescritura (**write-back**): Se actualiza la memoria principal reescribiendo el bloque completo cuando dicho bloque sale de la caché.
  - Disminuye los accesos a caché por los principios de localidad temporal y espacial. Además, es más rentable escribir el bloque entero después de las modificaciones.
  - El controlador de la caché tiene un bit con información sobre si un bloque ha sido modificado o no. En esta alternativa sí que se permiten fallos de caché, así que habría que **añadir hardware** para detectar dicho bit.



Cuando se comparten las cachés se utiliza posescritura, y en caso contrario, escritura inmediata.

##### B.2.- **ALTERNATIVAS DE PROPAGACIÓN DE ESCRITURAS**

- Escritura con actualización (**write-update**): Cada vez que un procesador escribe en una dirección en su caché, se escribe en las copias de esa dirección en las demás cachés.

  ​						<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-24 12-37-09.png" alt="Captura de pantalla de 2020-04-24 12-37-09" style="zoom:50%;" />	

- Escritura con invalidación (**write-invalidate**): Antes que un procesador modifique una dirección en su caché se invalidan las copias del bloque en las demás cachés.

  - Reduce el tráfico, especialmente si los datos los comparten pocos procesadores.

    <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-24 12-39-06.png" alt="Captura de pantalla de 2020-04-24 12-39-06" style="zoom:50%;" />

Aún así, aunque se usen mecanismos de propagación, no se puede garantizar la coherencia. 

##### **B.3. REQUISITOS PARA MANTENER COHERENCIA**

1. **Propagar las escrituras en una dirección** (se ocupa el controlador de caché)
   - La escritura en una dirección debe hacerse visible en un tiempo finito a otros procesadores.
   - CONEXIÓN:
     - **BUS**: los paquetes de actualización/invalidación son visibles a todos los controladores de caché conectados al bus (muy sencillo de implementar).
     - **Difusión**: los paquetes de actualización se envían a todas las cachés, causando desaprovechamiento del ancho de banda.
     - Para mayor escalabilidad:
       - Filtrar los envíos: sólo se envían paquetes a las cachés con copias del bloque.
       - Mantener en una tabla (**directorio**) información sobre qué caché tiene copia de cada bloque.
2. **Serializar las escrituras en una dirección**
   - Las escrituras en una dirección deben verse en el mismo orden por todos los procesadores.
   - CONEXIÓN
     - **BUS**: el orden en el que los paquetes aparecen en el bus determina el orden en el que se ven por todos los nodos.
     - **Otros**, el orden en el que las peticiones de escritura llegan a *home* (nodo que tiene en MP la dirección)  o al *directorio centralizado* sirve para serializar en sistemas de comunicación que garantizan el orden en las transferencias entre dos puntos.
       - **Difusión**, en los ejemplos de las diapositivas se utiliza actualización para propagar las escrituras y el orden de llegada al home es el orden real para todos. Al ser actualización se envía a todos los nodos, para que todos acaben con el mismo contenido: el primero que llega al home confirma y escribe en todos.
       - **Sin difusión con directorio distribuido**: en la caché hay copia del bloque y de quién tiene copia del bloque. Por tanto, las peticiones _solo se envían al home_. El home se encargará de mandar la actualización a quien tenga copia del bloque. 
         - Mantiene el orden de llegada al home.

##### B.4.**DIRECTORIO**

Ya hemos visto que el directorio almacena información sobre el almacenamiento y modificación de cada bloque de memoria.

**Alternativas de implementación**

- Centralizado
  - Se comparte por todos los nodos.
  - Tiene infomación sobre los bloques de todos los módulos de memoria.
- Distribuido
  - Las filas se dividen entre los nodos.
  - Cada nodo tiene un directorio con información sobre los bloques de su módulo de memoria.

------

#### C) Protocolos de mantenimiento de coherencia

Para diseñar un protocolo de mantenimiento de coherencia en el sistema de memoria se tienen que determinar:

- **Política de actualización de MP** (inmediata, posescritura o mixta).
- **Política de coherencia entre cachés** (invalidación, actualización , mixta).
- El **comportamiento**:
  - Definir los estados posibles de los bloques de caché y memoria.
  - Definir transferencias a generar entre eventos, es decir, qué paquetes se generan entre los eventos, qué nodos intervienen y el orden entre ellos. Los eventos pueden ser:
    - Lecturas/escrituras del procesador del nodo.
    - Llegada de paquetes de otro nodo.
  - Definir transiciones de estado para bloques de caché y memoria (cuando se pasa de un estado a otro).

Así, tenemos tres protocolos principales

1. Protocolos de espionaje (**snoopy**)
   - Se usa en buses y en sistemas con una difusión eficiente (nº de nodos pequeño o porque implementa difusión).
   - Rentable si el número de nodos es pequeño.
2. Protocolos basados en **directorios**
   - Para redes sin difusión.
   - Para redes escalables, multietapa y estáticas.
3. Esquemas **jerárquico**
   - Para redes jerárquicas, con jerarquía de buses, redes escalables, o redes escalables-buses.

Todos los que vamos a ver utilizan posescritura y escritura con invalidación.

------

------

#### D) Protocolo MSI de espionaje 

Este protocolo busca minimizar el número de estados usando posescritura e invalidación.

**ESTADOS**

1. ESTADOS EN CACHÉ: mínimo

   - Modificado (M), es la única copia válida en todo el sistema.
   - Compartido(C,S): está válido, también válido en memoria y puede que haya copia válida en otras cachés.
   - Inválido (I): se ha invalidado o no está físicamente.

   Si se pide un bloque inválido, puede ser que esté fisicamente pero no está actualizado, por tanto no se puede devolver.

2. ESTADOS EN MEMORIA

   - Válido: puede haber copia válida en una o varias cachés.
   - Inválido: habrá copia válida en una caché.

   La respuesta de la memoria muchas veces se inhibe por el controlador de caché. Por ejemplo, si una caché ve una petición de lectura de un bloque de memoria que tiene, y que no está en la memoria, tendrá que inhibir el mensaje de la memoria (bloque inválido) y devolver dicho bloque(respuesta con bloque).

   

**TRANSFERENCIAS**

| Nombre   | Descripción                   | Evento que la causa                                          |
| -------- | ----------------------------- | ------------------------------------------------------------ |
| PtLec    | Petición de lectura de bloque | Lectura con fallo de caché del procesador del nodo.          |
| PtLecEx  | Petición de acceso exclusivo  | Escritura del procesador (PrEsc) en un bloque compartido o inválido. (INVALIDACIÓN) |
| PtPEsc   | Petición de posescritura      | Por el reemplazo del bloque modificado                       |
| RpBloque | Respuesta con bloque          | Al tener en estado "M" el bloque solicitado por PtLec o PtLecEx |

*Directorio centralizado*.Los dos primeros eventos son entre el nodo origen y el procesador, el tercero es entre el  nodo origen y el controlador de cache y el último es entre el nodo origen y los demás nodos.

La diferencia entre el acceso exclusivo y la lectura exclusiva es: si ya se tiene una copia válida se genera PtEx, y si no tiene copia se genera PtLecExc. 

Hay que tener en cuenta que PtEx no existe en  buses porque en un ciclo de bus siempre se devuvelve un bloque, por tanto, sólo se usaría PtLexEx.

Sin embargo, este protocolo no resulta del todo eficiente, pues en un código secuencial, en un solo procesador, a cada escritura que se quiera hacer se generarán PtLecEx, sin ser necesarios.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-25 12-12-47.png" alt="Captura de pantalla de 2020-04-25 12-12-47" style="zoom:50%;" />

------

#### E) Protocolo MESI de espionaje

Este protocolo también utiliza posescritura e invalidación.

**ESTADOS**

1. ESTADOS EN CACHÉ

   - Modificado (M), es la única copia válida en todo el sistema.
   - **Exclusivo**(E), es la única copia del bloque válida en cachés, la memoria también está actualizada.
   - **Compartido(C,S)**: está válido, también válido en memoria y en **al menos** otra cache.
   - Inválido (I): se ha invalidado o no está físicamente.

   Si se pide un bloque inválido, puede ser que esté fisicamente pero no está actualizado, por tanto no se puede devolver.

2. ESTADOS EN MEMORIA

   - Válido: puede haber copia válida en una o varias cachés.
   - Inválido: habrá copia válida en una caché.

   

   <img src="../../../../../../Imágenes/Captura de pantalla de 2020-04-25 12-18-47.png" alt="Captura de pantalla de 2020-04-25 12-18-47" style="zoom:50%;" />

------

#### F) Protocolo MSI basado en directorios

**1. Sin difusión**
Recordamos que los estados en caché son Modificado(M), Compartido(C) e inválido(I), y los estados en bloque de memoria son Válido e Inválido.

- Tipos de paquetes

  Existen cinco tipos de nodos en MSI con directorios: 

  1- Solicitante(S)

  2- Origen(O), es el que tiene información en su directorio sobre el bloque que se está tratando.

  3- Modificado(M)

  4- Propietario(P), tiene un bloque válido.

  5- Compartidor(C).

  

  - **Petición**
    - De solicitante a origen: 
      - PtLec, lectura de un bloque.
      - PtLexEx, lectura con acceso exclusivo.
      - PtEx, petición de acceso exclusivo sin lectura.
      - PtPEsc, posescritura.
  - **Reenvío de petición**
    - Origen a nodos con copia (P, M , C):
      - RvInv, invalidación.
      - RvLec, lectura.
      - RvLecEx, lectura exclusiva.
  - **Respuesta**
    - Nodo propietario a origen
      - RPBloque, respuesta con bloque
      - RPInv, respuesta confirmando invalidación sin bloque.
      - RPBloqueInv, respuesta confirmando invalidación con bloque.
    - Nodo origen a solicitante
      - RPBloque, respuesta con bloque
      - RPInv, respuesta confirmando invalidación sin bloque.
      - RPBloqueInv, respuesta confirmando invalidación con bloque.

**2. Con difusión**

Los estados, tanto en caché como en memoria, son los mismos. La principal diferencia es que las peticiones se realizan por difusión, es decir, no van directamente al nodo origen, sino que se envían a los demás nodos también.

Aún así, las respuestas se envían al nodo origen, que es quién las gestiona.

Vemos las transferencias.

- Tipos de paquetes:

  Los tipos de nodos tampoco cambian.

  - **Difusión de petición**
    - De solicitante a origen y propietario:
      - PtLec, lectura de un bloque
      - PtLecEx, lectura con acceso exclusivo.
      - PtEx, acceso exclusivo sin lectura
    - De solicitante a origen
      - PtPEsc, posescritura
  - **Respuesta**
    - De propietario a origen
      - RPBloque, respuesta con bloque
      - RPInv, respuesta confirmando invalidación sin bloque.
      - RPBloqueInv, respuesta confirmando invalidación con bloque.
    - Origen a solicitante
      - RPBloque, respuesta con bloque
      - RPInv, respuesta confirmando invalidación sin bloque.
      - RPBloqueInv, respuesta confirmando invalidación con bloque.

### Lección 9: Consistencia del sistema de memoria

#### A) Concepto de consistencia de memoria

Los multiprocesadores, como ya hemos visto, implementan **coherencia** en el hardware, es decir, van a ver el mismo orden-serie en los accesos a _una dirección de memoria_. 

Sin embargo, diferenciamos este concepto con el de **consistencia**, que nos indica en qué orden van a ver los multiprocesadores todos los accesos a memoria.

La consistencia de memoria especifica las restricciones en el orden en el cual las operaciones de memoria deben parecer haberse realizado (operaciones a las mismas o distintas direcciones de memoria y emitidas por el mismo o distinto procesador).

Podemos diferenciar, según si estamos hablando de procesadores o de flujos de instrucciones, de **consistencia software** o **consistencia hardware**.

#### B) CONSISTENCIA SECUENCIAL

La **consistencia secuencial** es el modelo de consistencia que espera el programador de las herramientas de alto nivel. Es decir, es lo que se espera por lógica que ocurra.

La consistencia secuencial plantea varios requisitos:

- Todas las operaciones de un único procesador(thread) parezcan ejecutarse en el orden descrito por el programa de entrada al procesador. (**orden del programa**) 
  - Todos los procesadores ven los accesos a memoria en el mismo orden en el que aparecen en el código que se está ejecutando.
- Todas las operaciones de memoria parecen ser ejecutadas una cada vez (**ejecución atómica**), es decir que todos los procesadores ven el MISMO acceso a memoria.

Este último requisito hace que se presente el sistema de memoria como una memoria global conectada a todos los procesadores a través de un conmutador central.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-02 12-18-13.png" alt="Captura de pantalla de 2020-05-02 12-18-13" style="zoom:50%;" />

Veamos un ejemplo:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-02 12-19-47.png" align="left" alt="Captura de pantalla de 2020-05-02 12-19-47" style="zoom:50%;" />

- CASO 1: se espera que o bien entre P1 o bien entre P2 a la ejecución del bucle, pero que no entren los dos.
- CASO 2: se espera que, o bien reg1=1 o bien nada
- CASO 3: P2 se quedaría esperando hasta que se actualize el valor de A. 
  - Supuestamente este último código podría valer como un código de sincronización, siempre que el sistema en el que se ejecute tenga consistencia secuencial.

Sin embargo, actualmente ningún multiprocesador implementa consistencia secuencial, y por eso se recurre a un modelo más relajado con los requisitos.



#### C) MODELOS de consistencia relajados

 Son modelos de consistencia que, aunque se basan en el secuencial, difieren con él en los requisitos para poder incrementar las prestaciones.

Básicamente empiezan a permitir desordenar los accesos a memoria de _distintas direcciones_.

- **Orden del programa** 
  - Hay modelos que permiten que se relaje en el código ejecutado el orden entre dos accesos a distintas direcciones (W-R, W-W, R-RW).
- **Atomicidad**
  - Hay modelos que permiten que un procesador pueda ver el valor escrito por otro antes de que este valor sea visible al resto de los procesadores del sistema.

Los modelos relajados comprenden la especificación de:

- Los órdenes de acceso a memoria que no garantiza el sistema de memoria.
  - Órdenes de un mismo procesador
  - Atomicidad en las escrituras
- Mecanismos que ofrece el hardware para garantizar un orden cuando sea necesario.

Un procesador puede leer anticipadamente o bien su propia escritura o bien la escritura de otro procesador, es decir, puede acceder a lo que haya escrito algún procesador _antes de que lo vean todos los demás_, y es algo que permite el **hardware**.

Además, se pueden añadir **instrucciones máquina** que hacen que el orden se mantenga cuando es código de comunicación:

- LDA: se ocupa de la adquisición y liberación de la memoria.
- DMB (u MEMBAR) actúa como una barrera de memoria, es decir hasta que no se hacen todos los accesos anteriories no se hacen los otro.
- I-m-e: garantiza la lectura-modificación-escritura atómica, añade orden también. Es decir, mientras se hagan esas instrucciones, ningún otro procesador va a leer, modificar, etc.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-02 12-56-40.png" alt="Captura de pantalla de 2020-05-02 12-56-40" style="zoom:50%;" />

Volvemos al ejemplo anterior:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-02 12-57-16.png" alt="Captura de pantalla de 2020-05-02 12-57-16" style="zoom:50%;" />

- CASO 1:
- CASO 2: 
  - P1 actualiza su caché y propaga la actualización
  - P2 llega al if y como ya ha llegado la propagación de P1, entra a hacer escritura.
  - P2 actualiza las cachés con copia de B

##### **C.1. MODELO QUE RELAJA W-R**

Estos modelos permiten que una lectura pueda adelantar a una escritura previa en el orden del programa, **evitando dependencias RAW**.

Características:

- Es implementado por sistemas con buffer FIFO de escritura para los procesadores. El buffer evita que las escrituras retarden la ejecución del código bloqueando lecturas posteriores.
- Permiten que el procesador pueda leer una dirección directamente del buffer. Por ejemplo cuando se lee antes que otros procesadores una escritura propia.
- Puede incluir **instrucciones de serialización**, que garantizan un orden correcto.
- Hay sistemas en los que se permite que un procesador pueda hacer lectura anticipada (acceso no atómico).
  - Si queremos asegurar el acceso atómico se usarían instrucciones como la de Intel anterior.

##### **C.2. MODELO QUE RELAJA W-R Y W-W**

Además de tener lo mencionado anteriormente, que relaja W-R, tiene las siguientes características:

- Tiene buffer de escritura que permite que lecturas adelanten a escrituras.
- Permiten que el hw solape escrituras a memoria a distintas direcciones, para que puedan llegar a la memoria principal o a cachés de todos los procesadores fuera del orden del programa.
- Se proporciona hardware que garantiza los dos órdenes. Por ejemplo sistemas Sun Sparc.



### Lección 10: Sincronización

#### A) Comunicación en multiprocesadores y necesidad de usar código de sincronización

##### Comunicación uno-uno

Empecemos por la comunicación más básica que podemos utilizar: la comunicación uno a uno.

Esta comunicación debe ser la comunicación mínima garantizada y se da entre un generador, que es quien envía el mensaje, y un consumidor, que es quien lo recibe.

En este tipo de comunicación se tiene que garantizar que :

- El consumidor lea la **variable compartida** cuando el proceso que envía haya escrito en la variable el dato.
- Si reutilizamos la variable se debe garantizar que no se envía un nuevo dato hasta que se haya leído el anterior. 

Para poder asegurar lo anterior usamos **código de sincronización**. 

Sin embargo, hay veces que el código de sincronización no es suficiente para asegurar la comunicación correcta, y dependiendo del modelo de consistencia del sistema habría que añadir instrucciones que garantizan el orden (DMB).

##### Comunicación colectiva y condiciones de carrera

En la comunicación colectiva que vemos a continuación se reparten las iteraciones del bucle de manera implícita entre los distintos threads. Además, usamos variables privadas a cada thread que asegure que obtiene la suma correcta (_sump_).

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-07 12-56-52.png" alt="Captura de pantalla de 2020-05-07 12-56-52" style="zoom:50%;" />

Si analizamos dicho código llegamos a la conclusión de que si varios thread leen el mismo valor de sum, y le suman su correspondiente sump, el valor final de sum será el del thread más lento, y debería contener la suma de los dos thread.

$R_s^i - R_s^j- ( W_s^i || W_s^j)$. 

Esta situación se denomina **condición de carrera**, y el código afectado se denomina **sección crítica**. En este ejemplo resulta en que el valor final de sum no incluiría todos los sump. Por tanto, se debería cumplir lo siguiente:

- La lectura-modificación de sum se debe hacer en exclusión mutua
  - Usamos cerrojos
- El proceso 0 no tiene que imprimir hasta que todos los sump se hayan acumulado.
  - Usamos barreras

En resumen, las **secciones críticas** serán secuencias de instrucciones con una o varias direcciones compartidas (variables compartidas) que se deben acceder en exclusión mutua.

#### B) Soporte software y hardware de sincronización

El soporte necesario para la sincronización se representa por el siguiente esquema:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-07 12-59-07.png" alt="Captura de pantalla de 2020-05-07 12-59-07" style="zoom:67%;" />

Sobre el esquema haremos varias anotaciones:

- Los cerrojos, semáforos y barreras se implementan mediante instrucciones máquina.

- El soporte hardware es quién se encarga de hacer la sincronización eficiente.

- Conforme aumentamos la abstracción, menos conocimientos son necesarios para implementar sincronización, pero el resultado serán programas más ineficientes.

  

#### C) Cerrojos

##### C.1.- Definición y características

Los cerrojos son un soporte software que permiten sincronizar mediante dos operaciones:

- **`lock()` o cierre del cerrojo**: adquieren el derecho a acceder a una sección crítica mediante la apertura o cierre del cerrojo k:
  - Si varios procesos intentan el cierre a la vez, sólo uno de ellos lo consigue y el resto pasa a una etapa de espera.
  - Todos los procesos que accedan a `lock()`con el cerrojo cerrado se esperarán.
- **unlock()` o apertura del cerrojo**: libera a uno de los threads que esperan el acceso a una sección crítica (éste adquiere el cerrojo):
  - Si no hay threads en espera permite que el siguiente thread que ejecute `lock()` lo adquiera sin espera.

Básicamente, los cerrojos aseguran que solamente hay un FI (Flujo de Instrucciones) en la sección crítica, mediante una variable k, que indicará el estado del cerrojo (k=0 abierto, k=1 cerrado).

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-07 13-09-38.png" alt="Captura de pantalla de 2020-05-07 13-09-38" style="zoom:50%;" />



##### Componentes en un código para sincronización

######  1. Método de adquisición

Es el método por el cual un thread trata de adquirir el derecho a pasar a utilizar unas direcciones compartidas. En otras palabras, cómo actualiza k para que los demás no puedan verlo.

- Utilizando lectura-modificación-escritura atómicas.
- Utilizando LL/SC, instrucción compuesta por:
  - Instrucción de lectura enlazada (linked load LL)
  - Operación de escritura condicionada (Store Conditional) a que entre LL y SC ningún otro SC haya leído.
  - En esta operación se permite el acceso de otros FI pero no se llevará a cabo la escritura

###### 2. Método de espera

Es el método por el cual un thread espera a adquirir el derecho a pasar a utilizar unas direcciones compartidas. La espera se puede implementar mediante:

- **Espera ocupada**: los threads espera en un bucle while hasta que se abre el cerrojo.
- **Suspensión del proceso o del thread**, el procesador conmuta a otro thread y éste se pasa a una cola del SO.

Si la sección crítica es corta, se suele implementar una espera ocupada, pues la suspensión del proceso conlleva una llamada al sistema operativo y debemos asegurarnos de que no la hacemos para nada.

###### 3. Método de liberación

Es el método utilizado por un thread para liberar a uno (cerrojo) o varios thread en espera (barrera).

##### C.2.- Implementaciones

###### Cerrojo Simple

Se implementa con una variable compartida k que toma dos valores: abierto(0), cerrado(1).

- La apertura del cerrojo (`unlock()`) se realiza mediante una **operación indivisible** que escribe un 0.
- El cierre del cerrojo (`lock()`) primero lo lee y luego escribe un 1.
  - El resultado de la lectura:
    - Si el cerrojo estaba cerrado el trhead espera hasta que otro thread ejecute `unlock()`.
    - Si estaba abierto adquiere el derecho a pasar a la sección crítica y cierra el cerrojo (k=1).

Claramente la operación que incluye la lectura y escritura de k (pasar de 0 a 1) debe ser atómica, de lo contrario, varios threads podrían pasar a la sección crítica.

###### Cerrojo Simple II

Se debe añadir lo necesario para asegurar que la lectura y escritura de 1 en el cerrojo sea en **exclusión mutua**, es decir, que sea una **operación atómica**, que mientras un FI hace el R-W de k, ningún otro FI pueda acceder a k.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-07 13-23-34.png" alt="Captura de pantalla de 2020-05-07 13-23-34" style="zoom:50%;" />

En el caso de que haya más de un flujo de instrucciones, el primero que realice la operación atómica será el único que lea un 0, los demás leen un 1 y por tanto se quedan en espera.

###### Cerrojos con OpenMP

Utilizan las directivas de OpenMP vistas en prácticas:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-07 13-25-21.png" alt="Captura de pantalla de 2020-05-07 13-25-21" style="zoom:50%;" />

###### Cerrojos con etiqueta

Fijan un orden FIFO en la adquisición del cerrojo (se debe añadir lo necesario para garantizar el acceso en exclusión mutua al contador de adquisición y el orden imprescindible en los accesos a memoria). 

En este tipo de cerrojos se añaden dos contadores:

- Contador de adquisición: `contadores.adq`
- Contador de liberación: `contadores.lib`

Y funciona de la siguiente manera:

- Cuando un thread llega a la sección del cerrojo:
  - "Coge número", es decir, almacena en una variable local el valor de `contadores.adq` en el momento de su llegada.
  - Incrementa `contadores.adq` en uno.
  - Mientras no coincida su número con el número de `contadores.lib`, espera.
- Cuando un thread termina de ejecutar la sección crítica:
  - Incrementa `contadores.lib` para que el siguiente número pueda pasar.

Como ya he mencionado antes, es importante mantener el acceso a los contadores en exclusión mutua, teniendo en cuenta el orden de los accesos que proporcione el modelo de consistencia del sistema en el que estemos trabajando.

#### D) Barreras

Las barreras son zonas de código en las que los FI se quedan esperando hasta que llegan todos.

Normalmente consran de un **contador** y de una **bandera**. El primero informa de cuántos thread han llegado a la barrera, y la segunda "se levantará" cuando el contador tenga el valor necesitado, es decir, cuando todos los thread hayan llegado.

Claramente la consulta y modificación de la bandera (en el caso del primer thread que llegue, poner la bandera a 0, y en el caso del último thread que llegue poner el contador a 0 y la bandera a 1, los demás thread solamente tienen que incrementar el contador), se tiene que hacer en **exclusión mutua**, y por tanto se tendrá que tener en cuenta el modelo de consistencia del sistema. 

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-02-42.png" alt="Captura de pantalla de 2020-05-08 12-02-42" style="zoom:67%;" />

##### Problema con barreras reutilizadas

Sin embargo, surge un problema con las actualizaciones mencionadas anteriormente, y es que si bien hacen que las banderas se puedan reutilizar, puede darse el siguiente escenario:

- Los thread entran uno a uno en el bucle de espera.
- Antes de que llegue el último thread, uno de los que estaban esperando ($FI_i$) es suspendido por el SO.
- $FI_i$ pasa a ejecutar la rutina de tratamiento de interrupción.
- Llega el último thread y actualiza los valores:
  - Contador 0
  - Bandera 1
- Los thread continúan sus ejecuciones hasta llegar a una **segunda barrera**
- Van entrando y mientras tanto el $FI_i$ vuelve a su ejecución anterior y por tanto reinicia la ejecución dentro del bucle de espera.

Esto hace que en un momento dado como la bandera es 0 en ambas barreras, el $FI_i$ se quede esperando en la barrera anterior y todos los demás threads esperen en la segunda barrera esperando a que $FI_i$ llegue, pero no puede.

**Solución**: 

La solución para la situación descrita anteriormente pasa por mantener el uso de la bandera anterior, es decir si la barrera anterior se ha abierto con b=1, en la siguiente barrera _se esperará mientras b=1_. Por tanto, en cada barrera se espera en un valor distinto que se almacena en una variable local.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-09-41.png" alt="Captura de pantalla de 2020-05-08 12-09-41" style="zoom:67%;" />

#### E) Apoyo hardware a primitivas software

##### E.1.- Instrucciones de lectura-modificación-escritura atómicas

Tenemos tres operaciones básicas que se realizan en exclusión mutua:

- `Test&Set(x)`: hace una comprobación del contenido de una variable al mismo tiempo que varía su contenido en caso que la comprobación se realizó con el resultado verdadero.

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-13.png" align ="left" alt="Captura de pantalla de 2020-05-08 12-17-13" style="zoom:50%;" /> <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-25.png" alt="Captura de pantalla de 2020-05-08 12-17-25" style="zoom:50%;" />





- `Fetch&Oper(x, a)`: incrementa de manera atómica el valor pasado como parámetro x con a y devuelve el valor original de x.

  - El lock de la implementación ensamblador garantiza la exclusión mutua si en esa escritura se realiza alguna lectura o escritura.

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-32.png" align ="left" alt="Captura de pantalla de 2020-05-08 12-17-32" style="zoom:50%;" /> <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-37.png" alt="Captura de pantalla de 2020-05-08 12-17-37" style="zoom:50%;" />





- `Compare&Swap(a,b,x)`: compara los contenidos de a y x y si son el mismo intercambia b y x.

  - En la implementación ensamblador que vemos hay una pequeña diferencia, y es que se llama como operando implícito eax y en el caso de que coincidan los valores al intercambiar se carga el valor de la memoria a eax en vez de al registro.

  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-43.png" align="left" alt="Captura de pantalla de 2020-05-08 12-17-43" style="zoom:50%;" /> <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-17-48.png" alt="Captura de pantalla de 2020-05-08 12-17-48" style="zoom:50%;" />



##### E.2.- Cerrojos simples con instrucciones atómicas

###### Test&Set

Vemos que el software de bajo nivel está escrito en verde y las primitivas de los procesadores están escritas en morado. Básicamente garantizamos que el acceso a la variable de cierre k se haga en exclusión mutua, mediante la comprobación y posible escritura con test&set.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-23-37.png" alt="Captura de pantalla de 2020-05-08 12-23-37" style="zoom:50%;" />

###### Fetch&Or

Hace el or con k y 1 de manera atómica, para que si k=0, el or dará 1  y se quedará esperando y si k=1, es decir que no hay ningún FI en la sección crítica del cerrojo, el or sacará 0 y actualizará k y podrá continuar la ejecución.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-27-55.png" alt="Captura de pantalla de 2020-05-08 12-27-55" style="zoom:50%;" />

###### Compare&Swap

Con esta instrucción atómica aseguramos que si a(0) = k, entonces k empieza a valer b, por tanto solo se pasa a la sección crítica si k=0. Bastaría con poner como condición de espera b=1.

En otras palabras, se llama a compare&swap con (0,1,k):

- Si k =0, se intercambiarán valores y por tanto k=b=1 y b=0
- Si k=1, no se intercambian valores y b=1, k=1, **cerrojo cerrado**.

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-31-54.png" alt="Captura de pantalla de 2020-05-08 12-31-54" style="zoom:50%;" />

##### Consistencia de liberación

Podríamos poner todos los ejemplos que hay en las diapositivas, pero en realidad lo único que hace falta saber de esta sección es que hay procesadores (ARM) que por su consistencia en el sistema de memoria , donde una lectura puede adelantar a una escritura, no garantizan que no se abre el cerrojo hasta que no acabe todo el código de la sección crítica.

##### E.3.- Algoritmos eficientes con primitivas hardware

Usar primitivas hardware hace que los algoritmos con cerrojo sean más eficientes primero porque no hay código de lock(), y segundo porque no tenemos que hacer uso de variables locales.

En el primer caso vemos que aunque se use una instrucción atómica, no hay paralelismo:

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-35-17.png" alt="Captura de pantalla de 2020-05-08 12-35-17" style="zoom:50%;" />

En el segundo y tercer ejemplo se hace la suma de elementos de un vector con fetch&add() y con compare&swap():

<img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-36-53.png" alt="Captura de pantalla de 2020-05-08 12-36-53" style="zoom:50%;" />  <img src="../../../../../../Imágenes/Captura de pantalla de 2020-05-08 12-36-58.png" alt="Captura de pantalla de 2020-05-08 12-36-58" style="zoom:50%;" />

Por último, vemos la diferencia en OpenMP entre las directivas Atomic y Critical:

- Atomic: utiliza primitivas hardware como las vistas anteriormente
- Critical: utiliza los cerrojos más ineficientes

Por tanto, deberíamos usar **atomic**. 