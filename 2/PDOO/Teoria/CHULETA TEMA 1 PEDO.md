## CHULETA TEMA 1 PEDO

### Atributos y métodos

- No se puede acceder a atributos/métodos de instancia desde un método de clase (se solicita a una instancia concreta).

- JAVA:

  - Accesos a elementos privados permitidos:
    - Desde instancia a otra instancia
    - Desde ámbito de clase a una instancia de esa clase
    - Desde instancia a la clase de la que se es instancia.

- RUBY:

  - Los atributos de clase @@ son accesibles desde el ámbito de instancia.
  - Los atributos de instancia de clase NO son accesibles desde instancia (hacer un getter).

  

  ### Constructores

  Inicializar todos los atributos.

  - JAVA:
    - Se pueden sobrecargar, no problem
  - RUBY:
    - No hay sobrecarga, o bien:
      - Método de clase que llame a new
      - Num variable de parámetros

### Herencia

Se hereda todo, pero no se tiene por qué acceder a todo.

- JAVA: 
  - super se puede referir a cualquier método
  - en el constructor hay que poner super en la primera línea
  - no se redefinen los métodos private y final
  - Sí que se permite al redefinir métodos:
    - Aumentar la accesibilidad
    - El valor retornado puede ser una subclase del indicado en el ancestro
- RUBY:
  - super solo se refiere al método que se redefine
  - no se llama al constructor del padre automáticamente, es nuestra responsabilidad.



