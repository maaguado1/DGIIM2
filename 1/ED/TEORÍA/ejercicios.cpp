#include <list>
#include <iostream>
//María Aguado Martínez

//EJERCICIO 1

using namespace std;
int suma_subtree(ArbolBinario<int>& T){
    int suma =0;
    for (ArbolBinario<int>::iterador it = T.begin(); it != T.end(); it++)
        suma += *it;
    return suma;
}

int max_subtree(ArbolBinario<int>& T){

    int suma;
    int max;
    ArbolBinario<int>::postorden_iterador it = T.begin();
    ArbolBinario<int>::subarbol;
    subarbol.asignar_subarbol(T, (*it));
    suma = suma_subtree(subarbol);
    max = suma;
    it ++;
    while (it != T.end()){
        subarbol.asignar_subarbol(T, (*it));
        suma = suma_subtree(subarbol);
        it ++;
        if (max < suma)
            max = suma;
    }
    return max;
}

//EJERCICIO 2

void rutalista(list<int>&l, int n){
    int elemento;
    for (int i =0; i < n; i ++){
        elemento = *(l.begin());
        l.push_back(elemento);
        l.pop_front();
    }
}



//EJERCICIO 3

void juntalista(list<int> &l, int n){

    int suma =0, i=0;
    list<int> otra;
    for (list<int>::iterator it=l.begin();it != l.end(); l++){
        if (i<n){
            suma += *it; 
            i++;
        }
        else{
            otra.push_back(suma);
            suma =0;
            i=0;
            it--;
        }
        
    }
}

// EJERCICIO 4


void ordenarburbuja(list<int>::iterator inicio, lista<int>::iterator final){
    int cambio ;
    for (list<int>::iterator it = inicio +1; it!=final; it++){
        for (list<int>::iterator ot = inicio; it!=final-1; it++){
            if (*ot > *(ot++))
                cambio = *ot;
                *ot = *(ot++);
                *(ot++) = cambio;
        }
    }

}

void ordenag(list<int> &l, int n){

    int tamanio = l.size(), contador=0, t=0;
    int grupos = (l.size()%n ==0 ) ? (l.size()/n) :((l.size()/n)+1);
    list<int>::iterator inicio = l.begin();
    list<int>::iterator final =(inicio+n < l.end()) ? (inicio+n):(l.end());
    while (contador < grupos){
        ordenarburbuja(inicio, final);
        contador++;
        inicio = final;
        final = (inicio+n < l.end()) ? (inicio+n):(l.end());
    }

}


//EJERCICIO 5

int dminmax(list<int>&l){

    int min = *(l.begin());
    int max = *(l.begin());
    //Encuentro la posicion del minimo y del máximo
    for (list<int>::iterator it =l.begin(); it != l.end(); it++){
        if (min <= *it)
            min = *it;
        if (max >= *it)
            max = *it;
        
    }

    //encuentro la posicion por iteradores
    list<int>::iterator max, min;
    bool encontrado1=false, encontrado2=false;
    for (list<int>::iterator it =l.begin(); it != l.end(); it++){
        if (*it==min && !encontrado){
            min = it;
            encontrado1 = true;
        }
        
        if (*it == max)
            max = it;
    }

    //Calculo la distancia 
    int dis=0;
    for (list<int>::iterator it =min; it != max; it++){
        dis ++;
    }
    dis ++;
    return dis;

}   

//EJERCICIO 6

bool lexiord(list<int> l1, list<int> l2){
    bool distinto = false,  mayor = false;
    list<int>::iterator it=l1.begin();
    for (list<int>::iterator ot = l2.begin; !distinto && ot != l2.end() && it != l1.end() ; ot++){
        if (*ot != *it){
            distinto = true;
            if(*it > *ot)
                mayor = true;
        }
        it++;
    }
    if (distinto == false){ //Una está contenida dentro de la otra (ha llegado al final de una de ellas)
        mayor= (l2.size()<l1.size())? true:false;
    }

    return mayor;
}



//EJERCICIO 7

void rotacion (queue<int>& c){

    int numero = c.front();
    while (numero % 2 != 0 && !c.empty()){
        c.push(numero);
        c.pop();
        numero = c.front();
    }
}

//EJERICIO 8

bool pesointerior(bintree<int> a){

    bintree<int>::postorder_iterator it = a.begin();
    int sumahoj =0; sumanod=0;
    while (it != a.end()){

        //si es hoja
        if ( it.nodo.left().null() && it.nodo.right().null() )
            sumahoj += *(it);
        else 
            sumanod += (*it);

    }
    return (sumanod==sumahoj) ? true:false;
}



//EJERCICIO 9


bool compara(list<int> l,list<int> seq, list<int>::iterator p){
    bool esta= true;

    for (list<int>::iterator ot = seq.begin(); ot != seq.end() && p != l.end() && esta ; ot++){
        if (*ot != *p){
            esta = false;
        }
        p++;
    }
    return esta;

}

void sustituye (list<int> l, list<int> reemp, int n, list<int>::iterator p){

    //Elimino elementos
    int contador =0;
    while (contador <n)
    {
        l.erase(p);
        contador ++;
    }


    //Inserto elementos
    for (list<int>::iterator it = reemp.begin(); it!=reemp.end(); it++){
        l.insert(p, *it);
        p++;
    }
    
}


void reemplaza (list<int>&l, list<int> seq, list<int> reempl){

    for (list<int>::iterator it = l.begin(); it!= l.end(); it++){
        if (compara(l, seq, it)){
            sustituye(l, reemp, seq.size(), it);
        }

    }
}


//EJERCICIO 10

int sumaparantes(bintree<int> a, node n){
    int suma =0;
    int anterior;
    bintree<int>::preordeniterator it(n);
    //Verifico que el nodo no tiene 
    if (*n %2 == 0 ){
        suma += *n;

        if (!n.left().null())
            suma += sumaparantes(a, n.left())
        if (!n.right().null())
            suma +=sumaparantes(a, n.right());
    }
    return suma;
}


//EJERCICIO 11

node cont_hijos(list<int> l, bintree <int> a){
    bool encontrado = false;
    bool esta = true;
    nodo n = a.getRaiz(), p=n;
    while (*n!= *(a.end_preorder()) && !encontrado){
        if (!n.left().null())
        {
            ot = n.left();
            for (list<int>::iterator m = l.begin();  m!= l.end() && esta; m++ ){
                if (*m != *ot || l.size() == 2 && n.right().null()  ) 
                    esta = false;
                else 
                    ot = n.right()
                    
            }
            if (esta){
                encontrado = true;
                p = n
            }
            n = n.left();
        }
        else{
            n = n.right();
        }
        
    }
    return p;
}


//EJERCICIO 13

bool camino_ord(bintree<int> a, node n){
    bool devuelve;
    if (n.left().null())
        return true;
    else {
        if (*n.left() >= *n)
            if(camino_ord(a, n.left())
                return true;

        else if  (!n.right().null() && *n.left() >= *n){}
            if (camino_ord(a, n.right()))
                return true;


        else
            return false;
        
            
    }

}



// EXAMEN 2018-2019

//EJERCICIO 1

class documento{

    private:
        list<string> docu;
    public:

        documento(const list<string> &texto){
            docu = texto;
        }

        set<int> posiciones(string palabra)const{
            set<int> posic;
            int ola=0;
            for (list<int>::iterator it = docu.begin(); it!= docu.end(); it++){
                if (*it == palabra)
                    posic.insert(ola);
                ola ++;
            }
            return posic;
        }

        string palabra(int i )const{
            int hola=0;
            list<int>::iterator it = docu.begin();
            while (hola<i)
                it++;
            return *it;
        }
};




//EJERCICIO 4

list<int> nivel(const bintree<int>& A){

    queue<pair<ArbolBinario<int>::nodo, int> > q;
    ArbolBinario<int>::nodo n = A.getRaiz();
    pair<ArbolBinario<int>::nodo, int> p(n,0);

    int level =0;
    list<int> laux;
    list<int> lout;

    q.push(p);
    while( !q.empty()){

        p = q.front();
        if (p.second == level){
            laux.push_back(*(p.first));
        }
        else{

            if (laux.size() > lout.size()){
                lout = laux;
                level ++;
                laux.clear();
            }
            
            laux.push_back(*(p.first));
        }
        q.pop();
        n= p.first;
        int aux_level = p.second;
        if (!n.hi().nulo()){
            p.first=n.hi();
            p.second = aux_level;
            q.push(p);
        }

        if (!n.hd().nulo()){
            p.first=n.hd();
            p.second = aux_level;
            q.push(p);
        }
    
    }

    if (laux.size()>lout.size()){
        lout=laux;
    }
    return lout;



}


//EXAMEN 2019 FEBRERO

//EJERCICIO 1

void juntalista(list<int>&L, int n){
    int suma=0, contador=0;
    if (!L.empty() && n!= 0){
        list<int>::iterator it=L.begin();
        //Recorro la lista y sustituyo
        while(it!= L.end()){
            
            list <int>::iterator ot=it;
            while(contador < n && ot != L.end()){
                suma += *ot;
                ot ++;
                contador ++;
            }
            contador =0;
            L.erase(it, ot);
            L.push_front(suma);

            
        }
    }
    
}


//EJERCICIO 2

bool esABB(bintree<int> & A){
    ArbolBinario<int>::nodo n=A.getRaiz();
    bool loes = true;

    while(loes){
        if (!n.hi().nulo()){
            if (*n.hi() > *n)
                loes = false;
        }
        if (!n.hd().nulo()){
            if (*n.hd() < *n)
                loes= false;
        }
        if (loes){ //si todavía se mantiene la condición

            if (!n.hi().nulo())
                n = n.hi();
            else{
                if ()
            }
        }
        }
    }
}