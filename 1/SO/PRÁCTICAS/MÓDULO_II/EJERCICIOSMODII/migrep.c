#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>

int lineas =0;
int hijo;
int fderror=0;
void manejador(int sig){

    printf ("\nActualmente llevo %d lineas", lineas);
    //kill hijo
    kill (hijo, SIGKILL);
    close(fderror);
    exit(-1);
}



int main (int argc, char *argv[]){

    if ((argc != 4) && argv[1] == '-c' ){
        printf("\nError de argumentos: queríamos ./migrep -c <literal> <ruta_dir>");
        exit (-1);
    }


    //ACTUALIZO LA SALIDA DEL ERROR AL ARCHIVO
    int fderror;
    if (fderror = (open("ErrorMigrep", O_CREAT|O_RDWR, S_IWUSR|S_IRUSR))<0){
        printf ("\nError en el open del error. No se puede dirigir la salida. Aborto");
        exit (-1);
    }
    dup2(fderror, STDERR_FILENO);

    //ESTABLEZCO EL COMPORTAMIENTO PARA CNTRL+C
    struct sigaction nueva;
    int hijo = getpid();
    nueva.sa_handler = manejador;
    sigemptyset(&nueva.sa_mask);
    if (sigaction(SIGINT, &nueva, NULL) == -1){
        perror("error en el manejador");
        exit (-1);
    }

    //RECORRO EL DIRECTORIO

    DIR *direct; //abro directorio
    char *pathname;
    char *literal;
    char *cadena;
    char otro[500];
    int estado;
    struct dirent *lectura;
    struct stat atributos;
    pathname = argv[3];
    literal = argv[2];
    printf ("\nBUSCANDO EN ... %s/", pathname );
    if((direct=opendir(pathname)) == NULL){
        perror("\nError al abrir directorio\n");
        exit(-1);
    }
    printf ("\nVoy a leer el directoiro jejejej ");

    int fd[2];
    pipe(fd);

    while ((lectura =readdir(direct)) != NULL){
        printf ("\nHe leído ./%s", lectura->d_name);
        if (strcmp(lectura->d_name, ".") != 0 && strcmp(lectura->d_name, "..") != 0){
            sprintf ( cadena , "%s/%s", pathname,lectura->d_name);
            if ( stat(cadena, &atributos)<0){
                perror("\nError al obtener datos\n");
                exit(-1);
            }
            printf ("\nVeo si %s es regular", cadena);
            if (S_ISREG(atributos.st_mode)){
                printf ("\nEs reg");
                if( (hijo=fork())<0) {
                    perror("\nError en el fork");
                    exit(-1);
                }

                if (hijo == 0){
                    printf ("\nSoy el hijo");

                    close(fd[0]);
                    dup2(fd[1], STDOUT_FILENO);
                    execl("/bin/grep", "grep", "-c", literal , lectura->d_name ,NULL);

                }
                else{
                    printf ("\nSoy el padre");
                    waitpid(hijo,&estado);
                    printf ("\nHe esperado a m hijo");
                    close(fd[1]);
                    dup2(fd[0], STDIN_FILENO);
                    if (!(read (STDIN_FILENO, &otro, sizeof(int)))){
                        perror("\nError al obtener el número grep\n");
                        exit(-1);
                    }
                    lineas += atoi(otro);
                    printf("\nHE recibdioddnde: lineas actuales %d", lineas);


                }
            }
            else
            {
                printf("\nNo lo es");
            }
            
        }
    }
    close (fderror);

    return 0;

}


