#encoding:utf-8

require_relative 'SpaceStation'
require_relative 'Loot'
require_relative 'PowerEfficientSpaceStationToUI'
module Deepspace
class PowerEfficientSpaceStation < SpaceStation
  @@EFFICIENCYFACTOR = 1.10
  
  def initialize(station)
    newCopy(station)
    
  end
  
 
  
  def setLoot(l)
    transformation = super(l)
    if transformation == Transformation::SPACECITY
        return Transformation::NOTRANSFORM
        
    else
      return transformation
      
  end
  
  end
  
    
    def protection
      prot= super
      return prot*@@EFFICIENCYFACTOR;
    end
    
    def fire
      return super * @@EFFICIENCYFACTOR
    end
    
     
  def getUIversion
    return PowerEfficientSpaceStationToUI.new(self)
  end
  
  def to_s
    return super + "(ESTACIÓN EFICIENTE)"
  end
end


end
