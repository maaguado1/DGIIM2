//------------------------------------------------------------------------------
// parity.cc
//------------------------------------------------------------------------------

#include <x86intrin.h> // __rdtscp

#include <algorithm>   // generate
#include <array>       // array
#include <bitset>      // bitset
#include <chrono>      // now
#include <functional>  // bind
#include <iomanip>     // setw
#include <iostream>    // cout endl
#include <numeric>     // iota

//------------------------------------------------------------------------------

const unsigned GAP = 12;   // gap between columns
const unsigned REP = 100;  // how many times we repeat every algorithm

std::array<unsigned, 1 << 16> list; // 64K x 4B = 256KB

//------------------------------------------------------------------------------

unsigned parity00(unsigned elemento) // return true if parity of elem is even
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity01(unsigned elemento) // return true if parity of elem is even
{
	unsigned paridad = 1; // paridad par
	for (unsigned i = 0; i < sizeof(elemento) * 8; ++i)
	{
		unsigned mascara = 1 << i;
		paridad ^= (elemento & mascara) != 0;
	}
	return paridad;
}

//------------------------------------------------------------------------------

unsigned parity02(unsigned elemento)
{
	unsigned paridad = 1, mascara = 1; // paridad par
	for (unsigned i = 0; i < sizeof(elemento) * 8; ++i)
	{
		paridad ^= (elemento & mascara) != 0;
		mascara <<= 1;
	}
	return paridad;
}

//------------------------------------------------------------------------------

unsigned parity03(unsigned elemento)
{
	unsigned paridad = 1;
	while (elemento != 0) 
	{
		paridad ^= elemento & 1;
		elemento >>= 1;
	}
	return paridad;
}

//------------------------------------------------------------------------------

unsigned parity04(unsigned elemento)
{
	unsigned paridad = 1;
	while (elemento != 0) 
	{
		paridad ^= elemento;
		elemento >>= 1;
	}
	return paridad & 1;
}

//------------------------------------------------------------------------------

unsigned parity05(unsigned elemento)
{
	unsigned paridad = 1;
	do
		paridad ^= elemento;
	while (elemento >>= 1);
	return paridad & 1;
}

//------------------------------------------------------------------------------

unsigned parity06(unsigned elemento)
{
    unsigned par = 1;
    asm("0:             \n"
        "    xor %1, %0 \n"
        "    shr %1     \n"
        "    jnz 0b     \n"
        :"+r"(par)
        :"r"(elemento));
    return par & 1;
}

//------------------------------------------------------------------------------

unsigned parity07(unsigned elemento)
{
	for (unsigned i = sizeof(elemento) * 8 / 2; i; i >>= 1)
		elemento ^= elemento >> i;
	return !(elemento & 1);
}

//------------------------------------------------------------------------------

unsigned parity08(unsigned elemento)
{
	return !(__builtin_popcount(elemento) & 1);
}

//------------------------------------------------------------------------------

unsigned parity09(unsigned elemento)
{
	return !(std::bitset<sizeof(elemento) * 8>(elemento).count() & 1);
}

//------------------------------------------------------------------------------

unsigned parity10(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity11(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity12(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity13(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity14(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

unsigned parity15(unsigned elemento)
{
	return elemento;
}

//------------------------------------------------------------------------------

template <class _F> void test (_F& __f, const char* msg)
{
	using namespace std::chrono;

	unsigned id; // needed by __rdtscp()

	unsigned long long cycle_overhead = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		unsigned long long t2 = __rdtscp(&id);
		if (t1 < t2) cycle_overhead = std::min(cycle_overhead, t2 - t1);
	}

	unsigned long long cycles = std::numeric_limits<unsigned long long>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		unsigned long long t1 = __rdtscp(&id);
		__f(list[0]);
		unsigned long long t2 = __rdtscp(&id);
		if (t1 < t2) cycles = std::min(cycles, t2 - t1);
	}
	if (cycles > cycle_overhead) cycles -= cycle_overhead;

	auto time_overhead = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		auto t1 = high_resolution_clock::now();
		auto t2 = high_resolution_clock::now();
		if (t1 < t2) time_overhead = std::min(time_overhead, t2 - t1);
	}

	unsigned result;
	auto time = duration<high_resolution_clock::rep, std::nano>::max();

	for (unsigned i = 0; i < REP; ++i)
	{
		result = 0;
		auto t1 = high_resolution_clock::now();
		result = std::count_if(list.begin(), list.end(), __f);
		auto t2 = high_resolution_clock::now();
		time = std::min(time, t2 - t1);
	}
	if (time > time_overhead) time -= time_overhead;

	std::cout << '"' << std::setw(GAP * 2 - 2) << msg << '"' 
	          << std::setw(GAP) << result
	          << std::setw(GAP) << cycles
	          << std::setw(GAP) << std::fixed << std::setprecision(2)
	          << duration_cast<nanoseconds>(time).count() / static_cast<double>(list.size())
	          << std::setw(GAP) << duration_cast<nanoseconds>(time).count() / 1000.0
	          << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	std::iota(list.begin(), list.end(), 0); // 0 .. list.size()
	list[1] = 0; // list.size() / 2 + 1 elementos con paridad par
	// std::fill(list.begin(), list.end(), 0);

	std::cout << "#" << std::setw(GAP * 2 - 1) << "parity"
	          << std::setw(GAP) << "value"
	          << std::setw(GAP) << "cycles"
	          << std::setw(GAP) << "time(ns)"
	          << std::setw(GAP) << "total(us)"
	          << std::endl;

	test(parity00, "empty");
	test(parity01, "for");
	test(parity02, "for v2");
	test(parity03, "while");
	test(parity04, "while v2");
	test(parity05, "do while");
	test(parity06, "asm");
	test(parity07, "arbol");
	test(parity08, "builtin");
	test(parity09, "bitset");
	test(parity10, "");
	test(parity11, "");
	test(parity12, "");
	test(parity13, "");
	test(parity14, "");
	test(parity15, "");
}

//------------------------------------------------------------------------------
