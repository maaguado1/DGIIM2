
#encoding:utf-8

require_relative 'Dice'
require_relative 'EnemyStarShip'
require_relative 'SpaceStation'
require_relative 'CombatResult'
require_relative 'GameStateController'
require_relative 'GameState'
require_relative 'CardDealer'
require_relative 'GameUniverseToUI'
require_relative 'Loot'
require_relative 'GameCharacter'
require_relative 'ShotResult'


module Deepspace
  
# Class that executes game actions
#
# @author María Aguado Martínez

class GameUniverse
  
  
  # Class atributes
	# ==========================================================================

  # @!attribute [Integer] amount of medals necessary to win the game
  @@WIN=10
  
  
  
  # Constructors
	# ==========================================================================

  
  
  # Class initializer
  # @!attribute [GameStateController] game state
  # @!attribute [Integer] number of turns
  # @!attribute [Dice] game dice
  # @!attribute [Integer] index of the station that is currently playing
  # @!attribute [SpaceStation] current space station
  # @!attribute [Array<SpaceStation>] set of space stations that are playing
  # @!attribute [EnemyStarShip] current enemy star ship
  
  def initialize
    @currentStationIndex=0
    @turns=0
    @dice = Dice.new()
    @currentStation = nil
    @spaceStations = []
    @currentEnemy = nil
    @gameState = GameStateController.new() 
  end
  
  
  
  # Getters
	# ==========================================================================


  
  # Getter for gameState
	# @return [GameState] gameState
  def state
    return @gameState.state
  end
  
  # Setters
	# ==========================================================================

  
  # Discard the hangar from the current space station
	# Discarded if gameState is INIT or AFTERCOMBAT
  def discardHangar
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT
      @currentStation.discardHangar
    end
  end
  
  
  

	# Discard a certain shield booster from the current space station
	# Discarded if gameState is INIT or AFTERCOMBAT
	# @param i [Integer] position of the shield booster to be discarded
  def discardShieldBooster(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT

      @currentStation.discardShieldBooster(i)
    end
  end
  
  
  
    # Discard a certain weapon from the current space station.
    # Discarded if gameState is INIT or AFTERCOMBAT
    # @param i [Integer] position of the weapon to discard
  def discardWeapon(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT
      @currentStation.discardWeapon(i)
    end
  end
  
  
    # Discard a certain weapon in the hangar from the current space station.
    # Discarded if gameState is INIT or AFTERCOMBAT
    # @param i [Integer] position of the weapon to discard
  def discardWeaponInHangar(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT

      @currentStation.discardWeaponInHangar(i)
    end
  end
  
  
  # Discard a certain shield booster in the hangar from the current space station.
    # Discarded if gameState is INIT or AFTERCOMBAT
    # @param i [Integer] position of the shield booster to discard
  def discardShieldBoosterInHangar(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT

      @currentStation.discardShieldBoosterInHangar(i)
    end
  end
  
  
    # Mount a certain shield booster from the current space station.
    # Mounted if gameState is INIT or AFTERCOMBAT
    # @param i [Integer] position of the shield booster to mount
  def mountShieldBooster(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT
      @currentStation.mountShieldBooster(i)
    end
  end
  
  
    # Mount a certain weapon from the current space station.
    # Mounted if gameState is INIT or AFTERCOMBAT
    # @param i [Integer] position of the weapon to mount
  def mountWeapon(i)
    if @gameState.state == GameState::INIT or @gameState.state == GameState::AFTERCOMBAT
      @currentStation.mountWeapon(i)
    end
  end
  
  
  # Checks if space ship that has the turn has won
	# @return [Boolean] true, if the space ship has won; false, otherwise
  def haveAWinner()
    
    if @currentStation != nil
      return @currentStation.nMedals == @@WIN
    else
      return false
      
    end
  end
  
  
  
   # Game methods
   # ==========================================================================

  
  # Executes combat, according to game rules
	# @param station [SpaceStation] station in combat
	# @param enemy [EnemyStarShip] enemy in combat
	# @return [CombatResult] combat result
  def combatGo(station, enemy)
    ch =@dice.firstShot
    if (ch==GameCharacter::ENEMYSTARSHIP)
      puts "\nENEMIGO ATACA:"
      fire = enemy.fire
      result = station.receiveShot(fire)
      
      puts "\nEl resultado es ... #{result.to_s}"
      if (result == ShotResult::RESIST)
        puts "\nATACA LA ESTACIÓN"
        fire = station.fire
        result = enemy.receiveShot(fire)
        
        puts "\nEl resultado es ... #{result.to_s}"
        enemyWins = (result==ShotResult::RESIST)
      else
        enemyWins = true
      end
      
    else
       puts "\nATACA LA ESTACIÓN"
      fire = station.fire
      result = enemy.receiveShot(fire)
       puts "\nEl resultado es ... #{result.to_s}"
      enemyWins = (result == ShotResult::RESIST)
      
    end
    
    if enemyWins
      puts "\nGana el enemigo de momento"
      s = station.speed
      moves= @dice.spaceStationMoves(s)
      
      if !moves
        puts " \nNO SE HA MOVIDO"
        damage = enemy.damage
        station.setPendingDamage(damage)
        combatResult = CombatResult::ENEMYWINS
      else
        puts "\nSE MUEVE"
        station.move
        combatResult= CombatResult::STATIONESCAPES
   
      end
      
    else
      puts "\nGANA EL ENEMIGO"
      aLoot = enemy.loot
      station.setLoot(aLoot)
      combatResult = CombatResult::STATIONWINS
    end
    
    @gameState.next(@turns, @spaceStations.length)
    return combatResult
  end
  
  
  # Makes combat between the space station that holds the turn and the
	# current enemy. The combat is realized if the app is on a state where
	# combat is allowed
	# @return [CombatResult] combat result
  def combat()
    if state == GameState::BEFORECOMBAT or state == GameState::INIT
      return combatGo(@currentStation, @currentEnemy)
      
    else
      return CombatResult::NOCOMBAT
    end
      
end

  
  
  
  # Starts a game
	# @param names [Array<String>] collection with the names of the players
  def init(names)
    
    if (state == GameState::CANNOTPLAY)
      @spaceStations =Array.new()
      dealer = CardDealer.instance
      
      for h in names
        
        puts "\n#{h}"
        supplies = dealer.nextSuppliesPackage
        station = SpaceStation.new(h, supplies)
        puts "\nHe creado: #{station.to_s}"
        @spaceStations.push(station)
        
        nh = @dice.initWithNHangars
        nw = @dice.initWithNWeapons
        ns = @dice.initWithNShields
        
        lo = Loot.new(0, nw, ns, nh, 0)
        station.setLoot(lo)
        
        
      end
      
      @currentStationIndex = @dice.whoStarts(names.length)
      @currentStation = @spaceStations[@currentStationIndex]
      @currentEnemy = dealer.nextEnemy
      @gameState.next(@turns, @spaceStations.length)
    end
    
  end
  
  
  
  # Takes turn to the next player, if there is no pending damage
	# @return [Boolean] true, if the turn could be changed; else, otherwise
  def nextTurn
    if state == GameState::AFTERCOMBAT
      stationState = @currentStation.validState
      
      if stationState
        @currentStationIndex = (@currentStationIndex + 1)% @spaceStations.length
        @turns += 1
        
        @currentStation = @spaceStations[@currentStationIndex]
        @currentStation.cleanUpMountedItems
        dealer= CardDealer.instance
        @currentEnemy = dealer.nextEnemy
        @gameState.next(@turns, @spaceStations.length)
        return true
      else
        return false
      end
    else
      return false

    end
  end
  
  
  
  
	# String representation, UI version
	# ==========================================================================

  def to_s
    mensaje=  "GameUniverse(\n" +
                "\tcurrentStationIndex =  #{@currentStationIndex} \n" +
                "\tcurrentStation = #{@currentStation.to_s} \n" +
                "\tcurrentEnemy = #{@currentEnemy.to_s}\n" +
                "\tturns = #{@turns}\n" +
                "\tdice = #{@dice.to_s}\n" +
                "\tgameState = #{@gameState.to_s } \n" +
                "\tWIN = #{@@WIN}\n" +
                ")";
    
    mensaje += "\n\t SPACE STATIONS:"
    for s in @spaceStations
      mensaje += s.to_s + "\n\n"
    end
    return mensaje
  end
  
  
  def getUIversion
    return GameUniverseToUI.new(@currentStation, @currentEnemy)
  end
  
end
end


