#include <iostream>
#include <chrono>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>
#include <climits>
#include <stdlib.h>

using namespace std;

// Nos creamos un struct para almacenar las coordenadas
struct Punto{
  float x;
  float y;
};

struct Arista{
  int inicio;
  int final;
};
/**
 * @brief Función para leer un fichero que contendrá un conjunto de nodos.
 * @param Un string, que hace referencia al nombre del fichero de entrada.
 * @param Un vector de Punto, donde iremos introduciendo los nodos del fichero(por ello, se pasa por referencia).
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_entrada (const string origen, vector<Punto> & nodos) {

  ifstream f(origen);
  bool return_value;

  if (!f) {
      cerr << "Fallo al abrir el fichero\n";
      return_value = false;
  }
  else {
      float n;
      string dimension;

      do {
          f >> dimension;
      } while (dimension != "DIMENSION:");

      f >> n;

      string coordenadas;

      do {
          f >> coordenadas;
      } while (coordenadas != "NODE_COORD_SECTION");

      nodos.clear();

      int i, j;
	  Punto P;
      float temporal;
      vector<float> vector_coordenadas;
      for (j = 0; j<n; j++) {
          f >> i;
          f >> temporal;
          P.x=temporal;

          f >> temporal;
          P.y=temporal;
		  nodos.push_back(P);
      }
   }

      return_value = true;

      f.close();
      return return_value;
}

/**
 * @brief Función para sacar un fichero que contendrá la solución del problema
 * @param Un string, que hace referencia al nombre del fichero de salida.
 * @param Un vector de enteros, que es el resultado obtenido por el algoritmo escogido.
 * @param Un vector de Punto, que contiene los nodos.
 * @return Un booleano, que indica si la operación se ha realizado con éxito o no.
 * */
bool fichero_salida(const string fichero, const vector<int> & resultados, vector<Punto> nodos) {
    ofstream f(fichero);

    if (!f) {
        cerr << "Fallo al abrir el archivo de salida";
        return false;
    }
    else {
        f << "DIMENSION: " << resultados.size() << endl;
        f << "TOUR_SECTION" << endl;

        for (int i=0; i<resultados.size(); i++)
            f << resultados[i] + 1 << " " << nodos[i].x<< " " <<nodos[i].y<< endl;

        f << resultados[0] + 1 << " " << nodos[0].x << " " << nodos[0].y << endl;

        f.close();

        return true;
    }
}

/**
 * @brief Función que calcula la distancia entre dos nodos dados
 * @Param Dos tipos de dato "Punto".
 * @return La distancia entre dichos nodos.
 * */
int distancia(Punto uno, Punto dos){
  return rint(sqrt( pow((dos.x- uno.x), 2) + pow((dos.y-uno.y), 2)));
}

/**
 * @brief Saca por pantalla la matriz de distancias
 * */
void SalidaMatriz(vector<vector<int>> T){
  int n = T.size();
  for (int i = 0; i < n; i++)
    {
      cout << "\n| ";
      for (int j = 0; j< n; j ++){
    cout << T[i][j] <<" " ;
  }

  cout  << "  |";

  }
  cout << "\n\n";
}

/**
 * @brief Genera una matriz con las distancias entre los puntos
 * @param nodos, los puntos sobre los que calculamos las distancias
 * @return una matriz con las distancias entre los puntos
 *
 * Matriz[i][j] := distance(pi, pj)
 *
 * */
vector<vector<int>> genera_matriz(vector<Punto> nodos){
  vector<vector<int>> T;
  int n =nodos.size();
  for (int i =0; i < n; i ++){
    vector<int> fila;
    for (int j = 0; j < n; j ++){
      if (j < i )
        fila.push_back(T[j][i]);
      else if (j == i)
        fila.push_back(0.0);
      else
      fila.push_back(distancia( nodos[i], nodos[j]));

    }
    T.push_back(fila);
  }
  return T;
}


/**
 * @brief Calcula el valor de la solución, según las distancias de los nodos
 * @param solucion,  solución de la que se busca el calcular_valor
 * @param T, matriz de aristas
 * @return float, la suma de las aristas que conectan los nodos de la solución
 *
 * */

 int calcular_valor (vector<int> solucion, vector<vector<int>> T){
   int valor=0;
   for (int i =0; i < solucion.size()-1; i++){
     valor += T[solucion[i]][solucion[i+1]];
   }
   valor += T[solucion[0]][solucion[T.size()-1]];
   return valor;
 }

/**
* @brief Calcula el mínimo de dos valores
* @param los valores a los que se quiere calcular el mínimo
* @return el valor mínimo de los pasados como parámetro
**/
int minimo(int a, int b){
    if (a < b)
        return a;
    else
	return b;
}

/**
* @brief Devuelve S con un elemento menos
* @param j=elemento que queremos quitar de s
* @return un vector con los elementos de S menos el de la posición j
* S no se modifica
**/
vector<int> S_quitar(int j, vector<int> S){
	vector<int> aux;
	int i=0;
	while(i!=j){
		aux.push_back(S[i]);
		i++;
	}
	for(i=j+1; i<S.size(); i++)
		aux.push_back(S[i]);
	return aux;
}

/**
* @brief Función clave de nuestro algoritmo de programación dinámica
* g(i,S)= min(L_{i,j}+g(j, S\{j})), donde S={conjunto de vértices en estudio}
**/
int g(int i, vector<int> S, vector<vector<int>> distancias){
	int a, min;
	min=distancias[i][S[0]] + g(S[0], S_quitar(0,S),distancias);
	for(int j=1; j<S.size();j++){
		if(S.size()==0)
			return distancias[i][1];
		a=distancias[i][S[j]]+g(S[j], S_quitar(j,S),distancias);
		min=minimo(a,min);
	}
	return min;
}

// PROGRAMACIÓN DINAMICA.
/**
 * @brief Calcula la mejor solucion para un conjunto de puntos mediante Programación dinámica
 * @pre T.size() > 0
 * @return el camino cuya distancia es la mínima, si todo sale bien o un vector vacío, si ocurre algún error.
 * */
vector<int> solucion_PD(vector<vector<int>> distancias){
	// Creamos el vector que contendrá el recorrido.
	vector<int> solucion;			 //O(1)
	int n = distancias.size();		 //O(1)
  	vector<int> S;
	int min;
	
	for(int j=0; j<n; j++){
		S.push_back(j);
		for(int i=1; i<1; i++){
			min=minimo(g(i,S,distancias),min);
			if(min==g(i,S,distancias))
				solucion=S;
		}
	}
	
	return solucion;							  //O(1)
}


//MAIN

int main(int argc, char *argv[]){

	if(argc!=2){
		printf("Error, modo de ejecución; ./ejecutable <nombre_fichero>");
		exit(-1);
	}

	clock_t t_antes;    // Valor del reloj antes de la ejecución
 	clock_t t_despues;  // Valor del reloj después de la ejecución
	vector<Punto> ciudades;
	string f_entrada,algoritmo;
	string f_salida;
	vector<vector<int>> matriz;
	vector<int> solucion;

	f_entrada = argv[1];
	//Abrimos el fichero de entrada
	if(!fichero_entrada(f_entrada,ciudades)){
		printf("Error en el fichero de entrada\n");
		return 1;
	}

	//Calculamos la matriz de distancias y aplicamos el algoritmo indicado.
	matriz=genera_matriz(ciudades);


  //Mostramos las coordenadas de las ciudades introducidas
  cout<<"Las coordenadas de las ciudades introducidas son " << endl;
  for(int i=0; i<ciudades.size();i++){
    cout <<"("<< ciudades[i].x << "," << ciudades[i].y << ")" << endl;
  }

	t_antes=clock();
	solucion=solucion_PD(matriz);
	t_despues=clock();



	//Mostramos el recorrido
	cout<< "\nEl orden de ciudades recorrido es el siguiente: " << endl;
	for(int i=0; i<solucion.size(); i++){
		cout << solucion[i] + 1 << endl;
	}

	//Mostramos dicho tiempo
	cout << ((double)(t_despues-t_antes))/CLOCKS_PER_SEC << endl;

	f_salida = f_entrada.append("_salida");

	//Introducimos en el fichero de salida la solución(el recorrido).
	if(fichero_salida(f_salida, solucion, ciudades))
        cout << "\nGuardado. Salida en: " << fichero_salida << endl;
    else{
        cerr << "\nError al guardar el fichero\n";
		return 1;
	}

}
