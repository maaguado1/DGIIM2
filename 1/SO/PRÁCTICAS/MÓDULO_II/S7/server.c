#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#define BUF_SIZE 1024
#define FIFO_GENERAL "fifo."
#define LONG_NOMBRE 50

void CHILD_HANDLER(){
	//printf("SERVER: se ma muerto el hijo\n");
	wait(NULL); // mata el proceso hijo zombie
}

int main(int argc, char** argv){
	// MANEJO DE SEÑALES
    struct sigaction sa;	
	sa.sa_handler = CHILD_HANDLER;
	sa.sa_flags = SA_RESTART; //necesario
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGCHLD, &sa, NULL) == -1){
		perror("Sigaction");
		exit(EXIT_FAILURE);
	}

	// CREACIÓN DE CERROJO
	int fdbloqueo;
	if ((fdbloqueo = creat("bloqueo", S_IRUSR|S_IWUSR)) == -1){
		perror("Creación bloqueo");
		exit(EXIT_FAILURE);
	}

	// CREACIÓN Y APERTURA DE FIFOS
	char nombrefifogen_e[50], nombrefifogen_s[50];
	sprintf(nombrefifogen_e, "%se", FIFO_GENERAL);
	sprintf(nombrefifogen_s, "%ss", FIFO_GENERAL);

	if (mkfifo(nombrefifogen_e, S_IRUSR|S_IWUSR) == -1){
		perror("Mkfifo entrada");
		exit(EXIT_FAILURE);
	}
	if (mkfifo(nombrefifogen_s, S_IRUSR|S_IWUSR) == -1){
		perror("Mkfifo salida");
		exit(EXIT_FAILURE);
	}

	int fde, fds;
	if ((fde = open(nombrefifogen_e, O_RDWR)) == -1){
		perror("Open fifo general entrada");
		exit(EXIT_FAILURE);
	}
	if ((fds = open(nombrefifogen_s, O_WRONLY)) == -1){ //así es bloqueante, igual es RDWR
		perror("Open fifo general entrada");
		exit(EXIT_FAILURE);
	}

	// MAIN LOOP
	int pid, pid_cliente, fdp, leidos;
	char nombrefifopers[LONG_NOMBRE];
	while (1){
		// Bloquea esperando petición de cliente
		if (read(fde, &pid_cliente, sizeof(pid_cliente)) != sizeof(pid_cliente)){
			perror("Read fifo general entrada");
			exit(EXIT_FAILURE);
		}
		// Hace el fork para atender la petición
		if ((pid = fork()) == -1){
			perror("Fork");
			exit(EXIT_FAILURE);
		}
		if (pid == 0){
			// El hijo atiende la petición
			//printf("SERVER HIJO: Pid cliente: %d\n", pid_cliente);
			// Crea el fifo personalizado
			pid = getpid();
			sprintf(nombrefifopers, "fifo.%d", pid);
			if (mkfifo(nombrefifopers, S_IRUSR|S_IWUSR) == -1){
				perror("Mkfifo personalizado");
				exit(EXIT_FAILURE);	
			}

			/* Dos opciones para abrir fifo personalizdo y escribir pid en fifo salida
			- Open antes de write poniendo RW, ya que si pones RDONLY
			  la llamada es bloqueante hasta que alguien habra con write, pero
			  el cliente no podrá abrirlo ya que no hemos hecho el write, luego
			  necesitamos poner RW para que no sea bloqueante.
			- Write antes de open poniendo R, ya que open ya puede ser bloqueante
			  porque el cliente ya sabe el fifo personalizado y lo abrirá
			  rápidamente
			Sin embargo, en mi solución es necesario que se habra con RDONLY, para
			que cuando el cliente haya terminado de escribir y lo cierre, la llamada
			read del proxy devuelva EOF. Si no, al estar en RW, sigue habiendo gente
			escribiendo y el proxy se quedará pillado en el read.
			*/
			if (write(fds, &pid, sizeof(pid)) != sizeof(pid)){
				perror("Write PID en fifo salida");
				exit(EXIT_FAILURE);
			}
			if ((fdp = open(nombrefifopers, O_RDONLY)) == -1){ 
				perror("Open fifo personalizado");
				exit(EXIT_FAILURE);
			}
			
			// Duplicamos el fifo personalizado en la stdin, de donde leerá el proxy
			dup2(fdp, STDIN_FILENO);

			// Ejecutamos el proxy
			execl("proxy", "proxy", NULL);
		} else { // padre
			//printf("SERVER PADRE: soy el padre\n"); 
			//no hace nada mas
		}
	}

	return 0;
}
