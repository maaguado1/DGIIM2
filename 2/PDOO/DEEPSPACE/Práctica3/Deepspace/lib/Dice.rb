#encoding:utf-8

require_relative 'GameCharacter'

module Deepspace
  
# Class that makes all the random decisions of the game 
# @author María Aguado

class Dice
  
  # Constructors
	# ==========================================================================
  
  # Class initializer
  # @!attributes [Float] Constant probabilities for hangars, shields, weapons and first shots.
  # @!attribute [Random] Random number generator
  def initialize
    
     
    @NHANGARSPROB = 0.25
    
    @NSHIELDSPROB = 0.25
    
    @NWEAPONSPROB = 0.33
    
    @FIRSTSHOTPROB= 0.5
    
    @LOSTELEMENTPROB = 0.25
    
    @ENEMYLOSES = 0.3
    
    
    @generator = Random.new()
  end
  
  
  
  
  
	# Getters
	#=======================================================================
  
  # Determines the number of hangars that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NHANGARSPROB's probability.
  
  def initWithNHangars
    valor =1
    if @generator.rand < @NHANGARSPROB
        valor= 0
    end
    valor
    
  end
  
  
  
  # Determines the number of weapons that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NWEAPONSPROB's probability.
 
  def initWithNWeapons
    rand = @generator.rand(1.0)
    if ( rand < @NHANGARSPROB)
      return 1
    
    elsif (rand < (2*@NHANGARSPROB))
      return 2
    else 
      return 3
    end
  
  end
  
  
  
  # Determines the amount of shield enhancers that a space station will receive. The value is calculated
  # based on a random value between 0 and 1, and the return value depends on NSHIELDSPROB's probability.
 
  def initWithNShields
    if @generator.rand < @NSHIELDSPROB
        return 0
    else
      return 1
      
    end
  end
  
  
  # Determines the player index which will start the game
  # @param nPlayers number of players
  # @return player
 
  def whoStarts(nPlayers)
    @generator.rand(nPlayers)
  end
  
  # Determines who shoots first
  # @return [GameCharacter] SPACESTATION if the player shoots first,
	#                         ENEMYSTARSHIP if the enemy shoots first

  def firstShot
    if @generator.rand < @FIRSTSHOTPROB
      return GameCharacter::SPACESTATION
    else
      return GameCharacter::ENEMYSTARSHIP
    end
  end
  
  
  # Determines if a space station moves in order to avoid a shot
  def spaceStationMoves(speed)
    return ( @generator.rand() < speed)
  end
  
  
  #EXAMEN P3
  def enemyLoses
    rand = @generator.rand(1.0)
    if (rand < @ENEMYLOSES)
      return true
      
    end
    
    return false
  
  end
  
  
  
  def lostNElements
    rand = @generator.rand(1.0)
    
      if (rand < @LOSTELEMENTPROB)
        elements =1
      elsif (rand < @LOSTELEMENTPROB*2)
        elements = 2
      elsif (rand < @LOSTELEMENTPROB*3)
        elements = 3
      else
        elements = 4
      end
      
     return elements
  end
    
 
  
 
end
    
end
