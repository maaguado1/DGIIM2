#encoding:utf-8

require_relative 'SpaceStation'
require_relative 'Loot'
require_relative 'SpaceCityToUI'

module Deepspace
class SpaceCity < SpaceStation
  
   
  def initialize (station, collaborator)
    newCopy(station)
    @base =station
    @collaborators = collaborator

  end
  attr_reader :collaborators
 
  
  
  def fire()
    fuego = super 
    for c in @collaborators
      fuego += c.fire()
    end
    return fuego
  end
  
  def protection 
    proc = super 
    for c in @collaborators
      proc += c.protection()
    end
    return proc
  end
  
  
  
  def setLoot(l)
    super(l)
    return Transformation::NOTRANSFORM
  end
  
  
  #Override
  def to_s
    out = super
    out += "\n ------- My Collaborators are:"
    for c in @collaborators do
      out += "\n --- Collaborator --- \n"
      out += c.to_s
    end
    out += "\n ------- No More Collaborators \n"
    return out
  end
  
  def getUIversion
    return SpaceCityToUI.new(self)
  end
  
end
end

