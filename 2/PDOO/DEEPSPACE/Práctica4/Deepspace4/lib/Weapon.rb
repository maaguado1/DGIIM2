#encoding: utf-8

require_relative 'WeaponType'
require_relative 'WeaponToUI'

module Deepspace
 
  
# Class that represents the weapons that a space station can have
class Weapon
  
  
  # Class initializer
  def initialize(nam, typ, use)
    @name = nam;
    @type = typ;
    @uses = use;
    
  end
  
  
  # Copy constructor
  def self.newCopy(otro)
    new(otro.name, otro.type, otro.uses)
    
  end
  
  
  # Getters 
  def name
    @name
  end
  
  def type
    @type
  end
  
  def uses
    @uses
  end
  
  def power
    @type.power
  end
  
  
  # Uses the weapon
	# If it is still available (uses is greater than 0), we can use the weapon
	# Otherwise, we cannot use the weapon
	# @return [Float] boost if uses > 0; 1.0 otherwise
  
  def useIt
    valor = 1.0
    if @uses >0
      @uses = @uses -1
      valor = power
    end
    valor
  end
  
  def to_s
    mensaje = "[Weapon] -> Name: #{@name}, Type: #{@type}, Uses: #{@uses}, Power: #{power}"
    return mensaje
  end
  
  def getUIversion
    return WeaponToUI.new(self)
  end
end

end

